/**
 * @file xipvis.h
 *
 * @brief The xipvis API.
 *
 * @license Copyright (C) 2015 Xiphos Systems Corporation
 */

#ifndef XIPHOS_H
#define XIPHOS_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "../src/xipvis.h"
    
#ifdef __cplusplus
}
#endif

#endif /* #ifndef XIPHOS_H */
