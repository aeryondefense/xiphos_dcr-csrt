#include "gtest/gtest.h"
#include "../src/trackkf.h"
#include <iostream>
#include <stdio.h>
#include <vector>

using xsc::udot::TrackKF;
using xsc::udot::KF1Dof;

TEST(XiphosTestSuite, kf_init_fail) {
    KF1Dof kf;
    float dt = 1/30.f;
    int step = -10;
    float tgt_pos = -5.f;
    float cam_vel = -2.f;

    // Valid dt
    int rc = kf.init(dt, step, tgt_pos, cam_vel);
    EXPECT_EQ(rc, 0);

    // Invalid dt
    dt = -1.0f;
    rc = kf.init(dt, step, tgt_pos, cam_vel);
    EXPECT_EQ(rc, -1);

    dt = 0.0f;
    rc = kf.init(dt, step, tgt_pos, cam_vel);
    EXPECT_EQ(rc, -1);

    // Re-init Valid dt again
    dt = 1/30.f;
    rc = kf.init(dt, step, tgt_pos, cam_vel);
    EXPECT_EQ(rc, 0);

    // State covariance
    Vector6f P_diag;
    P_diag <<
        xsc::udot::p00,
        xsc::udot::p11,
        xsc::udot::p22,
        xsc::udot::p33,
        xsc::udot::p44,
        xsc::udot::p55;
    Matrix6f P0 = P_diag.asDiagonal();

    // Process covariance
    Vector6f q_diag;
    q_diag <<
        xsc::udot::q00,
        xsc::udot::q11,
        xsc::udot::q22,
        xsc::udot::q33,
        xsc::udot::q44,
        xsc::udot::q55;
    Matrix6f Q0 = q_diag.asDiagonal();

    // Measurement variance
    float r_tgt_pos = xsc::udot::r_tgt_pos;
    float r_cam_vel = xsc::udot::r_cam_vel;

    rc = kf.init(dt, step, tgt_pos, cam_vel, P0, Q0, r_tgt_pos, r_cam_vel);
    EXPECT_EQ(rc, 0);

    // Negative P0
    P0(0,0) = -1.f;
    rc = kf.init(dt, step, tgt_pos, cam_vel, P0, Q0, r_tgt_pos, r_cam_vel);
    EXPECT_EQ(rc, -1);
    // Valid P0
    P0(0,0) = xsc::udot::p00;
    rc = kf.init(dt, step, tgt_pos, cam_vel, P0, Q0, r_tgt_pos, r_cam_vel);
    EXPECT_EQ(rc, 0);

    // Negative Q0
    Q0(0,0) = -0.1f;
    rc = kf.init(dt, step, tgt_pos, cam_vel, P0, Q0, r_tgt_pos, r_cam_vel);
    EXPECT_EQ(rc, -1);
    // Valid Q0
    Q0(0,0) = xsc::udot::q00;
    rc = kf.init(dt, step, tgt_pos, cam_vel, P0, Q0, r_tgt_pos, r_cam_vel);
    EXPECT_EQ(rc, 0);

    // Negative r_tgt_pos
    r_tgt_pos = -0.001f;
    rc = kf.init(dt, step, tgt_pos, cam_vel, P0, Q0, r_tgt_pos, r_cam_vel);
    EXPECT_EQ(rc, -1);
    // Valid r_tgt_pos
    r_tgt_pos = xsc::udot::r_tgt_pos;
    rc = kf.init(dt, step, tgt_pos, cam_vel, P0, Q0, r_tgt_pos, r_cam_vel);
    EXPECT_EQ(rc, 0);

    // Negative r_cam_vel
    r_cam_vel = -0.0001f;
    rc = kf.init(dt, step, tgt_pos, cam_vel, P0, Q0, r_tgt_pos, r_cam_vel);
    EXPECT_EQ(rc, -1);
    // Valid r_tgt_pos
    r_cam_vel = xsc::udot::r_cam_vel;
    rc = kf.init(dt, step, tgt_pos, cam_vel, P0, Q0, r_tgt_pos, r_cam_vel);
    EXPECT_EQ(rc, 0);
}

enum KFObsType {
    NONE,
    TGT_POS,
    CAM_VEL,
};

struct KFObs {
    int step;
    int state_idx;
    float value;
};

struct KFState {
    int step;
    Vector6f states;
};

int load_kf_hist_data(std::string filename, std::vector<KFState> &kf_hist)
{
    FILE * f = fopen(filename.c_str(), "r");
    if (!f) return -1;

    int lines = 0;
    int step;
    float x0,x1,x2,x3,x4,x5;
    KFState data;
    fscanf(f, "%*[^\n]\n", NULL);

    while(1)
    {
        int i = fscanf(f, "%d,%f,%f,%f,%f,%f,%f\n",&step,&x0,&x1,&x2,&x3,&x4,&x5);
        if (i < 7) break;
        data.step = step;
        data.states << x0,x1,x2,x3,x4,x5;
        kf_hist.push_back(data);
        lines++;
    }

    return lines;
}

int load_kf_obs_data(std::string filename, std::vector<KFObs> &kf_obs)
{
    FILE * f = fopen(filename.c_str(), "r");
    if (!f) return -1;

    int lines = 0;
    KFObs obs;
    fscanf(f, "%*[^\n]\n", NULL);

    while(1)
    {
        int i = fscanf(f, "%d,%d,%f\n",&(obs.step),&(obs.state_idx),&(obs.value));
        if (i < 3) break;
        kf_obs.push_back(obs);
        lines++;
    }

    return lines;
}

TEST(XiphosTestSuite, predict_update) {
    KF1Dof kf;
    float dt = 1/30.f;

    // State covariance
    Vector6f P_diag;
    P_diag <<
        xsc::udot::p00,
        xsc::udot::p11,
        xsc::udot::p22,
        xsc::udot::p33,
        xsc::udot::p44,
        xsc::udot::p55;
    Matrix6f P0 = P_diag.asDiagonal();

    // Process covariance
    Vector6f q_diag;
    q_diag <<
        xsc::udot::q00,
        xsc::udot::q11,
        xsc::udot::q22,
        xsc::udot::q33,
        xsc::udot::q44,
        xsc::udot::q55;
    Matrix6f Q0 = q_diag.asDiagonal();

    // Measurement variance
    float r_tgt_pos = xsc::udot::r_tgt_pos;
    float r_cam_vel = xsc::udot::r_cam_vel;

    // Load KF validation data
    std::vector<KFObs> obs_data;
    int rc = load_kf_obs_data("unit_test/kf_obs_data.csv", obs_data);
    std::vector<KFState> pred_data;
    rc = load_kf_hist_data("unit_test/kf_pred_data.csv", pred_data);
    std::vector<KFState> up_data;
    rc = load_kf_hist_data("unit_test/kf_update_data.csv", up_data);

    int n_obs = obs_data.size();
    int n_pred = pred_data.size();
    int n_up = up_data.size();
    ASSERT_TRUE(n_obs >= 3);
    ASSERT_TRUE(n_pred >= 3);
    ASSERT_TRUE(n_up >= 3);
    int n_max = n_obs;
    if (n_pred > n_max) n_max = n_pred;
    if (n_up > n_max) n_max = n_up;
    
    for (int run = 0; run < 2; run++) {
        Matrix<float, 6, 1> xhat_actual;

        int i_obs = 0;
        int i_pred = 0;
        int i_up = 0;

        // Initialize KF
        KFObs obs = obs_data[0];
        float tgt_pos0 = 0.f;
        float cam_vel0 = 0.f;
        int step0 = obs.step;
        int step = step0;

        if (obs.state_idx == 0) {
            tgt_pos0 = obs.value;
        }
        if (obs.state_idx == 4) {
            cam_vel0 = obs.value;
        }

        int rc = 0;
        if (run == 0) {
            rc = kf.init(dt, obs.step, tgt_pos0, cam_vel0);
            EXPECT_EQ(rc, 0);
        }
        else {
            rc = kf.init(dt, step, tgt_pos0, cam_vel0, P0, Q0, r_tgt_pos, r_cam_vel);
            EXPECT_EQ(rc, 0);
        }

        // Validate dt config
        EXPECT_FLOAT_EQ(dt, kf.get_dt());

        KFState x_pred;
        KFState x_up;

        for (; i_obs < n_obs; i_obs++, step++) {
            rc = kf.predict(step);
            EXPECT_EQ(rc, 0);

            // Verify predicted state
            kf.get_states(xhat_actual);
            for (; i_pred < n_pred; i_pred++) {
                x_pred = pred_data[i_pred];
                if (x_pred.step >= step) {
                    if (x_pred.step == step) {
                        EXPECT_TRUE(xhat_actual.isApprox(x_pred.states));
                        i_pred++;
                    }
                    break;
                }
            }

            // Verify backward prediction fails and doesn't corrupt future ouputs
            rc = kf.predict(-100);
            EXPECT_EQ(rc, -1);

            // Apply all observations from this step
            if (step > step0) {
                while (1) {
                    if (obs_data[i_obs].state_idx == 0) {
                        kf.update_target_pos(step, obs_data[i_obs].value);
                    }
                    else if (obs_data[i_obs].state_idx == 4) {
                        kf.update_camera_vel(step, obs_data[i_obs].value);
                    }

                    if (i_obs+1 < n_obs && obs_data[i_obs+1].step == step) {
                        i_obs++;
                    }
                    else {
                        break;
                    }
                }
            }

            // Verify updated state
            kf.get_states(xhat_actual);
            for (; i_up < n_up; i_up++) {
                x_up = up_data[i_up];
                if (x_up.step >= step) {
                    if (x_up.step == step) {
                        EXPECT_TRUE(xhat_actual.isApprox(x_up.states));
                        i_up++;
                    }
                    break;
                }
            }
        }
    }
}

TEST(XiphosTestSuite, trackkf) {
    TrackKF kf;
    float dt = 1/30.f;

    // State covariance
    Vector6f P_diag;
    P_diag <<
        xsc::udot::p00,
        xsc::udot::p11,
        xsc::udot::p22,
        xsc::udot::p33,
        xsc::udot::p44,
        xsc::udot::p55;
    Matrix6f P0 = P_diag.asDiagonal();

    // Process covariance
    Vector6f q_diag;
    q_diag <<
        xsc::udot::q00,
        xsc::udot::q11,
        xsc::udot::q22,
        xsc::udot::q33,
        xsc::udot::q44,
        xsc::udot::q55;
    Matrix6f Q0 = q_diag.asDiagonal();

    // Measurement variance
    float r_tgt_pos = xsc::udot::r_tgt_pos;
    float r_cam_vel = xsc::udot::r_cam_vel;

    // Load KF validation data
    std::vector<KFObs> obs_data;
    int rc = load_kf_obs_data("unit_test/kf_obs_data.csv", obs_data);
    std::vector<KFState> pred_data;
    rc = load_kf_hist_data("unit_test/kf_pred_data.csv", pred_data);
    std::vector<KFState> up_data;
    rc = load_kf_hist_data("unit_test/kf_update_data.csv", up_data);

    int n_obs = obs_data.size();
    int n_pred = pred_data.size();
    int n_up = up_data.size();
    ASSERT_TRUE(n_obs >= 3);
    ASSERT_TRUE(n_pred >= 3);
    ASSERT_TRUE(n_up >= 3);
    int n_max = n_obs;
    if (n_pred > n_max) n_max = n_pred;
    if (n_up > n_max) n_max = n_up;
    
    for (int run = 0; run < 2; run++) {
        Matrix<float, 6, 1> xhat_actual, yhat_actual;

        int i_obs = 0;
        int i_pred = 0;
        int i_up = 0;

        // Initialize KF
        KFObs obs = obs_data[0];
        float tgt_pos0 = 0.f;
        float cam_vel0 = 0.f;
        int step0 = obs.step;
        int step = step0;

        if (obs.state_idx == 0) {
            tgt_pos0 = obs.value;
        }
        if (obs.state_idx == 4) {
            cam_vel0 = obs.value;
        }

        int rc = 0;
        if (run == 0) {
            rc = kf.init(dt, obs.step, tgt_pos0, tgt_pos0, cam_vel0, cam_vel0);
            EXPECT_EQ(rc, 0);
        }
        else {
            rc = kf.init(dt, step, tgt_pos0, tgt_pos0, cam_vel0, cam_vel0,
                    P0, Q0, r_tgt_pos, r_cam_vel);
            EXPECT_EQ(rc, 0);
        }

        // Validate dt config
        EXPECT_FLOAT_EQ(dt, kf.get_dt());

        KFState x_pred;
        KFState x_up;

        for (; i_obs < n_obs; i_obs++, step++) {
            rc = kf.predict(step);
            EXPECT_EQ(rc, 0);

            // Verify predicted state
            kf.get_states(xhat_actual, yhat_actual);
            for (; i_pred < n_pred; i_pred++) {
                x_pred = pred_data[i_pred];
                if (x_pred.step >= step) {
                    if (x_pred.step == step) {
                        EXPECT_TRUE(xhat_actual.isApprox(x_pred.states));
                        EXPECT_TRUE(yhat_actual.isApprox(x_pred.states));
                        i_pred++;
                    }
                    break;
                }
            }

            // Verify backward prediction fails and doesn't corrupt future ouputs
            rc = kf.predict(-100);
            EXPECT_EQ(rc, -1);

            // Apply all observations from this step
            if (step > step0) {
                while (1) {
                    if (obs_data[i_obs].state_idx == 0) {
                        kf.update_target_pos(step, obs_data[i_obs].value, obs_data[i_obs].value);
                    }
                    else if (obs_data[i_obs].state_idx == 4) {
                        kf.update_camera_vel(step, obs_data[i_obs].value, obs_data[i_obs].value);
                    }

                    if (i_obs+1 < n_obs && obs_data[i_obs+1].step == step) {
                        i_obs++;
                    }
                    else {
                        break;
                    }
                }
            }

            // Verify updated state
            kf.get_states(xhat_actual, yhat_actual);
            for (; i_up < n_up; i_up++) {
                x_up = up_data[i_up];
                if (x_up.step >= step) {
                    if (x_up.step == step) {
                        EXPECT_TRUE(xhat_actual.isApprox(x_up.states));
                        EXPECT_TRUE(yhat_actual.isApprox(x_up.states));
                        i_up++;
                    }
                    break;
                }
            }
        }
    }
}
