import numpy as np


class KF1DofCam:

    def __init__(self):
        self.initialized = False
        self.debug = False

    def init(self, dt, x0, step=0):
        self.step = step
        self.x = x0
        self.I = np.matrix(np.eye(6))
        self.dt = dt
        self.A = np.matrix(
            [[1, dt,   0, 0, dt,   0],
             [0,  1,  dt, 0,  0,  dt],
             [0,  0, .95, 0,  0,   0],
             [0,  0,   0, 1, dt,   0],
             [0,  0,   0, 0,  1,  dt],
             [0,  0,   0, 0,  0, .95]])
        self.H = np.matrix(
            [[1, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 1, 0]])
        self.Q = np.matrix(np.diag([1.0, 5.0, 25.0, 1.0, 5.0, 25.0]))
        self.R = np.matrix(np.diag([3.0, 5.0]))
        self.P = np.matrix(np.diag([1.0, 100.0, 1000.0, 1.0, 100.0, 1000.0]))
        self.initialized = True

    def predict(self, step):
        if not self.initialized:
            return False
        if step < self.step:
            return False
        while self.step < step:
            self.x = self.A * self.x
            self.P = self.A * self.P * self.A.T + self.Q
            self.step += 1

            if self.debug:
                print("predict: step=", kf.step)
                print("x:\n", self.x)
                print("P\n:", self.P)

        return True

    def update_target_pos(self, target_pos, step, R=None):
        if not self.initialized:
            return False
        if step < self.step:
            return False

        if not self.predict(step):
            return False
        if R:
            self.R = R

        H = np.matrix([1, 0, 0, 0, 0, 0])

#        K = self.P * H.T * np.linalg.inv(H * self.P * H.T + self.R[:1,:1])
        K = self.P[:,0] / (self.P[0,0] + self.R[0,0])
        self.x = self.x + K * (target_pos - H * self.x)
        self.P = (self.I - K * H) * self.P

        if self.debug:
            print("update_target_pos: step=", self.step)
            print("x:\n", self.x)
            print("P\n:", self.P)
            print("K\n:", K)

    def update_camera_vel(self, camera_vel, step, R=None):
        if not self.initialized:
            return False
        if step < self.step:
            return False

        if not self.predict(step):
            return False
        if R:
            self.R = R

        H = np.matrix([0, 0, 0, 0, 1, 0])

        K = self.P[:,4] / (self.P[4,4] + self.R[1,1])
        self.x = self.x + K * (camera_vel - self.x[4])
        self.P = (self.I - K * H) * self.P

        if self.debug:
            print("update_camera_vel: step=", self.step)
            print("x:\n", self.x)
            print("P\n:", self.P)
            print("K\n:", K)


#%% Produce KF1Dof Test Data

# Load observations from a file
obs_data = np.genfromtxt('kf_obs_data.csv', dtype=(int, int, float), delimiter=',', names=True)
print('obs_data:', obs_data)

kf = KF1DofCam()
kf.debug = True
dt = 1 / 30.

# Load first observation to initialize the filter
step, state_idx, value = obs_data[0]
x0 = np.matrix(np.zeros((6, 1)))
if (state_idx >= 0):
    x0[state_idx] = value

kf.init(dt, x0, step)

pred_data = []
state_data = []

pred_data.append({'step': step, 'xhat': kf.x})

# Iterate all subsequent observations and store the predicted state and the
# updated/corrected state after all observations for the step
for new_step, state_idx, value in obs_data[1:]:
    if new_step > step:
        state_data.append({'step': step, 'xhat': kf.x})

    for s in range(step + 1, new_step + 1):
        kf.predict(s)
        step = s
        pred_data.append({'step': step, 'xhat': kf.x})
    if state_idx >= 0:
        if state_idx == 0:
            kf.update_target_pos(value, step)
        if state_idx == 4:
            kf.update_camera_vel(value, step)

state_data.append({'step': step, 'xhat': kf.x})

print('pred_data:', pred_data)
print('state_data:', state_data)

# Write the prediction and update data to file
with open('kf_pred_data.csv', 'w') as f:
    f.write('step,x0,x1,x2,x3,x4,x5\n')
    for row in pred_data:
        fmt = '{:d}' + 6 * ',{:.16f}' + '\n'
        step = row['step']
        xhat = row['xhat']
        row_data = [step, xhat[0,0], xhat[1,0], xhat[2,0], xhat[3,0], xhat[4,0], xhat[5,0]]
        f.write(fmt.format(*row_data))

with open('kf_update_data.csv', 'w') as f:
    f.write('step,x0,x1,x2,x3,x4,x5\n')
    for row in state_data:
        fmt = '{:d}' + 6 * ',{:.16f}' + '\n'
        step = row['step']
        xhat = row['xhat']
        row_data = [step, xhat[0,0], xhat[1,0], xhat[2,0], xhat[3,0], xhat[4,0], xhat[5,0]]
        f.write(fmt.format(*row_data))
