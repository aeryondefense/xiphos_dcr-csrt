#include "config.h"


namespace xsc
{
namespace stabilize
{

static constexpr int   RADIUS_DEFAULT = 20;
static constexpr float STDDEV_DEFAULT = 10.0f;
static constexpr float SCALE_DEFAULT = 1.10f;
static constexpr int   SPLIT_DEFAULT = 0;

static constexpr float SCALE_INCREMENT = 0.005;
static constexpr float SCALE_DECREMENT = 0.001;

static constexpr int   MAX_RANSAC_ITS    =  10;
static constexpr int   MIN_RANSAC_ITS    =   1;
static constexpr int   MIN_SET_SIZE      =   3;
static constexpr int   MAX_SET_SIZE      = 256;
static constexpr float TARGET_INLIER_PCT =   0.8f;
static constexpr int   MIN_INLIER_COUNT  =   6;
static constexpr float INLIER_DISTANCE   =   1.f;
static constexpr float MAX_SOLN_SX       =   0.03f;
static constexpr float MAX_SOLN_SY       =   0.03f;
static constexpr float MAX_SOLN_THETA    =   2.f / 180.f * M_PI;
static constexpr float MAX_SOLN_SKEW     =   0.03f;

}
}


xsc::stabilize::stabilizer::stabilizer() : radius(xsc::stabilize::RADIUS_DEFAULT),
                                           stddev(xsc::stabilize::STDDEV_DEFAULT),
                                           scale(xsc::stabilize::SCALE_DEFAULT),
                                           marginal_scale(xsc::stabilize::SCALE_DEFAULT),
                                           scale_increment(xsc::stabilize::SCALE_INCREMENT),
                                           scale_decrement(xsc::stabilize::SCALE_DECREMENT),
                                           split(xsc::stabilize::SPLIT_DEFAULT),
                                           delay(0),
                                           valid_solution(FALSE),
                                           match_count(0),
                                           inlier_count(0)
{}

xsc::stabilize::stabilizer::~stabilizer()
{}

int xsc::stabilize::set_filter_radius(xsc::stabilize::stabilizer* const s, const int radius)
{
    return set_filter(s, radius, s->stddev);
}

int xsc::stabilize::set_filter_stddev(xsc::stabilize::stabilizer* const s, const float stddev)
{
    return set_filter(s, s->radius, stddev);
}

int xsc::stabilize::set_scale_increment(xsc::stabilize::stabilizer* const s, const float incr)
{
    s->scale_increment = MAX(0.0001f, incr);
    
    return 0;
}

int xsc::stabilize::set_scale_decrement(xsc::stabilize::stabilizer* const s, const float decr)
{
    s->scale_decrement = MAX(0.0001f, decr);
    
    return 0;
}

int xsc::stabilize::set_split_screen(xsc::stabilize::stabilizer* const s, const int split)
{
    if (split)
        s->split = 1;
    else
        s->split = 0;
    return s->split;
}

int xsc::stabilize::set_border_scale(xsc::stabilize::stabilizer* const s, const float scale)
{
    s->scale = scale;
    BOUND_VARIABLE(s->scale, SCALE_MIN, SCALE_MAX);
    s->marginal_scale = s->scale;
    
    return 0;
}

int xsc::stabilize::get_filter_radius(xsc::stabilize::stabilizer* const s)
{
    return s->radius;
}

float xsc::stabilize::get_filter_stddev(xsc::stabilize::stabilizer* const s)
{
    return s->stddev;
}

float xsc::stabilize::get_scale(xsc::stabilize::stabilizer* const s)
{
    return s->scale;
}

float xsc::stabilize::get_marginal_scale(xsc::stabilize::stabilizer* const s)
{
    return s->marginal_scale;
}

float xsc::stabilize::get_scale_increment(xsc::stabilize::stabilizer* const s)
{
    return s->scale_increment;
}

float xsc::stabilize::get_scale_decrement(xsc::stabilize::stabilizer* const s)
{
    return s->scale_decrement;
}

int xsc::stabilize::get_split_screen(xsc::stabilize::stabilizer* const s)
{
    return s->split;
}

// Get the address of the homography matrix data on the device
float* xsc::stabilize::get_homography_address(xsc::stabilize::stabilizer* const s)
{
    return s->h[1];
}

// Get the address of the current homography matrix data on the host
float* xsc::stabilize::get_current_homography(xsc::stabilize::stabilizer* const s)
{
    return s->hk;
}

int xsc::stabilize::current_homography_valid(xsc::stabilize::stabilizer* const s)
{
    return s->valid_solution;
}

// Assign homographies to the image level
int xsc::stabilize::assign_homographies(stabilizer* const s, xsc::image::level* p)
{
    for (int i = 0; i < 9; i++)
    {
        p->fhm[i] = s->h[0][i];
        p->hm[i] = s->hk[i];
    }
    
    p->hv = s->valid_solution;

    return 0;
}

int xsc::stabilize::init(stabilizer* const s, const int w, const int h, const float f, const float cx, const float cy, const float fps)
{
    if ((s->h[0] = new(std::nothrow) float[9]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    for (int i = 0; i < 9; i++)
    {
        s->h[0][i] = 0.f;
    }
    
    if ((s->hk = new(std::nothrow) float[9]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    for (int i = 0; i < 9; i++)
    {
        s->hk[i] = 0.f;
    }
    
    s->px0[0] = nullptr;
    s->py0[0] = nullptr;
    s->px1[0] = nullptr;
    s->py1[0] = nullptr;
    s->px2[0] = nullptr;
    s->py2[0] = nullptr;

    s->match_count = 0;
    
    if ((s->inx0 = new(std::nothrow) float[xsc::MAX_FLOW_KEYPOINTS]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    if ((s->iny0 = new(std::nothrow) float[xsc::MAX_FLOW_KEYPOINTS]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    if ((s->inx1 = new(std::nothrow) float[xsc::MAX_FLOW_KEYPOINTS]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    if ((s->iny1 = new(std::nothrow) float[xsc::MAX_FLOW_KEYPOINTS]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    s->inlier_count = 0;
    
    if (xsc::stabilize::init_gpu(s) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    s->width  = w;
    s->height = h;
    
    const float fps_factor = fps / 24.f;
            
    // Lower the filtering on the stabilization
    xsc::stabilize::set_filter(s, xsc::stabilize::RADIUS_DEFAULT * fps_factor, xsc::stabilize::STDDEV_DEFAULT * fps_factor);
    
    // Increase the scale increment since the FPS is slower
    xsc::stabilize::set_scale_decrement(s, xsc::stabilize::SCALE_DECREMENT / fps_factor);
  
    xsc::stabilize::set_border_scale(s, s->scale);
    
    return 0;
}

int xsc::stabilize::set_filter(stabilizer* const s, const int radius, const float stddev)
{
    s->radius = radius;
    s->stddev = stddev;
    
    BOUND_VARIABLE(s->radius, xsc::stabilize::RADIUS_MIN, xsc::stabilize::RADIUS_MAX); 
    BOUND_VARIABLE(s->stddev, xsc::stabilize::STDDEV_MIN, xsc::stabilize::STDDEV_MAX);
    
    // Initialize the weights and previous homographies
    const float std2 = stddev*stddev;
    const int length = s->radius + 1;
    float wsum = 0.f;
    SO3f eye(0.f, 0.f, 0.f);

    for (int i = -s->radius; i <= 0; i++) 
    {
        s->weights[s->radius + i] = exp(-(float) (i * i) / std2);
        wsum += s->weights[s->radius + i];
    }
    for (int i = 0; i < length; i++) 
    {
        s->weights[i] /= wsum;
        s->Hs[i] = eye.Adj();
    }
    
    return 0; 
}

int xsc::stabilize::free(stabilizer* const s)
{
    delete [] s->h[0];
    s->h[0] = nullptr;

    delete [] s->hk;
    s->hk = nullptr;
    
    s->px0[0] = nullptr;
    s->py0[0] = nullptr;
    s->px1[0] = nullptr;
    s->py1[0] = nullptr;
    s->px2[0] = nullptr;
    s->py2[0] = nullptr;

    delete [] s->inx0;
    s->inx0 = nullptr;

    delete [] s->iny0;
    s->iny0 = nullptr;

    delete [] s->inx1;
    s->inx1 = nullptr;

    delete [] s->iny1;
    s->iny1 = nullptr;

    if (xsc::stabilize::free_gpu(s) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    return 0;
}

int xsc::stabilize::print_homography(stabilizer* const s, FILE* const fd, const int id)
{
    fprintf(fd, "%d, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f\n", id,   s->hk[0],
                                                                                    s->hk[1],
                                                                                    s->hk[2],
                                                                                    s->hk[3],
                                                                                    s->hk[4],
                                                                                    s->hk[5],
                                                                                    s->hk[6],
                                                                                    s->hk[7],
                                                                                    s->hk[8]);
}

int xsc::stabilize::stabilize(stabilizer* const s, Matrix3f* const Hk, const float fit, const int new_keyframe, const int motion_delay)
{
    int done = 0;
    SO3f eye(0.f, 0.f, 0.f);
    
    s->Hk = *Hk;
    s->valid_solution = (fit <= xsc::RESIDUAL_MTI_THRESHOLD);
    
    // For each new homography, find the cumulative homographies
    for (int i = 0; i < s->radius; i++)
    {
        s->Hs[i] = s->Hs[i+1];
    }
    s->Hs[s->radius] = s->Hk;

    s->hk[0] = s->Hk(0,0);
    s->hk[1] = s->Hk(0,1);
    s->hk[2] = s->Hk(0,2);
    s->hk[3] = s->Hk(1,0);
    s->hk[4] = s->Hk(1,1);
    s->hk[5] = s->Hk(1,2);
    s->hk[6] = s->Hk(2,0);
    s->hk[7] = s->Hk(2,1);
    s->hk[8] = s->Hk(2,2);

    filter_homography(s);
    
    // Check if this homography shows any black borders
    Vector3f tl(                0.f,                  0.f, 1.f);
    Vector3f tr((float)(s->width-1),                  0.f, 1.f);
    Vector3f bl(                0.f, (float)(s->height-1), 1.f);
    Vector3f br((float)(s->width-1), (float)(s->height-1), 1.f);
    
    Vector3f htl = tl;
    Vector3f htr = tr;
    Vector3f hbl = bl;
    Vector3f hbr = br;
    
    Vector2f tl_pxl(0.f,0.f);
    Vector2f tr_pxl(0.f,0.f);
    Vector2f bl_pxl(0.f,0.f);
    Vector2f br_pxl(0.f,0.f);
    
    Matrix3f H = s->H;
    
    if (!s->delay)
    {
        s->marginal_scale  = MAX(s->marginal_scale - s->scale_decrement, s->scale);
    }
            
    const float initial_scale = s->marginal_scale;
    while (!done)
    {
        // Recompute the scaling matrix
        const float iscale = 1.f/s->marginal_scale;
        s->S << iscale,      0, s->width /2*(1-iscale),
                     0, iscale, s->height/2*(1-iscale),
                     0,      0,                    1.f;
        
        s->H = s->S * H;
        htl = s->H*tl;
        htr = s->H*tr;
        hbl = s->H*bl;
        hbr = s->H*br;
        
        tl_pxl(0) = htl(0)/htl(2); tl_pxl(1) = htl(1)/htl(2); 
        tr_pxl(0) = htr(0)/htr(2); tr_pxl(1) = htr(1)/htr(2); 
        bl_pxl(0) = hbl(0)/hbl(2); bl_pxl(1) = hbl(1)/hbl(2); 
        br_pxl(0) = hbr(0)/hbr(2); br_pxl(1) = hbr(1)/hbr(2); 
        
        if ((tl_pxl(0) < 0.f || tl_pxl(0) > (float)(s->width-1)) || (tl_pxl(1) < 0.f || tl_pxl(1) > (float)(s->height-1)) || (htl(2) <= 0.f)
         || (tr_pxl(0) < 0.f || tr_pxl(0) > (float)(s->width-1)) || (tr_pxl(1) < 0.f || tr_pxl(1) > (float)(s->height-1)) || (htr(2) <= 0.f)
         || (bl_pxl(0) < 0.f || bl_pxl(0) > (float)(s->width-1)) || (bl_pxl(1) < 0.f || bl_pxl(1) > (float)(s->height-1)) || (hbl(2) <= 0.f)
         || (br_pxl(0) < 0.f || br_pxl(0) > (float)(s->width-1)) || (br_pxl(1) < 0.f || br_pxl(1) > (float)(s->height-1)) || (hbr(2) <= 0.f))
        {
            s->marginal_scale += s->scale_increment;
            if (s->marginal_scale > xsc::stabilize::SCALE_MAX)
            {
                s->marginal_scale = initial_scale;
                // Recompute the scaling matrix
                const float mscale = 1.f/s->marginal_scale;
                s->S << mscale,      0, s->width /2*(1-mscale),
                             0, iscale, s->height/2*(1-mscale),
                             0,      0,                    1.f;
                
                // No way to zoom in and not show black borders, this solution is bad.
                s->valid_solution = FALSE;
                s->Hk = eye.Adj();
                
                s->Hs[s->radius] = s->Hk;

                s->hk[0] = s->Hk(0,0);
                s->hk[1] = s->Hk(0,1);
                s->hk[2] = s->Hk(0,2);
                s->hk[3] = s->Hk(1,0);
                s->hk[4] = s->Hk(1,1);
                s->hk[5] = s->Hk(1,2);
                s->hk[6] = s->Hk(2,0);
                s->hk[7] = s->Hk(2,1);
                s->hk[8] = s->Hk(2,2);

                filter_homography(s);
                
                s->H = s->S * s->H;
                
                done = 1;
            }
            s->delay = motion_delay;
        }
        else
        {
            s->delay = MAX(s->delay - 1, 0);
            
            done = 1;
        }
    }
    
    xsc::stabilize::copy_homography(s);
    
    return 0;
}

int xsc::stabilize::filter_homography(stabilizer* const s)
{
    SO3f eye(0.f, 0.f, 0.f);
    
    s->Hs[s->radius] = s->Hk;

    s->Hc[s->radius] = eye.Adj();

    for (int i = 0; i < s->radius; i++)
    {
        const int j = (s->radius - i);
        s->Hc[j - 1] = s->Hs[j] * s->Hc[j];
    }    

    // Result is sum of weighted homographies
    s->H = Matrix3f::Zero();

    for (int i = 0; i < s->radius + 1; i++)
    {
        s->H += s->weights[i] * s->Hc[i];
    }
    
    return 0;
}
    
static int solve_eigenvector9(Matrix<float, 9, 9>* AtA, Matrix3f* solution)
{
    SelfAdjointEigenSolver<Matrix<float, 9, 9>> es;
    es.compute(*AtA);
    
    (*solution)(0,0) = (es.eigenvectors())(0,0);
    (*solution)(0,1) = (es.eigenvectors())(1,0);
    (*solution)(0,2) = (es.eigenvectors())(2,0);
    (*solution)(1,0) = (es.eigenvectors())(3,0);
    (*solution)(1,1) = (es.eigenvectors())(4,0);
    (*solution)(1,2) = (es.eigenvectors())(5,0);
    (*solution)(2,0) = (es.eigenvectors())(6,0);
    (*solution)(2,1) = (es.eigenvectors())(7,0);
    (*solution)(2,2) = (es.eigenvectors())(8,0);
    
    return 0;
}

static int compute_homography_transformation(float* px0, float* py0, float* px1, float* py1, const int point_count, Matrix3f* H)
{
    SO3f eye(0, 0, 0);
    if (point_count < 4)
    {
        (*H) = eye.Adj();
        
        return 1;
    }
    
    // Find centroid of each point set dimension
    float cx0 = 0;
    float cy0 = 0;
    float cx1 = 0;
    float cy1 = 0;
    
    for (int j = 0; j < point_count; j++)
    {
        cx0 += px0[j];
        cy0 += py0[j];
        cx1 += px1[j];
        cy1 += py1[j];
    }
    
    const float count = point_count;
    const float scale = 1.f/count;
    cx0 *= scale;
    cy0 *= scale;
    cx1 *= scale;
    cy1 *= scale;
    
    // Make the average distance to the points 1 in each dimension
    float dx0 = 0;
    float dy0 = 0;
    float dx1 = 0;
    float dy1 = 0;
    
    for (int j = 0; j < point_count; j++)
    {
        dx0 += fabsf(px0[j] - cx0);
        dy0 += fabsf(py0[j] - cy0);
        dx1 += fabsf(px1[j] - cx1);
        dy1 += fabsf(py1[j] - cy1);
    }
    
    if ((dx0 < xsc::CLOSE_TO_ZERO) ||
        (dy0 < xsc::CLOSE_TO_ZERO) ||
        (dx1 < xsc::CLOSE_TO_ZERO) ||
        (dy1 < xsc::CLOSE_TO_ZERO))
    {
        (*H) = eye.Adj();
        
        return 1;
    }
    
    dx0 = count / dx0;
    dy0 = count / dy0;
    dx1 = count / dx1;
    dy1 = count / dy1;
    
    // Solve for the normalized homography using minimum eigenvector
    Matrix<float, 9, 9> AtA;
    
    float a00 = 0.f;
    float a11 = 0.f;
    float a66 = 0.f;
    float a77 = 0.f;
    float a88 = 0.f;
    float a10 = 0.f;
    float a20 = 0.f;
    float a60 = 0.f;
    float a70 = 0.f;
    float a80 = 0.f;
    float a21 = 0.f;
    float a71 = 0.f;
    float a81 = 0.f;
    float a82 = 0.f;
    float a63 = 0.f;
    float a73 = 0.f;
    float a83 = 0.f;
    float a74 = 0.f;
    float a84 = 0.f;
    float a85 = 0.f;
    float a76 = 0.f;
    float a86 = 0.f;
    float a87 = 0.f;
    
    for (int j = 0; j < point_count; j++)
    {
        const float x0 = dx0*(px0[j] - cx0);
        const float y0 = dy0*(py0[j] - cy0);
        const float x1 = dx1*(px1[j] - cx1);
        const float y1 = dy1*(py1[j] - cy1);
        
        const float x02 = x0*x0;
        const float y02 = y0*y0;
        const float x12 = x1*x1;
        const float y12 = y1*y1;
        const float d12 = x12 + y12;
        const float x0y0 = x0*y0;
        const float x0x1 = x0*x1;
        const float x0y1 = x0*y1;
        const float y0x1 = y0*x1;
        const float y0y1 = y0*y1;
        
        a00 += x02;
        a11 += y02;
      //AtA(2,2) += 1.f;
      //AtA(3,3) += x02;
      //AtA(4,4) += y02;
      //AtA(5,5) += 1.f;
        a66 += x02*d12;
        a77 += y02*d12;
        a88 += d12;
        
        a10 += x0y0;
        a20 += x0;
      //AtA(3,0) += 0.f;
      //AtA(4,0) += 0.f;
      //AtA(5,0) += 0.f;
        a60 += -x02*x1;
        a70 += -x0y0*x1;
        a80 += -x0x1;
        
        a21 += y0;
      //AtA(3,1) += 0.f;
      //AtA(4,1) += 0.f;
      //AtA(5,1) += 0.f;
      //AtA(6,1) += -x0y0*x1;
        a71 += -y02*x1;
        a81 += -y0x1;
        
      //AtA(3,2) += 0.f;
      //AtA(4,2) += 0.f;
      //AtA(5,2) += 0.f;
      //AtA(6,2) += -x0x1;
      //AtA(7,2) += -y0x1;
        a82 += -x1;
        
      //AtA(4,3) += x0y0;
      //AtA(5,3) += x0;
        a63 += -x02*y1;
        a73 += -x0y0*y1;
        a83 += -x0y1;
        
      //AtA(5,4) += y0;
      //AtA(6,4) += -x0y0*y1;
        a74 += -y02*y1;
        a84 += -y0y1;
        
      //AtA(6,5) += -x0y1;
      //AtA(7,5) += -y0y1;
        a85 += -y1;
        
        a76 += x0y0*d12;
        a86 += x0*d12;
        
        a87 += y0*d12;
    }
    
    /*
    AtA(0,3) = AtA(0,4) = AtA(0,5) = 0.f;
    AtA(1,3) = AtA(1,4) = AtA(1,5) = 0.f;
    AtA(2,3) = AtA(2,4) = AtA(2,5) = 0.f;
    */
    
    AtA(0,0) = AtA(3,3) = a00;
    AtA(1,0) = AtA(4,3) = a10;
    AtA(2,0) = AtA(5,3) = a20;
    AtA(3,0) = 0.f;
    AtA(4,0) = 0.f;
    AtA(5,0) = 0.f;
    AtA(6,0) = a60;
    AtA(7,0) = a70;
    AtA(8,0) = a80;
    
    AtA(1,1) = AtA(4,4) = a11;
    AtA(2,1) = AtA(5,4) = a21;
    AtA(3,1) = 0.f;
    AtA(4,1) = 0.f;
    AtA(5,1) = 0.f;
    AtA(6,1) = AtA(7,0) = a70;
    AtA(7,1) = a71;
    AtA(8,1) = a81;
    
    AtA(2,2) = AtA(5,5) = count;
    AtA(3,2) = 0.f;
    AtA(4,2) = 0.f;
    AtA(5,2) = 0.f;
    AtA(6,2) = AtA(8,0) = a80;
    AtA(7,2) = AtA(8,1) = a81;
    AtA(8,2) = a82;
    
    AtA(6,3) = a63;
    AtA(7,3) = AtA(6,4) = a73;
    AtA(8,3) = AtA(6,5) = a83;
    
    AtA(7,4) = a74;
    AtA(8,4) = AtA(7,5) = a84;
    
    AtA(8,5) = a85;
    
    AtA(6,6) = a66;
    AtA(7,6) = a76;
    AtA(8,6) = a86;
    
    AtA(7,7) = a77;
    AtA(8,7) = a87;
    
    AtA(8,8) = a88;
    
    solve_eigenvector9(&AtA, H);
    
    // Final homography is scaled and translated normalized homography
    const float fx1 = dx0*(cx1*((*H)(2,0)) + ((*H)(0,0))/dx1);
    const float fx2 = dx0*(cy1*((*H)(2,0)) + ((*H)(1,0))/dy1);
    
    const float fy1 = dy0*(cx1*((*H)(2,1)) + ((*H)(0,1))/dx1);
    const float fy2 = dy0*(cy1*((*H)(2,1)) + ((*H)(1,1))/dy1);
    
    const float denom = (*H)(2,2) - cx0*dx0*((*H)(2,0)) - cy0*dy0*((*H)(2,1));
    (*H)(0,0)  = fx1 / denom;
    (*H)(1,0)  = fx2 / denom;
    (*H)(0,1)  = fy1 / denom;
    (*H)(1,1)  = fy2 / denom;
    (*H)(0,2)  = (cx1*((*H)(2,2)) + ((*H)(0,2))/dx1 - cx0*fx1 - cy0*fy1) / denom;
    (*H)(1,2)  = (cy1*((*H)(2,2)) + ((*H)(1,2))/dy1 - cx0*fx2 - cy0*fy2) / denom;
    (*H)(2,0) *= dx0 / denom;
    (*H)(2,1) *= dy0 / denom;
    (*H)(2,2)  = 1.f;
    
    return 0;
}

static int solve_system4(Matrix4f* A, Vector4f* b, Vector4f* d)
{
    *d = A->colPivHouseholderQr().solve(*b);
    
    return 0;
}

static int compute_rigid_transformation(float* px0, float* py0, float* px1, float* py1, const int point_count, Matrix3f* H)
{
    if (point_count < 2)
    {
        (*H) = SO3f(0, 0, 0).Adj();
        
        return 1;
    }
    
    // Solve the system A*x = b for the 4-dof rigid transformation
    // Form (A.'*A)*x = A.'*b, so solution found using pseudo-inverse
    Matrix4f AtA;
    Vector4f Atb;
    
    AtA.setZero();
    Atb.setZero();
    for (int j = 0; j < point_count; j++)
    {
        const float x0 = px0[j];
        const float y0 = py0[j];
        const float x1 = px1[j];
        const float y1 = py1[j];
        
        AtA(0,0) += x0*x0 + y0*y0;
        AtA(0,2) += x0;
        AtA(0,3) += y0;
        
        Atb(0) += x0*x1 + y0*y1;
        Atb(1) += x0*y1 - y0*x1;
        Atb(2) += x1;
        Atb(3) += y1;
    }
    
    AtA(2,2) = AtA(3,3) = point_count;
    AtA(1,1) = AtA(0,0);
    AtA(1,2) = AtA(2,1) = -AtA(0,3);
    AtA(3,0) = AtA(0,3);
    AtA(2,0) = AtA(3,1) = AtA(1,3) = AtA(0,2);
    
    Vector4f d;
    solve_system4( &AtA, &Atb, &d );
    
    if (std::isnan(d.norm()))
    {
        *H = SO3f(0, 0, 0).Adj();
    }
    else
    {
        *H << d(0), -d(1), d(2),
              d(1),  d(0), d(3),
                 0,     0,    1;
    }
    
    return 0;
}

static int solve_system6(Matrix<float, 6, 6>* A, Matrix<float, 6, 1>* b, Matrix<float, 6, 1>* d)
{
    *d = A->colPivHouseholderQr().solve(*b);
    
    return 0;
}

static int compute_affine_transformation(float* px0, float* py0, float* px1, float* py1, const int point_count, Matrix3f* H)
{
    if (point_count < 3)
    {
        (*H) = SO3f(0, 0, 0).Adj();
        
        return 1;
    }
    
    // Solve the system A*x = b for the 6-dof affine transformation
    // Form (A.'*A)*x = A.'*b, so solution found using pseudo-inverse
    Matrix<float, 6, 6> AtA;
    Matrix<float, 6, 1> Atb;
    
    AtA.setZero();
    Atb.setZero();
    for (int j = 0; j < point_count; j++)
    {
        const float x0 = px0[j];
        const float y0 = py0[j];
        const float x1 = px1[j];
        const float y1 = py1[j];
        
        AtA(0,0) += x0*x0;
        AtA(1,1) += y0*y0;
        AtA(0,1) += x0*y0;
        AtA(0,4) += x0;
        AtA(1,4) += y0;
        
        Atb(0) += x0*x1;
        Atb(1) += y0*x1;
        Atb(2) += x0*y1;
        Atb(3) += y0*y1;
        Atb(4) += x1;
        Atb(5) += y1;
    }
    
    AtA(4,4) = AtA(5,5) = point_count;
    AtA(2,2) = AtA(0,0);
    AtA(3,3) = AtA(1,1);
    
    AtA(1,0) = AtA(3,2) = AtA(2,3) = AtA(0,1);
    
    AtA(4,0) = AtA(5,2) = AtA(2,5) = AtA(0,4);
    AtA(4,1) = AtA(5,3) = AtA(3,5) = AtA(1,4);
    
    Matrix<float, 6, 1> d;
    solve_system6( &AtA, &Atb, &d );
    
    if (std::isnan(d.norm()))
    {
        *H = SO3f(0, 0, 0).Adj();
    }
    else
    {
        *H << d(0), d(1), d(4),
              d(2), d(3), d(5),
                 0,    0,    1;
    }
    
    return 0;
}

int xsc::stabilize::update_optflo(  stabilizer* const s,
                                    unsigned int n,
                                    unsigned int small, 
                                    unsigned int levels, 
                                    xsc::image::level* pI, 
                                    xsc::image::level* pJ)
{
    // Set up the keypoint list pointers to point to current image
    s->px0[1] = pI->x0[1];
    s->py0[1] = pI->y0[1];
    s->px1[1] = pI->x1[1];
    s->py1[1] = pI->y1[1];
    s->px2[1] = pI->x2[1];
    s->py2[1] = pI->y2[1];
    
    s->px0[0] = pI->x0[0];
    s->py0[0] = pI->y0[0];
    s->px1[0] = pI->x1[0];
    s->py1[0] = pI->y1[0];
    s->px2[0] = pI->x2[0];
    s->py2[0] = pI->y2[0];
    
    // Forward-backward Pyramidal LK Optical Flow Matching
    if(xsc::stabilize::optflo(s->px0[1], s->py0[1], n, small, levels, pI, pJ, 
                              s->px1[1], s->py1[1], s->px2[1], s->py2[1]) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
}
                                            
                                            

int xsc::stabilize::homography_from_optflo( stabilizer* const s,
                                            unsigned int n,
                                            int w,
                                            int h,
                                            Matrix3f* Hk)
{
    static xsc::samplers::uint u;
    
    s->match_count = 0;
    s->inlier_count = 0;
    
    // If there are no keypoints
    if (n == 0)
    {
      // Solution is identity, early exit
      *Hk = SO3f(0, 0, 0).Adj();
      
      return 0;
    }
    
    // Sort the keypoints so that the matches are first  
    for (int j = 0; j < n; j++)
    {
        // If this match is valid
        if (s->px2[0][j] >= 1.f)
        {
            // If the reverse pass results in a coordinate close to
            // the original location, match is good.
            float xe = fabsf(s->px0[0][j] - s->px2[0][j]);
            float ye = fabsf(s->py0[0][j] - s->py2[0][j]);
            if (xe < xsc::stabilize::FLOW_DISTANCE_ERROR &&
                ye < xsc::stabilize::FLOW_DISTANCE_ERROR)
            {
                // Store these matching coordinates
                /*
                float   t = s->px0[0][s->match_count]; s->px0[0][s->match_count] = s->px0[0][j]; s->px0[0][j] = t;
                        t = s->py0[0][s->match_count]; s->py0[0][s->match_count] = s->py0[0][j]; s->py0[0][j] = t;
                        t = s->px1[0][s->match_count]; s->px1[0][s->match_count] = s->px1[0][j]; s->px1[0][j] = t;
                        t = s->py1[0][s->match_count]; s->py1[0][s->match_count] = s->py1[0][j]; s->py1[0][j] = t;
                */
                
                s->px0[0][s->match_count] = s->px0[0][j];
                s->py0[0][s->match_count] = s->py0[0][j];
                s->px1[0][s->match_count] = s->px1[0][j];
                s->py1[0][s->match_count] = s->py1[0][j];
                
                s->match_count++;
            }
        }
    }
    
    // Find homography transformation between current and previous frame
    int iterations[3] = {1,0,0};
    Matrix3f H = SO3f(0, 0, 0).Adj();
    
    // Maximum number of RANSAC iterations
    const int max_ransac_its        = xsc::stabilize::MAX_RANSAC_ITS;
    // Minimum number of RANSAC iterations
    const int min_ransac_its        = xsc::stabilize::MIN_RANSAC_ITS;
    // Minimum inlier count to accept a solution
    const int min_inlier_count      = xsc::stabilize::MIN_INLIER_COUNT;
    // Minimum inlier count to stop iterating early
    const int target_ransac_inliers = s->match_count * xsc::stabilize::TARGET_INLIER_PCT;
    // Minimum match set size for finding random solution
    const int min_set_size          = xsc::stabilize::MIN_SET_SIZE;
    // Maximum match set size for finding transformation solution
    const int max_set_size          = xsc::stabilize::MAX_SET_SIZE;
    // Maximum distance between predicted and measured point to call inlier
    const float ssd                 = xsc::stabilize::INLIER_DISTANCE;
    
    // Make sure there are enough matches
    if (s->match_count >= MAX(min_inlier_count, min_set_size))
    {
        // Start the RANSAC iterations
        int i;
        for (i = 0; i < max_ransac_its; i++)
        {
            if (i > 0) // Always try identity as the first iteration
            {
                // Pick xsc::MIN_SET_SIZE random points, put them at the front
                int success = TRUE;
                for (int j = 0; j < min_set_size; j++)
                {
                    // Swap this match with a random match later in the array
                    const int k = u() % (uint32_t)(s->match_count - j) + j;
                    
                    float t = s->px0[0][j]; s->px0[0][j] = s->px0[0][k]; s->px0[0][k] = t;
                          t = s->py0[0][j]; s->py0[0][j] = s->py0[0][k]; s->py0[0][k] = t;
                          t = s->px1[0][j]; s->px1[0][j] = s->px1[0][k]; s->px1[0][k] = t;
                          t = s->py1[0][j]; s->py1[0][j] = s->py1[0][k]; s->py1[0][k] = t;            
                }
                 
                // Fit a homography to the random-sampled points
                compute_affine_transformation(s->px1[0], s->py1[0], s->px0[0], s->py0[0], min_set_size, &H);
                
                // Check homography solution for bad scaling, in-plane rotations, or skew
                const float theta = atan2(H(1,0),H(1,1));
                const float sx    = cos(theta)*H(0,0) - sin(theta)*H(0,1);
                const float sy    = sqrt(H(1,0)*H(1,0) + H(1,1)*H(1,1));
                const float m     = sin(theta)*H(0,0) + cos(theta)*H(0,1);
                
                if ((fabsf(sy - 1.f) > xsc::stabilize::MAX_SOLN_SY)    ||
                    (fabsf(sx - 1.f) > xsc::stabilize::MAX_SOLN_SX)    || 
                    (fabsf(theta)    > xsc::stabilize::MAX_SOLN_THETA) ||
                    (fabsf(m)        > xsc::stabilize::MAX_SOLN_SKEW))
                {
                    success = FALSE;
                    H = SO3f(0, 0, 0).Adj();
                }
                
                iterations[0]++;
                if (!success)
                {
                    // If transformation is bad, skip this iteration
                    continue;
                }
            }
               
            // Count the number of inliers with this transformation
            int inliers = 0;
            for (int j = 0; j < s->match_count; j++)
            {
                const float xi = s->px1[0][j];
                const float yi = s->py1[0][j];
                
                // Apply the transformation to the previous location
                const float zf =  H(2,0) * xi + H(2,1) * yi + H(2,2);
                if ((zf - xsc::CLOSE_TO_ZERO) < 0.f)
                {
                    continue;
                }
                
                const float xf = (H(0,0) * xi + H(0,1) * yi + H(0,2)) / zf;
                const float yf = (H(1,0) * xi + H(1,1) * yi + H(1,2)) / zf;
                
                // If the point is outside of the image-space boundaries of image, ignore
                if (xf < 2 || w - 2 <= xf || yf < 2 || h - 2 <= yf)
                {
                    continue;
                }
                
                // Check that projection into current frame is close to measured location
                const float dx = fabsf(xf - s->px0[0][j]);
                const float dy = fabsf(yf - s->py0[0][j]);
                if (dx <= ssd && dy <= ssd)
                {
                    // Put the inliers at the start of the array
                    float t = s->px0[0][j]; s->px0[0][j] = s->px0[0][inliers]; s->px0[0][inliers] = t;
                          t = s->py0[0][j]; s->py0[0][j] = s->py0[0][inliers]; s->py0[0][inliers] = t;
                          t = s->px1[0][j]; s->px1[0][j] = s->px1[0][inliers]; s->px1[0][inliers] = t;
                          t = s->py1[0][j]; s->py1[0][j] = s->py1[0][inliers]; s->py1[0][inliers] = t; 
                    inliers++;
                }
            }
            
            // If this solution is the best yet in terms of number of inliers, save inliers
            if (inliers > MAX(min_inlier_count, s->inlier_count))
            {
                s->inlier_count = inliers;
                iterations[2] = i;

                memcpy(s->inx0, s->px0[0], sizeof(float)*inliers);
                memcpy(s->iny0, s->py0[0], sizeof(float)*inliers);
                memcpy(s->inx1, s->px1[0], sizeof(float)*inliers);
                memcpy(s->iny1, s->py1[0], sizeof(float)*inliers);
            }
            
            iterations[1]++;
            // If we have enough iterations and inliers, stop iterating
            if (iterations[1] >= min_ransac_its && s->inlier_count >= target_ransac_inliers)
            {
                break;
            }
        }

        // Use all of the inliers to find a better solution
        Matrix3f Ht;
        compute_homography_transformation(s->inx1, s->iny1, s->inx0, s->iny0, MIN(s->inlier_count, max_set_size), &Ht);
        
        // Make sure that this solution is valid
        const float theta = atan2(Ht(1,0),Ht(1,1));
        const float sx    = cos(theta)*Ht(0,0) - sin(theta)*Ht(0,1);
        const float sy    = sqrt(Ht(1,0)*Ht(1,0) + Ht(1,1)*Ht(1,1));
        const float m     = sin(theta)*Ht(0,0) + cos(theta)*Ht(0,1);
        
        if ((fabsf(sy - 1.f) > xsc::stabilize::MAX_SOLN_SY)    ||
            (fabsf(sx - 1.f) > xsc::stabilize::MAX_SOLN_SX)    || 
            (fabsf(theta)    > xsc::stabilize::MAX_SOLN_THETA) ||
            (fabsf(m)        > xsc::stabilize::MAX_SOLN_SKEW))
        {
            // Do nothing, leave H as is
        }
        else
        {
            H = Ht;
        }        
    }
    
    // Return the best solution found
    *Hk = H;
    
    // fprintf(stderr, "its:\t[%d, %d, %d]\tin:\t%d\tout:\t%d\ttot:\t%d\tpct:\t%3.2f \%\n", iterations[2], iterations[0], iterations[1], s->inlier_count, s->match_count - s->inlier_count, s->match_count, 100.f*(s->inlier_count)/(float)(MAX(s->match_count,1)));
    
    return 0;
}




