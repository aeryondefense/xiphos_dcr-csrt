/* Copyright (C) 2015 Xiphos Systems Corporation
 */

#ifndef PROFILE_H
#define PROFILE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <sys/time.h>
 
inline double seconds()
{
    struct timeval tp;
    struct timezone tzp;
    int i = gettimeofday(&tp, &tzp);
    return ((double)tp.tv_sec + (double)tp.tv_usec * 1.e-6);
}

#ifdef __cplusplus
}
#endif

#if RELEASE

#define PROFILE(op) \
    op

#else

#define PROFILE(op) \
({ \
    clock_t a = clock(); \
    op; \
    clock_t b = clock(); \
    float d = (b - a) * 1000.f / CLOCKS_PER_SEC; \
    fprintf(stderr, "%s, %d: " #op " completed in %f milliseconds\n", __FILE__, __LINE__, d); \
    d; \
})

#endif

#if RELEASE

#define PROF(start, last, step, id, limit, format, ...)

#else

#define PROF(start, last, step, id, limit, format, ...) \
({ \
    double end = seconds(); \
    fprintf(stderr, "{%d} [%d]\t%.3f ms,\t + %.3f ms, " format "\n", id, step, (end - start)*1000, (end - last)*1000, ##__VA_ARGS__); \
    last = end; \
    step++; \
})

#endif

#endif /* #ifndef PROFILE_H */
