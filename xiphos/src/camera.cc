/* Copyright (C) 2016 Xiphos Systems Corporation
 */

#include "config.h"


namespace xsc
{
static constexpr float GOOD_SENSITIVITY = 7.0;
}

static float bilerp_yuv420( const uint8_t *i, const int stride, int scale, float x, float y)
{
    int s = stride;

    x *= scale;
    y *= scale;
    
    int imin = x;
    int imax = imin + scale;

    int jmin = y;
    int jmax = jmin + scale;

    float xmin = imin;
    float xmax = imax;

    float ymin = jmin;
    float ymax = jmax;

    float v = 0;

    v += i[imin + jmin * s] * (xmax - x) * (ymax - y);
    v += i[imax + jmin * s] * (x - xmin) * (ymax - y);
    v += i[imin + jmax * s] * (xmax - x) * (y - ymin);
    v += i[imax + jmax * s] * (x - xmin) * (y - ymin);

    float factor = 1.0f/255.0/(scale*scale);
    return v*factor;
}

static float bilerp_UYVY( const uint8_t *i, const int stride, int scale, float x, float y)
{
    int s = stride;

    x *= scale;
    y *= scale;
    
    int imin = x;
    int imax = imin + scale;

    int jmin = y;
    int jmax = jmin + scale;

    float xmin = imin;
    float xmax = imax;

    float ymin = jmin;
    float ymax = jmax;

    float v = 0;

    v += i[imin * 2 + 1 + jmin * s] * (xmax - x) * (ymax - y);
    v += i[imax * 2 + 1 + jmin * s] * (x - xmin) * (ymax - y);
    v += i[imin * 2 + 1 + jmax * s] * (xmax - x) * (y - ymin);
    v += i[imax * 2 + 1 + jmax * s] * (x - xmin) * (y - ymin);

    float factor = 1.0f/255.0/(scale*scale);
    return v*factor;
}

// Scaled bi-linear interpolation of a YUV image buffer
static float bilerp(const uint8_t *im, const int stride, int scale, format_type format, float x, float y)
{
    if (format != I420)
    {
        return bilerp_UYVY(im, stride, scale, x, y);
    }
    else
    {
        return bilerp_yuv420(im, stride, scale, x, y);
    }
}

// Take the flow keypoints from current image
//  and form 3D hypotheses for each point position into p
static int sample_keypoints(xsc::camera::level *p, const xsc::image::level *q)
{
    const int scale = 1 << p->scale;
    p->ns = MIN(MIN(q->nf, q->np[0][0]), xsc::MAX_EGO_KEYPOINTS);
    
    for (int n = 0; n < p->ns; n++)
    {
        const float x = q->x0[0][n];
        const float y = q->y0[0][n];
        
        p->is[n] = bilerp(p->im_data, p->im_stride, scale, p->im_format, x, y);
        p->xs[n] = x;
        p->ys[n] = y;
    }

    return 0;
}


xsc::camera::level::level(void)
{
    nl = 0;
    ns = 0;
    is = nullptr;
    xs = nullptr;
    ys = nullptr;
    
    im_data = nullptr;
    im_stride = 0;
    im_width = 0;
    im_height = 0;

    kf_data = nullptr;
    kf_stride = 0;
    kf_width = 0;
    kf_height = 0;

    res_filt = 0.0;
    
    SO3f eye(0,0,0);
    Hk = eye.Adj();
    Hp = eye.Adj();
    
    initialized = 0;
}

xsc::camera::level::~level(void)
{
}

int xsc::camera::reset(xsc::camera::level *p)
{
    SO3f eye(0,0,0);
    p->Hk = eye.Adj();
    p->Hp = eye.Adj();
    
    p->im_data = nullptr;
    p->kf_data = nullptr;
    p->initialized = 0;
    
    p->res_filt = 0.0;
    
    p->ns = 0;
    p->nl = 0;

    return 0;
}

int xsc::camera::init(xsc::camera::level *p,
                        int scale,
                        int width,
                        int height,
                        sensor_type sensor,
                        int total_keypoints)
{
    if ((p->is = new(std::nothrow) float[xsc::MAX_EGO_KEYPOINTS]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if ((p->xs = new(std::nothrow) float[xsc::MAX_EGO_KEYPOINTS]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if ((p->ys = new(std::nothrow) float[xsc::MAX_EGO_KEYPOINTS]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    SO3f eye(0,0,0);
    p->Hk = eye.Adj();
    p->Hp = eye.Adj();

    p->ns = 0;
    p->scale = scale;
    
    p->im_width  = width;
    p->im_height = height;
    p->kf_width  = width;
    p->kf_height = height;
    
    p->im_data = nullptr;
    p->kf_data = nullptr;
    p->kf = nullptr;
    p->initialized = 0;
    p->res_filt = 0.0;
    p->nl = 0;

    // Increase MTI sensitivity with IR
    p->gsc = GOOD_SENSITIVITY;
    if (sensor == IR)
    {
        p->gsc *= 0.9;
    }
    
    return 0;
}

int xsc::camera::free(xsc::camera::level *p)
{
    delete [] p->is;
    p->is = nullptr;
    delete [] p->xs;
    p->xs = nullptr;
    delete [] p->ys;
    p->ys = nullptr;
        
    p->ns = 0;
    p->nl = 0;
    
    p->im_data = nullptr;
    p->kf_data = nullptr;
    p->initialized = 0;
    p->kf = nullptr;
 
    return 0;
}

// Set up a level frame, generate position hypotheses for the points in current image, in p.
// Level q is the reference keyframe for p.
int xsc::camera::iniframe(xsc::camera::level *p, const xsc::image::level *q)
{
    p->Hk = SO3f::exp(Vector3f(0, 0, 0)).Adj();
    p->Hp = p->Hk;

    p->kf_data   = p->im_data;
    p->kf_stride = p->im_stride;
    p->kf_format = p->im_format;
    p->kf_width  = p->im_width;
    p->kf_height = p->im_height;
    
    p->kf = q;
    
    if (sample_keypoints(p, q) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    p->initialized = 1;

    return 0;
}

// Set up a keyframe level frame, generate position hypotheses for the 'good' point matches between
//  levels p and q, store in p
int xsc::camera::keyframe(xsc::camera::level *p, const xsc::image::level *q)
{
    p->Hk = SO3f::exp(Vector3f(0, 0, 0)).Adj();
    p->Hp = p->Hk;

    p->kf_data   = p->im_data;
    p->kf_stride = p->im_stride;
    p->kf_format = p->im_format;
    p->kf_width  = p->im_width;
    p->kf_height = p->im_height;
    
    p->kf = q;
    
    if (sample_keypoints(p, q) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    return 0;
}



// Non-linear least-squares fit between two levels solving for relative pose and point positions
// Cost function is sum of squared-error on pixel intensities
int xsc::camera::auxframe(xsc::camera::level *p, const xsc::image::level *q)
{
    p->residual = 1E6;
    const int scale = 1 << p->scale;
    p->nl = 0;
    float cost = 0;

    // Loop over all of the point samples
    for (int n = 0; n < p->ns; n++)
    {
        // Project the points from (keyframe) p into image-space of current image
        const float xs = p->xs[n];
        const float ys = p->ys[n];
        
        const float zc = p->Hk(2,0)*xs + p->Hk(2,1)*ys + p->Hk(2,2);
        if (zc < xsc::CLOSE_TO_ZERO)
        {
            p->nl++;
            continue;
        }
        
        const float xc = (p->Hk(0,0)*xs + p->Hk(0,1)*ys + p->Hk(0,2)) / zc;
        const float yc = (p->Hk(1,0)*xs + p->Hk(1,1)*ys + p->Hk(1,2)) / zc;
        
        // If the projection is outside the image-space, skip
        if (xc < 2 || q->w - 2 <= xc || yc < 2 || q->h - 2 <= yc)
        {
            p->nl++;
            continue;
        }
        
        // Error of pixel intensity between matched point in p and current image
        const float rs = p->is[n] - bilerp(p->im_data, p->im_stride, scale, p->im_format, xc, yc);
    
        // Cost function is sum of squares of pixel intensity error
        cost += rs * rs;
    }

    // Update residual
    const int nt = p->ns - p->nl;
    if (nt)
    {
        p->residual = cost / nt;
    }
    LP_FILT(p->res_filt, p->residual, 1.f);

    return 0;
}
