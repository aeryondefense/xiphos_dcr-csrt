/* Copyright (C) 2015 Xiphos Systems Corporation
 */

#ifndef UINT_H
#define UINT_H

namespace xsc
{
namespace samplers
{
class uint
{
    uint32_t x_;
    uint32_t y_;
    uint32_t z_;
    uint32_t w_;

public:
    uint(void);
    uint(uint32_t x, uint32_t y, uint32_t z, uint32_t w);

    uint(const xsc::samplers::uint &) = delete;
    xsc::samplers::uint &operator = (const xsc::samplers::uint &) = delete;

    uint32_t operator () (void);
};
}
}

inline xsc::samplers::uint::uint(void) : x_(123456789), y_(362436069), z_(88675123), w_(88675123)
{
}

inline xsc::samplers::uint::uint(uint32_t x, uint32_t y, uint32_t z, uint32_t w) : x_(x), y_(y), z_(z), w_(w)
{
}

inline uint32_t xsc::samplers::uint::operator () (void)
{
    uint32_t t;

    t  = x_ ^ (x_ << 11);
    x_ = y_;
    y_ = z_;
    z_ = w_;
    w_ = w_ ^ (w_ >> 19) ^ t ^ (t >> 8);

    return w_;
}

#endif /* #ifndef UINT_H */
