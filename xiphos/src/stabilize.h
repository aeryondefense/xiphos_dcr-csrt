/*
 * ============================================================================
 *
 *       Filename:  stabilize.h
 *
 *    Description:  Video stabilization
 *
 *        Created:  2016-09-16
 *   Organization:  Aeryon Labs Inc.
 *
 * ============================================================================
 */


#ifndef STABILIZE_H
#define STABILIZE_H

namespace xsc
{
namespace stabilize
{

static constexpr int   MAX_HOMOGRAPHY_FILTER_LENGTH = 51;

static constexpr int   RADIUS_MIN = 0;
static constexpr int   RADIUS_MAX = MAX_HOMOGRAPHY_FILTER_LENGTH - 1;
static constexpr float STDDEV_MIN = 0.0f;
static constexpr float STDDEV_MAX = 100.0f;
static constexpr float SCALE_MIN = 0.5f;
static constexpr float SCALE_MAX = 2.0f;

static constexpr float FLOW_DISTANCE_THRESHOLD = 0.4;
static constexpr float FLOW_DISTANCE_ERROR = FLOW_DISTANCE_THRESHOLD;


struct stabilizer
{
    // Homography filter parameters
    int   radius; // radius of filter window (length = radius + 1)
    float stddev; // standard deviation of filter weights
    float scale;  // nominal scale factor to zoom video for black borders
    float marginal_scale;   // current scale factor for zooming video
    float scale_increment;  // amount to increment scale by for searching
    float scale_decrement;  // amount to decrement scale by for restoring scale
    int   width;  // image width
    int   height; // image height
    int   split;  // only warp half of the image
    int   delay;  // wait to stabilize until motion is complete
    int   valid_solution; // recent homography is from a valid ego motion solution
    
    float weights[MAX_HOMOGRAPHY_FILTER_LENGTH]; // weightings for homography element matrices
    Matrix3f Hs[MAX_HOMOGRAPHY_FILTER_LENGTH];   // previous homography matrices
    Matrix3f Hc[MAX_HOMOGRAPHY_FILTER_LENGTH];   // cumulative homography matrices
    Matrix3f Hk;  // homography at current time step
    Matrix3f H;   // filtered homography output    
    Matrix3f S;   // scaling matrix for final image    
    float* h[2];  // Homography matrix buffer on host and device
    float* hk;    // Current homography for predicting UDOT targets
    
    // Optical flow
    float* px0[2];
    float* py0[2];
    float* px1[2];
    float* py1[2];
    float* px2[2];
    float* py2[2];
    float* inx0;
    float* iny0;
    float* inx1;
    float* iny1;
    int match_count;
    int inlier_count;
    
    stabilizer();
    virtual ~stabilizer();

    stabilizer(const stabilizer &) = delete;
    stabilizer &operator = (const stabilizer &) = delete;
};


int    init(stabilizer* const s, const int w, const int h, const float f, const float cx, const float cy, const float fps);
int    free(stabilizer* const s);

int    init_gpu(stabilizer* const s);
int    free_gpu(stabilizer* const s);
int    copy_homography(stabilizer* const s);
int    print_homography(stabilizer* const s, FILE* const fd, const int id);

int    stabilize(stabilizer* const s, Matrix3f* const Hk, const float fit, const int new_keyframe, const int motion_delay);
int    filter_homography(stabilizer* const s);

int    set_filter(stabilizer* const s, const int radius, const float stddev);

float* get_homography_address(stabilizer* const s);
float* get_current_homography(stabilizer* const s);
int    current_homography_valid(stabilizer* const s);
int    assign_homographies(stabilizer* const s, xsc::image::level* p);

int    set_border_scale(stabilizer* const s, const float scale);

float  get_scale(stabilizer* const s);

float  get_marginal_scale(stabilizer* const s);

int    get_filter_radius(stabilizer* const s);
int    set_filter_radius(stabilizer* const s, const int radius);

float  get_filter_stddev(stabilizer* const s);
int    set_filter_stddev(stabilizer* const s, const float stddev);

float  get_scale_increment(stabilizer* const s);
int    set_scale_increment(stabilizer* const s, const float incr);

float  get_scale_decrement(stabilizer* const s);
int    set_scale_decrement(stabilizer* const s, const float decr);

int    get_split_screen(stabilizer* const s);
int    set_split_screen(stabilizer* const s, const int split);

int optflo( float* pIx, 
            float* pIy, 
            unsigned int n, 
            unsigned int small, 
            unsigned int levels,
            xsc::image::level* pI, 
            xsc::image::level* pJ,
            float* pJx, 
            float* pJy, 
            float* pKx, 
            float* pKy);

int update_optflo(  stabilizer* const s,
                    unsigned int n,
                    unsigned int small, 
                    unsigned int levels, 
                    xsc::image::level* pI, 
                    xsc::image::level* pJ);
                                                
int homography_from_optflo( stabilizer* const s,
                            unsigned int n,
                            int w,
                            int h,          
                            Matrix3f* Hk);

}
}

#endif

