#include "config.h"
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>

void xsc::udot::copy_patches(float* const dst, const int s, const float* const src, const int w, const int h)
{
    checkCudaErrors(cudaMemcpy2D(dst, s, src, s, w, h, cudaMemcpyDeviceToHost));
}


