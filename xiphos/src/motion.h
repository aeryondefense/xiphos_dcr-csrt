/* Copyright (C) 2016 Xiphos Systems Corporation
 */

// MTI

#ifndef MOTION_H
#define MOTION_H

namespace xsc
{
namespace mti
{

struct box
{
    int ns;
    float xmin;
    float ymin;
    float xmax;
    float ymax;
    int merged;
    int age;
};

static constexpr int   MIN_AGE_MIN = 0;
static constexpr int   MIN_AGE_MAX = 100;
static constexpr int   MAX_AGE_MIN = 0;
static constexpr int   MAX_AGE_MAX = 512;
static constexpr float MAX_SPEED_MIN = 0.0;
static constexpr float MAX_SPEED_MAX = 0.1;
static constexpr float MIN_RADIUS_MIN = 0.0;
static constexpr float MIN_RADIUS_MAX = 0.1;
static constexpr float FILTER_RATE_MIN = 0.0;
static constexpr float FILTER_RATE_MAX = 1.0;
static constexpr float DECAY_RATE_MIN = 0.0;
static constexpr float DECAY_RATE_MAX = 1.0;
static constexpr float SWEEP_SIZE_MIN = 0.0;
static constexpr float SWEEP_SIZE_MAX = 0.1;
static constexpr float MIN_BOX_SIZE_MIN = 0.0;
static constexpr float MIN_BOX_SIZE_MAX = 0.1;
static constexpr int   BLANKING_MIN = 0;
static constexpr int   BLANKING_MAX = 100;

static constexpr int   MIN_AGE_DEFAULT = 2;
static constexpr int   MAX_AGE_DEFAULT = 128;
static constexpr float MAX_SPEED_DEFAULT = 0.012;
static constexpr float MIN_RADIUS_DEFAULT = 0.012;
static constexpr float FILTER_RATE_DEFAULT = 0.4;
static constexpr float DECAY_RATE_DEFAULT = 0.03;
static constexpr float SWEEP_SIZE_DEFAULT = 0.019;
static constexpr float MIN_BOX_SIZE_DEFAULT = 0.018;
static constexpr int   BLANKING_DEFAULT = 0;

static constexpr float RADIAL_WEIGHT_EXP = 0.2;
static constexpr int   NUM_MOTION_SAMPLES = 3000;

template <int N>
struct motion
{
    int w;          // Width of the motion map
    int h;          // Height of the motion map
    uint8_t *map;   // Motion map
    float *rw;      // Radial weight map

    xsc::samplers::uint u;

    int nz;                     // Number of moving points
    int *xz;                    // Moving point x-coordinate
    int *yz;                    // Moving point y-coordinate
    int xs[NUM_MOTION_SAMPLES]; // Motion sample x-coordinate
    int ys[NUM_MOTION_SAMPLES]; // Motion sample y-coordinate
    int as[NUM_MOTION_SAMPLES]; // Motion sample age
    box b1[NUM_MOTION_SAMPLES]; // List of bounding boxes (input)
    box b2[NUM_MOTION_SAMPLES]; // List of bounding boxes (output)
    box b3[NUM_MOTION_SAMPLES]; // List of bounding boxes (previous)

    int nb;     // Number of bounding boxes
    box *boxes; // Bounding boxes around moving targets
    
    int   min_age;
    int   max_age;
    float max_speed;    // Maximum seed speed for each motion sample (pixels/width))
    float min_radius;   // Starting motion seed radius (pixels/width)
    float filter_rate;
    float decay_rate;
    float sweep_size;   // Radial sweep distance to join boxes together (pixels/width)
    float min_box_size; // Minimum acceptable box size to return (pixels/width)
    int   blanking;
    
    motion(void);
    virtual ~motion(void);

    motion(const motion &) = delete;
    motion &operator = (const motion &) = delete;
};

template <int N>
int filter_boxes(box *old, int os, box *src, int ss, box *dst, int *nd, const float s, const float r, const float d, const int m)
{
    // Method to join two boxes together
    auto join = [] (box *a, box *b)
    {
        if (a->ns == 0)
        {
            *a = *b;
        }
        a->xmin = (a->xmin < b->xmin) ? a->xmin : b->xmin;
        a->ymin = (a->ymin < b->ymin) ? a->ymin : b->ymin;
        a->xmax = (a->xmax < b->xmax) ? b->xmax : a->xmax;
        a->ymax = (a->ymax < b->ymax) ? b->ymax : a->ymax;
        b->merged = 1;
        a->ns++;
        a->age = MAX(a->age, b->age);
    };

    // method to blend two boxes together
    auto blend = [] (box *a, box *b, float r)
    {
        a->xmin = (a->xmin * (1.0 - r) + b->xmin * r);
        a->ymin = (a->ymin * (1.0 - r) + b->ymin * r);
        a->xmax = (a->xmax * (1.0 - r) + b->xmax * r);
        a->ymax = (a->ymax * (1.0 - r) + b->ymax * r);
        a->age = MAX(a->age, b->age);
    };

    // Method to diminish old boxes
    auto diminish = [] (box *a, float d)
    {
        float xd = a->xmax - a->xmin;
        float yd = a->ymax - a->ymin;
        a->xmin += xd * d;
        a->ymin += xd * d;
        a->xmax -= yd * d;
        a->ymax -= yd * d;
    };
    
    // Merge box b into box c if box b is near or inside box a
    auto sweep = [&join] (box *a, box *b, box *c, float s)
    {
        // If the corners of box j lie within box i, merge them into one (they overlap)
        if (a->xmin - s <= b->xmin && b->xmin <= a->xmax + s)
        {
            if (a->ymin - s <= b->ymin && b->ymin <= a->ymax + s)
            {
                join(c, b);
                return;
            }
            if (a->ymin - s <= b->ymax && b->ymax <= a->ymax + s)
            {
                join(c, b);
                return;
            }
        }

        if (a->xmin - s <= b->xmax && b->xmax <= a->xmax + s)
        {
            if (a->ymin - s <= b->ymin && b->ymin <= a->ymax + s)
            {
                join(c, b);
                return;
            }
            if (a->ymin - s <= b->ymax && b->ymax <= a->ymax + s)
            {
                join(c, b);
                return;
            }
        }
    };
    
    // Reset indicator that box has been consumed
    for (int i = 0; i < ss; i++)
    {
        src[i].merged = 0;
    }
    
    *nd = 0;
    
    int k = 0;
    for (int i = 0; i < os; i++)
    {
        if (k >= NUM_MOTION_SAMPLES)
        {
            break;
        }
        box tmp;
        tmp.ns = 0;
        
        // merge new into old
        for (int j = 0; j < ss; j++)
        {            
            if (src[j].merged == 1)
            {
                continue;
            }
            sweep (old + i, src + j, &tmp, s);
        }        
        // blend old and new overlapping boxes
        if (tmp.ns > 0)
        {
            blend(old + i, &tmp, r);
            dst[k] = old[i];
            k++;
        }
        // diminish old boxes
        else
        {
            diminish(old + i, d);
            if (old[i].xmax - old[i].xmin > m && old[i].ymax - old[i].ymin > m)
            {
                dst[k] = old[i];
                k++;
            }
        }
    }
        
    // Add any new boxes
    for (int j = 0; j < ss; j++)
    {        
        if (k >= NUM_MOTION_SAMPLES)
        {
            break;
        }
        
        if (src[j].merged != 1)
        {
            dst[k] = src[j];
            dst[k].age = 0;
            k++;
        }
    }
    
    *nd = k;

    return 0;
}

// Merge boxes that overlap within the src set, into dst set
template <int N>
int merge(box *src, int ns, box *dst, int *nd)
{
    // Method to join two boxes together
    auto join = [] (box *a, box *b)
    {
        a->xmin = (a->xmin < b->xmin) ? a->xmin : b->xmin;
        a->ymin = (a->ymin < b->ymin) ? a->ymin : b->ymin;
        a->xmax = (a->xmax < b->xmax) ? b->xmax : a->xmax;
        a->ymax = (a->ymax < b->ymax) ? b->ymax : a->ymax;
        b->merged = 1;
    };

    // Reset indicator that box has been consumed
    for (int i = 0; i < ns; i++)
    {
        src[i].merged = 0;
    }

    *nd = 0;

    // Loop over all of the boxes
    for (int i = 0; i < ns; i++)
    {
        // Skip any that have already been consumed
        if (src[i].merged == 1)
        {
            continue;
        }
        
        // Loop over all of the other boxes
        for (int j = i + 1; j < ns; j++)
        {
            // Skip those that have been consumed
            if (src[j].merged == 1)
            {
                continue;
            }
      
            // If the corners of box j lie within box i, merge them into one (they overlap)
            if (src[i].xmin <= src[j].xmin && src[j].xmin <= src[i].xmax)
            {
                if (src[i].ymin <= src[j].ymin && src[j].ymin <= src[i].ymax)
                {
                    join(src + i, src + j);
                    continue;
                }
                if (src[i].ymin <= src[j].ymax && src[j].ymax <= src[i].ymax)
                {
                    join(src + i, src + j);
                    continue;
                }
            }
            
            if (src[i].xmin <= src[j].xmax && src[j].xmax <= src[i].xmax)
            {
                if (src[i].ymin <= src[j].ymin && src[j].ymin <= src[i].ymax)
                {
                    join(src + i, src + j);
                    continue;
                }
                if (src[i].ymin <= src[j].ymax && src[j].ymax <= src[i].ymax)
                {
                    join(src + i, src + j);
                    continue;
                }
            }
        }

        // Add this new superbox to the output set
        dst[(*nd)] = src[i];
        (*nd)++;
    }

    return 0;
}

template <int N>
int   set_min_age(motion<N>* const m, const int age)
{
    m->min_age = age;
    BOUND_VARIABLE(m->min_age, MIN_AGE_MIN, MIN_AGE_MAX);
    
    return 0;
}

template <int N>
int   get_min_age(motion<N>* const m)
{
    return m->min_age;
}

template <int N>
int   set_max_age(motion<N>* const m, const int age)
{
    m->max_age = age;
    BOUND_VARIABLE(m->max_age, MAX_AGE_MIN, MAX_AGE_MAX);
    
    return 0;
}

template <int N>
int   get_max_age(motion<N>* const m)
{
    return m->max_age;
}

template <int N>
int   set_max_speed(motion<N>* const m, const float speed)
{
    m->max_speed = speed;
    BOUND_VARIABLE(m->max_speed, MAX_SPEED_MIN, MAX_SPEED_MAX);
    
    return 0;
}

template <int N>
float   get_max_speed(motion<N>* const m)
{
    return m->max_speed;
}

template <int N>
int   set_min_radius(motion<N>* const m, const float radius)
{
    m->min_radius = radius;
    BOUND_VARIABLE(m->min_radius, MIN_RADIUS_MIN, MIN_RADIUS_MAX);
    
    return 0;
}

template <int N>
float   get_min_radius(motion<N>* const m)
{
    return m->min_radius;
}

template <int N>
int   set_filter_rate(motion<N>* const m, const float rate)
{
    m->filter_rate = rate;
    BOUND_VARIABLE(m->filter_rate, FILTER_RATE_MIN, FILTER_RATE_MAX);
    
    return 0;
}

template <int N>
float get_filter_rate(motion<N>* const m)
{
    return m->filter_rate;
}

template <int N>
int   set_decay_rate(motion<N>* const m, const float rate)
{
    m->decay_rate = rate;
    BOUND_VARIABLE(m->decay_rate, DECAY_RATE_MIN, DECAY_RATE_MAX);
    
    return 0;
}

template <int N>
float get_decay_rate(motion<N>* const m)
{
    return m->decay_rate;
}

template <int N>
int   set_sweep_size(motion<N>* const m, const float size)
{
    m->sweep_size = size;
    BOUND_VARIABLE(m->sweep_size, SWEEP_SIZE_MIN, SWEEP_SIZE_MAX);
    
    return 0;
}

template <int N>
float   get_sweep_size(motion<N>* const m)
{
    return m->sweep_size;
}

template <int N>
int   set_min_box_size(motion<N>* const m, const float size)
{
    m->min_box_size = size;
    BOUND_VARIABLE(m->min_box_size, MIN_BOX_SIZE_MIN, MIN_BOX_SIZE_MAX);
    
    return 0;
}

template <int N>
float   get_min_box_size(motion<N>* const m)
{
    return m->min_box_size;
}

template <int N>
int   set_blanking(motion<N>* const m, const int blanking)
{
    m->blanking = blanking;
    BOUND_VARIABLE(m->blanking, BLANKING_MIN, BLANKING_MAX);
    
    return 0;
}

template <int N>
int   get_blanking(motion<N>* const m)
{
    return m->blanking;
}

template <int N>
motion<N>::motion(void)
{
    w = 0;
    h = 0;
    nz = 0;
    map = nullptr;
    rw =  nullptr;
    xz = nullptr;
    yz = nullptr;
    
    memset(as, 0, NUM_MOTION_SAMPLES * sizeof(int));
    
    min_age         = MIN_AGE_DEFAULT;
    max_age         = MAX_AGE_DEFAULT;
    max_speed       = MAX_SPEED_DEFAULT;
    min_radius      = MIN_RADIUS_DEFAULT;
    filter_rate     = FILTER_RATE_DEFAULT;
    decay_rate      = DECAY_RATE_DEFAULT;
    sweep_size      = SWEEP_SIZE_DEFAULT;
    min_box_size    = MIN_BOX_SIZE_DEFAULT;
    blanking        = BLANKING_DEFAULT;
}

template <int N>
motion<N>::~motion(void)
{
}

inline float radial_dist(const int x, const int y, const int cx, const int cy)
{
    const float dx = x - cx;
    const float dy = y - cy;
    const float den = sqrt(cx * cx + cy * cy);
    const float num = sqrt(dx * dx + dy * dy);
    return num/den;
}

template <int N>
int init(motion<N> *m, int w, int h, int total_keypoints)
{
    w >>= N - 1;
    h >>= N - 1;

    if ((m->map = new(std::nothrow) uint8_t[w * h]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    if ((m->rw = new(std::nothrow) float[w * h]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if ((m->xz = new(std::nothrow) int[total_keypoints * N]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if ((m->yz = new(std::nothrow) int[total_keypoints * N]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    // Generate the radial weight map
    int i, j;
    float d;
    const int cx = w/2;
    const int cy = h/2;
    for (i = 0; i < h; i++)
    {
        for (j = 0; j < w; j ++)
        {
            d = 1.0 - radial_dist(j, i, cx, cy);
            m->rw[j + i * w] = pow(d, RADIAL_WEIGHT_EXP);
        }
    }

    m->w = w;
    m->h = h;
    m->nz = 0;
    m->nb = 0;
    
    memset(m->as, 0, NUM_MOTION_SAMPLES * sizeof(int));
    memset(m->xs, 0, NUM_MOTION_SAMPLES * sizeof(int));
    memset(m->ys, 0, NUM_MOTION_SAMPLES * sizeof(int));

    return 0;
}

template <int N>
int free(motion<N> *m)
{
    delete [] m->map;
    m->map = nullptr;
    delete [] m->rw;
    m->rw = nullptr;
    delete [] m->xz;
    m->xz = nullptr;
    delete [] m->yz;
    m->yz = nullptr;

    m->w = 0;
    m->h = 0;

    return 0;
}

// Update step for the motion particle filter
template <int N>
int update(motion<N> *m, const xsc::image::level *p)
{
    // Reset the "moving" map to zero
    memset(m->map, 0, m->w * m->h);

    m->nz = 0;

    // Loop through the levels starting at the second
    for (int n = 1; n < N; n++)
    {
        p++;

        // Loop through the bad point correspondences (intensity has changed)
        for (int i = 0; i < *(p->nb[0]); i++)
        {
            int x = p->xyb[0][2*i+0];
            int y = p->xyb[0][2*i+1];

            x >>= N - n - 1;
            y >>= N - n - 1;

            // This point represents motion
            m->map[x + y * m->w] = 1;

            // Add this point to the potential set
            m->xz[m->nz] = x;
            m->yz[m->nz] = y;

            m->nz++;
        }
    }

    // Scale with motion width to get pixels
    int ms = (int)(m->max_speed * m->w + 0.5);
    int mr = (int)(m->min_radius * m->w + 0.5);
    
    // For all of the potential motion samples
    for (int n = 0; n < NUM_MOTION_SAMPLES; n++)
    {
        int a = m->as[n];
        int x = m->xs[n];
        int y = m->ys[n];

        // If this motion sample is inactive (age == 0)
        if (a == 0)
        {
            int i;

            if (m->nz == 0)
            {
                continue;
            }
            
            // Select a random point and use it to initialize this motion sample
            i = m->u() % m->nz;

            x = m->xz[i];
            y = m->yz[i];

            // Set this motion sample as active (age > 0)
            a = 1;
        }
        else
        {
            // Otherwise blend these points into this motion sample
            // Select a random increment within [-MAX_SPEED,MAX_SPEED]
            int dx = m->u() % (ms * 2) - ms;
            int dy = m->u() % (ms * 2) - ms;
            
            // Add the increment to the motion hypothesis
            x += dx;
            y += dy;

            // If the new motion does not correspond to a moving pixel in the new image
            if (m->map[x + y * m->w] == 0)
            {
                // Kill this motion sample
                a = 0;
            }
            else
            {
                // Randomly kill this motion sample based on its age.
                // More likely to kill it as it gets older.
                // Or, increase its age by one.
                a = (m->u() % m->max_age < a) ? 0 : a + 1;
            }
        }

        // If the motion increment is outside of the usable image
        if (x < mr + m->blanking || m->w - m->blanking - mr <= x || 
            y < mr + m->blanking || m->h - m->blanking - mr <= y)
        {
            // Kill the sample
            a = 0;
        }

        // Store the new motion sample
        m->as[n] = a;
        m->xs[n] = x;
        m->ys[n] = y;
    }

    return 0;
}

// Cluster motion samples that result in bounding boxes that overlap
template <int N>
int cluster(motion<N> *m)
{
    box *src;
    box *dst;
    box *old;
    box *tmp;

    int nsrc;
    int ndst;
    int ntmp;

    src = m->b1;
    dst = m->b2;
    old = m->b3;

    nsrc = 0;
    ndst = 0;

    int wmr;
    
    // Scale min_radius to pixels
    const float mr = m->min_radius * m->w;
    
    // Scale sweep_size to pixels
    const int ss = (m->sweep_size * m->w + 0.5);

    // Loop through all of the motion samples
    for (int n = 0; n < NUM_MOTION_SAMPLES; n++)
    {
        // If the sample age is too small, skip
        if (m->as[n] < m->min_age)
        {
            continue;
        }
        
        // Apply radial weighting to minimum radius
        wmr = (int)(mr * m->rw[m->xs[n] + m->ys[n] * m->w] + 0.5);

        // Set the boxes around each motion sample to a minimum seed
        src[nsrc].xmin = m->xs[n] - wmr;
        src[nsrc].ymin = m->ys[n] - wmr;
        src[nsrc].xmax = m->xs[n] + wmr;
        src[nsrc].ymax = m->ys[n] + wmr;

        nsrc++;
    }

    // Loop until all of the samples have been clustered.
    // Iterate until no more merging happens.
    while (nsrc != ndst)
    {        
        // Try one pass at merging any boxes that have overlap
        merge<N>(src, nsrc, dst, &ndst);

        tmp = src;
        src = dst;
        dst = tmp;

        ntmp = nsrc;
        nsrc = ndst;
        ndst = ntmp;
    }

    tmp = src;
    src = dst;
    dst = tmp;

    ntmp = nsrc;
    nsrc = ndst;
    ndst = ntmp;

    // Scale minimum box size to pixels
    const int mbs = (int)(m->min_box_size * m->w + 0.5);

    //filter old boxes - output goes in dst
    filter_boxes<N>(old, m->nb, src, nsrc, dst, &ndst, ss, m->filter_rate, m->decay_rate, mbs);

    tmp = src;
    src = dst;
    dst = tmp;

    ntmp = nsrc;
    nsrc = ndst;
    ndst = ntmp;
    ndst = 0;
    
    // Re-cluster the filtered boxes if needed.
    // Iterate until no more merging happens.
    while (nsrc != ndst)
    {        
        // Try one pass at merging any boxes that have overlap
        merge<N>(src, nsrc, dst, &ndst);

        tmp = src;
        src = dst;
        dst = tmp;

        ntmp = nsrc;
        nsrc = ndst;
        ndst = ntmp;
    }
    
    // Save filtered boxes for next time
    for (int n = 0; n < ndst; n++)
    {
        old[n] = dst[n];
        old[n].age++;
    }

    m->nb = ndst;
    m->boxes = dst;
    
    return 0;
}

}
}

#endif /* #ifndef MOTION_H */
