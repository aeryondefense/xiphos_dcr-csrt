/* Copyright (C) 2016 Xiphos Systems Corporation
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h>

#include <new>
#include <cmath>
#include <tuple>
#include <atomic>
#include <chrono>
#include <thread>
#include <iostream>
#include <type_traits>
#include <Dense>
#include <sophus.hpp>
#include <so3.hpp>
#include <trace.h>

using namespace Eigen;
using namespace Sophus;

namespace xsc
{
static constexpr int ALIGN2D = 16;
static constexpr int ALIGN1D = 256;

static constexpr int MAX_W = 1920;
static constexpr int MAX_H = 1280;
static constexpr int NUM_IMAGE_BINS = 20;
static constexpr int TOTAL_KEYPOINTS = 4096;
static constexpr float ADAPTIVE_GAIN = 0.00001;
static constexpr int   MOTION_DELAY = 60;
static constexpr float KEYPOINT_THRESHOLD_MIN = 10.f/255.f;
static constexpr float KEYPOINT_THRESHOLD_MAX = 1.f;
static constexpr float KEYPOINT_THRESHOLD_DEFAULT = 50.f/255.f;
static constexpr float VELOCITY_FILTER_DEFAULT = 3.f;

static constexpr int MIN_EGO_KEYPOINTS  =   6;
static constexpr int MAX_EGO_KEYPOINTS  =  64;
static constexpr int MAX_FLOW_KEYPOINTS =  64;
static constexpr int MAX_BAD_KEYPOINTS  = 128;
static constexpr float INITIAL_DEPTH = 1000.f;
static constexpr float RESIDUAL_KEYFRAME_THRESHOLD = 0.008f;
static constexpr float RESIDUAL_SUCCESS_THRESHOLD = 2.5 * RESIDUAL_KEYFRAME_THRESHOLD;
static constexpr float RESIDUAL_MTI_THRESHOLD = 0.024f;
static constexpr float CONVERGENCE_THRESHOLD = 0.00001f;
static constexpr float MIN_CLASSIFIER_RESIDUAL = 0.005f;

static constexpr float CLOSE_TO_ZERO = 0.0001f;
}

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

// Low-pass first-order IIR filter
// Time constant is ~ (N * T_sample), N=0 for no filter
#define LP_FILT(var, new_val, N)  \
  do { var = ((float)var * ((float)N) + ((float)new_val)) / ((float)N+1); } while(0)


#include "xipvis.h"
#include "gen_utils.h"
#include "error.h"
#include "profile.h"
#include "uint.h"
#include "unif.h"
#include "bern.h"
#include "norm.h"
#include "image.h"
#include "camera.h"
#include "patch.h"
#include "stabilize.h"
#include "overlay.h"
#include "renderer.h"
#include "patch.h"
#include "motion.h"
#include "log.h"
#endif /* #ifndef CONFIG_H */
