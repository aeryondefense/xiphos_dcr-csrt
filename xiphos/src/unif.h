/* Copyright (C) 2015 Xiphos Systems Corporation
 */

#ifndef UNIF_H
#define UNIF_H

namespace xsc
{
namespace samplers
{
class unif
{
    xsc::samplers::uint &u_;

public:
    unif(xsc::samplers::uint &u);

    unif(const xsc::samplers::uint &) = delete;
    xsc::samplers::unif &operator = (const xsc::samplers::uint &) = delete;

    float operator () (void);
    float operator () (float a, float b);
};
}
}

inline xsc::samplers::unif::unif(xsc::samplers::uint &u) : u_(u)
{
}

inline float xsc::samplers::unif::operator () (void)
{
    float s;

    *((uint32_t *)&s) = (u_() >> 9) | 0x3F800000;
    
    return s - 1;
}

inline float xsc::samplers::unif::operator () (float a, float b)
{
    return (*this)() * (b - a) + a;
}

#endif /* #ifndef UNIF_H */
