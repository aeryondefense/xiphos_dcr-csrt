/* Copyright (C) 2016 Xiphos Systems Corporation
 */

#include "config.h"

xsc::renderer::renderer(void)
{
    w = 0;
    h = 0;
    s = 0;
    ptr = nullptr;
}

xsc::renderer::~renderer(void)
{
}

int xsc::init(xsc::renderer *r, uint8_t *buffer, int width, int height, int stride)
{
    r->w = width;
    r->h = height;
    r->s = stride;
    r->ptr = buffer;

    return 0;
}

int xsc::free(xsc::renderer *r)
{
    r->w = 0;
    r->h = 0;
    r->s = 0;
    r->ptr = nullptr;

    return 0;
}

int xsc::circ_yuv420(xsc::renderer *r, int x, int y, int rad, uint8_t val)
{
    if (x - rad < 0 || r->w <= x + rad || y - rad < 0 || r->h <= y + rad)
    {
        return -1;
    }

    int f;
    int i;
    int j;
    int dx;
    int dy;

    r->ptr[(x + rad) + y * r->s] = val;
    r->ptr[(x - rad) + y * r->s] = val;
    r->ptr[x + (y + rad) * r->s] = val;
    r->ptr[x + (y - rad) * r->s] = val;

    f = 1 - rad;
    i = 0;
    j = rad;
    dx = 1;
    dy = -2 * rad;

    while (i < j)
    {
        if (0 <= f)
        {
            j--;
            dy += 2;
            f  += dy;
        }

        i++;
        dx += 2;
        f  += dx;
        
        r->ptr[(x + i) + (y + j) * r->s] = val;
        r->ptr[(x - i) + (y + j) * r->s] = val;
        r->ptr[(x + i) + (y - j) * r->s] = val;
        r->ptr[(x - i) + (y - j) * r->s] = val;
        r->ptr[(x + j) + (y + i) * r->s] = val;
        r->ptr[(x - j) + (y + i) * r->s] = val;
        r->ptr[(x + j) + (y - i) * r->s] = val;
        r->ptr[(x - j) + (y - i) * r->s] = val;
    }

    return 0;
}

int xsc::circ_yuy422(xsc::renderer *r, int x, int y, int rad, uint8_t val)
{
    if (x - rad < 0 || r->w <= x + rad || y - rad < 0 || r->h <= y + rad)
    {
        return -1;
    }

    int f;
    int i;
    int j;
    int dx;
    int dy;

    r->ptr[(x + rad) * 2 + y * r->s] = val;
    r->ptr[(x - rad) * 2 + y * r->s] = val;
    r->ptr[x * 2 + (y + rad) * r->s] = val;
    r->ptr[x * 2 + (y - rad) * r->s] = val;

    f = 1 - rad;
    i = 0;
    j = rad;
    dx = 1;
    dy = -2 * rad;

    while (i < j)
    {
        if (0 <= f)
        {
            j--;
            dy += 2;
            f  += dy;
        }

        i++;
        dx += 2;
        f  += dx;
        
        r->ptr[(x + i) * 2 + (y + j) * r->s] = val;
        r->ptr[(x - i) * 2 + (y + j) * r->s] = val;
        r->ptr[(x + i) * 2 + (y - j) * r->s] = val;
        r->ptr[(x - i) * 2 + (y - j) * r->s] = val;
        r->ptr[(x + j) * 2 + (y + i) * r->s] = val;
        r->ptr[(x - j) * 2 + (y + i) * r->s] = val;
        r->ptr[(x + j) * 2 + (y - i) * r->s] = val;
        r->ptr[(x - j) * 2 + (y - i) * r->s] = val;
    }

    return 0;
}

int xsc::line_yuv420(xsc::renderer *r, int x0, int y0, int x1, int y1, uint8_t *vals)
{
    if (x0 < 0 || r->w <= x0 || x1 < 0 || r->w <= x1)
    {
        return -1;
    }

    if (y0 < 0 || r->h <= y0 || y1 < 0 || r->h <= y1)
    {
        return -1;
    }

    int dx;
    int dy;
    int sx;
    int sy;
    int e1;
    int e2;
    uint t = r->h * r->w;

    if (x0 < x1)
    {
        sx = 1;
        dx = x1 - x0;
    }
    else
    {
        sx = -1;
        dx = x0 - x1;
    }

    if (y0 < y1)
    {
        sy = 1;
        dy = y1 - y0;
    }
    else
    {
        sy = -1;
        dy = y0 - y1;
    }

    e1 = dx - dy;

    while (1)
    {
        r->ptr[x0 + y0 * r->s] = vals[0];
        r->ptr[(y0 / 2) * (r->s / 2) + (x0 / 2) + t] = vals[1];
        r->ptr[(y0 / 2) * (r->s / 2) + (x0 / 2) + t + (t / 4)] = vals[2];
        
        if (x0 == x1 && y0 == y1)
        {
            break;
        }

        e2 = e1 + e1;

        if (-dy < e2)
        {
            e1 -= dy;
            x0 += sx;
        }

        if (x0 == x1 && y0 == y1)
        {
            break;
        }

        if (e2 < dx)
        {
            e1 += dx;
            y0 += sy;
        }
    }

    return 0;
}

int xsc::line_yuy422(xsc::renderer *r, int x0, int y0, int x1, int y1, uint8_t *vals)
{
    if (x0 < 0 || r->w <= x0 || x1 < 0 || r->w <= x1)
    {
        return -1;
    }

    if (y0 < 0 || r->h <= y0 || y1 < 0 || r->h <= y1)
    {
        return -1;
    }
    
    // Only luminance for now
    uint8_t val = vals[0];

    int dx;
    int dy;
    int sx;
    int sy;
    int e1;
    int e2;

    if (x0 < x1)
    {
        sx = 1;
        dx = x1 - x0;
    }
    else
    {
        sx = -1;
        dx = x0 - x1;
    }

    if (y0 < y1)
    {
        sy = 1;
        dy = y1 - y0;
    }
    else
    {
        sy = -1;
        dy = y0 - y1;
    }

    e1 = dx - dy;

    while (1)
    {
        r->ptr[x0 * 2 + y0 * r->s] = val;

        if (x0 == x1 && y0 == y1)
        {
            break;
        }

        e2 = e1 + e1;

        if (-dy < e2)
        {
            e1 -= dy;
            x0 += sx;
        }

        if (x0 == x1 && y0 == y1)
        {
            break;
        }

        if (e2 < dx)
        {
            e1 += dx;
            y0 += sy;
        }
    }

    return 0;
}

int xsc::rect_yuv420(xsc::renderer *r, int xmin, int ymin, int xmax, int ymax, int lw, const uint8_t *vals)
{
    if (xmin < 0 || ymin < 0 || r->w <= xmax || r->h <= ymax)
    {
        return -1;
    }

    int j;
    for (j = -lw/2; j <= lw/2; j++)
    {
    
        int xlo = xmin + j;
        int xhi = xmax - j;
        int ylo = ymin + j;
        int yhi = ymax - j;
        
        if (!(xlo < 0 || ylo < 0 || r->w <= xhi || r->h <= yhi))
        {
            uint t = r->h * r->w;
            uint8_t *y = r->ptr + xlo + r->s * ylo;
            uint8_t *u = r->ptr + (ylo / 2) * (r->s / 2) + (xlo / 2) + t;
            uint8_t *v = r->ptr + (ylo / 2) * (r->s / 2) + (xlo / 2) + t + (t / 4);
            int i;
            for (i = 0; i < xhi - xlo; i++, y++)
            {
                *y = vals[0];
                *u = vals[1];
                *v = vals[2];
                if (i & 1)
                {
                    u++;
                    v++;
                }
            }

            for (i = 0; i < yhi - ylo; i++, y += r->s)
            {
                *y = vals[0];
                *u = vals[1];
                *v = vals[2];
                if (i & 1)
                {
                    u += r->s / 2;
                    v += r->s / 2;
                }
            }

            for (i = 0; i < xhi - xlo; i++, y--)
            {
                *y = vals[0];
                *u = vals[1];
                *v = vals[2];
                if (i & 1)
                {
                    u--;
                    v--;
                }
            }

            for (i = 1; i <= yhi - ylo; i++, y -= r->s)
            {
                *y = vals[0];
                *u = vals[1];
                *v = vals[2];
                if (!(i & 1))
                {
                    u -= r->s / 2;
                    v -= r->s / 2;
                }
            }

        }
    }
    return 0;
}

int xsc::rect_yuy422(xsc::renderer *r, int xmin, int ymin, int xmax, int ymax, uint8_t* vals)
{
    if (xmin < 0 || ymin < 0 || r->w <= xmax || r->h <= ymax)
    {
        return -1;
    }
    
    // Only luminance for now
    uint8_t val = vals[0];    

    uint8_t *p = r->ptr + xmin * 2 + r->s * ymin;

    for (int i = 0; i < xmax - xmin; i++, p += 2)
    {
        *p = val;
    }
    
    for (int i = 0; i < ymax - ymin; i++, p += r->s)
    {
        *p = val;
    }
    
    for (int i = 0; i < xmax - xmin; i++, p -= 2)
    {
        *p = val;
    }
    
    for (int i = 1; i <= ymax - ymin; i++, p -= r->s)
    {
        *p = val;
    }

    return 0;
}

int xsc::cross_yuv420(xsc::renderer *r, int x, int y, int radius, int lw, const uint8_t *vals)
{
    if (x - radius < 0 || y - radius < 0 || radius <= 0 || r->h <= radius + y || r->w <= radius + x )
    {
        return -1;
    }
    
    uint8_t *l;
    uint8_t *u;
    uint8_t *v;
    
    int x0;
    int y0;
    
    uint t = r->h * r->w;

    int j;    
    for (j = -lw/2; j <= lw/2; j++)
    {
        x0 = x - radius;
        y0 = y + j;
        int i;
        
        if (y0 > 0 && y < r->h)
        {

            l = r->ptr + x0 + r->s * y0;
            u = r->ptr + (y0 / 2) * (r->s / 2) + (x0 / 2) + t;
            v = r->ptr + (y0 / 2) * (r->s / 2) + (x0 / 2) + t + (t / 4);
            for (i = 0; i < radius * 2; i++, l++)
            {
                *l = vals[0];
                *u = vals[1];
                *v = vals[2];
                if (i & 1)
                {
                    u++;
                    v++;
                }
            }
        }
    
        x0 = x + j;
        y0 = y - radius;

        if (x0 > 0 && x < r->w)
        {
            l = r->ptr + x0 + r->s * y0;
            u = r->ptr + (y0 / 2) * (r->s / 2) + (x0 / 2) + t;
            v = r->ptr + (y0 / 2) * (r->s / 2) + (x0 / 2) + t + (t / 4);

            for (i = 0; i < radius * 2; i++, l += r->s)
            {
                *l = vals[0];
                *u = vals[1];
                *v = vals[2];
                if (i & 1)
                {
                    u += r->s / 2;
                    v += r->s / 2;
                }
            }
        }
    }
    
    return 0;
}

int xsc::cross_yuy422(xsc::renderer *r, int x, int y, int radius, uint8_t* vals)
{
    if (x - radius < 0 || y - radius < 0 || radius <= 0 || r->h <= radius + y || r->w <= radius + x )
    {
        return -1;
    }

    // Only luminance for now
    uint8_t val = vals[0];    
    
    int x0;
    int y0;
    uint8_t *p;
    
    x0 = x - radius;
    y0 = y;
    p = r->ptr + x0 * 2 + r->s * y0;

    for (int i = 0; i < radius * 2; i++, p += 2)
    {
        *p = val;
    }
    
    x0 = x;
    y0 = y - radius;
    p = r->ptr + x0 * 2 + r->s * y0;

    for (int i = 0; i < radius * 2; i++, p += r->s)
    {
        *p = val;
    }

    return 0;
}
