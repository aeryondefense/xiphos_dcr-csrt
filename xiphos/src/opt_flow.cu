#include "config.h"
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>

namespace xsc
{
namespace stabilize
{
static constexpr int SMALL_PYRLK_RADIUS = 4;
static constexpr int LARGE_PYRLK_RADIUS = 8;
static constexpr int MAX_ITERATIONS     = 5;
}
}

// Reduction by summing all of the elements of fdata1, fdata2, fdata3.
// Overwrites the values in fdata.
// Maximum size of 512 elements.
template <unsigned int block_size>
__device__ __forceinline__ void reduce_sum3(float* fdata1, float* fdata2, float* fdata3)
{
    const unsigned int tid = threadIdx.y * blockDim.x + threadIdx.x;
    
    __syncthreads();
    
    if (block_size >= 512)
    {
        if (tid < 256)
        {
            fdata1[tid] += fdata1[tid+256];
            fdata2[tid] += fdata2[tid+256];
            fdata3[tid] += fdata3[tid+256];
        }
        
        __syncthreads();
    }
    
    if (block_size >= 256)
    {
        if (tid < 128)
        {
            fdata1[tid] += fdata1[tid+128];
            fdata2[tid] += fdata2[tid+128];
            fdata3[tid] += fdata3[tid+128];
        }
        
        __syncthreads();
    }
    
    if (block_size >= 128)
    {
        if (tid < 64)
        {
            fdata1[tid] += fdata1[tid+64];
            fdata2[tid] += fdata2[tid+64];
            fdata3[tid] += fdata3[tid+64];
        }
        
        __syncthreads();
    }
    
    if (tid < 32)
    {
        if (block_size >= 64)
        {
            fdata1[tid] += fdata1[tid + 32];
            fdata2[tid] += fdata2[tid + 32];
            fdata3[tid] += fdata3[tid + 32];
        }    
        if (block_size >= 32)
        {
            fdata1[tid] += fdata1[tid + 16];
            fdata2[tid] += fdata2[tid + 16];
            fdata3[tid] += fdata3[tid + 16];
        }   
        if (block_size >= 16)
        {
            fdata1[tid] += fdata1[tid + 8];
            fdata2[tid] += fdata2[tid + 8];
            fdata3[tid] += fdata3[tid + 8];
        }   
        if (block_size >= 8)
        {
            fdata1[tid] += fdata1[tid + 4];
            fdata2[tid] += fdata2[tid + 4];
            fdata3[tid] += fdata3[tid + 4];
        }   
        if (block_size >= 4)
        {
            fdata1[tid] += fdata1[tid + 2];
            fdata2[tid] += fdata2[tid + 2];
            fdata3[tid] += fdata3[tid + 2];
        }   
        if (block_size >= 2)
        {
            fdata1[tid] += fdata1[tid + 1];
            fdata2[tid] += fdata2[tid + 1];
            fdata3[tid] += fdata3[tid + 1];
        }
    }
    
    __syncthreads();
}

// Reduction by summing all of the elements of fdata1, fdata2.
// Overwrites the values in fdata.
// Maximum size of 512 elements.
template <unsigned int block_size>
__device__ __forceinline__ void reduce_sum2(float* fdata1, float* fdata2)
{
    const unsigned int tid = threadIdx.y * blockDim.x + threadIdx.x;
    
    __syncthreads();
    
    if (block_size >= 512)
    {
        if (tid < 256)
        {
            fdata1[tid] += fdata1[tid+256];
            fdata2[tid] += fdata2[tid+256];
        }
        
        __syncthreads();
    }
    
    if (block_size >= 256)
    {
        if (tid < 128)
        {
            fdata1[tid] += fdata1[tid+128];
            fdata2[tid] += fdata2[tid+128];
        }
        
        __syncthreads();
    }
    
    if (block_size >= 128)
    {
        if (tid < 64)
        {
            fdata1[tid] += fdata1[tid+64];
            fdata2[tid] += fdata2[tid+64];
        }
        
        __syncthreads();
    }
    
    if (tid < 32)
    {
        if (block_size >= 64)
        {
            fdata1[tid] += fdata1[tid + 32];
            fdata2[tid] += fdata2[tid + 32];
        }    
        if (block_size >= 32)
        {
            fdata1[tid] += fdata1[tid + 16];
            fdata2[tid] += fdata2[tid + 16];
        }   
        if (block_size >= 16)
        {
            fdata1[tid] += fdata1[tid + 8];
            fdata2[tid] += fdata2[tid + 8];
        }   
        if (block_size >= 8)
        {
            fdata1[tid] += fdata1[tid + 4];
            fdata2[tid] += fdata2[tid + 4];
        }   
        if (block_size >= 4)
        {
            fdata1[tid] += fdata1[tid + 2];
            fdata2[tid] += fdata2[tid + 2];
        }   
        if (block_size >= 2)
        {
            fdata1[tid] += fdata1[tid + 1];
            fdata2[tid] += fdata2[tid + 1];
        }
    }
    
    __syncthreads();
}

// Bilinear interpolation at coordinate (x, y) with the four adjacent pixels
__device__ __forceinline__ float interp2(float* p, unsigned int ss, float x, float y)
{
    const unsigned int xi = x;
    const unsigned int yi = y;
    
    const float al  = x - (float)xi;
    const float al1 = 1.0 - al;
    const float be  = y - (float)yi;
    const float be1 = 1.0 - be;
    
    const unsigned int yiss = yi*ss;
    const float a = p[xi   + yiss];
    const float b = p[xi+1 + yiss];
    const float c = p[xi   + yiss + ss];
    const float d = p[xi+1 + yiss + ss];
    
    return be1*(al1*a + al*b) + be*(al1*c + al*d);
}

// Reference:
//  Bouguet, Jean-Yves. "Pyramidal Implementation of the Lucas Kanade Feature Tracker Description of the algorithm"
// 
// Implements a single pass at a particular pyramid level.
// Each "block" represents a single keypoint in image I, to be matched in image J
// Each "thread" represents a single pixel in the patch around the keypoint coordinates
#define PYRLK_RADIUS xsc::stabilize::LARGE_PYRLK_RADIUS
__global__ void sparse_pyramid_lk_large(float* pI, unsigned int sI, float* pJ, unsigned int sJ, 
                                        float* pIx, float* pIy,
                                        float* dJx, float* dJy, unsigned int zero,
                                        unsigned int level, unsigned int width, unsigned int height)
#include "opt_flow.cuh"
#undef PYRLK_RADIUS

#define PYRLK_RADIUS xsc::stabilize::SMALL_PYRLK_RADIUS
__global__ void sparse_pyramid_lk_small(float* pI, unsigned int sI, float* pJ, unsigned int sJ, 
                                        float* pIx, float* pIy,
                                        float* dJx, float* dJy, unsigned int zero,
                                        unsigned int level, unsigned int width, unsigned int height)
#include "opt_flow.cuh"
#undef PYRLK_RADIUS

int xsc::stabilize::optflo(float* pIx, float* pIy, unsigned int n, unsigned int small, unsigned int levels, 
                           xsc::image::level* pI, xsc::image::level* pJ,
                           float* pJx, float* pJy, float* pKx, float* pKy)
{
    const unsigned int radius = small ? xsc::stabilize::SMALL_PYRLK_RADIUS : xsc::stabilize::LARGE_PYRLK_RADIUS;
    const unsigned int window_width  = 2*radius;
    const unsigned int window_height = 2*radius;
    
    if (n == 0)
    {
      return 0;
    }
    
    // Forward pass (keypoints in image I, find match in image J)
    dim3 bd(window_width, window_height);
    dim3 gd(n, 1);
    for(int l = levels-1; l >= 0; l--)
    {
        unsigned int scale = 1 << l;
        unsigned int w = pI->w / scale;
        unsigned int h = pI->h / scale;
        
        // Sparse Pyramidal Lucas-Kanade Optical Flow
        if (small)
        {
            sparse_pyramid_lk_small<<<gd, bd>>>(  (pI+l)->i[1], (pI+l)->hs,
                                                  (pJ+l)->i[1], (pJ+l)->hs,
                                                  pIx, pIy, 
                                                  pJx, pJy, (l == (levels-1)), 
                                                  l, w, h);
        }
        else
        {
            sparse_pyramid_lk_large<<<gd, bd>>>(  (pI+l)->i[1], (pI+l)->hs,
                                                  (pJ+l)->i[1], (pJ+l)->hs,
                                                  pIx, pIy, 
                                                  pJx, pJy, (l == (levels-1)),
                                                  l, w, h);
        }
    }

    // Reverse pass (keypoints in image J, find match in image I)
    for(int l = levels-1; l >= 0; l--)
    {
        unsigned int scale = 1 << l;
        unsigned int w = pI->w / scale;
        unsigned int h = pI->h / scale;
        
        // Sparse Pyramidal Lucas-Kanade Optical Flow        
        if (small)
        {
            sparse_pyramid_lk_small<<<gd, bd>>>(  (pJ+l)->i[1], (pJ+l)->hs,
                                                  (pI+l)->i[1], (pI+l)->hs,
                                                  pJx, pJy, 
                                                  pKx, pKy, (l == (levels-1)), 
                                                  l, w, h);
        }
        else
        {
            sparse_pyramid_lk_large<<<gd, bd>>>(  (pJ+l)->i[1], (pJ+l)->hs,
                                                  (pI+l)->i[1], (pI+l)->hs,
                                                  pJx, pJy, 
                                                  pKx, pKy, (l == (levels-1)),
                                                  l, w, h);
        }
    }

    return 0;
}



