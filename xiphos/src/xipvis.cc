/* Copyright (C) 2016 Xiphos Systems Corporation
 */

#include "config.h"
#include "xiphos.h"
#include "xipvis_private.h"

#include <math.h>

static constexpr unsigned int EGOMOTION_ENABLE     = 1 << 0;
static constexpr unsigned int UDOT_ENABLE          = 1 << 1;
static constexpr unsigned int MTI_ENABLE           = 1 << 2;
static constexpr unsigned int STABILIZATION_ENABLE = 1 << 3;
static constexpr unsigned int OVERLAYS_ENABLE      = 1 << 4;

// Enable/Disable Ego-motion optimization
int xipvis_enable_egomotion(void* const x)
{
    int result = 0;
    if(!xipvis_egomotion_enabled(x))
    {
        ((xipvis*)x)->features |= EGOMOTION_ENABLE;
    }
    
    return result;
}

int xipvis_disable_egomotion(void* const x)
{
    ((xipvis*)x)->features &= ~EGOMOTION_ENABLE;
    
    xsc::camera::reset(((xipvis*)x)->cp);

    // Disable the dependent features
    xipvis_disable_mti(x);
    
    return 0;
}

unsigned int xipvis_egomotion_enabled(void* const x)
{
    return (((xipvis*)x)->features & EGOMOTION_ENABLE) > 0;
}

// Enable/Disable User-Designated Object Tracking
int xipvis_enable_udot(void* const x)
{
    ((xipvis*)x)->features |= UDOT_ENABLE;
    
    // Enable the prerequisite features
    if(!xipvis_egomotion_enabled(x))
    {
      xipvis_enable_egomotion(x);
    }
    
    return 0;
}

int xipvis_disable_udot(void* const x)
{
    ((xipvis*)x)->features &= ~UDOT_ENABLE;
    
    return 0;
}

unsigned int xipvis_udot_enabled(void* const x)
{
    return (((xipvis*)x)->features & UDOT_ENABLE) > 0;
}

// Enable/Disable Moving Target Indicators
int xipvis_enable_mti(void* const x)
{
    ((xipvis*)x)->features |= MTI_ENABLE;
    
    // Enable the prerequisite features
    if(!xipvis_egomotion_enabled(x))
    {
      xipvis_enable_egomotion(x);
    }
    
    return 0;
}

int xipvis_disable_mti(void* const x)
{
    ((xipvis*)x)->features &= ~MTI_ENABLE;
    
    return 0;
}

unsigned int xipvis_mti_enabled(void* const x)
{
    return (((xipvis*)x)->features & MTI_ENABLE) > 0;
}

// Enable/Disable Digital Image Stabilization
int xipvis_enable_stabilization(void* const x)
{
    ((xipvis*)x)->features |= STABILIZATION_ENABLE;
    
    return 0;
}

int xipvis_disable_stabilization(void* const x)
{
    ((xipvis*)x)->features &= ~STABILIZATION_ENABLE;
    
    return 0;
}

unsigned int xipvis_stabilization_enabled(void* const x)
{
    return (((xipvis*)x)->features & STABILIZATION_ENABLE) > 0;
}

// Enable/Disable Overlays
int xipvis_enable_overlays(void* const x)
{
    ((xipvis*)x)->features |= OVERLAYS_ENABLE;
        
    return 0;
}

int xipvis_disable_overlays(void* const x)
{
    ((xipvis*)x)->features &= ~OVERLAYS_ENABLE;
    
    return 0;
}

unsigned int xipvis_overlays_enabled(void* const x)
{
    return (((xipvis*)x)->features & OVERLAYS_ENABLE) > 0;
}

static void unwarp_bounding_box(xipvis_bb* bb, int width, int height, float* hk)
{
    int w = bb->xmax - bb->xmin;
    int x = (bb->xmin + bb->xmax) / 2;
    int h = bb->ymax - bb->ymin;
    int y = (bb->ymin + bb->ymax) / 2;

    // P = H P(warped)
    float s0 = hk[0] * x + hk[1] * y + hk[2];
    float s1 = hk[3] * x + hk[4] * y + hk[5];
    float s2 = hk[6] * x + hk[7] * y + hk[8];

    // apply scale
    s0 /= s2;
    s1 /= s2;
    
    // assign new location
    if (!std::isnan(s0) && !std::isnan(s1) && !std::isinf(s0) && !std::isinf(s1))
    {
        // keep box in field of view
        x = MAX(w/2, (int)s0);
        x = MIN(width - w/2, x);
        y = MAX(h/2, (int)s1);
        y = MIN(height - h/2, y);
    }
    else
    {
        XIPVIS_LOG("Un-warping bounding box resulted in invalid location.");
        return;
    }
    
    // translate only
    bb->xmin = x - w/2;
    bb->xmax = bb->xmin + w;
    bb->ymin = y - h/2;
    bb->ymax = bb->ymin + h;
}

static void warp_point(float* xy, const float* hk)
{

    float x = xy[0];
    float y = xy[1];

    // P(warped) = H(inv) P(un-warped))
    Matrix3f Hk;
    Hk(0,0) = hk[0];
    Hk(0,1) = hk[1];
    Hk(0,2) = hk[2];
    Hk(1,0) = hk[3];
    Hk(1,1) = hk[4];
    Hk(1,2) = hk[5];
    Hk(2,0) = hk[6];
    Hk(2,1) = hk[7];
    Hk(2,2) = hk[8];

    Hk = Hk.inverse().eval();

    float s0 = Hk(0,0) * x + Hk(0,1) * y + Hk(0,2);
    float s1 = Hk(1,0) * x + Hk(1,1) * y + Hk(1,2);
    float s2 = Hk(2,0) * x + Hk(2,1) * y + Hk(2,2);

    // apply scale
    s0 /= s2;
    s1 /= s2;

    // assign new location
    if (!std::isnan(s0) && !std::isnan(s1) && !std::isinf(s0) && !std::isinf(s1))
    {
        xy[0] = s0;
        xy[1] = s1;
    }
    else
    {
        XIPVIS_LOG("Warping point resulted in invalid location.");
        return;
    }
}

static void warp_bounding_box(xipvis_bb* bb, const int width, const int height, const float* hk)
{
    
    int w = bb->xmax - bb->xmin;
    int x = (bb->xmin + bb->xmax) / 2;
    int h = bb->ymax - bb->ymin;
    int y = (bb->ymin + bb->ymax) / 2;

    // P(warped) = H(inv) P(un-warped))
    Matrix3f Hk;    
    Hk(0,0) = hk[0];
    Hk(0,1) = hk[1];
    Hk(0,2) = hk[2];
    Hk(1,0) = hk[3];
    Hk(1,1) = hk[4];
    Hk(1,2) = hk[5];
    Hk(2,0) = hk[6];
    Hk(2,1) = hk[7];
    Hk(2,2) = hk[8];

    Hk = Hk.inverse().eval();

    float s0 = Hk(0,0) * x + Hk(0,1) * y + Hk(0,2);
    float s1 = Hk(1,0) * x + Hk(1,1) * y + Hk(1,2);
    float s2 = Hk(2,0) * x + Hk(2,1) * y + Hk(2,2);

    // apply scale
    s0 /= s2;
    s1 /= s2;

    // assign new location
    if (!std::isnan(s0) && !std::isnan(s1) && !std::isinf(s0) && !std::isinf(s1))
    {
        // keep box in field of view
        x = MAX(w/2, (int)s0);
        x = MIN(width - w/2, x);
        y = MAX(h/2, (int)s1);
        y = MIN(height - h/2, y);
    }
    else
    {
        XIPVIS_LOG("Warping bounding box resulted in invalid location.");
        return;
    }

    // translate only
    bb->xmin = x - w/2;
    bb->xmax = bb->xmin + w;
    bb->ymin = y - h/2;
    bb->ymax = bb->ymin + h;
}

static void warp_bounding_boxes(overlay* o, int width, int height, float* hk)
{
    int nb = o->nb;
    for (int i =0; i < nb; i ++)
    {
        int w = o->bboxes[i].xmax - o->bboxes[i].xmin;
        int x = (o->bboxes[i].xmin + o->bboxes[i].xmax) / 2;
        int h = o->bboxes[i].ymax - o->bboxes[i].ymin;
        int y = (o->bboxes[i].ymin + o->bboxes[i].ymax) / 2;

        // P(warped) = H(inv) P(un-warped))
        Matrix3f Hk;    
        Hk(0,0) = hk[0];
        Hk(0,1) = hk[1];
        Hk(0,2) = hk[2];
        Hk(1,0) = hk[3];
        Hk(1,1) = hk[4];
        Hk(1,2) = hk[5];
        Hk(2,0) = hk[6];
        Hk(2,1) = hk[7];
        Hk(2,2) = hk[8];

        Hk = Hk.inverse().eval();

        float s0 = Hk(0,0) * x + Hk(0,1) * y + Hk(0,2);
        float s1 = Hk(1,0) * x + Hk(1,1) * y + Hk(1,2);
        float s2 = Hk(2,0) * x + Hk(2,1) * y + Hk(2,2);

        // apply scale
        s0 /= s2;
        s1 /= s2;
        
        // assign new location
        if (!std::isnan(s0) && !std::isnan(s1) && !std::isinf(s0) && !std::isinf(s1))
        {
            x = (int)s0;
            y = (int)s1;
        }
        else
        {
            XIPVIS_LOG("Warping bounding box resulted in invalid location.");
            break;
        }

        // translate only
        o->bboxes[i].xmin = x - w/2;
        o->bboxes[i].xmax =  o->bboxes[i].xmin + w;
        o->bboxes[i].ymin = y - h/2;
        o->bboxes[i].ymax =  o->bboxes[i].ymin + h;
    }    
}

static void drawbb(xipvis_bb *bb, uint8_t *colour, xsc::renderer *r)
{
    
    xsc::rect_yuv420(r, bb->xmin+2, bb->ymin+2, 
                 bb->xmax+2, bb->ymax+2,
                 1, colour);

    const int radx = (bb->xmax - bb->xmin) / 4;
    const int rady = (bb->ymax - bb->ymin) / 4;

    xsc::cross_yuv420(r, (bb->xmax + bb->xmin) / 2,
                      (bb->ymax + bb->ymin) / 2, 
                      MIN(radx, rady),
                      1, colour);
}

static unsigned int draw(overlay* o, uint8_t *buf, int w, int h, int s)
{
    xsc::renderer r;

    if (xsc::init(&r, buf, w, h, s) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    for (int i = 0; i < o->nb; i++)
    {
        uint8_t* colour;
        if (i == 1)
        {
            colour = (o->bboxes[i].confidence >= 1.0) ? xsc::GREEN : 
                            (o->bboxes[i].confidence >= 0.1) ? xsc::ORANGE :
                                                                xsc::RED;
        }
        else if (i == 2)
        {

            colour = (o->bboxes[i].confidence >= 1.0) ? xsc::WHITE : 
                            (o->bboxes[i].confidence >= 0.1) ? xsc::ORANGE :
                                                                xsc::RED;
        }
        else
        {
            colour = (o->bboxes[i].confidence >= 1.0) ? xsc::INDIGO : 
                            (o->bboxes[i].confidence >= 0.1) ? xsc::ORANGE :
                                                                xsc::RED;
        }
        
        xsc::rect_yuv420(&r, o->bboxes[i].xmin, o->bboxes[i].ymin, 
                             o->bboxes[i].xmax, o->bboxes[i].ymax,
                             1, colour);

        const int radx = (o->bboxes[i].xmax - o->bboxes[i].xmin) / 4;
        const int rady = (o->bboxes[i].ymax - o->bboxes[i].ymin) / 4;
           
        xsc::cross_yuv420(&r, (o->bboxes[i].xmax + o->bboxes[i].xmin) / 2,
                              (o->bboxes[i].ymax + o->bboxes[i].ymin) / 2, 
                              MIN(radx, rady),
                              1, colour);
    }
    
    if (xsc::free(&r) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    return 0;
}

int xipvis_update_kcft(void *ctx, uint8_t *inbuf, int stride, int id, struct xipvis_bb *bb, const float* const H, const int valid_H)
{
    // fprintf(stderr, "Entered %s\n\r", __FUNCTION__);
    xipvis *x = (xipvis *)ctx;

    int n = 0;
    cv::Rect2d cv_bb;

    if (valid_H && bb)
    {
        float w =   (x->kcft_bb.xmax - x->csrt_bb.xmin);
        float h =   (x->kcft_bb.ymax - x->csrt_bb.ymin);
        float c_x = (x->kcft_bb.xmax + x->csrt_bb.xmin) / 2.f;
        float c_y = (x->kcft_bb.ymax + x->csrt_bb.ymin) / 2.f;
        

        float xpred  =  H[0]*c_x + H[1]*c_y + H[2];
        float ypred  =  H[3]*c_x + H[4]*c_y + H[5];
        float zpred  = (H[6]*c_x + H[7]*c_y + H[8]);
        c_x = xpred/zpred;
        c_y = ypred/zpred;

        float xmin = c_x - w/2.;
        float ymin = c_y - h/2.;

        cv_bb.x = xmin;
        cv_bb.y = ymin;
        cv_bb.width = w;
        cv_bb.height = h;
    }

    int h = x->cam.height;
    h -= h % 3;
    int w = x->cam.width;

    // fprintf(stderr, "Img id=%d, %dx%d  ptr=0x%08x\r\n", id, w, h, (void *)inbuf);
    clock_t a = clock(); \
    cv::Mat img_yuv(h + h/2, w, CV_8U, inbuf);
    cv::Mat img_bgr(h, w, CV_8UC3);
    cv::cvtColor(img_yuv, img_bgr, CV_YUV2BGR_I420, 3);
    clock_t b = clock(); \
    float d = (b - a) * 1000.f / CLOCKS_PER_SEC; \
    // fprintf(stderr, "%s, %d: completed in %f milliseconds\n", __FILE__, __LINE__, d); \

// fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    PROFILE_TRK(cv_bb  = x->kcft->update(img_bgr));
// fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);

    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    /*
    cv::Mat cv_img(x->cam.height, x->cam.width, CV_8UC1, inbuf);
    cv::Mat img_bgr;

    cv::Mat channels[3];
    channels[0] = cv_img;
    channels[1] = cv_img;
    channels[2] = cv_img;
    
    cv::merge(channels, 3, img_bgr);

    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    bool found  = x->kcft->update(img_bgr, cv_bb);
    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    */

    if (bb) {
        bb->xmin = cv_bb.x;
        bb->ymin = cv_bb.y;
        bb->xmax = cv_bb.x + cv_bb.width;
        bb->ymax = cv_bb.y + cv_bb.height;
        bb->x_unwarped = (bb->xmin + bb->xmax) / 2.0f;
        bb->y_unwarped = (bb->ymin + bb->ymax) / 2.0f;
        // fprintf(stderr, "%s: bb= %3dx%3d   w=%d   h=%d\n\r",
        //     __FUNCTION__, bb->xmin, bb->ymin, bb->xmax - bb->xmin, bb->ymax - bb->ymin);
        x->kcft_bb = *bb;
    }
    else {
        fprintf(stderr, "%s: No output bb provided\n\r", __FUNCTION__);
    }
    
    return 0;
}
    
static FILE* csrt_log = nullptr;
// Update CSR Tracker
int xipvis_update_csrt(void *ctx, uint8_t *inbuf, int stride, int id, struct xipvis_bb *bb, const float* const H, const int valid_H)
{
    fprintf(stderr, "Entered %s\n\r", __FUNCTION__);
    xipvis *x = (xipvis *)ctx;

    int n = 0;
    cv::Rect2d cv_bb;

    float reg_diff_x = 0.0f, reg_diff_y = 0.0f;

    float c_x_pred, c_y_pred;
    std::cerr << __FUNCTION__ << " HERE: " << n++ << "\n";
    x->csrt_kf.get_state_positions(c_x_pred, c_y_pred);
    std::cerr << __FUNCTION__ << " HERE: " << n++ << "\n";
    x->csrt_kf.predict(x->image_id);
    std::cerr << __FUNCTION__ << " HERE: " << n++ << "\n";
    x->csrt_kf.get_state_positions(c_x_pred, c_y_pred);
    std::cerr << __FUNCTION__ << " HERE: " << n++ << "\n";

    float w = (x->csrt_bb.xmax - x->csrt_bb.xmin);
    float h = (x->csrt_bb.ymax - x->csrt_bb.ymin);
    float c_x = (x->csrt_bb.xmin + x->csrt_bb.xmax) / 2.f;
    float c_y = (x->csrt_bb.ymin + x->csrt_bb.ymax) / 2.f;

    float c_x_new = c_x;
    float c_y_new = c_y;

    if (valid_H)
    {
        float xwarp  =  H[0]*c_x + H[1]*c_y + H[2];
        float ywarp  =  H[3]*c_x + H[4]*c_y + H[5];
        float zwarp  = (H[6]*c_x + H[7]*c_y + H[8]);
        c_x_new = xwarp/zwarp;
        c_y_new = ywarp/zwarp;

        reg_diff_x = c_x_new - c_x;
        reg_diff_y = c_y_new - c_y;
        fprintf(stderr, "prev: c_x=%6.3f c_y=%6.3f new: c_x=%6.3f c_y=%6.3f  diff: x=%5.3f y=%5.3f\n", c_x, c_y, c_x_new, c_y_new, reg_diff_x, reg_diff_y);
        float cam_vel_x = reg_diff_x * x->cam.fps;;
        float cam_vel_y = reg_diff_y * x->cam.fps;

        x->csrt_kf.update_camera_vel(x->image_id, cam_vel_x, cam_vel_y);
    }
    else
    {
        reg_diff_x = 0.0f;
        reg_diff_y = 0.0f;
    }

    x->csrt_kf.get_state_positions(c_x_pred, c_y_pred);
    fprintf(stderr, "csrt_kf.getStatePositions: x=%6.3f y=%6.3f\n", c_x_pred, c_y_pred);


    float xmin = c_x_new - w/2.;
    float ymin = c_y_new - h/2.;

    cv_bb.x = xmin;
    cv_bb.y = ymin;
    cv_bb.width = w;
    cv_bb.height = h;

    int img_h = x->cam.height;
    img_h -= img_h % 3;
    int img_w = x->cam.width;

    // fprintf(stderr, "Img id=%d, %dx%d  ptr=0x%08x\r\n", id, w, h, (void *)inbuf);
    clock_t a = clock();
    cv::Mat img_yuv(img_h + img_h/2, img_w, CV_8U, inbuf);
    cv::Mat img_bgr(img_h, img_w, CV_8UC3);
    cv::cvtColor(img_yuv, img_bgr, CV_YUV2BGR_I420, 3);
    clock_t b = clock(); \
    float d = (b - a) * 1000.f / CLOCKS_PER_SEC; \
    // fprintf(stderr, "%s, %d: completed in %f milliseconds\n", __FILE__, __LINE__, d); \

// fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
float match_quality = -1.f;
bool found = false;
    PROFILE_TRK(found  = x->csrt->update(img_bgr, cv_bb, valid_H, match_quality));
    fprintf(stderr, "%s: %6d,%6.2f,%6.2f,%6.2f,%6.2f,%2.5f\n", __FUNCTION__, id, reg_diff_x, reg_diff_y, cv_bb.x, cv_bb.y, match_quality);
    if (csrt_log)
    {
        fprintf(csrt_log, "%d,%.4f,%.4f,%.4f,%.4f,%.6f\n", id, reg_diff_x, reg_diff_y, cv_bb.x, cv_bb.y, match_quality);
        fflush(csrt_log);
    }
    x->csrt_kf.get_state_positions(c_x_pred, c_y_pred);
    // {
    //     fprintf(stderr, "%s: bb pred error: %4.2f x %4.2f\n\r", __FUNCTION__, fabsf(cv_bb.x - cv_bb_pred.x), fabsf(cv_bb.y - cv_bb_pred.y));
    // }
// fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);

    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    /*
    cv::Mat cv_img(x->cam.height, x->cam.width, CV_8UC1, inbuf);
    cv::Mat img_bgr;

    cv::Mat channels[3];
    channels[0] = cv_img;
    channels[1] = cv_img;
    channels[2] = cv_img;
    
    cv::merge(channels, 3, img_bgr);

    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    bool found  = x->csrt->update(img_bgr, cv_bb);
    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    */

    c_x = cv_bb.x + cv_bb.width/2.f;
    c_y = cv_bb.y + cv_bb.height/2.f;

    x->csrt_kf.update_target_pos(x->image_id, c_x, c_y);

    if (bb) {
        bb->xmin = cv_bb.x;
        bb->ymin = cv_bb.y;
        bb->xmax = cv_bb.x + cv_bb.width;
        bb->ymax = cv_bb.y + cv_bb.height;
        bb->x_unwarped = (bb->xmin + bb->xmax) / 2.f;
        bb->y_unwarped = (bb->ymin + bb->ymax) / 2.f;
        // fprintf(stderr, "%s: bb= %3dx%3d   w=%d   h=%d\n\r",
        //     __FUNCTION__, bb->xmin, bb->ymin, bb->xmax - bb->xmin, bb->ymax - bb->ymin);

        x->csrt_bb = *bb;
    }
    else {
        fprintf(stderr, "%s: No output bb provided\n\r", __FUNCTION__);
    }
    
    return found;
}
    
extern "C"
{
int xipvis_init(void **ctx, const struct xipvis_cam *cam, const int index, const int pool_size)
{
    xipvis *x;
    
    if ((x = new(std::nothrow) xipvis) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    memcpy(&(x->cam), cam, sizeof(xipvis_cam));
    
    x->num_image_bins  = xsc::NUM_IMAGE_BINS;
    x->total_keypoints = xsc::TOTAL_KEYPOINTS;

    // Reduce the number of bins and keypoints for low input resolution
    if (cam->width < 1024)
    {
        x->num_image_bins = 16;
        x->total_keypoints = 1024;
    }

    x->total_image_bins = x->num_image_bins * x->num_image_bins;
    x->max_keypoints    = x->total_keypoints / x->total_image_bins;
    x->target_keypoints = 3 * x->max_keypoints / 4;
    x->avg_threshold = xsc::KEYPOINT_THRESHOLD_DEFAULT;

    if (xsc::mti::init<MOTION_LEVELS>(&(x->motion), cam->width, cam->height, x->total_keypoints) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    x->pool_size = pool_size;
    if ((x->ip = new(std::nothrow) xsc::image::pyramid<IMAGE_LEVELS>[x->pool_size]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    for (int i = 0; i < x->pool_size; i++)
    {
        if (xsc::image::init<IMAGE_LEVELS,CAMERA_LEVELS>(x->ip[i], cam->width, cam->height, x->total_image_bins, x->total_keypoints, xsc::MAX_FLOW_KEYPOINTS) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }

    if (xsc::camera::init<CAMERA_LEVELS>(x->cp, cam->width, cam->height, cam->sensor, x->total_keypoints) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    if (xsc::udot::init(&(x->udot)) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    cv::TrackerCSRT::Params csrt_params;
    csrt_params.use_channel_weights = true;
    csrt_params.use_segmentation = true;
    csrt_params.use_hog = true;
    csrt_params.use_color_names = true;
    csrt_params.use_gray = true;
    csrt_params.use_rgb = false;
    csrt_params.padding = 3.0f;

    x->csrt = cv::TrackerCSRT::create(csrt_params);
    if (!x->csrt)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    bool kcf_use_hog = true;
    bool kcf_use_fixed_window = true;
    bool kcf_use_multiscale = true;
    bool kcf_use_lab = true;

    x->kcft = new KCFTracker(kcf_use_hog, kcf_use_fixed_window, kcf_use_multiscale, kcf_use_lab);
    if (!x->kcft)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    // Initialize the memory for the adaptive thresholds and egomotion solution on host
    if ((x->thresholds[0] = new(std::nothrow) float[2 * x->total_image_bins + 12]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    for (int i = 0; i < x->total_image_bins; i++)
    {
        x->thresholds[0][i] = xsc::KEYPOINT_THRESHOLD_DEFAULT;
        x->thresholds[0][i + x->total_image_bins] = sqrt(x->thresholds[0][i]);
    }
    x->egomotion[0] = &(x->thresholds[0][2 * x->total_image_bins]);

    // Initialize the memory on the device
    if (xsc::init_gpu(x, cam->width, cam->height, cam->format) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    x->index = index;

    if (xsc::image::init_texture(x->d_luma, x->d_cra, x->d_crb, x->index, cam->width, cam->height, cam->format) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if (xsc::stabilize::init(&(x->stabilizer), cam->width, cam->height, cam->flen / cam->pixel_pitch, cam->centre_x, cam->centre_y, cam->fps) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    xipvis_disable_egomotion(x);
    xipvis_disable_stabilization(x);
    xipvis_disable_mti(x);
    xipvis_disable_udot(x);
    xipvis_disable_overlays(x);
    
    x->overlay.bbox_id = 1;

    x->image_id = 0;
    x->linear_velocity  = Vector3f(0.f, 0.f, 0.f);
    x->angular_velocity = Vector3f(0.f, 0.f, 0.f);
    x->velocity_filter  = xsc::VELOCITY_FILTER_DEFAULT;
    x->adaptive_gain    = xsc::ADAPTIVE_GAIN * x->total_image_bins;
    x->motion_delay     = xsc::MOTION_DELAY;
    
    const float fps_factor   = cam->fps / 24.f;

    // Adapt the keypoint thresholds faster
    x->adaptive_gain /= fps_factor;

    // Decrease the motion delay for trying to zoom out
    x->motion_delay *= fps_factor;
    
    *ctx = x;

    return 0;
}

int xipvis_free(void *ctx)
{
    xipvis *x = (xipvis *)ctx;


    if (xsc::stabilize::free(&(x->stabilizer)) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if (xsc::udot::free(&x->udot) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    if (xsc::mti::free<MOTION_LEVELS>(&x->motion) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if (xsc::camera::free<CAMERA_LEVELS>(x->cp) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    for (int i = 0; i < x->pool_size; i++)
    {
        if (xsc::image::free<IMAGE_LEVELS>(x->ip[i]) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }
    delete [] x->ip;
    x->ip = nullptr;


    x->csrt.release();

    if (x->kcft)
    {
        delete x->kcft;
        x->kcft = nullptr;
    }
    
    // Free the memory for adaptive thresholds on host and device
    delete [] x->thresholds[0];
    x->thresholds[0] = nullptr;
    x->egomotion[0] = nullptr;

    if (xsc::image::free_texture(0) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    if (xsc::free_gpu(x) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    delete x;

    return 0;
}

int xipvis_update(void *ctx, int dbb, uint8_t *inbuf, int stride, int *id, int *num, struct xipvis_bb **bbs, uint8_t *outbuf, int udotflag)
{
    double start = seconds();
    double last  = start;
    int step = 0;
    
    xipvis *x = (xipvis *)ctx;
    xsc::stabilize::stabilizer* st = &(x->stabilizer);
    int rc;
    
    xsc::image::level *curr_im = x->ip[(x->image_id)                    % x->pool_size];
    xsc::image::level *prev_im = x->ip[(x->image_id + x->pool_size - 1) % x->pool_size];
    
    const unsigned int egomotion_enabled     = xipvis_egomotion_enabled(ctx);
    const unsigned int stabilization_enabled = xipvis_stabilization_enabled(ctx);
    const unsigned int mti_enabled           = xipvis_mti_enabled(ctx);
    const unsigned int udot_enabled          = xipvis_udot_enabled(ctx);
    if (!egomotion_enabled)
    {
        // Just convert image colorspace to I420 (if needed) and copy back to host
        switch (x->cam.format)
        {
            case YUY2:
            {
                PROFILE(rc = xsc::image::upload_yuy422(curr_im, x->pb, inbuf, stride));
                PROFILE(rc = xsc::image::convert_yuy422(curr_im, x->px, x->pb));
                PROFILE(rc = xsc::image::download_yuy422(curr_im, outbuf, x->px));
                break;
            }
            case UYVY:
            {
                PROFILE(rc = xsc::image::upload_uyvy(curr_im, x->pb, inbuf, stride));
                PROFILE(rc = xsc::image::convert_uyvy(curr_im, x->px, x->pb));
                PROFILE(rc = xsc::image::download_uyvy(curr_im, outbuf, x->px));
                break;
            }
            default:
            {
                PROFILE(rc = xsc::image::copy_yuv420(curr_im, outbuf, inbuf, stride));
            }
        }
        PROF(start, last, step,  x->cam.sensor, 5.0, "conversion");
        return 1;
    }

    // Upload incoming image to CUDA device, create intensity image, and generate keypoints
    switch (x->cam.format)
    {
        case YUY2:
        {
            PROFILE(rc = xsc::image::upload_yuy422(curr_im, x->px, inbuf, stride));
            rc = xsc::image::update_yuy422( curr_im, 
                                            x->px);
            break;
        }
        case UYVY:
        {
            PROFILE(rc = xsc::image::upload_uyvy(curr_im, x->px, inbuf, stride));
            rc = xsc::image::update_uyvy_bilinear(  curr_im, 
                                                    x->d_luma,
                                                    x->d_cra,
                                                    x->d_crb,
                                                    x->px,
                                                    x->index);
            break;
        }
        default:
        {
            PROFILE(rc = xsc::image::upload_yuv420(curr_im, x->px, inbuf, stride));
            rc = xsc::image::update_yuv420_bilinear(curr_im,
                                                    x->d_luma,
                                                    x->d_cra,
                                                    x->d_crb,
                                                    x->px,
                                                    x->index);
            break;
        }
    }
    if (rc < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    PROF(start, last, step,  x->cam.sensor, 20.0, "upload, gray, downsample");
    
    
    // Extract the keypoints from the current image
    rc = xsc::image::keypoints<IMAGE_LEVELS,CAMERA_LEVELS>( curr_im,
                                                            x->thresholds[1],
                                                            x->num_image_bins,
                                                            x->total_keypoints,
                                                            x->max_keypoints);
    if (rc < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    PROF(start, last,step,  x->cam.sensor, 20.0, "keypoint detect");


    /// Optical Flow
    if (x->image_id > 0)
    {
        // Run the optical flow passes on the images
        rc = xsc::stabilize::update_optflo( st,
                                            xsc::MAX_FLOW_KEYPOINTS,
                                            FALSE, // (x->cam.sensor == IR),
                                            IMAGE_LEVELS - 1,
                                            &(curr_im[1]), 
                                            &(prev_im[1]));
        if (rc < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }
    PROF(start, last, step,  x->cam.sensor, 20.0, "optical flow");


    // Copy back stabilization matches from GPU
    rc = xsc::image::copyback_matches(  &(curr_im[1]), 
                                        CAMERA_LEVELS, 
                                        x->total_image_bins, 
                                        x->total_keypoints, 
                                        x->max_keypoints);
    if (rc < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    PROF(start, last, step,  x->cam.sensor, 20.0, "copyback optical flow matches");
    
    
    // Find the homography between current and previous image
    if (x->image_id > 0)
    {
        // Fit the image transformation to the matches
        rc = xsc::stabilize::homography_from_optflo(st,
                                                    xsc::MAX_FLOW_KEYPOINTS,
                                                    (curr_im[1]).w,
                                                    (curr_im[1]).h,
                                                    &(x->cp[1].Hp));
        if (rc < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }
    else
    {
        x->cp[1].Hp = SO3f(0.f, 0.f, 0.f).Adj();
    }
    PROF(start, last, step,  x->cam.sensor, 20.0, "homography fit");
    
    // Find the cumulative homography    
    Matrix3f H = x->cp[1].Hp * x->cp[1].Hk;
    Matrix3f T = H.inverse();
    
    // Send the ego-motion solution and thresholds to the GPU
    x->egomotion[0][0] = T(0,0); x->egomotion[0][1] = T(0,1); x->egomotion[0][2]  = T(0,2);
    x->egomotion[0][4] = T(1,0); x->egomotion[0][5] = T(1,1); x->egomotion[0][6]  = T(1,2);
    x->egomotion[0][8] = T(2,0); x->egomotion[0][9] = T(2,1); x->egomotion[0][10] = T(2,2);
    x->egomotion[0][3]  = 0.f;
    x->egomotion[0][7]  = 0.f;
    x->egomotion[0][11] = 0.f;
    
    PROFILE(rc = xsc::update_gpu(x));
    if (rc < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    PROF(start, last, step,  x->cam.sensor, 1.0, "threshold and egomotion upload.");
    
    
    // Check the residual at each camera level for cumulative homography estimate
    float residual = 0;
    
    // Set the homograpy for each level
    const float scale = 2.0;
    Matrix3f H1, H2;
    H1 << 1.f/scale, 0, 0, 0, 1.f/scale, 0, 0, 0, 1.f;
    H2 <<     scale, 0, 0, 0,     scale, 0, 0, 0, 1.f;
    for (int i = 1; i < CAMERA_LEVELS; i++)
    {
        x->cp[i].Hk = H;
        H = H1 * H * H2;
    }
    
    PROFILE(rc = xsc::camera::update<CAMERA_LEVELS>(x->cp, 
                                                    x->image_id, 
                                                    curr_im, 
                                                    inbuf, 
                                                    stride, 
                                                    x->cam.format, 
                                                    &residual, 
                                                    x->thresholds[0], 
                                                    x->avg_threshold, 
                                                    x->max_keypoints,
                                                    mti_enabled || udot_enabled));
    if (rc < 0)
    {
        // UDOT can track without camera motion updates as long as
        // it has been initialized at least once.
        if (x->image_id == 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }
    PROF(start, last, step,  x->cam.sensor, 20.0, "egomotion optimization");


    // Only classify keypoints if MTI or UDOT are enabled
    if (mti_enabled || udot_enabled)
    {
        // Classify the keypoints as bad based on ego-motion estimate
        const float factor = 1.f / MAX(x->avg_threshold, xsc::CLOSE_TO_ZERO);
        rc = xsc::camera::classify<CAMERA_LEVELS>(  x->cp,
                                                    curr_im,
                                                    &(x->thresholds[1][x->total_image_bins]),
                                                    factor,
                                                    x->egomotion[1],
                                                    x->total_keypoints);
        if (rc < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
        PROF(start, last, step,  x->cam.sensor, 1.0, "classify keypoints");
        
        // Copy back bad keypoints for MTI from GPU
        rc = xsc::image::copyback_keypoints(&(curr_im[1]), 
                                            CAMERA_LEVELS, 
                                            x->num_image_bins, 
                                            x->total_keypoints, 
                                            x->max_keypoints);
        if (rc < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
        PROF(start, last, step,  x->cam.sensor, 20.0, "copyback bad keypoints");
    }
    
    
    // Scale the camera homography from the second pyramid level
    H = H2 * x->cp[1].Hp * H1;

    int dokeyframe = (residual > xsc::RESIDUAL_KEYFRAME_THRESHOLD)
                    || (!mti_enabled)
                    || (x->image_id % x->pool_size == 0);
    
    xsc::stabilize::stabilize(st, &H, residual, dokeyframe, x->motion_delay);
    PROF(start, last, step,  x->cam.sensor, 2.0, "homography filter");
    
    // Assign the homographies to the base image
    xsc::stabilize::assign_homographies(st, curr_im);
    PROF(start, last, step,  x->cam.sensor, 2.0, "upload homography");
    
    uint8_t* src_gpu = x->pb;
    if (stabilization_enabled)
    {
        // Warp image with given Homography in CUDA.  Warp and convert.
        switch (x->cam.format)
        {
            case YUY2:
            {
                PROFILE(rc = xsc::image::convert_yuy422(curr_im, x->pb, x->px));
                PROFILE(rc = xsc::image::warp_yuy422(curr_im, x->px, x->pb, xsc::stabilize::get_homography_address(st)));
                src_gpu = x->px;
                break;
            }
            case UYVY:
            {
                PROFILE(rc = xsc::image::warp_uyvy_bilinear(curr_im, x->pb, x->index, xsc::stabilize::get_split_screen(st), xsc::stabilize::get_homography_address(st)));
                break;
            }
            default:
                PROFILE(rc = xsc::image::warp_yuv420_bilinear(curr_im, x->pb, x->index, xsc::stabilize::get_split_screen(st), xsc::stabilize::get_homography_address(st)));
                break;
        }
        if (rc < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }
    else
    {
        // Just convert the image if needed.
        switch (x->cam.format)
        {
            case YUY2:
            {
                src_gpu = x->px;
                break;
            }
            case UYVY:
            {
                PROFILE(rc = xsc::image::convert_uyvy_bilinear(curr_im, x->pb, x->index));
                break;
            }
            default:
                break;
        }
    }
    PROF(start, last, step,  x->cam.sensor, 2.0, "image warp");

    x->overlay.nb = 0;

    // If MTI is enabled, collect all the MTI samples into boxes
    if (mti_enabled)
    {
        if (rc >= 0)
        {
            // Update the MTI motion sample particle filter
            PROFILE(rc = xsc::mti::update<MOTION_LEVELS>(&x->motion, curr_im));
            if (rc < 0)
            {
                XIPVIS_ERROR(errno, , -1);
            }

            // Cluster all of the common motion samples across the image
            PROFILE(rc = xsc::mti::cluster<MOTION_LEVELS>(&x->motion));
            if (rc < 0)
            {
                XIPVIS_ERROR(errno, , -1);
            }

            // For each of the resulting super-boxes
            for (int n = 0; n < x->motion.nb; n++)
            {
                // If the boxes are too small, ignore them.
                if (x->motion.boxes[n].xmax - x->motion.boxes[n].xmin < (int)(x->motion.min_box_size * x->motion.w + 0.5)
                 && x->motion.boxes[n].ymax - x->motion.boxes[n].ymin < (int)(x->motion.min_box_size * x->motion.w + 0.5))
                {
                    continue;
                }

                // Otherwise, take the large super-boxes for output
                x->overlay.bboxes[n].xmin = (int)(x->motion.boxes[n].xmin) << (MOTION_LEVELS - 1);
                x->overlay.bboxes[n].ymin = (int)(x->motion.boxes[n].ymin) << (MOTION_LEVELS - 1);
                x->overlay.bboxes[n].xmax = (int)(x->motion.boxes[n].xmax) << (MOTION_LEVELS - 1);
                x->overlay.bboxes[n].ymax = (int)(x->motion.boxes[n].ymax) << (MOTION_LEVELS - 1);
                x->overlay.bboxes[n].x_unwarped = (x->overlay.bboxes[n].xmin + x->overlay.bboxes[n].xmax) / 2;
                x->overlay.bboxes[n].y_unwarped = (x->overlay.bboxes[n].ymin + x->overlay.bboxes[n].ymax) / 2;        
                x->overlay.bboxes[n].id = x->overlay.bbox_id++;
                x->overlay.bboxes[n].image_id = x->image_id;
                x->overlay.bboxes[n].confidence = 1.f;

                x->overlay.nb++;

                // If we have our maximum number of tracks, stop accumulating more
                if (MAX_TRACKS - 2 < x->overlay.nb)
                {
                    break;
                }
            }
            PROF(start, last, step,  x->cam.sensor, 1.0, "mti update");

        }
    }

    xipvis_bb csrt_bb, kcft_bb;

    // If UDOT is enabled
    if (udot_enabled && (udotflag != UDOT_SKIP))
    {
        int sf;
        
        if (udotflag == UDOT_RETRACK)
        {
            // Restart the track in the new image
            PROFILE(rc = xsc::udot::transfer_target(&(x->udot), curr_im));

            // TODO Implement transfer_target for CSR Tracker
            XIPVIS_LOG("Restarting track.");
        }
        else
        {
            // Update position of each tracked patch in the image levels
            PROFILE(rc = xsc::udot::update_target(&(x->udot), &(x->cp[1]), curr_im, 
                                                    xsc::stabilize::get_current_homography(st), 
                                                    xsc::stabilize::current_homography_valid(st),
                                                    x->pool_size));

            PROFILE(rc = xipvis_update_csrt(ctx, inbuf, stride, x->image_id, &csrt_bb,
                                            xsc::stabilize::get_current_homography(st), 
                                            xsc::stabilize::current_homography_valid(st)) );
            // fprintf(stderr, "%s: bb= %3dx%3d   w=%3d  h=%3d\n\r",
            //     __FUNCTION__, csrt_bb.xmin, csrt_bb.xmax, csrt_bb.xmax - csrt_bb.xmin, csrt_bb.ymax - csrt_bb.ymin);
            PROFILE(rc = xipvis_update_kcft(ctx, inbuf, stride, x->image_id, &kcft_bb,
                                            xsc::stabilize::get_current_homography(st), 
                                            xsc::stabilize::current_homography_valid(st)) );
            // fprintf(stderr, "%s: bb= %3dx%3d   w=%3d  h=%3d\n\r",
            //     __FUNCTION__, kcft_bb.xmin, kcft_bb.xmax, kcft_bb.xmax - kcft_bb.xmin, kcft_bb.ymax - kcft_bb.ymin);

        }

        if (rc < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        sf = 1 << x->udot.patch.scale;
       
        // Update the patch bounding-box (after the MTI boxes)
#if 1
        x->overlay.bboxes[x->overlay.nb].xmin = x->udot.patch.xmin * sf;
        x->overlay.bboxes[x->overlay.nb].ymin = x->udot.patch.ymin * sf;
        x->overlay.bboxes[x->overlay.nb].xmax = x->udot.patch.xmax * sf;
        x->overlay.bboxes[x->overlay.nb].ymax = x->udot.patch.ymax * sf;
#else
        // Draw region where we are searching for UDOT target
        x->overlay.bboxes[x->overlay.nb].xmin = (x->udot.patch.x - x->udot.patch.radius) * sf;
        x->overlay.bboxes[x->overlay.nb].ymin = (x->udot.patch.y - x->udot.patch.radius) * sf;
        x->overlay.bboxes[x->overlay.nb].xmax = (x->udot.patch.x + x->udot.patch.w + x->udot.patch.radius) * sf;
        x->overlay.bboxes[x->overlay.nb].ymax = (x->udot.patch.y + x->udot.patch.h + x->udot.patch.radius) * sf;
#endif
        x->overlay.bboxes[x->overlay.nb].x_unwarped = (x->overlay.bboxes[x->overlay.nb].xmin + x->overlay.bboxes[x->overlay.nb].xmax) / 2;
        x->overlay.bboxes[x->overlay.nb].y_unwarped = (x->overlay.bboxes[x->overlay.nb].ymin + x->overlay.bboxes[x->overlay.nb].ymax) / 2;        
        x->overlay.bboxes[x->overlay.nb].id = 0;
        x->overlay.bboxes[x->overlay.nb].image_id = x->image_id;
        x->overlay.bboxes[x->overlay.nb].confidence = 1.f / (x->udot.patch.occluded + 1.f);

        x->overlay.nb++;

        x->overlay.bboxes[x->overlay.nb].xmin = csrt_bb.xmin;
        x->overlay.bboxes[x->overlay.nb].ymin = csrt_bb.ymin;
        x->overlay.bboxes[x->overlay.nb].xmax = csrt_bb.xmax;
        x->overlay.bboxes[x->overlay.nb].ymax = csrt_bb.ymax;
        x->overlay.bboxes[x->overlay.nb].x_unwarped = (x->overlay.bboxes[x->overlay.nb].xmin + x->overlay.bboxes[x->overlay.nb].xmax) / 2;
        x->overlay.bboxes[x->overlay.nb].y_unwarped = (x->overlay.bboxes[x->overlay.nb].ymin + x->overlay.bboxes[x->overlay.nb].ymax) / 2;        
        x->overlay.bboxes[x->overlay.nb].id = 0;
        x->overlay.bboxes[x->overlay.nb].image_id = x->image_id;
        x->overlay.bboxes[x->overlay.nb].confidence = 1.f;

        x->overlay.nb++;

        x->overlay.bboxes[x->overlay.nb].xmin = kcft_bb.xmin;
        x->overlay.bboxes[x->overlay.nb].ymin = kcft_bb.ymin;
        x->overlay.bboxes[x->overlay.nb].xmax = kcft_bb.xmax;
        x->overlay.bboxes[x->overlay.nb].ymax = kcft_bb.ymax;
        x->overlay.bboxes[x->overlay.nb].x_unwarped = (x->overlay.bboxes[x->overlay.nb].xmin + x->overlay.bboxes[x->overlay.nb].xmax) / 2;
        x->overlay.bboxes[x->overlay.nb].y_unwarped = (x->overlay.bboxes[x->overlay.nb].ymin + x->overlay.bboxes[x->overlay.nb].ymax) / 2;        
        x->overlay.bboxes[x->overlay.nb].id = 0;
        x->overlay.bboxes[x->overlay.nb].image_id = x->image_id;
        x->overlay.bboxes[x->overlay.nb].confidence = 1.f;

        x->overlay.nb++;

        PROF(start, last, step,  x->cam.sensor, 1.0, "udot update");
    }

    // Adaptive keypoint detection thresholding
    x->avg_threshold = 0.f;
    for(int n = 0; n < x->total_image_bins; n++)
    {
        const int found = curr_im[1].n[0][n];
        const int err = x->target_keypoints - found;
        const float gain = x->target_keypoints * x->adaptive_gain / x->max_keypoints;
        const float delta = -gain*(float)err;
        x->thresholds[0][n] += delta;
        BOUND_VARIABLE(x->thresholds[0][n], xsc::KEYPOINT_THRESHOLD_MIN, xsc::KEYPOINT_THRESHOLD_MAX);
        
        x->thresholds[0][n+x->total_image_bins] = sqrt(x->thresholds[0][n]);
        
        x->avg_threshold += x->thresholds[0][n];
    }
    x->avg_threshold /= x->total_image_bins;

    // Copy the image back from the device
    switch (x->cam.format)
    {
        case YUY2:
        {
            PROFILE(rc = xsc::image::download_yuy422(curr_im, outbuf, src_gpu));
            break;
        }
        case UYVY:
        {
            PROFILE(rc = xsc::image::download_uyvy(curr_im, outbuf, src_gpu));
            break;
        }
        default:
        {
            if (stabilization_enabled)
            {
                PROFILE(rc = xsc::image::download_yuv420(curr_im, outbuf, src_gpu));
            }
            else
            {
                PROFILE(rc = xsc::image::copy_yuv420(curr_im, outbuf, inbuf, stride));
            }
            break;
        }
    }
    PROF(start, last, step,  x->cam.sensor, 9.0, "image copy back");

    // If image was converted to I420 we need to fix stride before drawing
    if (x->cam.format != I420)
        stride /= 2;
    
    // Draw the bounding-boxes if requested
    if (dbb)
    {
        if (stabilization_enabled)
        {
            warp_bounding_boxes(&x->overlay, curr_im->w, curr_im->h, curr_im->fhm);
        }
        PROFILE(rc = draw(&x->overlay, outbuf, curr_im->w, curr_im->h, stride));
        if (rc < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
        PROF(start, last, step,  x->cam.sensor, 1.0, "draw boxes");
    }
    
    const int drawdbg = 0;
    if (drawdbg)
    {
        int a;
        xsc::renderer r;
        
        if (xsc::init(&r, outbuf, curr_im->w, curr_im->h, stride))
        {
            XIPVIS_ERROR(errno, , -1);
        }
        
        int xmin;
        int xmax;
        int ymin;
        int ymax;
        int u;
        int v;
        int s;

#if 0        
        if (x->overlay.nb > 0)
        {
            xipvis_bb bb;
            bb = x->overlay.bboxes[0];
            // un-stabilized location
            drawbb(&bb, xsc::INDIGO, &r);
            warp_bounding_box(&bb, curr_im->w, curr_im->h, curr_im->fhm);
            // stabilized location
            drawbb(&bb, xsc::ORANGE, &r);
            unwarp_bounding_box(&bb, curr_im->w, curr_im->h, curr_im->fhm);
            // un-stabilized location
            drawbb(&bb, xsc::RED, &r);
        }
#endif

#if 0
        for (a = 0; a < x->cp[1].ng; a++)
        {
            // Good Matches
            u = x->cp[1].xg[a];
            v = x->cp[1].yg[a];
            xmin = MAX(0, u*2 - 2);
            xmax = MIN(curr_im->w, u*2 + 2); 
            ymin = MAX(0, v*2 - 2);
            ymax = MIN(stride, v*2 + 2); 
            
            xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::ORANGE);
        }
#endif

#if 0
        for (int j = 0; j < x->total_image_bins; j++)
        {
            const int k = j * x->max_keypoints;
            for (a = 0; a < curr_im[1].n[0][j]; a++)
            {
                // All keypoints
                u = curr_im[1].x[0][k+a];
                v = curr_im[1].y[0][k+a];
                xmin = MAX(0, u*2      - 2);
                xmax = MIN(curr_im->w, u*2  + 2); 
                ymin = MAX(0, v*2      - 2);
                ymax = MIN(stride, v*2 + 2); 
                
                xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::ORANGE);
            }
        }
#endif

#if 0
        for (a = 0; a < *(curr_im->nb[0]); a++)
        {
            // Bad Matches
            float uv[2];
            uv[0] = curr_im->xyb[0][2*a+0]*2.f;
            uv[1] = curr_im->xyb[0][2*a+1]*2.f;
            if (stabilization_enabled)
            {
                warp_point(uv, curr_im->fhm);
            }
            xmin = MAX(0, uv[0]      - 2);
            xmax = MIN(curr_im->w, uv[0]  + 2);
            ymin = MAX(0, uv[1]      - 2);
            ymax = MIN(stride, uv[1] + 2);
            xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::RED);
        }
#endif

#if 0
        for (a = 0; a < x->motion.nz; a++)
        {
            u = x->motion.xs[a] << 1;
            v = x->motion.ys[a] << 1;
            xmin = MAX(0, u*1      - 1);
            xmax = MIN(curr_im->w, u*1  + 1);
            ymin = MAX(0, v*1      - 1);
            ymax = MIN(stride, v*1 + 1);

            xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::WHITE);
        }
#endif

#if 0
        for (a = 0; a < st->match_count; a++)
        {
            // Keyframe points in current image
            u = 32 + 8*a;
            v = 32;
            xmin = MAX(0,     2*u - 3);
            xmax = MIN(curr_im->w, 2*u + 3); 
            ymin = MAX(0,     2*v - 3);
            ymax = MIN(curr_im->h, 2*v + 3);
            
            xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::RED);
        }
#endif
        
#if 0
        for (a = 0; a < st->match_count; a++)
        {
            // Keyframe points in current image
            u = 32 + 8*a + (st->px1[0][a] - st->px0[0][a]);
            v = 32 +       (st->py1[0][a] - st->py0[0][a]);
            xmin = MAX(0,     2*u - 5);
            xmax = MIN(curr_im->w, 2*u + 5); 
            ymin = MAX(0,     2*v - 5);
            ymax = MIN(curr_im->h, 2*v + 5);
            
            xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::GREEN);
        }
#endif

#if 0
        for (a = 0; a < st->match_count; a++)
        {
            // Keyframe points in current image
            u = 32 + 8*a + (st->px2[0][a] - st->px0[0][a]);
            v = 32 +       (st->py2[0][a] - st->py0[0][a]);
            xmin = MAX(0,     2*u - 4);
            xmax = MIN(curr_im->w, 2*u + 4); 
            ymin = MAX(0,     2*v - 4);
            ymax = MIN(curr_im->h, 2*v + 4);
            
            xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::ORANGE);
        }
#endif

#if 0
        // Draw All of the Matches from Optical Flow
        
        if (x->image_id > 0)
        {
            fprintf(stderr, "--\n");
            for (a = 0; a < st->match_count; a++)
            {
                fprintf(stderr, "* %d\t%3.1f\t%3.1f\t%3.1f\t%3.1f\t%3.1f\t%3.1f\n", a, st->px0[0][a], st->py0[0][a], st->px1[0][a], st->py1[0][a], st->px2[0][a], st->py2[0][a]);
            }
            for (; a < curr_im->nf; a++)
            {
                fprintf(stderr, "  %d\t%3.1f\t%3.1f\t%3.1f\t%3.1f\t%3.1f\t%3.1f\n", a, st->px0[0][a], st->py0[0][a], st->px1[0][a], st->py1[0][a], st->px2[0][a], st->py2[0][a]);
            }
            
            
            for (a = 0; a < st->match_count; a++)
            {
                float u1 = st->px0[0][a];
                float v1 = st->py0[0][a];
                float u2 = st->px1[0][a];
                float v2 = st->py1[0][a];
                xmin = MAX(0,       2*u1 - 2);
                xmax = MIN(curr_im->w-1, 2*u1 + 2); 
                ymin = MAX(0,       2*v1 - 2);
                ymax = MIN(curr_im->h-1, 2*v1 + 2);
                
                xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::ORANGE);
                
                xmin = MAX(0,     2*u2 - 2);
                xmax = MIN(curr_im->w-1, 2*u2 + 2); 
                ymin = MAX(0,     2*v2 - 2);
                ymax = MIN(curr_im->h-1, 2*v2 + 2);
                
                xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::RED);
            }
            
            for (a = 0; a < st->inlier_count; a++)
            {
                float u1 = st->inx0[a];
                float v1 = st->iny0[a];
                float u2 = st->inx1[a];
                float v2 = st->iny1[a];
                xmin = MAX(0,     2*u1 - 2);
                xmax = MIN(curr_im->w, 2*u1 + 2); 
                ymin = MAX(0,     2*v1 - 2);
                ymax = MIN(curr_im->h, 2*v1 + 2);
                
                xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::GREEN);
                
                xmin = MAX(0,     2*u2 - 2);
                xmax = MIN(curr_im->w, 2*u2 + 2); 
                ymin = MAX(0,     2*v2 - 2);
                ymax = MIN(curr_im->h, 2*v2 + 2);
                
                xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::INDIGO);
            }
        }
#endif
        
#if 0
        Matrix3f Hj = x->cp[1].Hk;
        for (a = 0; a < x->cp[1].ns; a++)
        {
            // Keyframe points in current image
            const float xs = (x->cp[1].xs[a]);
            const float ys = (x->cp[1].ys[a]);
            const float zk = Hj(2,0)*xs + Hj(2,1)*ys + Hj(2,2);
            u = (Hj(0,0)*xs + Hj(0,1)*ys + Hj(0,2)) / zk;
            v = (Hj(1,0)*xs + Hj(1,1)*ys + Hj(1,2)) / zk;
            
            xmin = MAX(0,     2*u - 3);
            xmax = MIN(curr_im->w, 2*u + 3); 
            ymin = MAX(0,     2*v - 3);
            ymax = MIN(curr_im->h, 2*v + 3);
            
            xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::RED);
        }
#endif

#if 0
        for (a = 0; a < xsc::MAX_EGO_KEYPOINTS; a++)
        {
            // Keyframe points in keyframe image
            u = (x->cp[1].xs[a]);
            v = (x->cp[1].ys[a]);
            xmin = MAX(0,     2*u - 3);
            xmax = MIN(curr_im->w, 2*u + 3); 
            ymin = MAX(0,     2*v - 3);
            ymax = MIN(curr_im->h, 2*v + 3);
            
            xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::INDIGO);
        }
#endif

#if 0
        for (a = 0; a < xsc::MAX_EGO_KEYPOINTS; a++)
        {
            // Keyframe points in initial condition
            u = x->cp[1].xi[a] - x->cp[1].cx;
            v = x->cp[1].yi[a] - x->cp[1].cy;
            u = (x->cp[1].best_ct * u - x->cp[1].best_st * v + x->cp[1].best_dx + x->cp[1].cx);
            v = (x->cp[1].best_st * u + x->cp[1].best_ct * v + x->cp[1].best_dy + x->cp[1].cy);
            xmin = MAX(0,     2*u - 4);
            xmax = MIN(curr_im->w, 2*u + 4); 
            ymin = MAX(0,     2*v - 4);
            ymax = MIN(curr_im->h, 2*v + 4);
            
            xsc::rect_yuv420(&r,xmin, ymin, xmax, ymax, 1, xsc::GREEN);
        }
#endif

#if 0
        // Draw UDOT descriptor and SSD weighting kernel
        for (int v = 0; v < xsc::udot::DESCRIPTOR_HEIGHT; v++)
        {
            for (int u = 0; u < xsc::udot::DESCRIPTOR_WIDTH; u+=1)
            {
                const int j = u + v*xsc::udot::DESCRIPTOR_WIDTH;
                const int w = 6;
                for (int k = 0; k < w; k++)
                    xsc::circ_yuv420(&r, w+u*w, 300+v*w, k, (uint8_t)(x->udot.patch.descriptor[j]*255));
            }
            
//            for (int u = 0; u < xsc::udot::DESCRIPTOR_WIDTH; u+=1)
//            {
//                const int j = u + v*xsc::udot::DESCRIPTOR_WIDTH;
//                const int w = 6;
//                for (int k = 0; k < w; k++)
//                xsc::circ_yuv420(&r, w+(xsc::udot::DESCRIPTOR_WIDTH*2+u)*w, 300+v*w, k, (uint8_t)MIN((x->udot.weights[j]*180*255), 255));
//            }
        }
#endif

#if 1
        // Draw search radius
        if (udot_enabled && x->overlay.nb > 0)
        {
            const int sf = 1 << x->udot.patch.scale;
            xipvis_bb bb;        
            bb.xmin = (uint32_t)(x->udot.patch.x + 0.5 - x->udot.patch.radius) * sf;
            bb.ymin = (uint32_t)(x->udot.patch.y + 0.5 - x->udot.patch.radius) * sf;
            bb.xmax = (uint32_t)(x->udot.patch.x + 0.5 + x->udot.patch.radius + x->udot.patch.w) * sf;
            bb.ymax = (uint32_t)(x->udot.patch.y + 0.5 + x->udot.patch.radius + x->udot.patch.h) * sf;
            if (stabilization_enabled)
            {
                warp_bounding_box(&bb, curr_im->w, curr_im->h, curr_im->fhm);
            }
            xsc::rect_yuv420(&r, bb.xmin, bb.ymin, 
                bb.xmax, bb.ymax,
                1, xsc::GREEN);
        }
#endif
        
        // Draw velocity
        if (udot_enabled && x->overlay.nb > 0)
        {
            int cx = (x->overlay.bboxes[0].xmax + x->overlay.bboxes[0].xmin)/2;
            int cy = (x->overlay.bboxes[0].ymax + x->overlay.bboxes[0].ymin)/2;
            int vx = x->udot.patch.vx * x->cam.fps;
            int vy = x->udot.patch.vy * x->cam.fps;
            xsc::line_yuv420(&r, cx, cy, cx + vx, cy + vy, xsc::GREEN);
        }
        
        if (xsc::free(&r) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }        
        PROF(start, last, step,  x->cam.sensor, 1.0, "debug draw");
    }

    if (stabilization_enabled && !dbb)
    {
        warp_bounding_boxes(&(x->overlay), curr_im->w, curr_im->h, curr_im->fhm);
    }

    if (dokeyframe)
    {
        PROFILE(rc = xsc::camera::update_keyframe<CAMERA_LEVELS>(x->cp, x->image_id, curr_im));
        if (rc < 0)
        {
            // UDOT can track without camera motion updates as long as
            // it has been initialized at least once.
            if (x->image_id == 0)
            {
                XIPVIS_ERROR(errno, , -1);
            }
        }
        PROF(start, last, step,  x->cam.sensor, 1.0, "keyframe");
    }
    
    *num = x->overlay.nb;
    *bbs = x->overlay.bboxes;

    *id = x->image_id;
    x->image_id++;

    return 0;
}

int xipvis_kcft_start_track(void *ctx, int id, int nowarp, struct xipvis_bb *bb, uint8_t *inbuf, int stride/*, const float* const H, const int valid_H*/)
{
    fprintf(stderr, "%s: Starting track in history pool\n\r", __FUNCTION__);
    xipvis *x = (xipvis *)ctx;

    // Don't wrap the buffer if we have not filled it at least once
    if (x->image_id <= x->pool_size && id > x->image_id)
    {
        id = 0;
    }
    else if (x->pool_size <= x->image_id - id || x->image_id - id < 0)
    {
        id = x->image_id - x->pool_size;
    }

    if (xipvis_stabilization_enabled(ctx) && !nowarp && id > 0)
    {
        xsc::image::level* im = x->ip[id % x->pool_size];
        unwarp_bounding_box(bb, im->w, im->h, im->fhm);
    }

    int n =0;
// fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    int h = x->cam.height;
    h -= h % 3;
    int w = x->cam.width;
    fprintf(stderr, "Img id=%d, %dx%d  ptr=0x%08x\r\n", id, w, h, (void *)inbuf);
    cv::Mat img_yuv(h + h/2, w, CV_8U, inbuf);
    cv::Mat img_bgr(h, w, CV_8UC3);
// fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    cv::cvtColor(img_yuv, img_bgr, CV_YUV2BGR_I420, 3);

    // cv::Mat cv_img(x->cam.height, x->cam.width, CV_8UC1, inbuf);
    // cv_img.convertTo(cv_img, CV_32FC1, 1.0/255.0);

    // cv::Mat img_bgr;
    // cv::Mat channels[3];
    // channels[0] = cv_img;
    // channels[1] = cv_img;
    // channels[2] = cv_img;
    // cv::merge(channels, 3, img_bgr);

    cv::Rect2d cv_bb;
    cv_bb.x = bb->xmin;
    cv_bb.y = bb->ymin;
    cv_bb.width = (bb->xmax - bb->xmin);
    cv_bb.height = (bb->ymax - bb->ymin);
// fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    x->kcft->init(cv_bb, img_bgr);
    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    
    xipvis_enable_udot(ctx);

    bb->id = 0;
    bb->image_id = id;

    id++;
    int rc;
    int next = x->image_id % x->pool_size;
    
    x->overlay.nb = 0;

    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    PROFILE(rc = xipvis_update_kcft(ctx, inbuf, stride, id, bb, nullptr, false) );
    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);

    x->kcft_bb = *bb;

    // Update the patch bounding-box
    x->overlay.bboxes[x->overlay.nb].xmin = bb->xmin; 
    x->overlay.bboxes[x->overlay.nb].ymin = bb->ymin;
    x->overlay.bboxes[x->overlay.nb].xmax = bb->xmax;
    x->overlay.bboxes[x->overlay.nb].ymax = bb->ymax;
    x->overlay.bboxes[x->overlay.nb].x_unwarped = (x->overlay.bboxes[x->overlay.nb].xmin + x->overlay.bboxes[x->overlay.nb].xmax) / 2;
    x->overlay.bboxes[x->overlay.nb].y_unwarped = (x->overlay.bboxes[x->overlay.nb].ymin + x->overlay.bboxes[x->overlay.nb].ymax) / 2;        
    x->overlay.bboxes[x->overlay.nb].id = 0;
    x->overlay.bboxes[x->overlay.nb].image_id = id++;
    x->overlay.bboxes[x->overlay.nb].confidence = 1.f / (x->udot.patch.occluded + 1.f);
    
    x->overlay.nb++;

    /*
    // TODO Implement retro tracks/history buffer intialization
    // Track the patch through history to current
    while( (id % x->pool_size) != next)
    {
        int pid = id % x->pool_size;
        PROFILE(rc = xsc::udot::update_target(&(x->udot), &(x->cp[1]),
                x->ip[pid],
                x->ip[pid]->hm,
                x->ip[pid]->hv,
                x->pool_size));
        if (rc < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        const int sf = 1 << x->udot.patch.scale;
        
        // Update the patch bounding-box
        x->overlay.bboxes[x->overlay.nb].xmin = x->udot.patch.xmin * sf;
        x->overlay.bboxes[x->overlay.nb].ymin = x->udot.patch.ymin * sf;
        x->overlay.bboxes[x->overlay.nb].xmax = x->udot.patch.xmax * sf;
        x->overlay.bboxes[x->overlay.nb].ymax = x->udot.patch.ymax * sf;
        x->overlay.bboxes[x->overlay.nb].x_unwarped = (x->overlay.bboxes[x->overlay.nb].xmin + x->overlay.bboxes[x->overlay.nb].xmax) / 2;
        x->overlay.bboxes[x->overlay.nb].y_unwarped = (x->overlay.bboxes[x->overlay.nb].ymin + x->overlay.bboxes[x->overlay.nb].ymax) / 2;        
        x->overlay.bboxes[x->overlay.nb].id = 0;
        x->overlay.bboxes[x->overlay.nb].image_id = id++;
        x->overlay.bboxes[x->overlay.nb].confidence = 1.f / (x->udot.patch.occluded + 1.f);
    }  
    
    x->overlay.nb++;
    */
      
    return 0;
}


int xipvis_csrt_start_track(void *ctx, int id, int nowarp, struct xipvis_bb *bb, uint8_t *inbuf, int stride/*, const float* const H, const int valid_H*/)
{
    fprintf(stderr, "%s: Starting track in history pool\n\r", __FUNCTION__);
    xipvis *x = (xipvis *)ctx;

    // Don't wrap the buffer if we have not filled it at least once
    if (x->image_id <= x->pool_size && id > x->image_id)
    {
        id = 0;
    }
    else if (x->pool_size <= x->image_id - id || x->image_id - id < 0)
    {
        id = x->image_id - x->pool_size;
    }

    if (xipvis_stabilization_enabled(ctx) && !nowarp && id > 0)
    {
        xsc::image::level* im = x->ip[id % x->pool_size];
        unwarp_bounding_box(bb, im->w, im->h, im->fhm);
    }

    int n =0;
// fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    int h = x->cam.height;
    h -= h % 3;
    int w = x->cam.width;
    // fprintf(stderr, "Img id=%d, %dx%d  ptr=0x%08x\r\n", id, w, h, (void *)inbuf);
    cv::Mat img_yuv(h + h/2, w, CV_8U, inbuf);
    cv::Mat img_bgr(h, w, CV_8UC3);
// fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    cv::cvtColor(img_yuv, img_bgr, CV_YUV2BGR_I420, 3);

    csrt_log = fopen("csrt_log.csv", "a");
    if (csrt_log) fprintf(csrt_log, "IDX,REG_X,REG_Y,C_X,C_Y,QUALITY\n");

    // cv::Mat cv_img(x->cam.height, x->cam.width, CV_8UC1, inbuf);
    // cv_img.convertTo(cv_img, CV_32FC1, 1.0/255.0);

    // cv::Mat img_bgr;
    // cv::Mat channels[3];
    // channels[0] = cv_img;
    // channels[1] = cv_img;
    // channels[2] = cv_img;
    // cv::merge(channels, 3, img_bgr);

    cv::Rect2d cv_bb;
    cv_bb.x = bb->xmin;
    cv_bb.y = bb->ymin;
    cv_bb.width = (bb->xmax - bb->xmin);
    cv_bb.height = (bb->ymax - bb->ymin);
// fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    if (x->csrt->init(img_bgr, cv_bb) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    
    // Initialize Kalman filter
    float c_x = (bb->xmin + bb->xmax) / 2.f;
    float c_y = (bb->ymin + bb->ymax) / 2.f;
    float dt;
    if (x->cam.fps > 0.)
    {
       dt = 1.f / x->cam.fps;
    }
    else
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    x->csrt_kf.init(dt, x->image_id, c_x, c_y, 0.0f, 0.0f);
    
    xipvis_enable_udot(ctx);

    bb->id = 0;
    bb->image_id = id;

    id++;
    int rc;
    int next = x->image_id % x->pool_size;
    
    x->overlay.nb = 0;

/*
    bool found = false;
    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);
    PROFILE(found = xipvis_update_csrt(ctx, inbuf, stride, id, bb, nullptr, false) );
    // fprintf(stderr, "%s: Here %d\n\r", __FUNCTION__, n++);

    if (found)
    {
        x->csrt_bb = *bb;
    }
*/
    x->csrt_bb = *bb;

    // Update the patch bounding-box
    x->overlay.bboxes[x->overlay.nb].xmin = bb->xmin; 
    x->overlay.bboxes[x->overlay.nb].ymin = bb->ymin;
    x->overlay.bboxes[x->overlay.nb].xmax = bb->xmax;
    x->overlay.bboxes[x->overlay.nb].ymax = bb->ymax;
    x->overlay.bboxes[x->overlay.nb].x_unwarped = (x->overlay.bboxes[x->overlay.nb].xmin + x->overlay.bboxes[x->overlay.nb].xmax) / 2;
    x->overlay.bboxes[x->overlay.nb].y_unwarped = (x->overlay.bboxes[x->overlay.nb].ymin + x->overlay.bboxes[x->overlay.nb].ymax) / 2;        
    x->overlay.bboxes[x->overlay.nb].id = 0;
    x->overlay.bboxes[x->overlay.nb].image_id = id++;
    x->overlay.bboxes[x->overlay.nb].confidence = 1.f / (x->udot.patch.occluded + 1.f);
    
    x->overlay.nb++;

    /*
    // TODO Implement retro tracks/history buffer intialization
    // Track the patch through history to current
    while( (id % x->pool_size) != next)
    {
        int pid = id % x->pool_size;
        PROFILE(rc = xsc::udot::update_target(&(x->udot), &(x->cp[1]),
                x->ip[pid],
                x->ip[pid]->hm,
                x->ip[pid]->hv,
                x->pool_size));
        if (rc < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        const int sf = 1 << x->udot.patch.scale;
        
        // Update the patch bounding-box
        x->overlay.bboxes[x->overlay.nb].xmin = x->udot.patch.xmin * sf;
        x->overlay.bboxes[x->overlay.nb].ymin = x->udot.patch.ymin * sf;
        x->overlay.bboxes[x->overlay.nb].xmax = x->udot.patch.xmax * sf;
        x->overlay.bboxes[x->overlay.nb].ymax = x->udot.patch.ymax * sf;
        x->overlay.bboxes[x->overlay.nb].x_unwarped = (x->overlay.bboxes[x->overlay.nb].xmin + x->overlay.bboxes[x->overlay.nb].xmax) / 2;
        x->overlay.bboxes[x->overlay.nb].y_unwarped = (x->overlay.bboxes[x->overlay.nb].ymin + x->overlay.bboxes[x->overlay.nb].ymax) / 2;        
        x->overlay.bboxes[x->overlay.nb].id = 0;
        x->overlay.bboxes[x->overlay.nb].image_id = id++;
        x->overlay.bboxes[x->overlay.nb].confidence = 1.f / (x->udot.patch.occluded + 1.f);
    }  
    
    x->overlay.nb++;
    */
      
    return 0;
}

int xipvis_udot_start_track(void *ctx, int id, int nowarp, struct xipvis_bb *bb)
{
    fprintf(stderr, "%s: Starting track in history pool\n\r", __FUNCTION__);
    xipvis *x = (xipvis *)ctx;

    // Don't wrap the buffer if we have not filled it at least once
    if (x->image_id <= x->pool_size && id > x->image_id)
    {
        id = 0;
    }
    else if (x->pool_size <= x->image_id - id || x->image_id - id < 0)
    {
        id = x->image_id - x->pool_size;
    }

    if (xipvis_stabilization_enabled(ctx) && !nowarp && id > 0)
    {
        xsc::image::level* im = x->ip[id % x->pool_size];
        unwarp_bounding_box(bb, im->w, im->h, im->fhm);
    }
    
    if (xsc::udot::init_target<IMAGE_LEVELS>(&(x->udot), bb->xmin, bb->ymin, bb->xmax, bb->ymax, x->ip[id % x->pool_size]) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    xipvis_enable_udot(ctx);

    bb->id = 0;
    bb->image_id = id;

    id++;
    int rc;
    int next = x->image_id % x->pool_size;
    
    x->overlay.nb = 0;

    // Track the patch through history to current
    while( (id % x->pool_size) != next)
    {
        int pid = id % x->pool_size;
        PROFILE(rc = xsc::udot::update_target(&(x->udot), &(x->cp[1]),
                x->ip[pid],
                x->ip[pid]->hm,
                x->ip[pid]->hv,
                x->pool_size));
        if (rc < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        const int sf = 1 << x->udot.patch.scale;
        
        // Update the patch bounding-box
        x->overlay.bboxes[x->overlay.nb].xmin = x->udot.patch.xmin * sf;
        x->overlay.bboxes[x->overlay.nb].ymin = x->udot.patch.ymin * sf;
        x->overlay.bboxes[x->overlay.nb].xmax = x->udot.patch.xmax * sf;
        x->overlay.bboxes[x->overlay.nb].ymax = x->udot.patch.ymax * sf;
        x->overlay.bboxes[x->overlay.nb].x_unwarped = (x->overlay.bboxes[x->overlay.nb].xmin + x->overlay.bboxes[x->overlay.nb].xmax) / 2;
        x->overlay.bboxes[x->overlay.nb].y_unwarped = (x->overlay.bboxes[x->overlay.nb].ymin + x->overlay.bboxes[x->overlay.nb].ymax) / 2;        
        x->overlay.bboxes[x->overlay.nb].id = 0;
        x->overlay.bboxes[x->overlay.nb].image_id = id++;
        x->overlay.bboxes[x->overlay.nb].confidence = 1.f / (x->udot.patch.occluded + 1.f);
    }  
    
    x->overlay.nb++;
      
    return 0;
}

int xipvis_print_stabilization_solution(void* const ctx, FILE* fd, int id)
{
    if (fd == NULL)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    else
    {
        xipvis *x = (xipvis *)ctx;
        return xsc::stabilize::print_homography(&(x->stabilizer), fd, id);
    }
}

// *************************************************
// UDOT Interface
int xipvis_udot_set_learning_rate(void* const x, const float rate)
{
    return xsc::udot::set_learning_rate(&(((xipvis*)x)->udot), rate);
}

int xipvis_udot_set_radius_increment(void* const x, const int inc)
{
    return xsc::udot::set_radius_increment(&(((xipvis*)x)->udot), inc);
}

int xipvis_udot_set_max_radius(void* const x, const int radius)
{
    return xsc::udot::set_max_radius(&(((xipvis*)x)->udot), radius);
}

int xipvis_udot_set_max_fb_error(void* const x, const int error)
{
    return xsc::udot::set_max_fb_error(&(((xipvis*)x)->udot), error);
}

int xipvis_udot_set_velocity_filter(void* const x, const float filt)
{
    return xsc::udot::set_velocity_filter(&(((xipvis*)x)->udot), filt);
}

int xipvis_udot_set_max_ssd(void* const x, const float max)
{
    return xsc::udot::set_max_ssd(&(((xipvis*)x)->udot), max);
}

float xipvis_udot_get_learning_rate(void* const x)
{
    return xsc::udot::get_learning_rate(&(((xipvis*)x)->udot));
}

int xipvis_udot_get_radius_increment(void* const x)
{
    return xsc::udot::get_radius_increment(&(((xipvis*)x)->udot));
}

int xipvis_udot_get_max_radius(void* const x)
{
    return xsc::udot::get_max_radius(&(((xipvis*)x)->udot));
}

int xipvis_udot_get_max_fb_error(void* const x)
{
    return xsc::udot::get_max_fb_error(&(((xipvis*)x)->udot));
}

float xipvis_udot_get_velocity_filter(void* const x)
{
    return xsc::udot::get_velocity_filter(&(((xipvis*)x)->udot));
}

float xipvis_udot_get_max_ssd(void* const x)
{
    return xsc::udot::get_max_ssd(&(((xipvis*)x)->udot));
}

// *************************************************
// MTI Interface
int xipvis_mti_set_min_age(void* const x, const int age)
{
    return xsc::mti::set_min_age(&(((xipvis*)x)->motion), age);
}

int xipvis_mti_set_max_age(void* const x, const int age)
{
    return xsc::mti::set_max_age(&(((xipvis*)x)->motion), age);
}

int xipvis_mti_set_max_speed(void* const x, const float speed)
{
    return xsc::mti::set_max_speed(&(((xipvis*)x)->motion), speed);
}

int xipvis_mti_set_min_radius(void* const x, const float radius)
{
    return xsc::mti::set_min_radius(&(((xipvis*)x)->motion), radius);
}

int xipvis_mti_set_filter_rate(void* const x, const float rate)
{
    return xsc::mti::set_filter_rate(&(((xipvis*)x)->motion), rate);
}

int xipvis_mti_set_decay_rate(void* const x, const float rate)
{
    return xsc::mti::set_decay_rate(&(((xipvis*)x)->motion), rate);
}

int xipvis_mti_set_sweep_size(void* const x, const float size)
{
    return xsc::mti::set_sweep_size(&(((xipvis*)x)->motion), size);
}

int xipvis_mti_set_min_box_size(void* const x, const float size)
{
    return xsc::mti::set_min_box_size(&(((xipvis*)x)->motion), size);
}

int xipvis_mti_set_blanking(void* const x, const int size)
{
    return xsc::mti::set_blanking(&(((xipvis*)x)->motion), size);
}

int xipvis_mti_get_min_age(void* const x)
{
    return xsc::mti::get_min_age(&(((xipvis*)x)->motion));
}

int xipvis_mti_get_max_age(void* const x)
{
    return xsc::mti::get_max_age(&(((xipvis*)x)->motion));
}

float xipvis_mti_get_max_speed(void* const x)
{
    return xsc::mti::get_max_speed(&(((xipvis*)x)->motion));
}

float xipvis_mti_get_min_radius(void* const x)
{
    return xsc::mti::get_min_radius(&(((xipvis*)x)->motion));
}

float xipvis_mti_get_filter_rate(void* const x)
{
    return xsc::mti::get_filter_rate(&(((xipvis*)x)->motion));
}

float xipvis_mti_get_decay_rate(void* const x)
{
    return xsc::mti::get_decay_rate(&(((xipvis*)x)->motion));
}

float xipvis_mti_get_sweep_size(void* const x)
{
    return xsc::mti::get_sweep_size(&(((xipvis*)x)->motion));
}

float xipvis_mti_get_min_box_size(void* const x)
{
    return xsc::mti::get_min_box_size(&(((xipvis*)x)->motion));
}

int xipvis_mti_get_blanking(void* const x)
{
    return xsc::mti::get_blanking(&(((xipvis*)x)->motion));
}

// *************************************************
// Stabilization Interface
int xipvis_stbl_set_filter_radius(void* const x, const int radius)
{
    return xsc::stabilize::set_filter_radius(&(((xipvis*)x)->stabilizer), radius);
}

int xipvis_stbl_set_filter_stddev(void* const x, const float stddev)
{
    return xsc::stabilize::set_filter_stddev(&(((xipvis*)x)->stabilizer), stddev);
}

int xipvis_stbl_get_filter_radius(void* const x)
{
    return xsc::stabilize::get_filter_radius(&(((xipvis*)x)->stabilizer));
}

float xipvis_stbl_get_filter_stddev(void* const x)
{
    return xsc::stabilize::get_filter_stddev(&(((xipvis*)x)->stabilizer));
}

int xipvis_stbl_set_split_screen(void* const x, const int split)
{
    return xsc::stabilize::set_split_screen(&(((xipvis*)x)->stabilizer), split);
}

int xipvis_stbl_get_split_screen(void* const x)
{
    return xsc::stabilize::get_split_screen(&(((xipvis*)x)->stabilizer));
}

float xipvis_stbl_get_scale(void* const x)
{
    return xsc::stabilize::get_scale(&(((xipvis*)x)->stabilizer));
}

float xipvis_stbl_get_marginal_scale(void* const x)
{
    return xsc::stabilize::get_marginal_scale(&(((xipvis*)x)->stabilizer));
}

void xipvis_device_reset(void* const x)
{
    xsc::image::cuda_device_reset();
}

}

