/* Copyright (C) 2016 Xiphos Systems Corporation
 */

// UDOT

#ifndef PATCH_H
#define PATCH_H

namespace xsc
{
namespace udot
{

static constexpr int MIN_PATCH = 16;
static constexpr int DESCRIPTOR_WIDTH = 16;
static constexpr int DESCRIPTOR_HEIGHT = 16;
static constexpr int DESCRIPTOR_LENGTH = DESCRIPTOR_WIDTH * DESCRIPTOR_HEIGHT;
static constexpr int NUM_THREADS = 2;
static constexpr int TOTAL_THREADS = NUM_THREADS * NUM_THREADS;

static constexpr float LEARNING_RATE_MIN = 0.0;
static constexpr float LEARNING_RATE_MAX = 1.0;
static constexpr int   RADIUS_INCREMENT_MIN = 0;
static constexpr int   RADIUS_INCREMENT_MAX = 100;
static constexpr int   MAX_RADIUS_MIN = 8;
static constexpr int   MAX_RADIUS_MAX = 100;
static constexpr int   MAX_FB_ERROR_MIN = 0;
static constexpr int   MAX_FB_ERROR_MAX = 100;
static constexpr float VELOCITY_FILTER_MIN = 0.f;
static constexpr float VELOCITY_FILTER_MAX = 30.f;
static constexpr float MAX_SSD_MIN = 0.f;
static constexpr float MAX_SSD_MAX = DESCRIPTOR_LENGTH;

static constexpr int MAX_NUM_SEARCHES = (2*MAX_RADIUS_MAX + 1) * (2*MAX_RADIUS_MAX + 1);

static constexpr int MASK_DONE   = 1;
static constexpr int MASK_TODO   = 0;

struct patch
{
    int w;         // Width in pixels
    int h;         // Height in pixels
    int xmin;      // Left pixel coordinate
    int ymin;      // Top pixel coordinate
    int xmax;      // Right pixel coordinate
    int ymax;      // Bottom pixel coordinate
    float x;       // Position x-coordinate
    float y;       // Position y-coordinate
    float vx;      // Velocity x-coordinate
    float vy;      // Velocity y-coordinate
    int scale;     // Scale of pixels relative to pyramid-level 0
    int radius;    // Search radius for matching patches
    int occluded;  // Frames patch has not been found in image sequence
    float ssd;     // Sum of squared difference for match
    float descriptor[DESCRIPTOR_LENGTH];  // Patch descriptor for matching
    const xsc::image::level *reference;   // Reference image patch was taken from
};

struct udot
{
    int radius_increment;
    int max_radius;
    int max_fb_error;
    float learning_rate;
    float velocity_filter;
    float max_ssd;
    int palette;
    
    float weights[DESCRIPTOR_LENGTH];
    float l_weights[DESCRIPTOR_LENGTH];
    int lookup[DESCRIPTOR_LENGTH];
    int mask[MAX_NUM_SEARCHES];
    
    xsc::udot::patch  patch;
    xsc::udot::patch* f_patches[TOTAL_THREADS];
    xsc::udot::patch* b_patches[TOTAL_THREADS];
    
    udot();
    
    udot(const udot &) = delete;
    udot &operator = (const udot &) = delete;
};

int init(udot* const u);

int update_target(udot* const u, const xsc::camera::level *c, const xsc::image::level *i, const float* const H, const int valid_H, const int pool_size);
int update_patch(udot* const u, const xsc::camera::level *c, const xsc::image::level *i, const float* const H, const int valid_H, const int pool_size);
int transfer_target(udot* const u, const xsc::image::level *i);
int transfer_patch(udot* const u, const xsc::image::level *i);

int free(udot* const u);

void copy_patches(float* const dst, const int s, const float* const src, const int w, const int h);

int   set_learning_rate(udot* const u, const float rate);
float get_learning_rate(udot* const u);
int   set_max_radius(udot* const u, const int radius);
int   get_max_radius(udot* const u);
int   set_radius_increment(udot* const u, const int inc);
int   get_radius_increment(udot* const u);
int   set_max_fb_error(udot* const u, const int error);
int   get_max_fb_error(udot* const u);
int   set_velocity_filter(udot* const u, const float filt);
float get_velocity_filter(udot* const u);
int   set_max_ssd(udot* const u, const float max);
float get_max_ssd(udot* const u);

int init_patch( udot* const u, int xmin, int ymin, int xmax, int ymax, const xsc::image::level *i);

// Initialize a UDOT target at the efficient pyramid level
template<int N>
int init_target(udot* const u, int xmin, int ymin, int xmax, int ymax, const xsc::image::level *i)
{
    patch *p = &(u->patch);
    
    int w = xmax - xmin;
    int h = ymax - ymin;
    int m = (w < h) ? w : h;

    int s;
    int j;
    int sf;

    if (m < MIN_PATCH)
    {
        XIPVIS_ERROR(EINVAL, , -1);
    }
    
    // Find the minimum pyramid scale where the 2x minimum patch size covers this patch
    for (s = 0, j = MIN_PATCH * 2; s < N; s++, j *= 2)
    {
        if (m < j)
        {
            break;
        }
    }
    
    // Hack until I figure out the cause of poor tracking with scale = 5.
    if (s > 4)
        s = 4;

    sf = 1 << s;
    p->scale = s;

    // Call this xsc::init() method to initialize the patch at the proper pyramid level
    if (init_patch(u, xmin / sf, ymin / sf, xmax / sf, ymax / sf, i + s) < 0)
    {
        // Propogate any error back through
        XIPVIS_ERROR(errno, , -1);
    }

    return 0;
}

}
}


#endif /* #ifndef PATCH_H */

