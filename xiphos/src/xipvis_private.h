/*
 * ============================================================================
 *
 *       Filename:  xipvis_private.h
 *
 *    Description:  Private declarations of Xipvis types
 *
 *        Created:  2016-09-20
 *   Organization:  Aeryon Labs Inc.
 *
 * ============================================================================
 */


#ifndef XIPVIS_PRIVATE_H
#define XIPVIS_PRIVATE_H

#include "tracker.hpp"
#include "kcftracker.hpp"
#include "trackkf.h"


static constexpr int MAX_TRACKS = 100;
static constexpr int IMAGE_LEVELS = 5;
static constexpr int CAMERA_LEVELS = 2;
static constexpr int MOTION_LEVELS = 2;

struct overlay
{
    // Boxes to draw on-screen
    xipvis_bb bboxes[MAX_TRACKS]; // Bounding box parameters
    unsigned int nb; // Number of bounding boxes to draw
    unsigned int bbox_id; // Global box identifier generator
};

struct xipvis
{
    unsigned int image_id;
    xipvis_cam cam;
    
    unsigned int features; // Bit fields for enabled features
    
    xsc::stabilize::stabilizer stabilizer;                 // Image Stabilization
    xsc::udot::udot udot;                                  // UDOT Tracker
    cv::Ptr<cv::TrackerCSRT> csrt;                         // CSR Tracker
    xipvis_bb csrt_bb;                                     // CSRT Bounding Box
    xsc::udot::TrackKF csrt_kf;                            // CSRT Kalman Filter
    KCFTracker * kcft;                                     // KCF Tracker
    xipvis_bb kcft_bb;                                     // KCFT Bounding Box
    xsc::mti::motion<MOTION_LEVELS> motion;                // MTI    
    xsc::camera::pyramid<CAMERA_LEVELS> cp;                // Motion and Structure
    xsc::image::pyramid<IMAGE_LEVELS> *ip;                 // Image Pool

    Vector3f linear_velocity;
    Vector3f angular_velocity;
    float velocity_filter;
    
    int target_keypoints;
    int max_keypoints;
    int total_keypoints;
    int total_image_bins;
    int num_image_bins;
    float* thresholds[2];  // Adaptive keypoint detection thresholds
    float* egomotion[2];   // Ego-motion solution matrix (3x4)
    float avg_threshold;
    float adaptive_gain;
    int motion_delay;
    
    uint8_t *pb;  // Image buffers in the GPU memory
    uint8_t *px;
    uint8_t *d_luma;
    uint8_t *d_cra;
    uint8_t *d_crb;
    void *tex_luma;
    void *tex_cra;
    void *tex_crb;
    
    int index;         // Stream id
    int pool_size;     // The size of the image pyramid pool

    overlay overlay;   // Overlay Drawing
};

namespace xsc
{

int init_gpu(xipvis* const x, int w, int h, const int format);
int update_gpu(xipvis* const x);
int free_gpu(xipvis* const x);

}

#endif

