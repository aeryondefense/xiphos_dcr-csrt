/* Copyright (C) 2016 Xiphos Systems Corporation
 */

#ifndef LOG_H
#define LOG_H

namespace xsc
{
namespace log
{
#if !RELEASE

template <int N>
int float_to_uint8(const float *src, int w, int h, int s, uint8_t **dst)
{
    uint8_t *buf = new(std::nothrow) uint8_t[w * h];
    if (buf == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    for (int y = 0; y < h; y++)
    {
        for (int x = 0; x < w; x++)
        {
            buf[x + y * w] = src[x + y * s] * 128.f;
        }
    }

    *dst = buf;

    return 0;
}

template <int N>
int draw_points(uint8_t *buf, int s, int n, float xoff, float yoff, float *x, float *y)
{
    for (int i = 0; i < n; i++)
    {
        int xi = x[i] + xoff;
        int yi = y[i] + yoff;
        buf[xi + yi * s] = 255;
    }

    return 0;
}

template <int N>
int uint8_to_pgm(uint8_t *buf, int w, int h, char *path)
{
    FILE *f = fopen(path, "wb");
    if (f == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    fprintf(f, "P5\n%d %d\n255\n", w, h);
    fwrite(buf, 1, w * h, f);
    fclose(f);

    return 0;
}

template <int N>
int binarymask(const uint8_t *src1, const uint8_t *src2, int w, int h, uint8_t *dst)
{
    for (int i = 0; i < w * h; i++)
    {
        dst[i] = (src2[i] == 0) ? src1[i] : 255;
    }

    return 0;
}

#if 0
template <int N>
int image(const xsc::image::level *p, int frame, int level)
{
    char path[80];
    uint8_t *buf = nullptr;

    sprintf(path, "image_%06d_%02d.pgm", frame, level);

    if (float_to_uint8<N>(p->i[0], p->w, p->h, p->hs, &buf) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if (draw_points<N>(buf, p->w, p->np, 0, 0, p->x[0], p->y[0]) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
            
    if (uint8_to_pgm<N>(buf, p->w, p->h, path) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    delete [] buf;

    return 0;
}
#endif

template <int N>
int camera(const xsc::camera::level *p, int frame, int level, const xsc::image::level *q)
{
    char path[80];

    {
        uint8_t *buf = nullptr;

        sprintf(path, "camera_keyframe_%06d_%02d.pgm", frame, level);

        /*
        if (float_to_uint8<N>(p->kf->i[0], p->kf->w, p->kf->h, p->kf->hs, &buf) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        if (draw_points<N>(buf, p->kf->w, xsc::MAX_RESIDUALS, p->cx, p->cy, p->xs, p->ys) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        if (uint8_to_pgm<N>(buf, p->kf->w, p->kf->h, path) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
        */

        delete [] buf;

    }

#if 0
    {
        uint8_t *buf = nullptr;

        sprintf(path, "camera_g_%06d_%02d.pgm", frame, level);

        if (float_to_uint8<N>(q->i[0], q->w, q->h, q->hs, &buf) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        if (draw_points<N>(buf, q->w, p->ng, 0, 0, p->xg, p->yg) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
                
        if (uint8_to_pgm<N>(buf, q->w, q->h, path) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        delete [] buf;

    }

    {
        uint8_t *buf = nullptr;

        sprintf(path, "camera_b_%06d_%02d.pgm", frame, level);

        if (float_to_uint8<N>(q->i[0], q->w, q->h, q->hs, &buf) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        if (draw_points<N>(buf, q->w, p->nb, 0, 0, p->xb, p->yb) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
                
        if (uint8_to_pgm<N>(buf, q->w, q->h, path) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        delete [] buf;

    }
#endif

    return 0;
}

template <int N>
int motion_map(const xsc::mti::motion<N> *m, int frame, const xsc::image::level *p)
{
    char path[80];
    uint8_t *buf = nullptr;

    sprintf(path, "motion_map_%06d.pgm", frame);

    if (float_to_uint8<N>(p->i[0], p->w, p->h, p->hs, &buf) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if (binarymask<N>(buf, m->map, p->w, p->h, buf) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if (uint8_to_pgm<N>(buf, p->w, p->h, path) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    delete [] buf;

    return 0;
}

template <int N>
int motion_samples(const xsc::mti::motion<N> *m, int frame, const xsc::image::level *p)
{
    char path[80];
    uint8_t *buf = nullptr;

    sprintf(path, "motion_samples_%06d.pgm", frame);

    if (float_to_uint8<N>(p->i[0], p->w, p->h, p->hs, &buf) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    for (int i = 0; i < xsc::mti::NUM_MOTION_SAMPLES; i++)
    {
        if (1 < m->as[i])
        {
            buf[m->xs[i] + m->ys[i] * p->w] = 255;
        }
    }

    if (uint8_to_pgm<N>(buf, p->w, p->h, path) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    delete [] buf;

    return 0;
}

template <int N>
int motion_clusters(const xsc::mti::motion<N> *m, int frame, const xsc::image::level *p)
{
    char path[80];
    uint8_t *buf = nullptr;
    xsc::renderer r;
    
    sprintf(path, "motion_clusters_%06d.pgm", frame);

    if (float_to_uint8<N>(p->i[0], p->w, p->h, p->hs, &buf) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if (xsc::init(&r, buf, p->w, p->h, p->w) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    for (int i = 0; i < m->nb; i++)
    {
        xsc::rect_yuv420(&r, m->boxes[i].xmin, m->boxes[i].ymin, m->boxes[i].xmax, m->boxes[i].ymax, xsc::WHITE);
    }

    if (uint8_to_pgm<N>(buf, p->w, p->h, path) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    xsc::free(&r);
    delete [] buf;

    return 0;

}

#endif /* #if !RELEASE */

template <int N>
int image(const xsc::image::level *p, int frame)
{
#if !RELEASE

    for (int i = 0; i < N; i++)
    {
        if (image<N>(p + i, frame, i) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }

#endif

    return 0;
}

template <int N>
int camera(const xsc::camera::level *p, int frame, const xsc::image::level *q)
{
#if !RELEASE

    for (int i = 1; i < N; i++)
    {
        if (camera<N>(p + i, frame, i, q + i) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }

#endif

    return 0;
}

template <int N>
int motion(const xsc::mti::motion<N> *m, int frame, const xsc::image::level *p)
{
#if !RELEASE

    if (motion_map<N>(m, frame, p + (N - 1)) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if (motion_samples<N>(m, frame, p + (N - 1)) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    if (motion_clusters<N>(m, frame, p + (N - 1)) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

#endif

    return 0;
}
}
}

#endif /* #ifndef LOG_H */
