/* Copyright (C) 2015 Xiphos Systems Corporation
 */

#ifndef NORM_H
#define NORM_H

namespace xsc
{
namespace samplers
{
class norm 
{
    xsc::samplers::unif &u_;

public:
    norm(xsc::samplers::unif &u);

    norm(const xsc::samplers::norm &) = delete;
    xsc::samplers::norm &operator = (const xsc::samplers::norm &) = delete;

    float operator () (void);
    float operator () (float mean, float stddev);
};
}
}

inline xsc::samplers::norm::norm(xsc::samplers::unif &u) : u_(u)
{
}

inline float xsc::samplers::norm::operator () (void)
{
    float x;
    float y;
    constexpr float pi = 3.141592654f;
    
    x = u_();
    while ((y = u_()) == 0);
    return sqrt(-2.f * std::log(y)) * std::sin(2.f * pi * x);
}

inline float xsc::samplers::norm::operator () (float mean, float stddev)
{
    return (*this)() * stddev + mean;
}

#endif /* #ifndef NORM_H */
