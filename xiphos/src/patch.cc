/* Copyright (C) 2016 Xiphos Systems Corporation
 */

#include "config.h"
#include <pthread.h>

namespace xsc
{
namespace udot
{

static constexpr int   RADIUS_INCREMENT_DEFAULT = 8;
static constexpr int   MAX_RADIUS_DEFAULT = 48;
static constexpr int   MAX_FB_ERROR_DEFAULT = 3;
static constexpr float VELOCITY_FILTER_DEFAULT = 3.f;
static constexpr float LEARNING_RATE_DEFAULT = .07;
static constexpr float MAX_SSD_DEFAULT = 5.f / 256.f;

}
}

xsc::udot::udot::udot() : radius_increment(xsc::udot::RADIUS_INCREMENT_DEFAULT),
                          max_radius(xsc::udot::MAX_RADIUS_DEFAULT),
                          max_fb_error(xsc::udot::MAX_FB_ERROR_DEFAULT),
                          learning_rate(xsc::udot::LEARNING_RATE_DEFAULT),
                          velocity_filter(xsc::udot::VELOCITY_FILTER_DEFAULT),
                          max_ssd(xsc::udot::MAX_SSD_DEFAULT),
                          palette(0)
{}

// Return the number of moving correspondences a patch contains
static int correlate_motion(const xsc::image::level  *q, 
                            const xsc::camera::level *c, 
                            const xsc::udot::patch   *p)
{
    // Bail early if the residual on camera motion is too great
    if (c->residual > xsc::RESIDUAL_MTI_THRESHOLD)
    {
        return 0;
    }

    // Map patch level to camera level
    int s = c->scale - p->scale;
    int x1 = (s > 0)? p->xmin >> s : p->xmin << -s;
    int x2 = (s > 0)? p->xmax >> s : p->xmax << -s;
    int y1 = (s > 0)? p->ymin >> s : p->ymin << -s;
    int y2 = (s > 0)? p->ymax >> s : p->ymax << -s;
    
    int nb = 0;
    for (int i = 0; i < *(q->nb[0]); i++)
    {
        const unsigned short xb = q->xyb[0][2*i+0];
        const unsigned short yb = q->xyb[0][2*i+1];
        if (xb >= x1 && xb <= x2 && yb >= y1 && yb <= y2)
        {
            nb++;
        } 
    }
    
    return nb;
}

// Sum of squared difference between two patch descriptors
static float descriptor_ssd(const xsc::udot::udot *u, const xsc::udot::patch *p1, const xsc::udot::patch *p2)
{
    const float *d1 = p1->descriptor;
    const float *d2 = p2->descriptor;

    float s = 0;

    for (int i = 0; i < xsc::udot::DESCRIPTOR_LENGTH; i++)
    {
        float e = d1[i] - d2[i];
        s += u->weights[i] * e * e;
    }

    return s;
}

// Create a descriptor from an image patch
static void init_descriptor(xsc::udot::udot *u, xsc::udot::patch *p, const xsc::image::level *i)
{
    float *d = p->descriptor;

#if 0
    // Descriptor is (DESCRIPTOR_WIDTH,DESCRIPTOR_HEIGHT) equal-spaced grid
    //  sampling of intensity and gradients within bounds of patch.
    const float *ptr = i->i[0] + p->xmin + p->ymin * i->hs;
    for (int y = 0; y < xsc::udot::DESCRIPTOR_HEIGHT; y+=2)
    {
        for (int x = 0; x < xsc::udot::DESCRIPTOR_WIDTH; x+=2)
        {
            const int o = y * xsc::udot::DESCRIPTOR_WIDTH + x;
            const int j = u->lookup[o];
            const int k = u->lookup[o + 1];
            
            d[o+1] = ptr[k];
            
            const float xd = ptr[k] - ptr[j];
            //d[o] = MIN(MAX((xd/2.0 + 0.5), 0.f), 1.f);
            d[o] = fabsf(xd);
        }
    }
    
    for (int y = 1; y < xsc::udot::DESCRIPTOR_HEIGHT; y+=2)
    {
        for (int x = 0; x < xsc::udot::DESCRIPTOR_WIDTH; x+=2)
        {
            const int o = y * xsc::udot::DESCRIPTOR_WIDTH + x;
            const int j = u->lookup[o];
            const int l = u->lookup[o - xsc::udot::DESCRIPTOR_WIDTH];
            
            d[o] = ptr[j];
            
            const float yd = ptr[l] - ptr[j];
                
            //d[o+1] = MIN(MAX((yd/2.0 + 0.5), 0.f), 1.f);
            d[o+1] = fabsf(yd);
        }
    }
#else
    const float *ptr = i->i[0] + p->xmin + p->ymin * i->hs;
    for (int y = 0; y < xsc::udot::DESCRIPTOR_HEIGHT; y++)
    {
        for (int x = 0; x < xsc::udot::DESCRIPTOR_WIDTH; x++)
        {
            const int j = y * xsc::udot::DESCRIPTOR_WIDTH + x;
            const float l = ptr[u->lookup[j]];            
            
            d[0] = l;
            d++;
        }
    }
#endif

}

// LP_FILT-style blend descriptors p1 and p2 with N = 1 - xsc::learning_rate.
static void update_descriptor(xsc::udot::udot* const u, xsc::udot::patch* const p1, const xsc::udot::patch* const p2)
{
    float *d1 = p1->descriptor;
    const float *d2 = p2->descriptor;
        
    for (int i = 0; i < xsc::udot::DESCRIPTOR_LENGTH; i++)
    {
        const float lr = u->learning_rate * (u->l_weights[i]);
        d1[i] = d1[i] * (1.f - lr) + d2[i] * lr;
    }
}

// Thread arguments
typedef struct find_params 
{
    const xsc::udot::patch *p;
    const xsc::image::level *i;
    xsc::udot::patch *patches;
    int x_min;
    int y_min;
    int x_max;
    int y_max;
    int result;
    xsc::udot::udot *u;
    int use_mask;
    int mask_offset;
} find_params_t;

// Thread function to find a patch p in image i within specified range
void *tf_find(void *ctx)
{
    find_params_t *params = (find_params_t*)ctx;
    xsc::udot::udot *u = params->u;

    const int mask_offset = params->mask_offset;
    int best_k = mask_offset;
    float best_s = 1e6;
    
    // Top-corner of search window (x,y) covers 1-radius search area
    for (int y = params->y_min; y < params->y_max; y++)
    {
        for (int x = params->x_min; x < params->x_max; x++)
        {
            int xmin = x;
            int ymin = y;
            int xmax = x + params->p->w;
            int ymax = y + params->p->h;
            const int k = mask_offset + (x - params->x_min) + (y - params->y_min) * (2*u->max_radius+1);
            
            xsc::udot::patch *q = params->patches + k;
            if (params->use_mask && u->mask[k] == xsc::udot::MASK_DONE) 
            {
                // This patch has already been processed
            }
            else if (xmin < 0 || ymin < 0 || params->i->w <= xmax || params->i->h <= ymax)
            {
                // This patch is outside the image
                q->ssd = 1e6;
            }
            else
            {
                // Create patch q (at patches[j]) descriptor at (x,y) with same size and compare to patch p
                q->w = params->p->w;
                q->h = params->p->h;
                q->xmin = xmin;
                q->ymin = ymin;
                q->xmax = xmax;
                q->ymax = ymax;
                init_descriptor(u, q, params->i);
                q->ssd = descriptor_ssd(u, q, params->p);
            }

            if (params->use_mask)
            {
                u->mask[k] = xsc::udot::MASK_DONE;
            }

            if (q->ssd < best_s)
            {
                best_k = k;
                best_s = q->ssd;
            }
        }
    }

    params->result = best_k; // Best patch id

    return 0;
}

// Find a patch p in image i within a radius
static const xsc::udot::patch *find(const xsc::udot::udot *u, 
                                    const xsc::udot::patch *p, 
                                    const xsc::image::level *i, 
                                    xsc::udot::patch *patches[], 
                                    const int radius, 
                                    const int use_mask)
{
    pthread_t t_finds[xsc::udot::TOTAL_THREADS];
    find_params_t params[xsc::udot::TOTAL_THREADS];
    
    const int w  = 2*radius + 1;
    const int h  = 2*radius + 1;
    const int tw = w / xsc::udot::NUM_THREADS;
    const int th = h / xsc::udot::NUM_THREADS;

    for (int y = 0; y < xsc::udot::NUM_THREADS; y++)
    {
        for (int x = 0; x < xsc::udot::NUM_THREADS; x++)
        {
            const int j = y*xsc::udot::NUM_THREADS + x;
            
            params[j].p = p;
            params[j].i = i;
            params[j].patches = patches[j];
            params[j].x_min = p->xmin - radius + tw* x;
            params[j].y_min = p->ymin - radius + th* y;
            params[j].x_max = p->xmin - radius + tw*(x+1) - 1;
            params[j].y_max = p->ymin - radius + th*(y+1) - 1;
            params[j].result = 0;
            params[j].u = u;
            params[j].use_mask = use_mask;
            params[j].mask_offset = (params[j].x_min - (p->xmin - u->max_radius))
                    + (2*u->max_radius+1)*(params[j].y_min - (p->ymin - u->max_radius));
            
            pthread_create(&t_finds[j], NULL, tf_find, &params[j]);
        }
    }

    for (int j = 0; j < xsc::udot::TOTAL_THREADS; j++)
    {
        pthread_join(t_finds[j], NULL);
    }
    
    int best_t = 0; // best thread
    int best_p = 0; // best patch
    float best_s = 1e6; // best score
    int m = 0;

    // Find the lowest of the lowest SSD and return that patch
    for (int j = 0; j < xsc::udot::TOTAL_THREADS; j++)
    {
        m = params[j].result;
        if (patches[j][m].ssd < best_s)
        {
            best_s = patches[j][m].ssd;
            best_t = j;
            best_p = m;
        }
    }

    return patches[best_t] + best_p;  // patches[best_t][best_p]

}

// Copy an image patch from the device back to the host
static void copy(xsc::udot::udot* const u, xsc::udot::patch* const p, const xsc::image::level *i)
{
    int w;
    int h;
    int s;
    float *src;
    float *dst;

    int xmin = p->xmin - u->max_radius * 2;
    int ymin = p->ymin - u->max_radius * 2;
    int xmax = p->xmax + u->max_radius * 2;
    int ymax = p->ymax + u->max_radius * 2;

    if (xmin < 0)
    {
        xmin = 0;
    }
    if (ymin < 0)
    {
        ymin = 0;
    }
    if (i->w <= xmax)
    {
        xmax = i->w - 1;
    }
    if (i->h <= ymax)
    {
        ymax = i->h - 1;
    }

    w = (xmax - xmin) * sizeof(float);
    h = (ymax - ymin);
    s = i->hs * sizeof(float);

    src = i->i[1] + xmin + ymin * i->hs;
    dst = i->i[0] + xmin + ymin * i->hs;

    xsc::udot::copy_patches(dst, s, src, w, h);
}

// Update each patch location in the new image
int xsc::udot::update_patch(xsc::udot::udot* const udot,
        const xsc::camera::level *c,
        const xsc::image::level *i,
        const float* const H,
        const int valid_H,
        const int pool_size)
{
    xsc::udot::patch* p = &(udot->patch);
    
    // Reset the search mask
    memset(udot->mask, xsc::udot::MASK_TODO, sizeof(udot->mask));
    
    // Decaying-velocity model
    p->vx *= 0.99;
    p->vy *= 0.99;
    
    // Reset the starting search radius
    p->radius = udot->radius_increment;

    // Predict where the patch will be this time around
    float s = 1 << p->scale;
    const int   xiprv = p->xmin;
    const int   yiprv = p->ymin;
    
    float xmin  =  H[0]*s*p->x + H[1]*s*p->y + H[2];
    float ymin  =  H[3]*s*p->x + H[4]*s*p->y + H[5];
    float zmin  = (H[6]*s*p->x + H[7]*s*p->y + H[8])*s;
    float xfprv = xmin/zmin;
    float yfprv = ymin/zmin;
    
    if (std::isnan(xfprv) || std::isnan(yfprv) || std::isinf(xfprv) || std::isinf(yfprv))
    {
        XIPVIS_LOG("Failed to predict new target coordinates. Using previous.");
        xfprv = p->x;
        yfprv = p->y;        
    }
    
    // Overwrite the patch location with the predicted coordinates
    p->x = xfprv + ((valid_H) ? p->vx : 0.f);
    p->y = yfprv + ((valid_H) ? p->vy : 0.f);
        
    // Effectively move patch back by 0.5 pixels by flooring instead of rounding
    // to ensure equal per pixel descriptor blending
    p->xmin = MIN(MAX((int)(p->x), p->radius), i->w - p->w - p->radius);
    p->ymin = MIN(MAX((int)(p->y), p->radius), i->h - p->h - p->radius);
    p->xmax = p->xmin + p->w;
    p->ymax = p->ymin + p->h;
    
    // Set velocity to zero if on the image boundary
    if (p->xmin <= p->radius || p->xmax >= (i->w - p->radius)
            || p->ymin <= p->radius || p->ymax >= (i->h - p->radius))
    {
        p->vx = 0.f;
        p->vy = 0.f;
    }
    
    // Find the patch p in the image i, fill in f_patches with the candidates
    copy(udot, p, i);
    
    int fpx = 0;
    int fpy = 0;
    
    int found = 0;
    const int full_searches = 4;

    const int max_radius = udot->max_radius - ((p->occluded/full_searches * udot->radius_increment) % udot->max_radius);
    while (p->radius <= max_radius)
    {
        xsc::udot::patch *fp = find(udot, p, i, udot->f_patches, p->radius, 1);
        
        // If the sum-of-squares difference (SSD) is less than the threshold            
        if (fp->ssd <= udot->max_ssd)
        {
            fpx = fp->xmin;
            fpy = fp->ymin;
            int nb = 0;
            if (p->occluded < pool_size)
            {
                // Find this patch fp in the reference image (usually previous except when lost)
                fp->xmin = xiprv;
                fp->ymin = yiprv;
                fp->xmax = fp->xmin + fp->w;
                fp->ymax = fp->ymin + fp->h;
                const xsc::udot::patch *bp = find(udot, fp, p->reference, udot->b_patches, udot->radius_increment, 0);
                // If the distance to the patch in the current and reference image is too large
                // (Forward/backward pass consistency, checks 1-to-1)
                if ((abs(yiprv - bp->ymin) <= udot->max_fb_error) && (abs(xiprv - bp->xmin) <= udot->max_fb_error))
                {
                    found = 1;
                }
                // Motion consistency check               
                else if (nb = correlate_motion(i, c, p))
                {
                    found = 1;
                }

                // Put the fp patch back together
                fp->xmin = fpx;
                fp->ymin = fpy;
                fp->xmax = fp->xmin + fp->w;
                fp->ymax = fp->ymin + fp->h;
            }
            else
            {
                // Assign the best SDD that is less than the threshold since forward/back checks are no longer valid
                found = 1;
            }
        }
        
        if (found)
        {
            update_descriptor(udot, p, fp);
            break;
        }
        
        // Increase the search radius for next time
        p->radius += udot->radius_increment;
    }
    
    if (!found)
    {
        // Not found in this image
        p->occluded++;
        
        // Restore the old target location in previous frame but carry the velocity
        // Note: Instead of assigning back xiprv and yiprv it makes more sense to
        // assign back the predicted float value. The box coordinates need to be 
        // updated to match the last predicted x,y.
        p->xmin = p->x;
        p->ymin = p->y;
        p->xmax = p->xmin + p->w;
        p->ymax = p->ymin + p->h;
    }
    else
    {
        // Update the patch with the most recent observation
        p->x = fpx;
        p->y = fpy;
        
        if(!p->occluded && valid_H)
        {
            LP_FILT(p->vx, p->x - xfprv, udot->velocity_filter);
            LP_FILT(p->vy, p->y - yfprv, udot->velocity_filter);
        }
        
        p->xmin = fpx;
        p->ymin = fpy;
        p->xmax = p->xmin + p->w;
        p->ymax = p->ymin + p->h;
        p->occluded = 0;
        p->reference = i;        
    }  
    
    return 0;
}

// Transfer patch location into the new image
int xsc::udot::transfer_patch(xsc::udot::udot* const u, const xsc::image::level *i)
{
    xsc::udot::patch* p = &(u->patch);

    // Zero out velocities
    p->vx = 0.f;
    p->vy = 0.f;
    
    // Effectively move patch back by 0.5 pixels by flooring instead of rounding
    // to ensure equal per pixel descriptor blending
    p->xmin = MIN(MAX((int)(p->x), p->radius), i->w - p->w - p->radius);
    p->ymin = MIN(MAX((int)(p->y), p->radius), i->h - p->h - p->radius);
    p->xmax = p->xmin + p->w;
    p->ymax = p->ymin + p->h;
    
    p->radius = u->radius_increment;
    p->reference = i;
    p->occluded = 0;
    
    // Copy the image patch back from the device and create new descriptor
    copy(u, p, i);
    init_descriptor(u, p, i);
    
    return 0;
}

int xsc::udot::init(xsc::udot::udot* const u)
{
    for (int i = 0; i < xsc::udot::TOTAL_THREADS; i++)
    {
        if ((u->f_patches[i] = new(std::nothrow) patch[xsc::udot::MAX_NUM_SEARCHES]) == nullptr)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        if ((u->b_patches[i] = new(std::nothrow) patch[xsc::udot::MAX_NUM_SEARCHES]) == nullptr)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }

    // 2D Gaussian weighting matrix for patch matching
    float std = 10.f;
    float wsum = 0.f;
    for (int y = 0; y < xsc::udot::DESCRIPTOR_HEIGHT; y++)
    {
        for (int x = 0; x < xsc::udot::DESCRIPTOR_WIDTH; x++)
        {
            const float dx = (float)x - (xsc::udot::DESCRIPTOR_WIDTH-1) /2.f;
            const float dy = (float)y - (xsc::udot::DESCRIPTOR_HEIGHT-1) /2.f;
            u->weights[x+y*xsc::udot::DESCRIPTOR_WIDTH] = exp(-(float) (dx * dx)
                    / (std*std)) * exp(-(float) (dy * dy) / (std*std));
            wsum += u->weights[x+y*xsc::udot::DESCRIPTOR_WIDTH];
        }
    }
    
    wsum = MAX(wsum, 0.0001);
    for (int i = 0; i < xsc::udot::DESCRIPTOR_LENGTH; i++)
    {
        u->weights[i] /= wsum;
    }

    // Reversed 2D Gaussian weighting matrix for learning background faster
    std = 14.f; // To get center weighting ~ 0.5
    for (int y = 0; y < xsc::udot::DESCRIPTOR_HEIGHT; y++)
    {
        for (int x = 0; x < xsc::udot::DESCRIPTOR_WIDTH; x++)
        {
            const float dx = abs((float)x - (xsc::udot::DESCRIPTOR_WIDTH-1) / 2.f)
                    - (xsc::udot::DESCRIPTOR_WIDTH-1) / 2.f;
            const float dy = abs((float)y - (xsc::udot::DESCRIPTOR_HEIGHT-1) / 2.f)
                    - (xsc::udot::DESCRIPTOR_HEIGHT-1) / 2.f;
            u->l_weights[x+y*xsc::udot::DESCRIPTOR_WIDTH] = exp(-(float) (dx * dx)
                    / (std*std)) * exp(-(float) (dy * dy) / (std*std));
        }
    }
    
    return 0;
}

// Initialize a UDOT tracking patch
int xsc::udot::init_patch(xsc::udot::udot* const u, int xmin, int ymin, int xmax, int ymax, const xsc::image::level *i)
{
    xsc::udot::patch* const p = &(u->patch);
    
    // Make sure that the patch is within the image
    if (xmin < 0 || ymin < 0 || i->w <= xmax || i->h <= ymax)
    {
        return -1;
    }

    p->w = xmax - xmin;
    p->h = ymax - ymin;

    p->vx = 0.f;
    p->vy = 0.f;
    p->x    = xmin;
    p->y    = ymin;
    p->xmin = xmin;
    p->ymin = ymin;
    p->xmax = xmax;
    p->ymax = ymax;
    p->radius = u->radius_increment;
    p->occluded = 0;
    p->reference = i;

    // Generate look-up for evenly sampling descriptors    
    const float xs = (float)p->w / xsc::udot::DESCRIPTOR_WIDTH;
    const float ys = (float)p->h / xsc::udot::DESCRIPTOR_HEIGHT;
    for (int y = 0; y < xsc::udot::DESCRIPTOR_HEIGHT; y++)
    {
        for (int x = 0; x < xsc::udot::DESCRIPTOR_WIDTH; x++)
        {
            const int j = x + y*xsc::udot::DESCRIPTOR_WIDTH;
            u->lookup[j] = (int)(xs*(float)x + i->hs * (int)(ys*(float)y));
        }
    }
    
    // Copy the image patch back from the device and create descriptor
    copy(u, p, i);
    init_descriptor(u, p, i);

    return 0;
}

// Update the UDOT target according to its efficient pyramid level
int xsc::udot::update_target(xsc::udot::udot* const u, 
        const xsc::camera::level *c, 
        const xsc::image::level *i, 
        const float* const H, 
        const int valid_H, 
        const int pool_size)
{
    return xsc::udot::update_patch(u, c, i + u->patch.scale, H, valid_H, pool_size);
}

// Transfer the UDOT target to the new image according to its efficient pyramid level
int xsc::udot::transfer_target(xsc::udot::udot* const u, const xsc::image::level *i)
{
    return xsc::udot::transfer_patch(u, i + u->patch.scale);
}

int xsc::udot::free(xsc::udot::udot* const u)
{
    for (int i = 0; i < xsc::udot::TOTAL_THREADS; i++)
    {
        delete [] u->f_patches[i];
        delete [] u->b_patches[i];
        
        u->f_patches[i] = nullptr;
        u->b_patches[i] = nullptr;
    }
    
    return 0;
}

int xsc::udot::set_learning_rate(xsc::udot::udot* const u, const float rate)
{
    u->learning_rate = rate;
    BOUND_VARIABLE(u->learning_rate, LEARNING_RATE_MIN, LEARNING_RATE_MAX);
    
    return 0;
}

float xsc::udot::get_learning_rate(xsc::udot::udot* const u)
{
    return u->learning_rate;
}

int xsc::udot::set_max_radius(xsc::udot::udot* const u, int radius)
{
    u->max_radius = radius;
    BOUND_VARIABLE(u->max_radius, MAX_RADIUS_MIN, MAX_RADIUS_MAX);
    
    return 0;
}

int xsc::udot::get_max_radius(xsc::udot::udot* const u)
{
    return u->max_radius;
}

int xsc::udot::set_radius_increment(xsc::udot::udot* const u, const int inc)
{
    u->radius_increment = inc;
    BOUND_VARIABLE(u->radius_increment, RADIUS_INCREMENT_MIN, RADIUS_INCREMENT_MAX);
    
    return 0;
}

int xsc::udot::get_radius_increment(xsc::udot::udot* const u)
{
    return u->radius_increment;
}

int xsc::udot::set_max_fb_error(xsc::udot::udot* const u, const int error)
{
    u->max_fb_error = error;
    BOUND_VARIABLE(u->max_fb_error, MAX_FB_ERROR_MIN, MAX_FB_ERROR_MAX);
    
    return 0;
}

int xsc::udot::get_max_fb_error(xsc::udot::udot* const u)
{
    return u->max_fb_error;
}

int xsc::udot::set_velocity_filter(xsc::udot::udot* const u, const float filt)
{
    u->velocity_filter = filt;
    BOUND_VARIABLE(u->velocity_filter, VELOCITY_FILTER_MIN, VELOCITY_FILTER_MAX);
    
    return 0;
}

float xsc::udot::get_velocity_filter(xsc::udot::udot* const u)
{
    return u->velocity_filter;
}

int xsc::udot::set_max_ssd(xsc::udot::udot* const u, const float max)
{
    u->max_ssd = max;
    BOUND_VARIABLE(u->max_ssd, MAX_SSD_MIN, MAX_SSD_MAX);
    
    return 0;
}

float xsc::udot::get_max_ssd(xsc::udot::udot* const u)
{
    return u->max_ssd;
}


