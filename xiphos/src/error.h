/* Copyright (C) 2015 Xiphos Systems Corporation
 */

#ifndef ERROR_H
#define ERROR_H

#ifndef RELEASE
#define XIPVIS_ERROR(errno_, dtor_, rc_) \
{ \
    fprintf(stderr, "%s, %d: %s\n", __FILE__, __LINE__, strerror(errno_)); \
    errno = errno_; \
    dtor_; \
    return rc_; \
}
#else
#define XIPVIS_ERROR(errno_, dtor_, rc_) \
{ \
    tracef(TRACE_ERROR, "%s, %s, %d: %s\n", __FILE__, __func__, __LINE__, strerror(errno_)); \
    return rc_; \
}
#endif

#ifndef RELEASE
#define XIPVIS_LOG(format, ...) \
{ \
    fprintf(stderr, "%s, %s, %d: " format "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__); \
}
#else
#define XIPVIS_LOG(format, ...) \
{ \
    tracef(TRACE_WARN, "%s, %s, %d: " format "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__); \
}
#endif

#endif /* #ifndef ERROR_H */
