/**
 * @file xipvis.h
 *
 * @brief The xipvis API.
 *
 * @license Copyright (C) 2015 Xiphos Systems Corporation
 */

#ifndef XIPVIS_H
#define XIPVIS_H

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief Object bounding box
 */
struct xipvis_bb
{
    /**
     * Unique bounding box ID.
     */
    int id;

    /**
     * Each bounding box is associated with a particular image identified by
     * this ID.
     */
    int image_id;

    /**
     * Left edge of the bounding box in units of pixels.
     */
    int xmin;

    /**
     * Top edge of the bounding box in units of pixels.
     */
    int ymin;

    /**
     * Right edge of the bounding box in units of pixels.
     */
    int xmax;

    /**
     * Bottom edge of the bounding box in units of pixels.
     */
    int ymax;

    /**
     * The center x position of the un-warped bounding box in units of pixels
     */
    int x_unwarped;
    
    /**
     * The center y position of the un-warped bounding box in units of pixels
     */
    int y_unwarped;
    
    /**
     * Trackers use this to report object track quality.
     */
    float confidence;
};

typedef struct xipvis_bb xipvis_bb;

typedef enum
{
    I420 = 0,
    UYVY,        
    YUY2
} format_type;

typedef enum
{
    EO = 0,
    IR
} sensor_type;

typedef enum
{
    UDOT_NORMAL = 0,
    UDOT_SKIP,
    UDOT_RETRACK
} udot_flag;

/**
 * @brief Camera parameters.
 */
struct xipvis_cam
{
    /**
     * Visible width of images produced by the camera in units of pixels.
     */
    int width;

    /**
     * Visible height of images produced by the camera in units of pixels.
     */
    int height;

    /**
     * Horizontal coordinate of the camera's principal point in units of pixels.
     */
    float centre_x;

    /**
     * Vertical coordinate of the camera's principal point in units of pixels.
     */
    float centre_y;

    /**
     * Camera focal length in units of mm.
     */
    float flen;

    /**
     * Camera pixel pitch in units of mm.
     */
    float pixel_pitch;
    
    /**
     * Camera source fps
     */
    float fps;

    /**
     * Camera colourspace format
     */
    format_type format;
    
    /**
     * Camera sensor type.
     */
    sensor_type sensor;    
};

typedef struct xipvis_cam xipvis_cam;

/**
 * @brief Initialise xipvis.
 *
 * @param ctx[out] xipvis context.
 * @param cam[in] Parameters describing the camera providing xipvis input.
 * @param index[in] The texture buffer index to use
 * @param pool_size[in] The size of the image pool buffer
 * @return 0 upon success, -1 otherwise and errno is set.
 */
int xipvis_init(void **ctx, const struct xipvis_cam *cam, const int index, const int pool_size);

/**
 * @brief Deallocate resources consumed by xipvis and stop xipvis processing.
 *
 * @param ctx[in] xipvis context.
 * @return 0 upon success, -1 otherwise and errno is set.
 */
int xipvis_free(void *ctx);

/**
 * @brief Process an image and obtain tracking results.
 *
 * xipvis assigns a unique ID to each processed image. The ID is returned to
 * the application via the id parameter.
 *
 * @param ctx[in] xipvis context.
 * @param threshold[in] Feature detection threshold.
 * @param dbb[in] Draw boxes if not zero.
 * @param buf[in] Input image pixel buffer.
 * @param stride[in] Input image pixel buffer stride in units of bytes.
 * @param id[out] Unique ID assigned to the input image.
 * @param num[out] Number of bounding boxes updated by xipvis according to the input image.
 * @param bbs[out] Bounding boxes updated by xipvis according to the input image.
 * @param outbuf[out] Output image pixel buffer
 * @param udotflag[out] udot_flag enumeration for behaviour
 */
int xipvis_update(void *ctx, int dbb, uint8_t *buf, int stride, int *id, int *num, struct xipvis_bb **bbs, uint8_t *outbuf, int udotflag);

/**
 * Start UDOT.
 *
 * The application provides an initial object bounding box via the bb parameter.
 * The bounding box xmin, ymin, xmax and ymax parameters are supplied by the
 * application. The bounding box id is populated by xipvis and is available to
 * the application upon return. Tracking starts at the image specified by the id
 * parameter.
 *
 * @param ctx[in] xipvis context.
 * @param id[in] ID of the initial tracking image.
 * @param nowarp[in] 1 to bypass warping of the target coordinates
 * @param bb[in/out] Initial object bounding box.
 * @return 0 upon success, -1 otherwise and errno is set.
 */
int xipvis_udot_start_track(void* const ctx, int id, int nowarp, struct xipvis_bb *bb);


/**
 * Stop UDOT.
 *
 * @param ptr[in] Pointer to xipvis context.
 * @return 0 upon success, -1 otherwise and errno is set.
 */
int xipvis_udot_stop_track(void* const ctx);

int xipvis_update_csrt(void *ctx, uint8_t *inbuf, int stride, int id, struct xipvis_bb bb, const float* const H, const int valid_H);
int xipvis_csrt_start_track(void *ctx, int id, int nowarp, struct xipvis_bb *bb, uint8_t *inbuf, int stride /*, const float* const H, const int valid_H*/);
int xipvis_csrt_stop_track(void* const ctx);

int xipvis_update_kcft(void *ctx, uint8_t *inbuf, int stride, int id, struct xipvis_bb bb, const float* const H, const int valid_H);
int xipvis_kcft_start_track(void *ctx, int id, int nowarp, struct xipvis_bb *bb, uint8_t *inbuf, int stride /*, const float* const H, const int valid_H*/);
int xipvis_kcft_stop_track(void* const ctx);

/**
 * Print image stabilization solution to file.
 *
 * @param ctx[in] xipvis context.
 * @param fd[in] File descriptor to print the solution.
 * @param id[in] ID of the current image.
 * @return 0 upon success, -1 otherwise.
 */
int xipvis_print_stabilization_solution(void* const ctx, FILE* fd, int id);

         int xipvis_enable_egomotion(void* const x);
         int xipvis_disable_egomotion(void* const x);
unsigned int xipvis_egomotion_enabled(void* const x);

         int xipvis_enable_udot(void* const x);
         int xipvis_disable_udot(void* const x);
unsigned int xipvis_udot_enabled(void* const x);

         int xipvis_enable_mti(void* const x);
         int xipvis_disable_mti(void* const x);
unsigned int xipvis_mti_enabled(void* const x);

         int xipvis_enable_overlays(void* const x);
         int xipvis_disable_overlays(void* const x);
unsigned int xipvis_overlays_enabled(void* const x);

         int xipvis_enable_stabilization(void* const x);
         int xipvis_disable_stabilization(void* const x);
unsigned int xipvis_stabilization_enabled(void* const x);

/**
 * Set Parameters
 * 
 * @param size
 * @return 
 */
int   xipvis_udot_set_learning_rate(void* const x, const float rate);
float xipvis_udot_get_learning_rate(void* const x);
int   xipvis_udot_set_radius_increment(void* const x, const int inc);
int   xipvis_udot_get_radius_increment(void* const x);
int   xipvis_udot_set_max_radius(void* const x, const int radius);
int   xipvis_udot_get_max_radius(void* const x);
int   xipvis_udot_set_max_fb_error(void* const x, const int error);
int   xipvis_udot_get_max_fb_error(void* const x);
int   xipvis_udot_set_velocity_filter(void* const x, const float filt);
float xipvis_udot_get_velocity_filter(void* const x);
int   xipvis_udot_set_max_ssd(void* const x, const float max);
float xipvis_udot_get_max_ssd(void* const x);

int   xipvis_mti_set_min_age(void* const x, const int age);
int   xipvis_mti_get_min_age(void* const x);
int   xipvis_mti_set_max_age(void* const x, const int age);
int   xipvis_mti_get_max_age(void* const x);
int   xipvis_mti_set_max_speed(void* const x, const float speed);
float xipvis_mti_get_max_speed(void* const x);
int   xipvis_mti_set_min_radius(void* const x, const float radius);
float xipvis_mti_get_min_radius(void* const x);
int   xipvis_mti_set_filter_rate(void* const x, const float rate);
float xipvis_mti_get_filter_rate(void* const x);
int   xipvis_mti_set_decay_rate(void* const x, const float rate);
float xipvis_mti_get_decay_rate(void* const x);
int   xipvis_mti_set_sweep_size(void* const x, const float size);
float xipvis_mti_get_sweep_size(void* const x);
int   xipvis_mti_set_min_box_size(void* const x, const float size);
float xipvis_mti_get_min_box_size(void* const x);
int   xipvis_mti_set_blanking(void* const x, const int size);
int   xipvis_mti_get_blanking(void* const x);
int   xipvis_mti_set_max_rotation(void* const x, const float r);
float xipvis_mti_get_max_rotation(void* const x);
int   xipvis_mti_set_max_translation(void* const x, const float t);
float xipvis_mti_get_max_translation(void* const x);

int   xipvis_stbl_set_split_screen(void* const x, const int split);
int   xipvis_stbl_get_split_screen(void* const x);
float xipvis_stbl_get_scale(void* const x);
float xipvis_stbl_get_marginal_scale(void* const x);

void xipvis_device_reset(void* const x);

#ifdef __cplusplus
}
#endif

#endif /* #ifndef XIPVIS_H */
