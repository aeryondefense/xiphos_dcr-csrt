/* Copyright (C) 2016 Xiphos Systems Corporation
 */

#ifndef RENDERER_H
#define RENDERER_H

namespace xsc
{
struct renderer
{
    int w;
    int h;
    int s;
    uint8_t *ptr;

    renderer(void);
    renderer(const renderer &) = delete;
    renderer &operator = (const renderer &) = delete;
    virtual ~renderer(void);
};

static constexpr uint8_t WHITE[3]  = {235, 128, 128};
static constexpr uint8_t GREEN[9]  = {145,  54,  34};
static constexpr uint8_t INDIGO[9] = {138, 184, 119}; 
static constexpr uint8_t ORANGE[9] = {159,  46, 184};
static constexpr uint8_t RED[9]    = { 82,  90, 240};

int init(renderer *r, uint8_t *buffer, int width, int height, int stride);

int free(renderer *r);

int circ_yuv420(renderer *r, int x, int y, int rad, uint8_t val);

int circ_yuy422(renderer *r, int x, int y, int rad, uint8_t val);

int line_yuv420(renderer *r, int x0, int y0, int x1, int y1, uint8_t *vals);

int line_yuy422(renderer *r, int x0, int y0, int x1, int y1, uint8_t *vals);

int rect_yuv420(renderer *r, int xmin, int ymin, int xmax, int ymax, int lw, const uint8_t *vals);

int rect_yuy422(renderer *r, int xmin, int ymin, int xmax, int ymax, uint8_t *vals);

int cross_yuv420(renderer *r, int x, int y, int radius, int lw, const uint8_t *vals);

int cross_yuy422(renderer *r, int x, int y, int radius, uint8_t *vals);
}

#endif /* #ifndef RENDERER_H */
