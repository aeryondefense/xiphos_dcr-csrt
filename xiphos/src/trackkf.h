#ifndef __TRACKKF_H__
#define __TRACKKF_H__

#include <Dense>

// #define TRACKKF_DEBUG

namespace Eigen {
    typedef Eigen::Matrix<float, 6, 6> Matrix6f;
    typedef Eigen::Matrix<float, 6, 1> Vector6f;
}

using Eigen::Matrix;
using Eigen::Matrix6f;
using Eigen::Vector6f;


namespace xsc
{
namespace udot
{
    // m_Q process covariance default diagonal values
    static constexpr float q00 = 1.0f;
    static constexpr float q11 = 5.0f;
    static constexpr float q22 = 25.0f;
    static constexpr float q33 = 1.0f;
    static constexpr float q44 = 5.0f;
    static constexpr float q55 = 25.0f;

    // m_R measurement covariance default diagonal values
    static constexpr float r_tgt_pos = 3.0f;
    static constexpr float r_cam_vel = 5.0f;

    // m_P state error covariance default diagonal values
    static constexpr float p00 = 1.0f;
    static constexpr float p11 = 100.0f;
    static constexpr float p22 = 1000.0f;
    static constexpr float p33 = 1.0f;
    static constexpr float p44 = 100.0f;
    static constexpr float p55 = 1000.0f;

    static constexpr float acc_decay_rate = 0.95f;

class KF1Dof
{
protected:
    static constexpr unsigned int tgt_pos_idx = 0;
    static constexpr unsigned int cam_vel_idx = 4;
    static constexpr float dt_min = 0.0000001;

    Matrix6f m_I;            //! Identity matrix
    Matrix6f m_A;            //! System dynamics/state transition matrix
    Matrix6f m_A_T;          //! System dynamics matrix transpose
    Matrix<float, 1, 6> m_H_tgt_pos;    //! Target position observation model 
    Vector6f m_H_tgt_pos_T;  //! Target position observation model transpose
    Matrix<float, 1, 6> m_H_cam_vel;    //! Camera velocity observation model 
    Vector6f m_H_cam_vel_T;  //! Camera velocity observation model transpose
    Matrix6f m_Q;            //! Process noise covariance
    Matrix6f m_P;            //! system state error covariance
    Vector6f m_K;            //! Preallocated Kalman gain matrix
    float m_r_tgt_pos;                  //! Measurement noise covariance
    float m_r_cam_vel;                  //! Measurement noise covariance
    Vector6f m_xhat;         //! System state

    bool m_user_defaults;               //! Use user defaults flag
    bool m_initialized;                 //! Initialization flag
    int m_step;                         //! Step count
    float m_dt;                         //! Time step

public:

    KF1Dof() :
        m_initialized{false},
        m_user_defaults{false}
    {}

    virtual ~KF1Dof() {}

    int init(float dt, int step, float tgt_pos, float cam_vel,
            Matrix6f P0, Matrix6f Q0, float r_tgt_pos_0, float r_cam_vel_0)
    {
        m_user_defaults = true;

        m_Q = Q0;
        m_r_tgt_pos = r_tgt_pos_0;
        m_r_cam_vel = r_cam_vel_0;
        m_P = P0;

        return init(dt, step, tgt_pos, cam_vel);
    }

    int init(float dt, int step, float tgt_pos, float cam_vel)
    {
        int rc = 0;

        m_step = step;
        m_I = Matrix6f::Identity();

        // Initialize system states
        m_xhat << tgt_pos, 0.0, 0.0, 0.0, cam_vel, 0.0;

        // Set m_dt, and state transition matrix (i.e. m_A, m_A_T)
        if (set_dt(dt))
        {
            return -1;
        }

        // Target position observational model
        m_H_tgt_pos << 1.0, 0.0, 0.0, 0.0, 0.0, 0.0;
        m_H_tgt_pos_T = m_H_tgt_pos.transpose(); // Precompute the tranpose

        // Camera velocity observational model
        m_H_cam_vel << 0.0, 0.0, 0.0, 0.0, 1.0, 0.0;
        m_H_cam_vel_T = m_H_cam_vel.transpose(); // Precompute the tranpose

        if (!m_user_defaults)
        {
            // Process covariance
            Vector6f q_diag;
            q_diag << q00, q11, q22, q33, q44, q55;
            m_Q = q_diag.asDiagonal();

            // Measurement variance
            m_r_tgt_pos = r_tgt_pos;
            m_r_cam_vel = r_cam_vel;

            // State covariance
            Vector6f P_diag;
            P_diag << p00, p11, p22, p33, p44, p55;
            m_P = P_diag.asDiagonal();
        }

#ifdef TRACKKF_DEBUG
        std::cerr << __PRETTY_FUNCTION__  << ": step=" << m_step << "\n";
        std::cerr << "r_tgt_pos: " << m_r_tgt_pos << "\n";
        std::cerr << "r_cam_vel: " << m_r_cam_vel << "\n";
        std::cerr << "Q:\n"    << m_Q << "\n";
        std::cerr << "xhat:\n" << m_xhat << "\n";
        std::cerr << "A:\n"    << m_A << "\n";
        std::cerr << "P:\n"    << m_P << "\n";
#endif

        // Validate initialized values
        if ((m_P.array() < 0.f).any()) return -1;
        if ((m_Q.array() < 0.f).any()) return -1;
        if (m_r_tgt_pos < 0.f) return -1;
        if (m_r_cam_vel < 0.f) return -1;

        m_initialized = true;

        return rc;
    }

    int predict(int step) 
    {
        if (!m_initialized) return -1;

        // Ensure filter isn't running backward
        if (step < m_step) return -1;

        while (m_step < step)
        {
            // Predict xhat_x
            m_xhat = m_A * m_xhat;
            m_P = m_A * m_P * m_A_T + m_Q;
            m_step++;

#ifdef TRACKKF_DEBUG
            std::cerr << __PRETTY_FUNCTION__ << ": step=" << m_step << "\n";
            std::cerr << "xhat:\n" << m_xhat << "\n";
            std::cerr << "P:\n" << m_P << "\n";
#endif
        }
        
        return 0;
    }

    int update_target_pos(int step, float tgt_pos)
    {
        return update_target_pos(step, tgt_pos, m_r_tgt_pos);
    }

    int update_target_pos(int step, float tgt_pos, float r_tgt_pos)
    {
        if (!m_initialized) return -1;
        if (step < m_step) return -1; // Don't run backwards

        if (predict(step))
        {
            return -1;
        }

        m_r_tgt_pos = r_tgt_pos;

        // Update/correct xhat_x with a measurement
        // Naive kalman gain computation
        // m_K = m_P * m_H_tgt_pos_T / (m_H_tgt_pos * m_P * m_H_tgt_pos_T + m_r_tgt_pos);
        // Optimized kalman gain computation
        m_K = m_P.block<6,1>(0,tgt_pos_idx) / (m_P(tgt_pos_idx,tgt_pos_idx) + m_r_tgt_pos);

        // Naive state update implementation
        // m_xhat = m_xhat + m_K * (tgt_pos - m_H_tgt_pos * m_xhat);
        // Optimized state update implementation
        m_xhat = m_xhat + m_K * (tgt_pos - m_xhat(tgt_pos_idx));

        // Update state error covariance
        m_P = (m_I - m_K * m_H_tgt_pos) * m_P;

#ifdef TRACKKF_DEBUG
        std::cerr << __PRETTY_FUNCTION__ << ": step=" << m_step << "\n";
        std::cerr << "tgt_pos: " << tgt_pos << "\n";
        std::cerr << "xhat:\n" << m_xhat << "\n";
        std::cerr << "P:\n"    << m_P << "\n";
        std::cerr << "K:\n"    << m_K << "\n";
#endif

        return 0;
    }

    int update_camera_vel(int step, float cam_vel)
    {
        return update_camera_vel(step, cam_vel, m_r_cam_vel);
    }

    int update_camera_vel(int step, float cam_vel, float r_cam_vel)
    {
        if (!m_initialized) return -1;
        if (step < m_step) return -1; // Don't run backwards

        if (predict(step))
        {
            return -1;
        }

        m_r_cam_vel = r_cam_vel;

        // Update/correct xhat_x with a measurement
        // Naive kalman gain computation
        // m_K = m_P * m_H_cam_vel_T / (m_H_cam_vel * m_P * m_H_cam_vel_T + m_r_cam_vel);
        // Optimized kalman gain computation
        m_K = m_P.block<6,1>(0,cam_vel_idx) / (m_P(cam_vel_idx,cam_vel_idx) + m_r_cam_vel);

        // Naive state update implementation
        // m_xhat = m_xhat + m_K * (cam_vel - m_H_cam_vel * m_xhat); 
        // Optimized state update implementation
        m_xhat = m_xhat + m_K * (cam_vel - m_xhat(cam_vel_idx));

        // Update state error covariance
        m_P = (m_I - m_K * m_H_cam_vel) * m_P;

#ifdef TRACKKF_DEBUG
        std::cerr << __PRETTY_FUNCTION__ << ": step=" << m_step << "\n";
        std::cerr << "cam_vel: " << cam_vel << "\n";
        std::cerr << "xhat:\n" << m_xhat << "\n";
        std::cerr << "P:\n"    << m_P << "\n";
        std::cerr << "K:\n"    << m_K << "\n";
#endif

        return 0;
    }

    int get_step()
    {
        return m_step;
    }

    void get_states(Vector6f &xhat)
    {
        xhat = m_xhat;
    }

    void get_state_position(float &x)
    {
        x = m_xhat(0);
    }

    void get_state_error_covariance(Matrix6f &P)
    {
        P = m_P;
    }

    float get_dt()
    {
        return m_dt;
    }

    int set_dt(float dt)
    {
        if (dt < dt_min)
        {
            return -1;
        }

        m_dt = dt;
        // State transition matrix
        m_A << 1.0,  dt, 0.0, 0.0,  dt, 0.0,
               0.0, 1.0,  dt, 0.0, 0.0,  dt,
               0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
               0.0, 0.0, 0.0, 1.0,  dt, 0.0,
               0.0, 0.0, 0.0, 0.0, 1.0,  dt,
               0.0, 0.0, 0.0, 0.0, 0.0, 1.0;

        // Decaying acceleration model
        m_A(2,2) = acc_decay_rate;
        m_A(5,5) = acc_decay_rate;

        m_A_T = m_A.transpose(); // Precompute the transpose

        return 0;
    }

};

class TrackKF {
protected:
    KF1Dof m_kf_x;
    KF1Dof m_kf_y;
    bool m_initialized;

public:
    TrackKF() :
        m_initialized{false}
    {}

    virtual ~TrackKF() {}

    int init(float dt, int step, float tgt_pos_x, float tgt_pos_y, float cam_vel_x, float cam_vel_y,
            Matrix6f P0, Matrix6f Q0, float r_tgt_pos0, float r_cam_vel0)
    {
        int rc = 0;
        if (rc = m_kf_x.init(dt, step, tgt_pos_x, cam_vel_x, P0, Q0, r_tgt_pos0, r_cam_vel0)) return rc;
        if (rc = m_kf_y.init(dt, step, tgt_pos_y, cam_vel_y, P0, Q0, r_tgt_pos0, r_cam_vel0)) return rc;

        m_initialized = true;
        return rc;
    }

    int init(float dt, int step, float tgt_pos_x, float tgt_pos_y, float cam_vel_x=0.0, float cam_vel_y=0.0)
    {
        int rc = 0;
        if (rc = m_kf_x.init(dt, step, tgt_pos_x, cam_vel_x)) return rc;
        if (rc = m_kf_y.init(dt, step, tgt_pos_y, cam_vel_y)) return rc;

        m_initialized = true;
        return rc;
    }

    int predict(int step)
    {
        int rc = 0;
        if (rc = m_kf_x.predict(step)) return rc;
        if (rc = m_kf_y.predict(step)) return rc;
        return rc;
    }

    int update_target_pos(int step, float tgt_pos_x, float tgt_pos_y)
    {
        int rc = 0;
        if (rc = m_kf_x.update_target_pos(step, tgt_pos_x)) return rc;
        if (rc = m_kf_y.update_target_pos(step, tgt_pos_y)) return rc;
        return rc;
    }

    int update_target_pos(int step, float tgt_pos_x, float tgt_pos_y,
            float r_x, float r_y)
    {
        int rc = 0;
        if (rc = m_kf_x.update_target_pos(step, tgt_pos_x, r_x)) return rc;
        if (rc = m_kf_y.update_target_pos(step, tgt_pos_y, r_y)) return rc;
        return rc;
    }

    int update_camera_vel(int step, float cam_vel_x, float cam_vel_y)
    {
        int rc = 0;
        if (rc = m_kf_x.update_camera_vel(step, cam_vel_x)) return rc;
        if (rc = m_kf_y.update_camera_vel(step, cam_vel_y)) return rc;
        return rc;
    }

    int update_camera_vel(int step, float cam_vel_x, float cam_vel_y, float r_x, float r_y)
    {
        int rc = 0;
        if (m_kf_x.update_camera_vel(step, cam_vel_x, r_x)) return -1;
        if (m_kf_y.update_camera_vel(step, cam_vel_y, r_y)) return -1;
        return rc;
    }

    int get_step()
    {
        return m_kf_x.get_step();
    }

    float get_dt()
    {
        return m_kf_x.get_dt();
    }

    int set_dt(float dt)
    {
        int rc = 0;
        if (rc = m_kf_x.set_dt(dt)) return rc;
        if (rc = m_kf_y.set_dt(dt)) return rc;
        return rc;
    }

    void get_states(Vector6f &x, Vector6f &y)
    {
        m_kf_x.get_states(x);
        m_kf_y.get_states(y);
    }

    void get_state_positions(float &x, float &y)
    {
        m_kf_x.get_state_position(x);
        m_kf_y.get_state_position(y);
    }

    void get_state_error_covariances(Matrix6f &P_x, Matrix6f &P_y)
    {
        m_kf_x.get_state_error_covariance(P_x);
        m_kf_y.get_state_error_covariance(P_y);
    }
};

} // namespace udot

} // namespace xsc

#endif // #ifndef __TRACKKF_H__
