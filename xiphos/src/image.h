/* Copyright (C) 2016 Xiphos Systems Corporation
 */

#ifndef IMAGE_H
#define IMAGE_H

namespace xsc
{
namespace image
{
struct level
{
    int w;
    int h;
    int hs;
    int vs;
    int *n[2];              // Number of keypoints in each bin          (0-host, 1-device)
    int *nb[2];             // Number of bad keypoint correspondences   (0-host, 1-device)
    int *np[2];             // Number of keypoints in the image         (0-host, 1-device)
    unsigned short *bin;    // Keypoint bin index                       (device)
    float *i[2];            // Pixel intensity image                    (0-host, 1-device)
    unsigned short *xy;     // Keypoint x & y-coordinates interleaved   (device)
    unsigned short *xyb[2]; // Bad correspondence x & y-coordinates     (0-host, 1-device)
    float fhm[9];           // Filtered homography matrix
    float hm[9];            // Homography matrix
    int   hv;               // Homography valid
    
    // Optical Flow Vectors
    float* x0[2]; // Keypoint in current  image x-coordinate        (0-host, 1-device)
    float* y0[2]; // Keypoint in current  image y-coordinate        (0-host, 1-device)
    float* x1[2]; // Match in previous image x-coordinate           (0-host, 1-device)
    float* y1[2]; // Match in previous image y-coordinate           (0-host, 1-device)
    float* x2[2]; // Reverse match in current image x-coordinate    (0-host, 1-device)
    float* y2[2]; // Reverse match in current image y-coordinate    (0-host, 1-device)
    int nf;

    level(void);
    virtual ~level(void);

    level(const level &) = delete;
    level &operator = (const level &) = delete;
};

template <int N> using pyramid = level[N];

int init(   xsc::image::level *p, 
            unsigned short *bin, 
            unsigned short *pxy, 
            unsigned short *pxyb0, 
            unsigned short *pxyb1, 
            int *n0, 
            int *n1, 
            int *np0, 
            int *np1, 
            int *nb0, 
            int *nb1, 
            float *fx00,
            float *fy00,
            float *fx01,
            float *fy01,
            float *fx02,
            float *fy02,
            float *fx10,
            float *fy10,
            float *fx11,
            float *fy11,
            float *fx12,
            float *fy12,
            int total_flows,
            int w, 
            int h);

int init_keypoints( level *p, 
                    const int levels, 
                    const int total_image_bins, 
                    const int total_keypoints);

int copyback_keypoints( level *p, 
                        const int levels, 
                        const int num_image_bins, 
                        const int total_keypoints, 
                        const int max_keypoints);

int copyback_matches( level *p, 
                      const int levels, 
                      const int total_image_bins, 
                      const int total_keypoints, 
                      const int max_keypoints);

int zero_keypoints(level *p, int levels, const int total_image_bins);

int free_keypoints(level *p);

int free(level *p);

int update_downsample(level *p);

int update_keypoints(   xsc::image::level *p, 
                        float* const thresholds, 
                        const int num_image_bins, 
                        const int max_keypoints);

int sample_flow_points( xsc::image::level *p, const int total_keypoints);

int init_texture(   const uint8_t* const d_luma,
                    const uint8_t* const d_cra,
                    const uint8_t* const d_crb,
                    const int index,
                    int w, int h, int format_type);
                    
int free_texture(int index);

int upload_uyvy(level *p, uint8_t* const dst_gpu, const uint8_t* const src_cpu, int stride);
int convert_uyvy(level *p, uint8_t* const dst_gpu, uint8_t* const src_gpu );
int convert_uyvy_bilinear(level *p, uint8_t* const dst_gpu, const int index);
int update_uyvy_bilinear(level *p, uint8_t* const dst_luma, uint8_t* const dst_cra, uint8_t* const dst_crb, uint8_t* const src_gpu, const int index);
int download_uyvy(level *p, uint8_t* const dst_cpu, uint8_t* const src_gpu);
int warp_uyvy_bilinear( level *p, uint8_t* const dst_gpu, const int index, const int split, const float* const h);

int upload_yuy422(level *p, uint8_t* const dst_gpu, const uint8_t* const src_cpu, int stride);
int convert_yuy422(level *p, uint8_t* const dst_gpu, uint8_t* const src_gpu );
int download_yuy422(level *p, uint8_t* const dst_cpu, uint8_t* const src_gpu);
int update_yuy422(level *p, uint8_t* const src_gpu);
int warp_yuy422(level *p, uint8_t* const dst_gpu, uint8_t* const src_gpu, const float* const h);

int copy_yuv420(xsc::image::level *p, uint8_t* const dst_cpu, uint8_t* const src_cpu, int stride);
int upload_yuv420(level *p, uint8_t* const dst_gpu, const uint8_t* const src_cpu, int stride);
int download_yuv420(level *p, uint8_t* const dst_cpu, uint8_t* const src_gpu);
int update_yuv420_bilinear(level *p, uint8_t* const dst_luma, uint8_t* const dst_cra, uint8_t* const dst_crb, uint8_t* const src_gpu, const int index);
int warp_yuv420_bilinear(level *p, uint8_t* const dst_gpu, const int index, const int split, const float* const h);

int cuda_device_reset();


template <int N, int M>
int init(level *p, int w, int h, const int total_image_bins, const int total_keypoints, const int total_flows)
{
    // Assuming that we do not collect keypoints on camera pyramid level zero
    xsc::image::init_keypoints(p, M-1, total_image_bins, total_keypoints);

    for (int i = 0; i < N; i++)
    {
        int j = (i > 0) ? ((i < M) ? i-1 : 0) : 0;
        if (xsc::image::init(p + i, 
                &(p->bin[j * total_keypoints]),
                &(p->xy[2 * j * total_keypoints]),
                &(p->xyb[0][2 * j * total_keypoints]),
                &(p->xyb[1][2 * j * total_keypoints]),
                &(p->n[0][j * total_image_bins]),
                &(p->n[1][j * total_image_bins]),
                &(p->np[0][j * total_image_bins]),
                &(p->np[1][j * total_image_bins]),
                &(p->nb[0][j * total_image_bins]),
                &(p->nb[1][j * total_image_bins]),
                p->x0[0],
                p->y0[0],
                p->x1[0],
                p->y1[0],
                p->x2[0],
                p->y2[0],
                p->x0[1],
                p->y0[1],
                p->x1[1],
                p->y1[1],
                p->x2[1],
                p->y2[1],
                total_flows,
                w, h) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        w /= 2;
        h /= 2;
    }

    return 0;
}

template <int N>
int free(level *p)
{
    xsc::image::free_keypoints(p);
    
    for (int i = 0; i < N; i++)
    {
        if (xsc::image::free(p + i) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }

    return 0;
}

template <int N, int M>
int keypoints(  level *p,
                float* const thresholds,
                int const num_image_bins,
                int const total_keypoints,
                int const max_keypoints)
              
{
    const int total_image_bins = num_image_bins * num_image_bins;
    
    if (xsc::image::zero_keypoints(p, M-1, total_image_bins) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    // For each image pyramid level
    for (int i = 1; i < N; i++)
    {
        // Downsample the image
        if (xsc::image::update_downsample(p + i) < 0) 
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }   
    
    // For each camera pyramid level
    for (int i = 1; i < M; i++)
    {
        // Process the image for keypoints
        if (xsc::image::update_keypoints(p + i, thresholds, num_image_bins, max_keypoints) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }

    // Sample the flow keypoints
    if (xsc::image::sample_flow_points(p, total_keypoints) < 0)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    return 0;
}

}
}

#endif /* #ifndef IMAGE_H */
