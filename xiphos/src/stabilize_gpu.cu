#include "config.h"
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>

int xsc::stabilize::init_gpu(stabilizer* const s)
{
    checkCudaErrors(cudaMalloc((void **)&s->h[1], 9 * sizeof(float)));
    if (s->h[1] == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    s->px0[1] = nullptr;
    s->py0[1] = nullptr;
    s->px1[1] = nullptr;
    s->py1[1] = nullptr;
    s->px2[1] = nullptr;
    s->py2[1] = nullptr;
    
    return 0;
}

int xsc::stabilize::free_gpu(stabilizer* const s)
{
    cudaFree(s->h[1]);
    s->h[1] = nullptr;
    
    s->px0[1] = nullptr;
    s->py0[1] = nullptr;
    s->px1[1] = nullptr;
    s->py1[1] = nullptr;
    s->px2[1] = nullptr;
    s->py2[1] = nullptr;
    
    return 0;
}

int xsc::stabilize::copy_homography(stabilizer* const s)
{
    // Copy the homography data to the device
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            s->h[0][i*3+j] = s->H(i,j);
        }
    }
    
    checkCudaErrors(cudaMemcpy(s->h[1], s->h[0], 9 * sizeof(float), cudaMemcpyHostToDevice));

    return 0;
}  

