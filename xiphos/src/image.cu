/* Copyright (C) 2016 Xiphos Systems Corporation
 */

#include "config.h"
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>

#if 0
#define CUDA_PROF(elapsed, format, ...) \
({ \
        fprintf(stderr, "Cuda Time, %.3f, " format "\n", elapsed, ##__VA_ARGS__); \
})
#else
#define CUDA_PROF(elapsed, format, ...)
#endif

#if 0
#define CPU_PROF(start, format, ...) \
({ \
    double end = seconds(); \
    fprintf(stderr, "Cpu Time, %.3f, " format "\n", (end - start)*1000, ##__VA_ARGS__); \
    start = end; \
})
#else
#define CPU_PROF(start, format, ...)
#endif

#define BLANK 4

static texture<uint8_t, 2, cudaReadModeNormalizedFloat> s_tex_luma_0;
static texture<uint8_t, 2, cudaReadModeNormalizedFloat> s_tex_cra_0;
static texture<uint8_t, 2, cudaReadModeNormalizedFloat> s_tex_crb_0;

static texture<uint8_t, 2, cudaReadModeNormalizedFloat> s_tex_luma_1;
static texture<uint8_t, 2, cudaReadModeNormalizedFloat> s_tex_cra_1;
static texture<uint8_t, 2, cudaReadModeNormalizedFloat> s_tex_crb_1;

static texture<uint8_t, 2, cudaReadModeNormalizedFloat> s_tex_luma_2;
static texture<uint8_t, 2, cudaReadModeNormalizedFloat> s_tex_cra_2;
static texture<uint8_t, 2, cudaReadModeNormalizedFloat> s_tex_crb_2;

static __global__ void format_yuy422(float* const imdest, int ds, const uint8_t* const imsrc, int ss, int w, int h)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    imdest[x + y * ds] = imsrc[x * 2 + y * ss] / 255.f;
}

static __global__ void yuv420_to_luma_kernel(uint32_t *dst, int ds,
                                             const uint32_t* const src, int ss,
                                             int w, int h)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    dst[x + y * ds] = src[x + y * ss];
}

static __global__ void yuv420_to_chroma_kernel(uint32_t *dst, int ds,
                                               const uint32_t *src, int ss, int chroff,
                                               int w, int h)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    dst[x + y * ds] = src[x + y * ss + chroff];
}

// Copy the image data from device UYVY to the textures only
// src - input uyvy image
// ldst - luma texture destination
// udst - cra texture destination
// vdst - crb texture destination
// w - width of the source image in pixels
// h - height of the source image in pixels
// ss - source image stride width (uint32_t blocks means should be w/2)
// ls - dest luma texture stride width
// lu - dest cra texture stride width
// vu - dest cra texture stride width
static __global__ void uyvy_to_tex_kernel(  uint8_t* const ldst, int ls,
                                            uint8_t* const udst, int us,
                                            uint8_t* const vdst, int vs,
                                            const uint32_t* const src, int ss,
                                            int w, int h)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x * 2 || h <= y)
    {
        return;
    }

    const uint32_t val = src[x + y * ss];

    // low first - high last
	udst[x + y * us] = (uint8_t)(val & 255);
	ldst[x * 2 + y * ls] = (uint8_t)(val>>8 & 255);
	vdst[x + y * vs] = (uint8_t)(val>>16 & 255);
	ldst[x * 2 + y * ls + 1] = (uint8_t)(val>>24 & 255);
}

static __global__ void tex_to_iimage_kernel_0(float* const imdest, int ds, int w, int h)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    imdest[x + y * ds] = tex2D(s_tex_luma_0, x, y);
}

static __global__ void tex_to_iimage_kernel_1(float* const imdest, int ds, int w, int h)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    imdest[x + y * ds] = tex2D(s_tex_luma_1, x, y);
}

static __global__ void tex_to_iimage_kernel_2(float* const imdest, int ds, int w, int h)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    imdest[x + y * ds] = tex2D(s_tex_luma_2, x, y);
}

// Convert a YUY2 image into a I420 image
static __global__ void yuy422_to_yuv420(uint8_t* const imdest, int ds, const uint8_t* const imsrc, int ss, int w, int h)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h * 3 / 2 <= y)
    {
        return;
    }

    const int dst_ys = w * h;                 // Y Plane size in I420 image
    const int cps = dst_ys / 4;               // Chroma U plane size

    if ( x + y * ds <= dst_ys) // Y Plane
    {
        imdest[x + y * ds] = imsrc[x * 2 + y * ss];
    }
    else if ( x + y * ds <= dst_ys + cps) // U Plane
    {
        const int cpr = ss / 4;              // Chroma U per row in YUY2 image
        const int dst_ud = (x + y * w) - dst_ys; // U Plane delta in I420 Image
        const int offset = dst_ud * 4 + 1 + dst_ud / cpr * ss;
        imdest[x + y * ds] = imsrc[offset] / 2 + imsrc[offset + ss] / 2;
    }
    else // V Plane
    {
        const int cpr = ss / 4;                    // Chroma V per row in YUY2 image
        const int dst_vd = (x + y * w) - dst_ys - cps; // V Plane delta in I420 Image
        const int offset = dst_vd * 4 + 3 + dst_vd / cpr * ss;
        imdest[x + y * ds] = imsrc[offset] / 2 + imsrc[offset + ss] / 2;
    }
}

// Convert a UYVY image into a I420 image
static __global__ void uyvy_to_yuv420(uint8_t* const imdest, int ds, const uint8_t* const imsrc, int ss, int w, int h)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h * 3 / 2 <= y)
    {
        return;
    }

    const int dst_ys = w * h;                 // Y Plane size in I420 image
    const int cps = dst_ys / 4;               // Chroma U plane size

    if ( x + y * ds <= dst_ys) // Y Plane
    {
        imdest[x + y * ds] = imsrc[x * 2 + y * ss + 1];
    }
    else if ( x + y * ds <= dst_ys + cps) // U Plane
    {
        const int cpr = ss / 4;              // Chroma U per row in YUY2 image
        const int dst_ud = (x + y * w) - dst_ys; // U Plane delta in I420 Image
        const int offset = dst_ud * 4 + dst_ud / cpr * ss;
        imdest[x + y * ds] = imsrc[offset] / 2 + imsrc[offset + ss] / 2;
    }
    else // V Plane
    {
        const int cpr = ss / 4;                    // Chroma V per row in YUY2 image
        const int dst_vd = (x + y * w) - dst_ys - cps; // V Plane delta in I420 Image
        const int offset = dst_vd * 4 + 2 + dst_vd / cpr * ss;
        imdest[x + y * ds] = imsrc[offset] / 2 + imsrc[offset + ss] / 2;
    }
}

static __global__ void warp_image_yuv420_naive(uint8_t* const imdest, int ds, const uint8_t* const imsrc, int ss, int w, int h, const float* const hk)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }
    const int ps = h * ds;
    const int us = ps / 4;

    // hk is the Homography 3x3 matrix array
    float s0 = hk[0] * x + hk[1] * y + hk[2];
    float s1 = hk[3] * x + hk[4] * y + hk[5];
    float s2 = hk[6] * x + hk[7] * y + hk[8];

    // apply scale
    s0 /= s2;
    s1 /= s2;
    const int ss0 = (int)s0;
    const int ss1 = (int)s1;

    const int dco = x / 2 + y / 2 * ds / 2;
    const int sco =  ss0 / 2 + ss1 / 2 * ss / 2;

    // fill with black
    if (w <= ss0 ||ss0 < 0 || h <= ss1 || ss1 < 0)
    {
        imdest[x + y * ds] = 0;
        imdest[ps + dco] = 128;
        imdest[us + ps + dco] = 128;
        return;
    }

    // map src to dest
    imdest[x + y * ds] = imsrc[ss0 + ss1 * ss];
    imdest[ps + dco] = imsrc[ps + sco];
    imdest[us + ps + dco] = imsrc[ us + ps + sco];
}

// Warp to I420 from the textures. This will perform bilinear filtering
// if the texture's filterMode is cudaFilterModeLinear.
//
// dst - I420 image destination
// ds - destination stride width
// w - image width in pixels
// h - image height in pixels
// cxs - chroma horizontal scale
// cys - chroma vertical scale
// hk - warp homography
static __global__ void warp_to_I420_from_textures_kernel_0(uint8_t* const dst, int ds,
                                                         int w, int h, const int ps, const int us,
                                                         float cxs, float cys, const float* const hk)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    // hk is the Homography 3x3 matrix array
    float s0 = hk[0] * x + hk[1] * y + hk[2];
    float s1 = hk[3] * x + hk[4] * y + hk[5];
    float s2 = hk[6] * x + hk[7] * y + hk[8];
    // apply scale
    s0 /= s2;
    s1 /= s2;
    const int ss0 = (int)s0;
    const int ss1 = (int)s1;

    const int dco = x / 2 + y / 2 * ds / 2;

    // fill with black
    if (w <= ss0 ||ss0 < 0 || h <= ss1 || ss1 < 0)
    {
        dst[x + y * ds] = 0;
        dst[ps + dco] = 128;
        dst[us + ps + dco] = 128;
        return;
    }

    // map src to dest (data is normalized, so need to map back to integers)
    dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_0, s0, s1));
    dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_0,  s0 * cxs, s1 * cys));
    dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_0,  s0 * cxs, s1 * cys));
}

static __global__ void warp_to_I420_from_textures_kernel_1(uint8_t* const dst, int ds,
                                                         int w, int h, const int ps, const int us,
                                                         float cxs, float cys, const float* const hk)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    // hk is the Homography 3x3 matrix array
    float s0 = hk[0] * x + hk[1] * y + hk[2];
    float s1 = hk[3] * x + hk[4] * y + hk[5];
    float s2 = hk[6] * x + hk[7] * y + hk[8];
    // apply scale
    s0 /= s2;
    s1 /= s2;
    const int ss0 = (int)s0;
    const int ss1 = (int)s1;

    const int dco = x / 2 + y / 2 * ds / 2;

    // fill with black
    if (w <= ss0 ||ss0 < 0 || h <= ss1 || ss1 < 0)
    {
        dst[x + y * ds] = 0;
        dst[ps + dco] = 128;
        dst[us + ps + dco] = 128;
        return;
    }

    // map src to dest (data is normalized, so need to map back to integers)
    dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_1, s0, s1));
    dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_1,  s0 * cxs, s1 * cys));
    dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_1,  s0 * cxs, s1 * cys));
}

static __global__ void warp_to_I420_from_textures_kernel_2(uint8_t* const dst, int ds,
                                                         int w, int h, const int ps, const int us,
                                                         float cxs, float cys, const float* const hk)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    // hk is the Homography 3x3 matrix array
    float s0 = hk[0] * x + hk[1] * y + hk[2];
    float s1 = hk[3] * x + hk[4] * y + hk[5];
    float s2 = hk[6] * x + hk[7] * y + hk[8];
    // apply scale
    s0 /= s2;
    s1 /= s2;
    const int ss0 = (int)s0;
    const int ss1 = (int)s1;

    const int dco = x / 2 + y / 2 * ds / 2;

    // fill with black
    if (w <= ss0 ||ss0 < 0 || h <= ss1 || ss1 < 0)
    {
        dst[x + y * ds] = 0;
        dst[ps + dco] = 128;
        dst[us + ps + dco] = 128;
        return;
    }

    // map src to dest (data is normalized, so need to map back to integers)
    dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_2, s0, s1));
    dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_2,  s0 * cxs, s1 * cys));
    dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_2,  s0 * cxs, s1 * cys));
}

static __global__ void half_warp_to_I420_from_textures_kernel_0(uint8_t* const dst, int ds,
                                                         int w, int h, const int ps, const int us,
                                                         float cxs, float cys, const float* const hk)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    const int dco = x / 2 + y / 2 * ds / 2;

    if (x < w/2)
    {
        // hk is the Homography 3x3 matrix array
        float s0 = hk[0] * x + hk[1] * y + hk[2];
        float s1 = hk[3] * x + hk[4] * y + hk[5];
        float s2 = hk[6] * x + hk[7] * y + hk[8];
        // apply scale
        s0 /= s2;
        s1 /= s2;
        const int ss0 = (int)s0;
        const int ss1 = (int)s1;


        // fill with black
        if (w <= ss0 ||ss0 < 0 || h <= ss1 || ss1 < 0)
        {
            dst[x + y * ds] = 0;
            dst[ps + dco] = 128;
            dst[us + ps + dco] = 128;
            return;
        }

        // map src to dest (data is normalized, so need to map back to integers)
        dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_0, s0, s1));
        dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_0,  s0 * cxs, s1 * cys));
        dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_0,  s0 * cxs, s1 * cys));
    }
    else
    {
        dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_0, x, y));
        dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_0,  x * cxs, y * cys));
        dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_0,  x * cxs, y * cys));
    }
}

static __global__ void half_warp_to_I420_from_textures_kernel_1(uint8_t* const dst, int ds,
                                                         int w, int h, const int ps, const int us,
                                                         float cxs, float cys, const float* const hk)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    const int dco = x / 2 + y / 2 * ds / 2;

    if (x < w/2)
    {
        // hk is the Homography 3x3 matrix array
        float s0 = hk[0] * x + hk[1] * y + hk[2];
        float s1 = hk[3] * x + hk[4] * y + hk[5];
        float s2 = hk[6] * x + hk[7] * y + hk[8];
        // apply scale
        s0 /= s2;
        s1 /= s2;
        const int ss0 = (int)s0;
        const int ss1 = (int)s1;


        // fill with black
        if (w <= ss0 ||ss0 < 0 || h <= ss1 || ss1 < 0)
        {
            dst[x + y * ds] = 0;
            dst[ps + dco] = 128;
            dst[us + ps + dco] = 128;
            return;
        }

        // map src to dest (data is normalized, so need to map back to integers)
        dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_1, s0, s1));
        dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_1,  s0 * cxs, s1 * cys));
        dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_1,  s0 * cxs, s1 * cys));
    }
    else
    {
        dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_1, x, y));
        dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_1,  x * cxs, y * cys));
        dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_1,  x * cxs, y * cys));
    }
}

static __global__ void half_warp_to_I420_from_textures_kernel_2(uint8_t* const dst, int ds,
                                                         int w, int h, const int ps, const int us,
                                                         float cxs, float cys, const float* const hk)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    const int dco = x / 2 + y / 2 * ds / 2;

    if (x < w/2)
    {
        // hk is the Homography 3x3 matrix array
        float s0 = hk[0] * x + hk[1] * y + hk[2];
        float s1 = hk[3] * x + hk[4] * y + hk[5];
        float s2 = hk[6] * x + hk[7] * y + hk[8];
        // apply scale
        s0 /= s2;
        s1 /= s2;
        const int ss0 = (int)s0;
        const int ss1 = (int)s1;


        // fill with black
        if (w <= ss0 ||ss0 < 0 || h <= ss1 || ss1 < 0)
        {
            dst[x + y * ds] = 0;
            dst[ps + dco] = 128;
            dst[us + ps + dco] = 128;
            return;
        }

        // map src to dest (data is normalized, so need to map back to integers)
        dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_2, s0, s1));
        dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_2,  s0 * cxs, s1 * cys));
        dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_2,  s0 * cxs, s1 * cys));
    }
    else
    {
        dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_2, x, y));
        dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_2,  x * cxs, y * cys));
        dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_2,  x * cxs, y * cys));
    }
}

static __global__ void copy_to_I420_from_textures_kernel_0(uint8_t* const dst, int ds,
                                                         int w, int h, const int ps, const int us,
                                                         float cxs, float cys)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    const int dco = x / 2 + y / 2 * ds / 2;

    // map src to dest (data is normalized, so need to map back to integers)
    dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_0, x, y));
    dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_0,  x * cxs, y * cys));
    dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_0,  x * cxs, y * cys));
}

static __global__ void copy_to_I420_from_textures_kernel_1(uint8_t* const dst, int ds,
                                                         int w, int h, const int ps, const int us,
                                                         float cxs, float cys)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    const int dco = x / 2 + y / 2 * ds / 2;

    // map src to dest (data is normalized, so need to map back to integers)
    dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_1, x, y));
    dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_1,  x * cxs, y * cys));
    dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_1,  x * cxs, y * cys));
}

static __global__ void copy_to_I420_from_textures_kernel_2(uint8_t* const dst, int ds,
                                                         int w, int h, const int ps, const int us,
                                                         float cxs, float cys)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    const int dco = x / 2 + y / 2 * ds / 2;

    // map src to dest (data is normalized, so need to map back to integers)
    dst[x + y * ds]    = 255 * __saturatef(tex2D(s_tex_luma_2, x, y));
    dst[ps + dco]      = 255 * __saturatef(tex2D(s_tex_cra_2,  x * cxs, y * cys));
    dst[us + ps + dco] = 255 * __saturatef(tex2D(s_tex_crb_2,  x * cxs, y * cys));
}

// Down-sample by factor 2 using bi-linear interpolation
static __global__ void downsample(float* const imdest, int ds, const float* const imsrc, int ss, int w, int h)
{
    const int y = threadIdx.y + blockIdx.y * blockDim.y;
    const int x = threadIdx.x + blockIdx.x * blockDim.x;

    if (w <= x || h <= y)
    {
        return;
    }

    const int j = (x + y * ss) * 2;
    float sum = 0;

    sum += imsrc[j];
    sum += imsrc[j + 1];
    sum += imsrc[j + ss];
    sum += imsrc[j + ss + 1];

    imdest[x + y * ds] = sum * 0.25f;
}

// Detect keypoints in image
__global__ void keypoint_detect(const float *pi,
                                int si,
                                int w,
                                int h,
                                unsigned short bw,
                                unsigned short bh,
                                float *thresholds,
                                int num_image_bins,
                                int max_keypoints,
                                unsigned short *pxy,
                                unsigned short *pb,
                                int *n)
{
    const unsigned short b  = threadIdx.z + blockIdx.z * blockDim.z;
    const unsigned short by = b / num_image_bins;
    const unsigned short bx = b - by * num_image_bins;

    const unsigned short ix = bx * bw;
    const unsigned short iy = by * bh;

    const unsigned short x = threadIdx.x + blockIdx.x * blockDim.x + ix;
    const unsigned short y = threadIdx.y + blockIdx.y * blockDim.y + iy;

    if (x < BLANK || w - BLANK <= x || y < BLANK || h - BLANK <= y || num_image_bins <= bx || num_image_bins <= by)
    {
        return;
    }

    const int i  = x + y * si;
    float p = pi[i] * 2.f;
    float m = 1.f;

    // Second-derivative of image gradient in direction (u,v) at pixel.
    // This will prefer points or corners over edges.
#define KEYPOINT_MIN(u, v) \
    { \
        const float q = pi[i + u + v * si]; \
        const float r = pi[i - u - v * si]; \
        const float n = q + r - p; \
        if (fabsf(n) < fabsf(m)) \
        { \
            m = n; \
        } \
    }

    // Test cardinal directions
    KEYPOINT_MIN(0, -3);
    KEYPOINT_MIN(3,  0);

    // Check for early exit
    if (thresholds[b] < fabsf(m))
    {
        KEYPOINT_MIN(1, -3);
        KEYPOINT_MIN(2, -2);
        KEYPOINT_MIN(3, -1);
        KEYPOINT_MIN(3,  1);
        KEYPOINT_MIN(2,  2);
        KEYPOINT_MIN(1,  3);

        // If minimum second-derivative is greater than threshold
        if (thresholds[b] < fabsf(m))
        {
            // Accept this as a good point
            const int total_image_bins = num_image_bins * num_image_bins;
            const int j = atomicAdd(&(n[b]), 1);
            if (j <= max_keypoints)
            {
                const int total_keypoints = total_image_bins * max_keypoints;
                const int k = atomicAdd(&(n[total_image_bins]), 1);
                if (k <= total_keypoints)
                {
                  pxy[2*k+0] = x;
                  pxy[2*k+1] = y;
                  pb[k] = b;
                }
            }
        }
    }
}

// Sub-sample the keypoints found in the image
// Take every 'step'-th keypoint [xi, yi] and store in [xk, yk]
__global__ void sample_flows(float* xk, float* yk, unsigned short* xyi, int* m, int n, int max_m)
{
    const int step = MAX(MIN(*m, max_m) / n, 1);
    const int tid  = threadIdx.x + blockIdx.x * blockDim.x;
    const int key  = 2 * tid * step;

    if (tid >= n)
    {
        return;
    }

    xk[tid] = xyi[key+0];
    yk[tid] = xyi[key+1];
}

// Bilinear interpolation at coordinate (x, y) with the four adjacent pixels
__device__ __forceinline__ float interp2(float* p, unsigned int ss, float x, float y)
{
    const unsigned int xi = x;
    const unsigned int yi = y;

    const float al  = x - (float)xi;
    const float al1 = 1.0 - al;
    const float be  = y - (float)yi;
    const float be1 = 1.0 - be;

    const unsigned int yiss = yi*ss;
    const float a = p[xi   + yiss];
    const float b = p[xi+1 + yiss];
    const float c = p[xi   + yiss + ss];
    const float d = p[xi+1 + yiss + ss];

    return be1*(al1*a + al*b) + be*(al1*c + al*d);
}

__global__ void
classify_keypoints( const unsigned int width,
                    const unsigned int height,
                    const float factor,
                    const float* const sqrt_thresholds,
                    const float* const T,
                    float* p,
                    unsigned int pss,
                    float* q,
                    unsigned int qss,
                    unsigned short* b,
                    unsigned short* xy,
                    int* n,
                    unsigned short* xyb,
                    int* nb)
{
    const unsigned int i = threadIdx.x + blockIdx.x * blockDim.x;

    if (i >= *(n))
    {
        return;
    }

    const unsigned short xi = xy[2*i+0];
    const unsigned short yi = xy[2*i+1];
    const float x  = xi;
    const float y  = yi;

    const float x2 = T[0]*x + T[1]*y + T[2];
    const float y2 = T[4]*x + T[5]*y + T[6];
    const float z2 = T[8]*x + T[9]*y + T[10];

    const float xk = x2 / z2;
    const float yk = y2 / z2;

    // If the point is outside of the image-space boundaries of q, ignore
    if (xk < 2 || width - 2 <= xk || yk < 2 || height - 2 <= yk)
    {
        return;
    }

    const float di = p[xi + yi * pss] - interp2(q, qss, xk, yk);

    // Classify correspondence
    if (di * di >= factor * sqrt_thresholds[b[i]])
    {
        const int j = atomicAdd(nb, 1);
        if (j <= xsc::MAX_BAD_KEYPOINTS)
        {
            // This is a bad correspondence, save for MTI
            xyb[2*j+0] = xi;
            xyb[2*j+1] = yi;
        }
    }
}


xsc::image::level::level(void)
{
    w = 0;
    h = 0;
    hs = 0;
    vs = 0;
    xy  = nullptr;
    bin = nullptr;

    i[0] = nullptr;
    xyb[0] = nullptr;
    n[0] = nullptr;
    np[0] = nullptr;
    nb[0] = nullptr;
    x0[0] = nullptr;
    y0[0] = nullptr;
    x1[0] = nullptr;
    y1[0] = nullptr;
    x2[0] = nullptr;
    y2[0] = nullptr;

    i[1] = nullptr;
    xyb[1] = nullptr;
    n[1] = nullptr;
    np[1] = nullptr;
    nb[1] = nullptr;
    x0[1] = nullptr;
    y0[1] = nullptr;
    x1[1] = nullptr;
    y1[1] = nullptr;
    x2[1] = nullptr;
    y2[1] = nullptr;
}

xsc::image::level::~level(void)
{
}

// Initialize the texture mapping
// Set format to 0 if YUV420
int xsc::image::init_texture(   const uint8_t* const d_luma,
                                const uint8_t* const d_cra,
                                const uint8_t* const d_crb,
                                const int index,
                                int w, int h, int format)
{
    const int chroma_hs = w / 2;
    const int chroma_vs = (format > I420) ? h : h / 2;

    cudaChannelFormatDesc channel_desc = cudaCreateChannelDesc<uint8_t>();

    if (index == 0)
    {
        // Offset guaranteed to be zero when backed with cudaMalloc() so use NULL
        checkCudaErrors(cudaBindTexture2D(NULL, s_tex_luma_0, d_luma, channel_desc, w, h, sizeof(uint8_t) * w));
        checkCudaErrors(cudaBindTexture2D(NULL, s_tex_cra_0,  d_cra,  channel_desc, chroma_hs, chroma_vs, sizeof(uint8_t) * chroma_hs));
        checkCudaErrors(cudaBindTexture2D(NULL, s_tex_crb_0,  d_crb,  channel_desc, chroma_hs, chroma_vs, sizeof(uint8_t) * chroma_hs));

        s_tex_luma_0.filterMode = cudaFilterModeLinear;
        s_tex_luma_0.addressMode[0] = cudaAddressModeClamp;
        s_tex_luma_0.addressMode[1] = cudaAddressModeClamp;

        s_tex_cra_0.filterMode = cudaFilterModeLinear;
        s_tex_cra_0.addressMode[0] = cudaAddressModeClamp;
        s_tex_cra_0.addressMode[1] = cudaAddressModeClamp;

        s_tex_crb_0.filterMode = cudaFilterModeLinear;
        s_tex_crb_0.addressMode[0] = cudaAddressModeClamp;
        s_tex_crb_0.addressMode[1] = cudaAddressModeClamp;
    }
    else if (index == 1)
    {
        // Offset guaranteed to be zero when backed with cudaMalloc() so use NULL
        checkCudaErrors(cudaBindTexture2D(NULL, s_tex_luma_1, d_luma, channel_desc, w, h, sizeof(uint8_t) * w));
        checkCudaErrors(cudaBindTexture2D(NULL, s_tex_cra_1,  d_cra,  channel_desc, chroma_hs, chroma_vs, sizeof(uint8_t) * chroma_hs));
        checkCudaErrors(cudaBindTexture2D(NULL, s_tex_crb_1,  d_crb,  channel_desc, chroma_hs, chroma_vs, sizeof(uint8_t) * chroma_hs));

        s_tex_luma_1.filterMode = cudaFilterModeLinear;
        s_tex_luma_1.addressMode[0] = cudaAddressModeClamp;
        s_tex_luma_1.addressMode[1] = cudaAddressModeClamp;

        s_tex_cra_1.filterMode = cudaFilterModeLinear;
        s_tex_cra_1.addressMode[0] = cudaAddressModeClamp;
        s_tex_cra_1.addressMode[1] = cudaAddressModeClamp;

        s_tex_crb_1.filterMode = cudaFilterModeLinear;
        s_tex_crb_1.addressMode[0] = cudaAddressModeClamp;
        s_tex_crb_1.addressMode[1] = cudaAddressModeClamp;
    }
    else
    {
        // Offset guaranteed to be zero when backed with cudaMalloc() so use NULL
        checkCudaErrors(cudaBindTexture2D(NULL, s_tex_luma_2, d_luma, channel_desc, w, h, sizeof(uint8_t) * w));
        checkCudaErrors(cudaBindTexture2D(NULL, s_tex_cra_2,  d_cra,  channel_desc, chroma_hs, chroma_vs, sizeof(uint8_t) * chroma_hs));
        checkCudaErrors(cudaBindTexture2D(NULL, s_tex_crb_2,  d_crb,  channel_desc, chroma_hs, chroma_vs, sizeof(uint8_t) * chroma_hs));

        s_tex_luma_2.filterMode = cudaFilterModeLinear;
        s_tex_luma_2.addressMode[0] = cudaAddressModeClamp;
        s_tex_luma_2.addressMode[1] = cudaAddressModeClamp;

        s_tex_cra_2.filterMode = cudaFilterModeLinear;
        s_tex_cra_2.addressMode[0] = cudaAddressModeClamp;
        s_tex_cra_2.addressMode[1] = cudaAddressModeClamp;

        s_tex_crb_2.filterMode = cudaFilterModeLinear;
        s_tex_crb_2.addressMode[0] = cudaAddressModeClamp;
        s_tex_crb_2.addressMode[1] = cudaAddressModeClamp;
    }

    checkCudaErrors(cudaPeekAtLastError());

    return 0;
}

int xsc::image::free_texture(int index)
{
    if (index == 0)
    {
        checkCudaErrors(cudaUnbindTexture(s_tex_luma_0));
        checkCudaErrors(cudaUnbindTexture(s_tex_cra_0));
        checkCudaErrors(cudaUnbindTexture(s_tex_crb_0));
    }
    else if (index == 1)
    {
        checkCudaErrors(cudaUnbindTexture(s_tex_luma_1));
        checkCudaErrors(cudaUnbindTexture(s_tex_cra_1));
        checkCudaErrors(cudaUnbindTexture(s_tex_crb_1));
    }
    else
    {
        checkCudaErrors(cudaUnbindTexture(s_tex_luma_2));
        checkCudaErrors(cudaUnbindTexture(s_tex_cra_2));
        checkCudaErrors(cudaUnbindTexture(s_tex_crb_2));
    }
    //
    // Note: one of the cuda ops before xiphos_free returning error 'code=17(cudaErrorInvalidDevicePointer)'
    // The checkCudaErrors here is picking this error up and incorrectly shows unbinding fails.
    // If we already here, it means that we are already exiting the xiphos and we will do a cuda_reset anyway.
    // Therefore, it might be okay to just ignore this invalid device error.
    // checkCudaErrors(cudaPeekAtLastError());
    return 0;
}

int xsc::image::init_keypoints( xsc::image::level *p,
                                const int cam_levels,
                                const int total_image_bins,
                                const int total_keypoints)
{
    const int flow_buffer     = 6 * xsc::MAX_FLOW_KEYPOINTS;
    const int keypoint_buffer = cam_levels * 2 * xsc::MAX_BAD_KEYPOINTS;
    const int buffer_size     = flow_buffer * sizeof(float) / sizeof(unsigned short) + keypoint_buffer;

    // Flow vectors and bad keypoint correspondences
    if ((p->x0[0] = (float*)(new(std::nothrow) unsigned short[buffer_size])) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    checkCudaErrors(cudaMalloc((void **)&p->x0[1], buffer_size * sizeof(unsigned short)));
    if (p->x0[1] == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    checkCudaErrors(cudaMemset(p->x0[1], 0, buffer_size * sizeof(unsigned short)));

    p->xyb[0] = (unsigned short*)&(p->x0[0][flow_buffer]);
    p->xyb[1] = (unsigned short*)&(p->x0[1][flow_buffer]);

    p->y0[0] = p->x0[0] + xsc::MAX_FLOW_KEYPOINTS;
    p->x1[0] = p->y0[0] + xsc::MAX_FLOW_KEYPOINTS;
    p->y1[0] = p->x1[0] + xsc::MAX_FLOW_KEYPOINTS;
    p->x2[0] = p->y1[0] + xsc::MAX_FLOW_KEYPOINTS;
    p->y2[0] = p->x2[0] + xsc::MAX_FLOW_KEYPOINTS;

    p->y0[1] = p->x0[1] + xsc::MAX_FLOW_KEYPOINTS;
    p->x1[1] = p->y0[1] + xsc::MAX_FLOW_KEYPOINTS;
    p->y1[1] = p->x1[1] + xsc::MAX_FLOW_KEYPOINTS;
    p->x2[1] = p->y1[1] + xsc::MAX_FLOW_KEYPOINTS;
    p->y2[1] = p->x2[1] + xsc::MAX_FLOW_KEYPOINTS;

    p->nf = xsc::MAX_FLOW_KEYPOINTS;

    // Number of keypoints in each image bin
    if ((p->n[0] = new(std::nothrow) int[(total_image_bins+2) * cam_levels]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    checkCudaErrors(cudaMalloc((void **)&p->n[1], (total_image_bins+2) * cam_levels * sizeof(int)));
    if (p->n[1] == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    checkCudaErrors(cudaMemset(p->n[1], 0, (total_image_bins+2) * cam_levels * sizeof(int)));

    p->np[0] = &(p->n[0][total_image_bins+0]);
    p->np[1] = &(p->n[1][total_image_bins+0]);
    p->nb[0] = &(p->n[0][total_image_bins+1]);
    p->nb[1] = &(p->n[1][total_image_bins+1]);

    // Keypoint lists on GPU
    checkCudaErrors(cudaMalloc((void **)&p->bin, total_keypoints * cam_levels * sizeof(unsigned short)));
    if (p->bin == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    checkCudaErrors(cudaMemset(p->bin, 0, total_keypoints * cam_levels * sizeof(unsigned short)));

    checkCudaErrors(cudaMalloc((void **)&p->xy, 2 * total_keypoints * cam_levels * sizeof(unsigned short)));
    if (p->xy == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    checkCudaErrors(cudaMemset(p->xy, 0, 2 * total_keypoints * cam_levels * sizeof(unsigned short)));

    return 0;
}

int xsc::image::free_keypoints(xsc::image::level *p)
{
    if(p->x0[0])
    {
        delete [] p->x0[0];
        p->x0[0] = nullptr;
    }

    if(p->n[0])
    {
        delete [] p->n[0];
        p->n[0] = nullptr;
    }

    if(p->bin)
    {
        cudaFree(p->bin);
        p->bin = nullptr;
    }


    if(p->xy)
    {
        cudaFree(p->xy);
        p->xy = nullptr;
    }

    if(p->x0[1])
    {
        cudaFree(p->x0[1]);
        p->x0[1] = nullptr;
    }

    if(p->n[1])
    {
        cudaFree(p->n[1]);
        p->n[1] = nullptr;
    }

    return 0;
}

int xsc::image::init(xsc::image::level *p,
                     unsigned short *bin,
                     unsigned short *pxy,
                     unsigned short *pxyb0,
                     unsigned short *pxyb1,
                     int *n0,
                     int *n1,
                     int *np0,
                     int *np1,
                     int *nb0,
                     int *nb1,
                     float *fx00,
                     float *fy00,
                     float *fx01,
                     float *fy01,
                     float *fx02,
                     float *fy02,
                     float *fx10,
                     float *fy10,
                     float *fx11,
                     float *fy11,
                     float *fx12,
                     float *fy12,
                     int total_flows,
                     
                     int w,
                     int h)
{
    int hr = w % xsc::ALIGN2D;
    int vr = h % xsc::ALIGN2D;

    int hs = (hr == 0) ? w : w + xsc::ALIGN2D - hr;
    int vs = (vr == 0) ? h : h + xsc::ALIGN2D - vr;

    if ((p->i[0] = new(std::nothrow) float[hs * vs]) == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    p->bin    = bin;
    p->xy     = pxy;
    p->xyb[0] = pxyb0;
    p->xyb[1] = pxyb1;
    p->n[0]   = n0;
    p->n[1]   = n1;
    p->np[0]  = np0;
    p->np[1]  = np1;
    p->nb[0]  = nb0;
    p->nb[1]  = nb1;

    checkCudaErrors(cudaMalloc((void **)&p->i[1], hs * vs * sizeof(float)));
    if (p->i[1] == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }

    for (int i = 0; i < 9; i++)
    {
        p->fhm[i] = 0.f;
        p->hm[i] = 0.f;
    }

    p->x0[0] = fx00;
    p->y0[0] = fy00;
    p->x1[0] = fx01;
    p->y1[0] = fy01;
    p->x2[0] = fx02;
    p->y2[0] = fy02;

    p->x0[1] = fx10;
    p->y0[1] = fy10;
    p->x1[1] = fx11;
    p->y1[1] = fy11;
    p->x2[1] = fx12;
    p->y2[1] = fy12;

    p->nf = total_flows;

    p->w = w;
    p->h = h;
    p->hs = hs;
    p->vs = vs;
    p->hv = 0;

    return 0;
}

int xsc::image::free(xsc::image::level *p)
{
    delete [] p->i[0];
    p->i[0]  = nullptr;
    p->n[0]   = nullptr;
    p->np[0]  = nullptr;
    p->nb[0]  = nullptr;
    p->xyb[0] = nullptr;

    cudaFree(p->i[1]);
    p->i[1]   = nullptr;
    p->bin    = nullptr;
    p->xy     = nullptr;
    p->n[1]   = nullptr;
    p->np[1]  = nullptr;
    p->nb[1]  = nullptr;
    p->xyb[1] = nullptr;

    for (int i = 0; i < 9; i++)
    {
        p->fhm[i] = 0.f;
        p->hm[i] = 0.f;
    }

    p->w = 0;
    p->h = 0;
    p->hs = 0;
    p->vs = 0;
    p->hv = 0;

    return 0;
}

int xsc::image::update_downsample(xsc::image::level *p)
{
    dim3 bd(xsc::ALIGN2D, xsc::ALIGN2D, 1);
    dim3 gd(p->hs / xsc::ALIGN2D, p->vs / xsc::ALIGN2D);

    // Down-sample the image to the level prescribed by the pyramid level
    downsample<<<gd, bd>>>(p[0].i[1], p->hs, p[-1].i[1], p[-1].hs, p->w, p->h);

    getLastCudaError("downsample failed");

    return 0;
}

int xsc::image::update_keypoints(   xsc::image::level *p,
                                    float* const thresholds,
                                    int const num_image_bins,
                                    int const max_keypoints)
{
    const unsigned short bw = p->w / num_image_bins;
    const unsigned short bh = p->h / num_image_bins;

    const int total_image_bins = num_image_bins * num_image_bins;

    const int bx = xsc::ALIGN2D;
    const int by = xsc::ALIGN2D;
    const int bz = 4;

    const int gx = bw / bx + 1;
    const int gy = bh / by + 1;
    const int gz = total_image_bins / bz + 1;

    dim3 bd(bx, by, bz);
    dim3 gd(gx, gy, gz);

    // Detect keypoints in this down-sampled image
    keypoint_detect<<<gd, bd>>>(p->i[1],
                                p->hs,
                                p->w,
                                p->h,
                                bw,
                                bh,
                                thresholds,
                                num_image_bins,
                                max_keypoints,
                                p->xy,
                                p->bin,
                                p->n[1]);
    getLastCudaError("keypoints failed");

    return 0;
}

int xsc::image::sample_flow_points( xsc::image::level *p, const int total_keypoints)
{
    const int bx = xsc::ALIGN1D;
    const int by = 1;
    const int gx = (p->nf + xsc::ALIGN1D - 1) / xsc::ALIGN1D;
    const int gy = 1;

    dim3 bd(bx, by);
    dim3 gd(gx, gy);

    sample_flows<<<gd, bd>>>(   p->x0[1],
                                p->y0[1],
                                p->xy,
                                p->np[1],
                                p->nf,
                                total_keypoints);
    getLastCudaError("sample_flows failed");

    return 0;
}

int xsc::image::zero_keypoints(xsc::image::level *p, int levels, const int total_image_bins)
{
    checkCudaErrors(cudaMemset(p->n[1], 0, (total_image_bins+2) * sizeof(int) * levels ));

    return 0;
}

int xsc::image::copyback_keypoints( xsc::image::level *p,
                                    const int levels,
                                    const int num_image_bins,
                                    const int total_keypoints,
                                    const int max_keypoints)
{
    const int keypoint_buffer = (levels - 1) * 2 * xsc::MAX_BAD_KEYPOINTS;
    const int buffer_size     = keypoint_buffer * sizeof(unsigned short);

    const int total_image_bins = num_image_bins * num_image_bins;

    // Copy the keypoints and float image back to the host
    // Assume that we do not get keypoints on pyramid level zero
    checkCudaErrors(cudaMemcpy(p->n[0], p->n[1], (total_image_bins+2) * (levels - 1) * sizeof(int), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(p->xyb[0], p->xyb[1], buffer_size, cudaMemcpyDeviceToHost));

    for (int j = 1; j < levels; j++)
    {
        xsc::image::level *q = p + j;
        for(int i = 0; i < total_image_bins; i++)
        {
            q->n[0][i] = MIN(q->n[0][i], max_keypoints);
        }

        // fprintf(stderr, "%d\tq->nb[0][0] : %d\tq->np[0][0] : %d\n", j, q->nb[0][0], q->np[0][0]);
        q->nb[0][0] = MIN(q->nb[0][0], xsc::MAX_BAD_KEYPOINTS);
        q->np[0][0] = MIN(q->np[0][0], total_keypoints);
    }

    getLastCudaError("keypoints copy failed");

    return 0;
}

int xsc::image::copyback_matches( xsc::image::level *p,
                                  const int levels,
                                  const int total_image_bins,
                                  const int total_keypoints,
                                  const int max_keypoints)
{
    const int flow_buffer = 6 * p->nf;
    const int buffer_size = flow_buffer * sizeof(float);

    checkCudaErrors(cudaMemcpy(p->n[0], p->n[1], (total_image_bins+2) * (levels - 1) * sizeof(int), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(p->x0[0], p->x0[1], buffer_size, cudaMemcpyDeviceToHost));

    for (int j = 1; j < levels; j++)
    {
        xsc::image::level *q = p + j;
        for(int i = 0; i < total_image_bins; i++)
        {
            q->n[0][i] = MIN(q->n[0][i], max_keypoints);
        }

        q->np[0][0] = MIN(q->np[0][0], total_keypoints);
    }

    getLastCudaError("matches copy failed");

    return 0;
}

int xsc::camera::classify(xsc::camera::level* p,
                          xsc::image::level*  q,
                          float* thresholds,
                          float factor,
                          float* T,
                          const int total_keypoints)
{
    const int bx = xsc::ALIGN1D;
    const int by = 1;
    const int gx = (total_keypoints + xsc::ALIGN1D - 1) / xsc::ALIGN1D;
    const int gy = 1;

    dim3 bd(bx, by);
    dim3 gd(gx, gy);

    // set a lower bound on the residual
    const float res = MAX(p->res_filt, xsc::MIN_CLASSIFIER_RESIDUAL);

    classify_keypoints<<<gd, bd>>>( q->w,
                                    q->h,
                                    factor * p->gsc * res,
                                    thresholds,
                                    T,
                                    q->i[1],
                                    q->hs,
                                    p->kf->i[1],
                                    p->kf->hs,
                                    q->bin,
                                    q->xy,
                                    q->np[1],
                                    q->xyb[1],
                                    q->nb[1]);

    return 0;
}

// UYVY ===============================================================
int xsc::image::upload_uyvy(xsc::image::level *p, uint8_t* const dst_gpu,
                              const uint8_t* const src_cpu, int stride)
{
    // Copy the image to the device (luma and chroma)
    checkCudaErrors(cudaMemcpy(dst_gpu, src_cpu, p->w * 2 * p->h, cudaMemcpyHostToDevice));
    return 0;
}

int xsc::image::convert_uyvy(xsc::image::level *p, uint8_t* const dst_gpu,
                             uint8_t* const src_gpu )
{
    dim3 bd(xsc::ALIGN2D, xsc::ALIGN2D, 1);
    dim3 ngd(p->hs / xsc::ALIGN2D, (p->vs * 3/2 + xsc::ALIGN2D) / xsc::ALIGN2D);

    // Convert the image to I420
    uyvy_to_yuv420<<<ngd, bd>>>(dst_gpu, p->hs, src_gpu, p->hs * 2, p->w, p->h);

    getLastCudaError("convert_uyvy failed");

    return 0;
}

int xsc::image::convert_uyvy_bilinear( level *p,
                                        uint8_t* const dst_gpu,
                                        const int index)
{
    dim3 bd(xsc::ALIGN2D, xsc::ALIGN2D);
    dim3 gd(p->hs / xsc::ALIGN2D, p->vs / xsc::ALIGN2D);

     // I420 luma and chroma channel size
    const int ps = p->h * p->w;
    const int us = ps / 4;

    // Warp image with filtering
    if (index == 0)
    {
        copy_to_I420_from_textures_kernel_0<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 1.f);
    }
    else if (index == 1)
    {
        copy_to_I420_from_textures_kernel_1<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 1.f);
    }
    else
    {
        copy_to_I420_from_textures_kernel_2<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 1.f);
    }

    return 0;
}

int xsc::image::update_uyvy_bilinear(   level *p,
                                        uint8_t* const dst_luma,
                                        uint8_t* const dst_cra,
                                        uint8_t* const dst_crb,
                                        uint8_t* const src_gpu,
                                        const int index)
{
    dim3 bd(xsc::ALIGN2D, xsc::ALIGN2D);
    dim3 gd(p->hs / xsc::ALIGN2D, p->vs / xsc::ALIGN2D);

    // Copy the image into the texture buffers
    uyvy_to_tex_kernel<<<gd, bd>>>( dst_luma, p->w,
                                    dst_cra, p->w/2,
                                    dst_crb, p->w/2,
                                    (uint32_t*)src_gpu, p->hs/2,
                                    p->w, p->h);

    // Convert the image to grayscale floating point [0.0, 1.0]
    if (index == 0)
    {
        tex_to_iimage_kernel_0<<<gd, bd>>>(p->i[1], p->hs, p->w, p->h);
    }
    else if (index == 1)
    {
        tex_to_iimage_kernel_1<<<gd, bd>>>(p->i[1], p->hs, p->w, p->h);
    }
    else
    {
        tex_to_iimage_kernel_2<<<gd, bd>>>(p->i[1], p->hs, p->w, p->h);
    }

    getLastCudaError("update_uyvy_bilinear failed");

    return 0;
}

int xsc::image::download_uyvy(xsc::image::level *p, uint8_t* const dst_cpu, uint8_t* const src_gpu)
{
    // Copy the image back to host
    checkCudaErrors(cudaMemcpy((void *)dst_cpu, src_gpu, p->w * p->h * 3 / 2, cudaMemcpyDeviceToHost));
    return 0;
}

int xsc::image::warp_uyvy_bilinear( level *p,
                                    uint8_t* const dst_gpu,
                                    const int index, const int split,
                                    const float* const h)
{
    dim3 bd(xsc::ALIGN2D, xsc::ALIGN2D);
    dim3 gd(p->hs / xsc::ALIGN2D, p->vs / xsc::ALIGN2D);

    // I420 luma and chroma channel size
    const int ps = p->h * p->w;
    const int us = ps / 4;

    // Warp image with filtering
    if (index == 0)
    {
        if (split)
        {
            half_warp_to_I420_from_textures_kernel_0<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 1.f, h);
        }
        else
        {
            warp_to_I420_from_textures_kernel_0<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 1.f, h);
        }
    }
    else if (index == 1)
    {
        if (split)
        {
            half_warp_to_I420_from_textures_kernel_1<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 1.f, h);
        }
        else
        {
            warp_to_I420_from_textures_kernel_1<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 1.f, h);
        }
    }
    else
    {
        if (split)
        {
            half_warp_to_I420_from_textures_kernel_2<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 1.f, h);
        }
        else
        {
            warp_to_I420_from_textures_kernel_2<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 1.f, h);
        }
    }

    getLastCudaError("warp_uyvy_bilinear failed");

    return 0;
}

// YUY422 ===============================================================
int xsc::image::upload_yuy422(xsc::image::level *p, uint8_t* const dst_gpu,
                              const uint8_t* const src_cpu, int stride)
{
    // Copy the image to the device (luma and chroma))
    checkCudaErrors(cudaMemcpy2D(dst_gpu, p->hs * 2, src_cpu, stride, p->w * 2, p->h, cudaMemcpyHostToDevice));

    return 0;
}

int xsc::image::convert_yuy422(xsc::image::level *p, uint8_t* const dst_gpu,
                             uint8_t* const src_gpu )
{
    dim3 bd(xsc::ALIGN2D, xsc::ALIGN2D, 1);
    dim3 ngd(p->hs / xsc::ALIGN2D, (p->vs * 3/2 + xsc::ALIGN2D) / xsc::ALIGN2D);

    // Convert the image to I420
    yuy422_to_yuv420<<<ngd, bd>>>(dst_gpu, p->hs, src_gpu, p->hs * 2, p->w, p->h);

    getLastCudaError("convert_yuy422 failed");

    return 0;
}

int xsc::image::download_yuy422(xsc::image::level *p, uint8_t* const dst_cpu, uint8_t* const src_gpu)
{
    // Copy the image back to host
    checkCudaErrors(cudaMemcpy2D((void *)dst_cpu, p->hs, src_gpu, p->hs, p->w, p->h * 3 / 2, cudaMemcpyDeviceToHost));

    return 0;
}

int xsc::image::update_yuy422(level *p, uint8_t* const src_gpu)
{
    dim3 bd(xsc::ALIGN2D, xsc::ALIGN2D, 1);
    dim3 gd(p->hs / xsc::ALIGN2D, p->vs / xsc::ALIGN2D);

    // Convert the image to grayscale floating point [0.0, 1.0]
    format_yuy422<<<gd, bd>>>(p->i[1], p->hs, src_gpu, p->hs * 2, p->w, p->h);

    getLastCudaError("update_yuy422 failed");

    return 0;
}

int xsc::image::warp_yuy422(xsc::image::level *p, uint8_t* const dst_gpu, uint8_t* const src_gpu, const float* const h)
{
    dim3 bd(xsc::ALIGN2D, xsc::ALIGN2D, 1);
    dim3 gd(p->hs / xsc::ALIGN2D, p->vs / xsc::ALIGN2D);

    // Warp image
    warp_image_yuv420_naive<<<gd, bd>>>(dst_gpu, p->hs, src_gpu, p->hs, p->w, p->h, h);

    getLastCudaError("warp_yuy422 failed");

    return 0;
}

// YUV420 ===============================================================
int xsc::image::upload_yuv420(xsc::image::level *p, uint8_t* const dst_gpu,
                              const uint8_t* const src_cpu, int stride)
{
    // Copy the image to the device (luma and chroma))
    checkCudaErrors(cudaMemcpy2D(dst_gpu, p->hs, src_cpu, stride, p->w, p->h * 3 / 2, cudaMemcpyHostToDevice));

    return 0;
}

int xsc::image::copy_yuv420(xsc::image::level *p,
                             uint8_t* const dst_cpu, uint8_t* const src_cpu,
                             int stride)
{
    // Copy the image from host to host
    checkCudaErrors(cudaMemcpy2D(dst_cpu, stride, src_cpu, stride, p->w, p->h * 3 / 2, cudaMemcpyHostToHost));
    getLastCudaError("copy_yuv420 failed");

    return 0;
}

int xsc::image::download_yuv420(xsc::image::level *p, uint8_t* const dst_cpu, uint8_t* const src_gpu)
{
    // Copy the image back to host
    checkCudaErrors(cudaMemcpy2D((void *)dst_cpu, p->hs, src_gpu, p->hs, p->w, p->h * 3 / 2, cudaMemcpyDeviceToHost));
    return 0;
}

int xsc::image::update_yuv420_bilinear( level *p,
                                        uint8_t* const dst_luma,
                                        uint8_t* const dst_cra,
                                        uint8_t* const dst_crb,
                                        uint8_t* const src_gpu,
                                        const int index)
{
    dim3 bd(xsc::ALIGN2D, xsc::ALIGN2D);

    // Copy into the luma buffer
    dim3 gd(p->hs / xsc::ALIGN2D, (p->vs + xsc::ALIGN2D) / xsc::ALIGN2D); // 32 bit access in luma
    yuv420_to_luma_kernel<<<gd, bd>>>((uint32_t*)dst_luma, p->w / 4,
                                      (uint32_t*)src_gpu, p->hs / 4,
                                      p->w / 4, p->h);

    // Copy into the chroma buffers
    dim3 cgd(p->hs / xsc::ALIGN2D / 8, (p->vs + xsc::ALIGN2D) / xsc::ALIGN2D / 2); // 32 bit access in chroma
    int crap = p->w * p->h / 4; // in 32 bit
    int crbp = crap + crap / 4; // in 32 bit

    yuv420_to_chroma_kernel<<<cgd, bd>>>((uint32_t*)dst_cra, p->w / 8,
                                         (uint32_t*)src_gpu, p->hs / 4 / 2, crap,
                                         p->w / 8, p->h / 2);

    yuv420_to_chroma_kernel<<<cgd, bd>>>((uint32_t*)dst_crb, p->w / 8,
                                         (uint32_t*)src_gpu, p->hs / 4 / 2, crbp,
                                         p->w / 8, p->h / 2);

    dim3 igd(p->hs / xsc::ALIGN2D, (p->vs + xsc::ALIGN2D) / xsc::ALIGN2D);

    if (index == 0)
    {
        tex_to_iimage_kernel_0<<<igd, bd>>>(p->i[1], p->hs, p->w, p->h);
    }
    else if (index == 1)
    {
        tex_to_iimage_kernel_1<<<igd, bd>>>(p->i[1], p->hs, p->w, p->h);
    }
    else
    {
        tex_to_iimage_kernel_2<<<igd, bd>>>(p->i[1], p->hs, p->w, p->h);
    }

    getLastCudaError("update_yuv420_bilinear failed");

    return 0;
}

int xsc::image::warp_yuv420_bilinear(   level *p,
                                        uint8_t* const dst_gpu,
                                        const int index, const int split,
                                        const float* const h)
{
    dim3 bd(xsc::ALIGN2D, xsc::ALIGN2D);
    dim3 gd(p->hs / xsc::ALIGN2D, (p->vs + xsc::ALIGN2D) / xsc::ALIGN2D);

    // I420 luma and chroma channel size
    const int ps = p->h * p->w;
    const int us = ps / 4;

    // Warp image with filtering
    if (index == 0)
    {
        if (split)
        {
            half_warp_to_I420_from_textures_kernel_0<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 0.5f, h);
        }
        else
        {
            warp_to_I420_from_textures_kernel_0<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 0.5f, h);
        }
    }
    else if (index == 1)
    {
        if (split)
        {
            half_warp_to_I420_from_textures_kernel_1<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 0.5f, h);
        }
        else
        {
            warp_to_I420_from_textures_kernel_1<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 0.5f, h);
        }
    }
    else
    {
        if (split)
        {
            half_warp_to_I420_from_textures_kernel_2<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 0.5f, h);
        }
        else
        {
            warp_to_I420_from_textures_kernel_2<<<gd, bd>>>(dst_gpu, p->w,
                                                          p->w, p->h, ps, us,
                                                          0.5f, 0.5f, h);
        }
    }

    getLastCudaError("warp_uyvy_bilinear failed");

    return 0;
}

int xsc::image::cuda_device_reset()
{
    checkCudaErrors(cudaDeviceReset());
    getLastCudaError("cuda device reset failed");
    return 0;
}
