#include "config.h"
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>

#include "xipvis.h"
#include "xipvis_private.h"

int xsc::init_gpu(xipvis* const x, int w, int h, const int format)
{
    checkCudaErrors(cudaMalloc((void **)&x->thresholds[1], (2 * x->total_image_bins + 12) * sizeof(float)));
    if (x->thresholds[1] == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    x->egomotion[1] = &(x->thresholds[1][2 * x->total_image_bins]);
    
    // Ensure proper byte alignment
    h += xsc::ALIGN2D - h % xsc::ALIGN2D;
    w += xsc::ALIGN2D - w % xsc::ALIGN2D;
        
    checkCudaErrors(cudaMalloc((void **)&x->pb, w * h * 2));
    if (x->pb == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    checkCudaErrors(cudaMalloc((void **)&x->px, w * h * 2));
    if (x->px == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    checkCudaErrors(cudaMalloc((void **)&x->d_luma, w * h));
    if (x->d_luma == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    checkCudaErrors(cudaMalloc((void **)&x->d_cra, w * h / 2));
    if (x->d_cra == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    
    checkCudaErrors(cudaMalloc((void **)&x->d_crb, w * h / 2));
    if (x->d_crb == nullptr)
    {
        XIPVIS_ERROR(errno, , -1);
    }
    

    return 0;
}

int xsc::update_gpu(xipvis* const x)
{
    // Copy the thresholds to the device
    checkCudaErrors(cudaMemcpy(x->thresholds[1], x->thresholds[0], (2 * x->total_image_bins + 12) * sizeof(float), cudaMemcpyHostToDevice));
    
    return 0;
}

int xsc::free_gpu(xipvis* const x)
{    
    if(x->thresholds[1])
    {
        cudaFree(x->thresholds[1]);
        x->thresholds[1] = nullptr;
    }

    x->egomotion[1] = nullptr;
    
    if(x->pb)
    {
        cudaFree(x->pb);
        x->pb = nullptr;
    }

    if(x->px)
    {
        cudaFree(x->px);
        x->px = nullptr;
    }

    
    x->tex_luma = nullptr;    
    x->tex_cra  = nullptr;    
    x->tex_crb  = nullptr;
    
    if(x->d_luma)
    {
        cudaFree(x->d_luma);
        x->d_luma = nullptr;
    }

    if(x->d_cra)
    {
        cudaFree(x->d_cra);
        x->d_cra = nullptr;
    }

    if(x->d_crb)
    {
        cudaFree(x->d_crb);
        x->d_crb = nullptr;
    }
    
    return 0;
}
    
