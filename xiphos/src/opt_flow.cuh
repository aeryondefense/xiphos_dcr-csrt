// Implementation of the Pyramidal Lucas-Kanade Optical Flow CUDA Kernel
// Define PYRLK_RADIUS to be the window_half_width for a particular definition,
//  then include this file to create the kernel body.
{
    const unsigned int window_half_width  = PYRLK_RADIUS;
    const unsigned int window_half_height = PYRLK_RADIUS;
    const unsigned int window_width       = 2*window_half_width;
    const unsigned int window_height      = 2*window_half_height;
    const unsigned int window_size        = window_width*window_height;
    const unsigned int block_size         = window_size;
    
    // Accumulation buffers in shared memory
    __shared__ float buff1[block_size];
    __shared__ float buff2[block_size];
    __shared__ float buff3[block_size];
    
    // Thread index
    const unsigned int tid = threadIdx.y * blockDim.x + threadIdx.x;
    
    // Keypoint index
    unsigned int idx = blockIdx.x;
    
    // Find the scaling for this pyramid level
    const float scale = (1 << level);
    const float inv_scale = 1.0 / scale;
    
    // Scale the previous feature positions
    float ix = pIx[idx] * inv_scale - window_half_width  + 0.5;
    float iy = pIy[idx] * inv_scale - window_half_height + 0.5;
    
    // Check if this point is out of bounds in the previous image
    if (ix < 1.0 || ix > (float)(width  - 2 - window_width) 
     || iy < 1.0 || iy > (float)(height - 2 - window_height))
    {
        // If this is the base level (0), mark the point as invalid
        if (tid == 0)
        {
            if (level == 0)
            {
                dJx[idx] = -10.f;
                dJy[idx] = 0.f;
            }
            else if (zero)
            {
                dJx[idx] = 0.f;
                dJy[idx] = 0.f;
            }
        }
        
        return;
    }
    
    // Add on the incremental position for this thread
    float iix = ix + threadIdx.x;
    float iiy = iy + threadIdx.y;
    
    // Find the spatial derivatives around the patch in image I
    const float I  = interp2(pI, sI, iix, iiy);
    const float Ix =  -3.0*interp2(pI, sI, iix-1, iiy-1) +  3.0*interp2(pI, sI, iix+1, iiy-1)
                     -10.0*interp2(pI, sI, iix-1, iiy)   + 10.0*interp2(pI, sI, iix+1, iiy)
                      -3.0*interp2(pI, sI, iix-1, iiy+1) +  3.0*interp2(pI, sI, iix+1, iiy+1);
    const float Iy =  -3.0*interp2(pI, sI, iix-1, iiy-1) - 10.0*interp2(pI, sI, iix, iiy-1) - 3.0*interp2(pI, sI, iix+1, iiy-1)
                     + 3.0*interp2(pI, sI, iix-1, iiy+1) + 10.0*interp2(pI, sI, iix, iiy+1) + 3.0*interp2(pI, sI, iix+1, iiy+1);
    
    // Set this term for matrix G summing
    buff1[tid] = Ix*Ix;
    buff2[tid] = Ix*Iy;
    buff3[tid] = Iy*Iy;
    
    // Sum all the terms of matrix G elements, results in buffX[0]
    reduce_sum3<block_size>(buff1, buff2, buff3);
    
    // Matrix G = [ gxx, gxy ; gxy, gyy ] elements
    float gxx = buff1[0];
    float gxy = buff2[0];
    float gyy = buff3[0];
    
    // Find the determinant to check invertibility
    const float det = gxx*gyy - gxy*gxy;
    
    // Check for zero determinant
    if (abs(det) < 0.000001)
    {
        // If this is the base level (0), mark the point as invalid
        if (tid == 0)
        {
            if (level == 0)
            {
                dJx[idx] = -20.f;
                dJy[idx] = 0.f;
            }
            else if (zero)
            {
                dJx[idx] = 0.f;
                dJy[idx] = 0.f;
            }
        }
        
        return;
    }
    
    const float inv_det = 32.0/det;
    
    // Starting position of the feature in the next image
    float jx = ix;
    float jy = iy;
    
    // If this is not the first level, use the previous flow vector
    if (!zero)
    {
        jx += dJx[idx] * inv_scale;
        jy += dJy[idx] * inv_scale;
    }
    
    // Iterate on the LK update step
    int k;
    for (k = 0; k < xsc::stabilize::MAX_ITERATIONS; k++)
    {
        // Check if this point is out of bounds in the previous image
        if (jx < 1.0 || jx > (float)(width  - 2 - window_width) 
         || jy < 1.0 || jy > (float)(height - 2 - window_height))
        {
            // If this is the base level (0), mark the point as invalid
            if (tid == 0)
            {
                if (level == 0)
                {
                    dJx[idx] = -30.f;
                    dJy[idx] = 0.f;
                }
                else if (zero)
                {
                    dJx[idx] = 0.f;
                    dJy[idx] = 0.f;
                }
            }
            
            return;
        }
        
        // Find the pixel intensity error at the current location
        const float dI = inv_det * (I - interp2(pJ, sJ, jx + threadIdx.x, jy + threadIdx.y));
        
        // Form the image mismatch vector, b = [ bx ; by ]
        buff1[tid] = dI*Ix;
        buff2[tid] = dI*Iy;
        
        // Sum all the terms of vector b elements, results in buffX[0]
        reduce_sum2<block_size>(buff1, buff2);

        const float bx = buff1[0];
        const float by = buff2[0];
        
        // Find update vector, v = [ vx ; vy ] = inv(G)*b
        const float vx = (gyy*bx - gxy*by);
        const float vy = (gxx*by - gxy*bx);
        
        // Add the update vector to the flow vector
        jx += vx;
        jy += vy;
        
        // If the update vector is small, early exit
        if (abs(vx) < 0.01 && abs(vy) < 0.01)
        {
            break;
        }
    }
    
    // Store the new coordinates for this feature
    if (tid == 0)
    {
        if (level == 0)
        {
            // Full image coordinates at pyramid level zero
            dJx[idx] = (jx + window_half_width  - 0.5) * scale;
            dJy[idx] = (jy + window_half_height - 0.5) * scale;
        }
        else
        {
            // Only delta coordinates for non-zero pyramid level (will be used again)
            dJx[idx] = (jx - ix) * scale;
            dJy[idx] = (jy - iy) * scale;
        }
    }
}

