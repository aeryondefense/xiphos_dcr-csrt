/* Copyright (C) 2015 Xiphos Systems Corporation
 */

#ifndef BERN_H
#define BERN_H

namespace xsc
{
namespace samplers
{
class bern
{
    xsc::samplers::uint &u_;

public:
    bern(xsc::samplers::uint &u);

    bern(const xsc::samplers::uint &) = delete;
    xsc::samplers::bern &operator = (const xsc::samplers::uint &) = delete;

    float operator () (void);
    float operator () (float a, float b);
};
}
}

inline xsc::samplers::bern::bern(xsc::samplers::uint &u) : u_(u)
{
}

inline float xsc::samplers::bern::operator () (void)
{
    return ((u_() & 1) == 0) ? 0 : 1;
}

inline float xsc::samplers::bern::operator () (float a, float b)
{
    return ((u_() & 1) == 0) ? a : b;
}

#endif /* #ifndef BERN_H */
