/* Copyright (C) 2015 Xiphos Systems Corporation
 */

#ifndef CAMERA_H
#define CAMERA_H

namespace xsc
{
namespace camera
{
struct level
{
    float *is;  // Samples of point pixel intensity
    float *xs;  // Samples of point x camera-frame coordinate in keyframe
    float *ys;  // Samples of point y camera-frame coordinate
    int    ns;  // Number of sampled points
    
    int   nl;   // Number of lost point matches
    float residual; // Residual for current camera motion
    float res_filt; // Filtered camera motion residual
    float gsc;  // Good point correspondence sensitivity constant
    
    Matrix3f Hk; // Cumulative homography to keyframe
    Matrix3f Hp; // Homography to previous image
    
    int scale;  // Pyramid level
    
    uint8_t*    im_data;
    int         im_stride;
    int         im_width;
    int         im_height;
    format_type im_format;

    uint8_t*    kf_data;
    int         kf_stride;
    int         kf_width;
    int         kf_height;
    format_type kf_format;
    
    xsc::image::level* kf; // Image used as the keyframe
    
    int initialized; // Has been initialized with a valid keyframe

    level(void);
    virtual ~level(void);

    level(const level &) = delete;
    level &operator = (const level &) = delete;
};

template <int N> using pyramid = level[N];

int init(level *p, int scale, int width, int height, sensor_type sensor, int total_keypoints);

int free(level *p);

int reset(level *p);

int iniframe(level *p, const xsc::image::level *q);

int keyframe(level *p, const xsc::image::level *q);

int auxframe(level *p, const xsc::image::level *q);

template <int N>
int init(level *p, int width, int height, sensor_type sensor, int total_keypoints)
{
    for (int i = 0; i < N; i++)
    {
        if (xsc::camera::init(p + i, i, width, height, sensor, total_keypoints) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }

        width  /= 2;
        height /= 2;
    }

    return 0;
}

template <int N>
int free(level *p)
{
    for (int i = 0; i < N; i++)
    {
        if (xsc::camera::free(p + i) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }

    return 0;
}

template <int N>
int reset(level *p)
{
    for (int i = 0; i < N; i++)
    {
        if (xsc::camera::reset(p + i) < 0)
        {
            XIPVIS_ERROR(errno, , -1);
        }
    }

    return 0;
}

// Camera-level operations on each incoming image.
// Track the camera motion across the incoming images relative
//  to the chosen keyframe.
template <int N>
int update( level *p, int frame, 
            xsc::image::level *q, 
            uint8_t* im_data, 
            int im_stride, 
            format_type im_format, 
            float *res, 
            const float* thresholds, 
            const float avg_keypoint_thresh, 
            const int max_keypoints,
            const int solve_egomotion)
{
    *res = 1E6;
    
    // Set up the current image information for each level
    for (int i = 0; i < N; i++)
    {
        p[i].im_data   = im_data;
        p[i].im_stride = im_stride;
        p[i].im_format = im_format;
    }
    
    // For all of the pyramid levels, starting at the top (smallest image)
    for (int i = N - 1; 0 < i; i--)
    {
        if (!p[i].initialized)
        {
            p[i].kf = q;
        }
        else
        {
            // Track the relative pose of this frame wrt the current keyframe
            if (xsc::camera::auxframe(p + i, q + i) < 0)
            {
                XIPVIS_ERROR(errno, , -1);
            }
            
            *res = p[i].residual;
        }
    }
    
    return 0;
}

template <int N>
int update_keyframe(level *p, int frame, const xsc::image::level *q)
{
    // Make sure that all camera levels are initialized
    for (int i = N - 1; 0 < i; i--)
    {
        if (!p[i].initialized)
        {
            if (xsc::camera::iniframe(p + i, q + i) < 0)
            {
                XIPVIS_ERROR(errno, , -1);
            }
        }
    }
    
    int doiniframe = 0;
    // Add this frame as a keyframe.
    for (int i = N - 1; 0 < i; i--)
    {
        // Add each level as part of the keyframe.
        // If it fails, restart the estimation by re-initializing the keyframes.
        if (xsc::camera::keyframe(p + i, q + i) < 0)
        {
            doiniframe = 1;
        }
    }

    // If we need to restart the motion tracking from scratch
    if (doiniframe)
    {
        for (int i = N - 1; 0 < i; i--)
        {
            if (xsc::camera::iniframe(p + i, q + i) < 0)
            {
                XIPVIS_ERROR(errno, , -1);
            }
        }

    }
    
    return 0;
}

int classify( xsc::camera::level* p,
              xsc::image::level*  q,
              float* thresholds,
              float factor,
              float* T,
              const int total_keypoints);

template <int M>
int classify( xsc::camera::level* p,
              xsc::image::level*  q,
              float* thresholds,
              float factor,
              float* T,
              const int total_keypoints)
{
    for (int i = 1; i < M; i++)
    {
        xsc::camera::classify(  p + i,
                                q + i,
                                thresholds,
                                factor,
                                T,
                                total_keypoints);
    }
    
    return 0;
}

}
}

#endif /* #ifndef CAMERA_H */

