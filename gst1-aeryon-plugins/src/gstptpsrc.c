/*
 * GStreamer
 * Copyright (C) 2005 Thomas Vander Stichele <thomas@apestaart.org>
 * Copyright (C) 2005 Ronald S. Bultje <rbultje@ronald.bitfreak.net>
 * Copyright (C) 2015  <<user@hostname.org>>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-ptp_src
 *
 * FIXME:Describe ptp_src here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m ptpsrc is-1080p=true ! "image/jpeg, width=1920, height=1080" ! fakesink
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <unistd.h>
#include <gst/gst.h>
#include <string.h>

#include "gstptpsrc.h"
#include "gstptpbufferpool.h"

#include "trace.h"

#define USLEEP_CAM_COMMAND_LATENCY_FACTOR 150000

GST_DEBUG_CATEGORY (gst_ptp_src_debug);
#define GST_CAT_DEFAULT gst_ptp_src_debug

/* Filter signals and args */
enum
{
    /* FILL ME */
    LAST_SIGNAL
};

enum
{
    PROP_0,
    PROP_1080P,
    PROP_LAST_PTS
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
        GST_PAD_SRC,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS ("image/jpeg, width=1024, height=768, framerate=(fraction)30/1;"
                         "image/jpeg, width=1920, height=1080, framerate=(fraction)30/1"));


#define gst_ptp_src_parent_class parent_class
G_DEFINE_TYPE (GstPtpSrc, gst_ptp_src, GST_TYPE_PUSH_SRC);

static void gst_ptp_src_set_property (GObject * object, guint prop_id,
        const GValue * value, GParamSpec * pspec);
static void gst_ptp_src_get_property (GObject * object, guint prop_id,
        GValue * value, GParamSpec * pspec);

/* GObject vmethod implementations */

/* element methods */
static GstStateChangeReturn gst_ptp_src_change_state(GstElement *element, GstStateChange transition);
static void gst_ptp_src_set_context(GstElement *element, GstContext *context);

/* basesrc methods */
//static GstCaps *gst_ptp_src_get_caps(GstBaseSrc *src, GstCaps *filter);
static gboolean gst_ptp_src_set_caps(GstBaseSrc *src, GstCaps *caps);
static gboolean gst_ptp_src_start(GstBaseSrc *src);
static gboolean gst_ptp_src_stop(GstBaseSrc *src);
static gboolean gst_ptp_src_decide_allocation(GstBaseSrc *src, GstQuery *query);

/* pushsrc methods */
static GstFlowReturn gst_ptp_src_fill(GstPushSrc *src, GstBuffer *out);

GstContext *gst_context_new_ptpcam(GstPTPCam *ptpcam, gboolean persistent)
{
    GstContext *context;
    GstStructure *s;

    context = gst_context_new(GST_PTPCAM_CONTEXT_TYPE, persistent);
    s = gst_context_writable_structure(context);
    gst_structure_set(s, "camera", GST_TYPE_PTPCAM, ptpcam, NULL);

    return context;
}

gboolean gst_context_get_ptpcam(GstContext *context, GstPTPCam **ptpcam)
{
    const GstStructure *s;

    g_return_val_if_fail(GST_IS_CONTEXT(context), FALSE);
    g_return_val_if_fail(strcmp(gst_context_get_context_type(context), 
                GST_PTPCAM_CONTEXT_TYPE) == 0, FALSE);

    s = gst_context_get_structure(context);
    return gst_structure_get(s, "camera", GST_TYPE_PTPCAM, ptpcam, NULL);
}

void gst_ptpcam_destroy(gpointer data)
{
    PTPCam *camera = (PTPCam *)data;

    close_camera(camera);
}

GstPTPCam *gst_ptpcam_new(PTPCam *camera, GDestroyNotify destroy_notify)
{
    GstPTPCam *ptpcam;

    ptpcam = g_slice_new(GstPTPCam);
    ptpcam->camera = camera;
    ptpcam->refcount = 1;
    ptpcam->destroy_notify = destroy_notify;

    return ptpcam;
}

GstPTPCam *gst_ptpcam_ref(GstPTPCam *ptpcam)
{
    g_return_val_if_fail(ptpcam != NULL, NULL);

    g_atomic_int_inc(&ptpcam->refcount);

    return ptpcam;
}

void gst_ptpcam_unref(GstPTPCam *ptpcam)
{
    g_return_if_fail(ptpcam != NULL);

    if (g_atomic_int_dec_and_test(&ptpcam->refcount))
    {
        if (ptpcam->destroy_notify)
            ptpcam->destroy_notify(ptpcam->camera);

        g_slice_free(GstPTPCam, ptpcam);
    }
}

static void gst_ptp_src_set_context(GstElement *element, GstContext *context)
{
    GstPTPCam *ptpcam;
    GstPtpSrc *ptp_src = GST_PTP_SRC(element);

    if (GST_STATE(element) != GST_STATE_NULL)
    {
        GST_ERROR_OBJECT(ptp_src, "trying to set context when ptp_src state is NOT null, ignoring");
        return;
    }

    GST_INFO_OBJECT(ptp_src, "setting new context");
    if (gst_context_get_ptpcam(context, &ptpcam))
    {
        GST_OBJECT_LOCK(ptp_src);
        if (ptp_src->ptpcam)
            gst_ptpcam_unref(ptp_src->ptpcam);

        ptp_src->ptpcam = ptpcam;
        GST_OBJECT_UNLOCK(ptp_src);
    }
}

G_DEFINE_BOXED_TYPE(GstPTPCam, gst_ptpcam, (GBoxedCopyFunc) gst_ptpcam_ref, (GBoxedFreeFunc) gst_ptpcam_unref);

/* initialize the ptp_src's class */
static void
gst_ptp_src_class_init (GstPtpSrcClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *element_class;
    GstBaseSrcClass *basesrc_class;
    GstPushSrcClass *pushsrc_class;

    gobject_class = G_OBJECT_CLASS(klass);
    element_class = GST_ELEMENT_CLASS(klass);
    basesrc_class = GST_BASE_SRC_CLASS(klass);
    pushsrc_class = GST_PUSH_SRC_CLASS(klass);

    gobject_class->set_property = gst_ptp_src_set_property;
    gobject_class->get_property = gst_ptp_src_get_property;

    g_object_class_install_property (gobject_class, PROP_1080P,
            g_param_spec_boolean ("is-1080p", "1080P Mode", 
            "Start ptpcam in 1080p mode ?", FALSE,  
            G_PARAM_CONSTRUCT | G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, PROP_LAST_PTS,
        g_param_spec_uint64 ("last-pts", "Last Video Frame Timestamp",
            "Timestamp of last captured video frame.", 
            0, G_MAXUINT64, 0,
            G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

    gst_element_class_set_static_metadata(element_class,
            "ptpSrc",
            "ptpsrc",
            "ptpcam",
            "nhandojo@aeryon.com");

    gst_element_class_add_pad_template (element_class,
            gst_static_pad_template_get (&src_factory));

    element_class->change_state = gst_ptp_src_change_state;
    element_class->set_context = gst_ptp_src_set_context;

    //basesrc_class->get_caps = GST_DEBUG_FUNCPTR(gst_ptp_src_get_caps);
    basesrc_class->set_caps = GST_DEBUG_FUNCPTR(gst_ptp_src_set_caps);
    basesrc_class->start = GST_DEBUG_FUNCPTR(gst_ptp_src_start);
    basesrc_class->stop = GST_DEBUG_FUNCPTR(gst_ptp_src_stop);
    basesrc_class->decide_allocation = GST_DEBUG_FUNCPTR(gst_ptp_src_decide_allocation);

    pushsrc_class->fill = GST_DEBUG_FUNCPTR(gst_ptp_src_fill);

    klass->camera.bus = PTP_BUS_USB;
    klass->camera.ctx.transport.usb.vendor_id = 0x054c;
    klass->camera.ctx.transport.usb.product_id = 0xa77;

    //TODO: remove this once logging is fixed
    gv_trace_level = TRACE_WARN;

    GST_DEBUG_CATEGORY_INIT(gst_ptp_src_debug, "ptpsrc", 0, "PTP source element");
}

static gboolean ptpcam_init_context(GstPtpSrc *ptp_src)
{
    GstMessage *msg;
    GstPtpSrcClass *klass = GST_PTP_SRC_GET_CLASS(ptp_src);
    gboolean ret = FALSE, new_cam_ctx = FALSE;

    if (!ptp_src->ptpcam)
    {
        msg = gst_message_new_need_context(GST_OBJECT_CAST(ptp_src), GST_PTPCAM_CONTEXT_TYPE);
        gst_element_post_message(GST_ELEMENT(ptp_src), msg);
    }

    GST_OBJECT_LOCK(ptp_src);
    if (!ptp_src->ptpcam)
    {
        GstContext *context;
        GST_WARNING_OBJECT(ptp_src, "creating new ptpcam context");

        ptp_src->ptpcam = gst_ptpcam_new(&klass->camera, gst_ptpcam_destroy);
        context = gst_context_new_ptpcam(ptp_src->ptpcam, FALSE);
        msg = gst_message_new_have_context(GST_OBJECT(ptp_src), context);

        // open and auth camera before posting have new ctx msg
        if (!klass->cam_opened)
        {
            PTPCam *camera = ptp_src->ptpcam->camera;
            PTPDeviceInfo info;
            // camera close is called in gst_ptpcam_destroy
            uint16_t retval = open_camera(camera, &info);
            if (retval != PTP_RC_OK)
                goto exit;
        
            retval = auth_sony_camera(camera);
            if (retval != PTP_RC_OK)
                goto exit;

            int delay_us = 100000;
            static const int max_retry = 15;
            int i;
            for (i = 0; i < max_retry; ++i, delay_us *= 2) 
            { 
                //attempt to get liveview status, up to ~6 seconds
                PTPObjectInfo liveview_info; uint32_t framesize_est;
                if (PTP_RC_OK == get_sony_liveview_status(camera, &liveview_info, &framesize_est)) break;
                usleep(delay_us);
            }

            if (i == max_retry) 
            {
                GST_WARNING_OBJECT(ptp_src, "LiveViewStatus did not enable in time\n");
                klass->cam_opened = FALSE;
            } 
            else
            {
                klass->cam_opened = TRUE;
                new_cam_ctx = TRUE;
            }

            if (klass->cam_opened)
            {
                PTPSDIExtDevicePropInfo propval;
                uint16_t num_retry;
                uint16_t result = PTP_RC_OK;

                if(ptp_src->is_1080p)
                {
                    // switch op mode before enabling 1080p liveview
                    uint8_t target_mode = SONY_OPERATING_MODE_MOVIE_REC;
                    uint8_t target_width = SONY_LIVEVIEW_HORIZONTAL_WIDTH_1920;
                    uint8_t value = target_mode;
                    result = set_sony_cam_OperatingMode(camera, &value);

                    usleep(300000);
                    //make sure we are in correct mode before setting resolution
                    num_retry = 0;
                    do {
                        usleep(300000);
                        result = get_sony_cam_OperatingMode(camera, &propval);
                        num_retry++;
                    } while (result == PTP_RC_OK && propval.currentVal.val_u8 != target_mode && (num_retry < max_retry));  

                    if(result != PTP_RC_OK || num_retry == max_retry) {
                        tracef(TRACE_ERROR, "setting OPERATING_MODE_MOVIE_REC failed\n");
                        goto exit;
                    }

                    result = set_sony_cam_LiveViewHorizontalRes(camera, &target_width);            

                    num_retry = 0;
                    do {
                        usleep(300000);
                        result = get_sony_cam_LiveViewHorizontalRes(camera, &propval);
                        num_retry++;
                    } while (result == PTP_RC_OK && propval.currentVal.val_u8 != target_width && (num_retry < max_retry));

                    if(result != PTP_RC_OK || num_retry == max_retry) {
                        tracef(TRACE_ERROR, "setting HORIZONTAL_WIDTH_1920 failed\n");
                        goto exit;
                    }                    
                } 
                else 
                {
                    result = sony_configure_opmode_and_res(
                            camera, 
                            &info,
                            SONY_OPERATING_MODE_STILL_REC,
                            SONY_PTP_ASPECT_RATIO_4_3,
                            SONY_LIVEVIEW_HORIZONTAL_WIDTH_1024);

                    if (result != PTP_RC_OK)
                    {
                        GST_ERROR_OBJECT(ptp_src, "Unable to configure liveview resolution, aspect ratio or camera operating mode!");
                    }
                }

            }
        }

        ret = TRUE;
    }
    else 
        ret = TRUE;
exit:
    GST_OBJECT_UNLOCK(ptp_src);
    if (new_cam_ctx)
    {
        GST_INFO_OBJECT(ptp_src, "posting have_new_context");
        gst_element_post_message(GST_ELEMENT(ptp_src), msg);
    }
    return ret;
}

static GstStateChangeReturn gst_ptp_src_change_state(GstElement *element, GstStateChange transition)
{
    GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS, parent_ret;
    GstPtpSrc *ptp_src = GST_PTP_SRC(element);
    switch (transition)
    {
        // state change ready -> null is handled in gst_ptp_src_stop
        // liveview streaming is stopped when bufferpool is stopped
        // camera resources are freed when gst_ptpcam refcount reaches 0

        case GST_STATE_CHANGE_NULL_TO_READY:
            if (!ptpcam_init_context(ptp_src))
            {
                ret = GST_STATE_CHANGE_FAILURE;
            }
            GST_WARNING_OBJECT(ptp_src, "change state from NULL -> READY");
            break;

        case GST_STATE_CHANGE_READY_TO_PAUSED:
            ptp_pause_liveview(ptp_src->ptpcam->camera->vendor_ext.sony.video_queue, PTP_STREAM_PAUSING);
            GST_WARNING_OBJECT(ptp_src, "change state from READY -> PAUSED");
            ret = GST_STATE_CHANGE_NO_PREROLL;
            break;

        case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
            ptp_resume_liveview(ptp_src->ptpcam->camera->vendor_ext.sony.video_queue);
            GST_WARNING_OBJECT(ptp_src, "change state from PAUSED -> PLAYING");
            break;
        default:
            break;
    }

    if ((parent_ret = GST_ELEMENT_CLASS(parent_class)->change_state(element, transition)) != GST_STATE_CHANGE_SUCCESS)
        return parent_ret;

    switch (transition)
    {
        case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
            ptp_pause_liveview(ptp_src->ptpcam->camera->vendor_ext.sony.video_queue, PTP_STREAM_PAUSING);
            GST_WARNING_OBJECT(ptp_src, "change state from PLAYING -> PAUSED");
            ret = GST_STATE_CHANGE_NO_PREROLL;
            break;
        case GST_STATE_CHANGE_READY_TO_NULL:

            break;
        default:
            break;
    }

    return ret;
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
    static void
gst_ptp_src_init (GstPtpSrc *ptp_src)
{
    ptp_src->ptpcam = NULL;
    ptp_src->is_1080p = FALSE;
    ptp_src->last_pts = GST_CLOCK_TIME_NONE;
    gst_base_src_set_format(GST_BASE_SRC(ptp_src), GST_FORMAT_TIME);
    gst_base_src_set_live(GST_BASE_SRC(ptp_src), TRUE);
}

    static void
gst_ptp_src_set_property (GObject * object, guint prop_id,
        const GValue * value, GParamSpec * pspec)
{
    GstPtpSrc *ptp_src = GST_PTP_SRC(object);
    switch (prop_id) {
        case PROP_1080P:
            ptp_src->is_1080p = g_value_get_boolean (value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

    static void
gst_ptp_src_get_property (GObject * object, guint prop_id,
        GValue * value, GParamSpec * pspec)
{
    GstPtpSrc *ptp_src = GST_PTP_SRC(object);

    switch (prop_id) {
        case PROP_LAST_PTS:
            g_value_set_uint64(value, ptp_src->last_pts);
            break;         
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static gboolean gst_ptp_src_decide_allocation(GstBaseSrc *src, GstQuery *query)
{
    GstBufferPool *pool;
    guint size, min, max;
    GstStructure *config;
    GstCaps *caps;
    GstPtpSrc *ptp_src = GST_PTP_SRC(src);

    GST_LOG_OBJECT(ptp_src, "using PTP buffer pool");
    pool = ptp_src->pool;

    config = gst_buffer_pool_get_config(pool);
    gst_buffer_pool_config_get_params(config, &caps, &size, &min, &max);
    if (gst_query_find_allocation_meta(query, GST_VIDEO_META_API_TYPE, NULL))
    {
        gst_buffer_pool_config_add_option(config, GST_BUFFER_POOL_OPTION_VIDEO_META);
    }
    gst_buffer_pool_set_config(pool, config);

    gst_query_add_allocation_pool(query, pool, size, min, max);

    return GST_BASE_SRC_CLASS(parent_class)->decide_allocation(src, query);
}

static gboolean gst_ptp_src_set_caps(GstBaseSrc *src, GstCaps *caps)
{
    GstStructure *structure;
    gint width, height, fps_n, fps_d;
    const gchar *mimetype;
    GstPtpSrc *ptp_src = GST_PTP_SRC(src);

    structure = gst_caps_get_structure(caps, 0);
    mimetype = gst_structure_get_name(structure);
    if (!gst_video_info_from_caps(&ptp_src->info, caps))
        goto invalid_format;

    if (!g_str_equal(mimetype, "image/jpeg"))
        goto unhandled_format;

    fps_n = GST_VIDEO_INFO_FPS_N(&ptp_src->info);
    fps_d = GST_VIDEO_INFO_FPS_D(&ptp_src->info);
    width = GST_VIDEO_INFO_WIDTH(&ptp_src->info);
    height = GST_VIDEO_INFO_HEIGHT(&ptp_src->info);

    GST_WARNING_OBJECT(ptp_src, "desired format: %dx%d @ %d/%dfps", width, height, fps_n, fps_d);

    //TODO configure PTPCam with these desired streaming params
    ptp_src->pool = gst_ptp_buffer_pool_new(ptp_src->ptpcam, caps);

    return TRUE;

invalid_format:
    GST_WARNING_OBJECT(ptp_src, "invalid format");
    return FALSE;

unhandled_format:
    GST_WARNING_OBJECT(ptp_src, "unhandled format");
    return FALSE;
}

static gboolean gst_ptp_src_start(GstBaseSrc *src)
{
    GstPtpSrc *ptp_src = GST_PTP_SRC(src);

    ptp_src->offset = 0;

    return TRUE;
}

static gboolean gst_ptp_src_stop(GstBaseSrc *src)
{
    GstPtpSrc *ptp_src = GST_PTP_SRC(src);
    GST_WARNING_OBJECT(ptp_src, "stopping\n");

    GstPtpSrcClass *klass = GST_PTP_SRC_GET_CLASS(ptp_src);
    if (gst_buffer_pool_is_active(ptp_src->pool))
    {
        GST_WARNING_OBJECT(ptp_src, "deactivating pool");
        gst_buffer_pool_set_active(ptp_src->pool, FALSE);
    }
    gst_ptpcam_unref(ptp_src->ptpcam);
    klass->cam_opened = FALSE;
    ptp_src->ptpcam = NULL;

    gst_object_unref(ptp_src->pool);
    ptp_src->pool = NULL;

    return TRUE;
}

/* GstPushSrc vmethod implementations */
static GstFlowReturn gst_ptp_src_fill(GstPushSrc *src, GstBuffer *out)
{
    GstPtpSrc *ptp_src = GST_PTP_SRC(src);

    GST_BUFFER_OFFSET(out) = ptp_src->offset++;
    GST_BUFFER_OFFSET_END(out) = ptp_src->offset;

    GstClock *clk          = GST_ELEMENT_CLOCK(ptp_src);
    GstClockTime abs_time  = gst_clock_get_time(clk);
    GstClockTime base_time = GST_ELEMENT(ptp_src)->base_time;
    GstClockTime ts        = abs_time - base_time;

    GST_BUFFER_DTS(out) = ts;
    GST_BUFFER_PTS(out) = ts;
    ptp_src->last_pts   = ts;

    GST_BUFFER_DURATION(out) = gst_util_uint64_scale_int(GST_SECOND, 1, 30);

    return GST_FLOW_OK;
}
