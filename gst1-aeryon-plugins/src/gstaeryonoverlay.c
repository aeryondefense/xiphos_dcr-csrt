#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gstaeryonoverlay.h"
#include "gstaeryonbuffermeta.h"
#include "gstaeryonevent.h"
#include "gstaeryonmessage.h"
#include "yuvdrawutils.h"
#include <gst/video/video.h>
#include <sys/time.h>
#include <stdio.h>

GST_DEBUG_CATEGORY (gst_aeryon_overlay_debug);
#define GST_CAT_DEFAULT gst_aeryon_overlay_debug

/* Filter signals and args */
enum
{
    /* FILL ME */
    LAST_SIGNAL
};

enum
{
    PROP_0,
    PROP_SILENT,
    PROP_DRAW
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE("sink",
                                                                   GST_PAD_SINK,
                                                                   GST_PAD_ALWAYS,
                                                                   GST_STATIC_CAPS("ANY"));

static GstStaticPadTemplate srcany_factory = GST_STATIC_PAD_TEMPLATE("any",
                                                                     GST_PAD_SRC,
                                                                     GST_PAD_ALWAYS,
                                                                     GST_STATIC_CAPS("ANY"));

#define gst_aeryon_overlay_parent_class parent_class
G_DEFINE_TYPE (GstAeryonOverlay, gst_aeryon_overlay, GST_TYPE_ELEMENT);

static void gst_aeryon_overlay_set_property (GObject * object, guint prop_id,
                                           const GValue * value, GParamSpec * pspec);
static void gst_aeryon_overlay_get_property (GObject * object, guint prop_id,
                                           GValue * value, GParamSpec * pspec);

static gboolean gst_aeryon_overlay_sink_event (GstPad * pad, GstObject * parent, GstEvent * event);
static GstFlowReturn gst_aeryon_overlay_chain (GstPad * pad, GstObject * parent, GstBuffer * buf);

/* GObject vmethod implementations */

/* initialize the aeryon_overlay's class */
static void
gst_aeryon_overlay_class_init (GstAeryonOverlayClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;

    gobject_class = (GObjectClass *) klass;
    gstelement_class = (GstElementClass *) klass;

    gobject_class->set_property = gst_aeryon_overlay_set_property;
    gobject_class->get_property = gst_aeryon_overlay_get_property;

    g_object_class_install_property(gobject_class, PROP_SILENT,
        g_param_spec_boolean("silent", "Silent", "Produce verbose output ?",
        FALSE, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, PROP_DRAW,
        g_param_spec_boolean("draw", "Draw", "Draw overlays ?",
        FALSE, G_PARAM_READWRITE));

    gst_element_class_set_details_simple(gstelement_class,
        "Aeryon overlay filter",
        "Aeryon overlay filter",
        "Overlay boxes, circles, and reticles on the video for target tracking.",
        "april@aeryon.com");

    gst_element_class_add_pad_template(gstelement_class,
        gst_static_pad_template_get(&srcany_factory));
    gst_element_class_add_pad_template(gstelement_class,
        gst_static_pad_template_get(&sink_factory));
}

/* initialize the new aeryon_overlay element
 * instantiate pads and add them to element
 * set pad callback functions
 * initialize instance structure
 */
static void
gst_aeryon_overlay_init (GstAeryonOverlay * filter)
{
    filter->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
    gst_pad_set_event_function (filter->sinkpad,
        GST_DEBUG_FUNCPTR (gst_aeryon_overlay_sink_event));
    gst_pad_set_chain_function (filter->sinkpad,
        GST_DEBUG_FUNCPTR (gst_aeryon_overlay_chain));
    GST_PAD_SET_PROXY_CAPS (filter->sinkpad);
    gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);

    filter->srcpad_any = gst_pad_new_from_static_template(&srcany_factory, "any");
    GST_PAD_SET_PROXY_CAPS (filter->srcpad_any);
    gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad_any);

    filter->silent = FALSE;
    filter->draw = TRUE;
}

static void
gst_aeryon_overlay_set_property (GObject * object, guint prop_id,
                               const GValue * value, GParamSpec * pspec)
{
    GstAeryonOverlay *filter = GST_AERYON_OVERLAY (object);

    switch (prop_id)
    {
        case PROP_SILENT:
            filter->silent = g_value_get_boolean (value);
            break;
        case PROP_DRAW:
            filter->draw = g_value_get_boolean (value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
gst_aeryon_overlay_get_property (GObject * object, guint prop_id,
                               GValue * value, GParamSpec * pspec)
{
    GstAeryonOverlay *filter = GST_AERYON_OVERLAY (object);

    switch (prop_id)
    {
        case PROP_SILENT:
            g_value_set_boolean (value, filter->silent);
            break;
        case PROP_DRAW:
            g_value_set_boolean (value, filter->draw);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

/* GstElement method implementations */

/* this function handles sink events */
static gboolean
gst_aeryon_overlay_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
    gboolean ret;
    GstAeryonOverlay *filter;
    USE(filter);

    filter = GST_AERYON_OVERLAY (parent);
    
    switch (GST_EVENT_TYPE (event))
    {
        case GST_EVENT_CAPS:
        {
            GstCaps * caps;

            gst_event_parse_caps (event, &caps);
            /* do something with the caps */

            /* and forward */
            ret = gst_pad_push_event(filter->srcpad_any, event);
            break;
        }
        default:
            ret = gst_pad_event_default (pad, parent, event);
            break;
    }
    return ret;
}

static void
gst_aeryon_overlay_draw( Metadata2 *meta, GstVideoInfo *info, GstVideoFrame *frame)
{
    if (!meta)
    {
        return;
    }
        
    yuv_frame_info_t f;    
    f.h = info->height;
    f.w = info->width;
    f.s = info->stride[0];
    f.ptr = frame->data[0];
    int nt = meta->tracks.num_targets;
    int lw = 1;
    if (info->width > 1024)
        lw = 3;
    
    // Handle possible scale changes between vision and overlay
    float sx = 1.0;
    float sy = 1.0;
    if (meta->tracks.image_w != 0)
        sx = (float)info->width / meta->tracks.image_w;
    if (meta->tracks.image_h != 0)
        sy = (float)info->height / meta->tracks.image_h;
    
    int i;
    for (i = 0; i < nt; i++)
    {
        int xmin = sx * (meta->tracks.x[i] - meta->tracks.w[i] / 2);
        int xmax = sx * (xmin + (int)meta->tracks.w[i]);
        int ymin = sy * (meta->tracks.y[i] - meta->tracks.h[i] / 2);
        int ymax = sy * (ymin + (int)meta->tracks.h[i]);
        float conf = meta->tracks.confidence[i];
        
        if (meta->tracks.state == TRACKER_STATE_UDOT)
        {
            recticle_yuy420(&f, xmin, ymin, xmax, 
                    ymax, lw, (conf >= 1.0)?
                    GREEN:(conf > 0.1)?ORANGE:RED);
        }
        else
        {
            rect2_yuy420(&f, xmin, ymin, xmax, 
                    ymax, lw, (conf >= 1.0)?
                    GREEN:(conf > 0.1)?ORANGE:RED);            
        }
    }

}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_aeryon_overlay_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
    USE(pad);
    GstAeryonOverlay *filter;
    
    filter = GST_AERYON_OVERLAY (parent);
    
    gchar* name = gst_object_get_name(parent);

    GstAeryonBufferMeta *buffer_meta =  NULL;

    if (filter->draw)
    {
        // Open buffer's metadata (if present)
        buffer_meta = GST_AERYON_BUFFER_META_GET(buf);

        // Get Metadata2
        Metadata2 *meta = NULL;
        if (buffer_meta && buffer_meta->gstmetadata && buffer_meta->gstmetadata->metadata)
        {
            gst_aeryon_metadata_ref(buffer_meta->gstmetadata);
            meta = buffer_meta->gstmetadata->metadata;
        }
        else
        {
            fprintf(stderr,"%s does not have meta\n", name);
            goto exit_overlay;
        }

        // Get the video frame 
        GstCaps *caps = gst_pad_get_current_caps(pad);
        if (caps == 0)
        {
            fprintf(stderr, "%s could not get caps\n", name);
            goto exit_overlay;
        }

        GstVideoInfo info;
        if (gst_video_info_from_caps(&info, caps) == FALSE)
        {
            fprintf(stderr, "%s could not get info\n", name);
            goto exit_overlay;
        }
        gst_caps_unref(caps);
        
        // Handle the case where there is  more than one references to this 
        // buffer out there.
        if (!gst_buffer_is_writable(buf))
        {
            buf = gst_buffer_make_writable(buf);
        }

        GstVideoFrame frame;
        if (gst_video_frame_map(&frame, &info, buf, GST_MAP_READ | GST_MAP_WRITE) == FALSE)
        {
            fprintf(stderr, "%s could not map frame\n", name);
            goto exit_overlay;
        }

        gst_aeryon_overlay_draw(meta, &info, &frame);

        gst_video_frame_unmap(&frame);
    }
    
exit_overlay:
                       
    if (buffer_meta && buffer_meta->gstmetadata && buffer_meta->gstmetadata->metadata)
        gst_aeryon_metadata_unref(buffer_meta->gstmetadata);

    g_free(name);

    /* push out the incoming buffer */
    return gst_pad_push (filter->srcpad_any, buf);
}

