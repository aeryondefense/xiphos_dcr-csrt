#ifdef HAVE_CONFIG_H // What does this do?
#include <config.h>
#endif

#include "gstaeryonvision.h"
#include "gstaeryonbuffermeta.h"
#include "gstaeryonevent.h"
#include "gstaeryonmessage.h"
#include <sys/time.h>

GST_DEBUG_CATEGORY (gst_aeryon_vision_debug);
#define GST_CAT_DEFAULT gst_aeryon_vision_debug

/* Filter signals and args */
enum
{
    /* FILL ME */
    LAST_SIGNAL
};

enum
{
    PROP_0,
    PROP_SILENT
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE("sink",
                                                                   GST_PAD_SINK,
                                                                   GST_PAD_ALWAYS,
                                                                   GST_STATIC_CAPS("ANY"));

static GstStaticPadTemplate srcany_factory = GST_STATIC_PAD_TEMPLATE("any",
                                                                     GST_PAD_SRC,
                                                                     GST_PAD_ALWAYS,
                                                                     GST_STATIC_CAPS("ANY"));

#define gst_aeryon_vision_parent_class parent_class
G_DEFINE_TYPE (GstAeryonVision, gst_aeryon_vision, GST_TYPE_ELEMENT);

static void gst_aeryon_vision_set_property (GObject * object, guint prop_id,
                                           const GValue * value, GParamSpec * pspec);
static void gst_aeryon_vision_get_property (GObject * object, guint prop_id,
                                           GValue * value, GParamSpec * pspec);

static gboolean gst_aeryon_vision_sink_event (GstPad * pad, GstObject * parent, GstEvent * event);
static GstFlowReturn gst_aeryon_vision_chain (GstPad * pad, GstObject * parent, GstBuffer * buf);

/* GObject vmethod implementations */

/* initialize the aeryon_vision's class */
static void
gst_aeryon_vision_class_init (GstAeryonVisionClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;

    gobject_class = (GObjectClass *) klass;
    gstelement_class = (GstElementClass *) klass;

    gobject_class->set_property = gst_aeryon_vision_set_property;
    gobject_class->get_property = gst_aeryon_vision_get_property;

    g_object_class_install_property(gobject_class, PROP_SILENT,
        g_param_spec_boolean("silent", "Silent", "Produce verbose output ?",
        FALSE, G_PARAM_READWRITE));

    gst_element_class_set_details_simple(gstelement_class,
        "Aeryon vision filter",
        "Aeryon vision filter",
        "This is a pass through placeholder for Xiphos, but may in the future "
        "actually do something really cool with the video feed. ",
        "april@aeryon.com");

    gst_element_class_add_pad_template(gstelement_class,
        gst_static_pad_template_get(&srcany_factory));
    gst_element_class_add_pad_template(gstelement_class,
        gst_static_pad_template_get(&sink_factory));
}

/* initialize the new aeryon_vision element
 * instantiate pads and add them to element
 * set pad callback functions
 * initialize instance structure
 */
static void
gst_aeryon_vision_init (GstAeryonVision * filter)
{
    filter->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
    gst_pad_set_event_function (filter->sinkpad,
        GST_DEBUG_FUNCPTR (gst_aeryon_vision_sink_event));
    gst_pad_set_chain_function (filter->sinkpad,
        GST_DEBUG_FUNCPTR (gst_aeryon_vision_chain));
    GST_PAD_SET_PROXY_CAPS (filter->sinkpad);
    gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);

    filter->srcpad_any = gst_pad_new_from_static_template(&srcany_factory, "any");
    GST_PAD_SET_PROXY_CAPS (filter->srcpad_any);
    gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad_any);

    filter->tracker_config.state = TRACKER_STATE_INIT;

    filter->silent = FALSE;
}

static void
gst_aeryon_vision_set_property (GObject * object, guint prop_id,
                               const GValue * value, GParamSpec * pspec)
{
    GstAeryonVision *filter = GST_AERYON_VISION (object);

    switch (prop_id)
    {
        case PROP_SILENT:
            filter->silent = g_value_get_boolean (value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
gst_aeryon_vision_get_property (GObject * object, guint prop_id,
                               GValue * value, GParamSpec * pspec)
{
    GstAeryonVision *filter = GST_AERYON_VISION (object);

    switch (prop_id)
    {
        case PROP_SILENT:
            g_value_set_boolean (value, filter->silent);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

/* GstElement method implementations */

/* this function handles sink events */
static gboolean
gst_aeryon_vision_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
    gboolean ret;
    GstAeryonVision *filter;
    USE(filter);
    GstTrackerConfig *gstconfig;

    filter = GST_AERYON_VISION (parent);

    /* Handle any Aeryon sink events first */
    if ((GstAeryonEventType) GST_EVENT_TYPE(event) == GST_EVENT_TRACKER_CONFIG)
    {
        ret = gst_event_get_tracker_config(event, &gstconfig);
        if (ret)
        {
            /* Do something with this tracker config msg and remove this debug
             * print */
            g_print("Received cam tracker config.\n"
                "State: [%d]\n"
                "Coordinates: [%d,%d]\n",
                gstconfig->config->state,
                gstconfig->config->pixel_x,
                gstconfig->config->pixel_y);
            gst_tracker_config_unref(gstconfig);
        }
    }

    switch (GST_EVENT_TYPE (event))
    {
        case GST_EVENT_CAPS:
        {
            GstCaps * caps;

            gst_event_parse_caps (event, &caps);
            /* do something with the caps */

            /* and forward */
            ret = gst_pad_push_event (filter->srcpad_any, event);
            break;
        }
        default:
            ret = gst_pad_event_default (pad, parent, event);
            break;
    }
    return ret;
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_aeryon_vision_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
    USE(pad);
    GstAeryonVision *filter;
    

    filter = GST_AERYON_VISION (parent);
    USE(filter);
    //  if (filter->silent == FALSE)
    //    g_print ("I'm plugged, therefore I'm in.\n");

    /* Open buffer's metadata (if present) */
    GstAeryonBufferMeta *buffer_meta = GST_AERYON_BUFFER_META_GET(buf);
    if (buffer_meta && buffer_meta->gstmetadata)
    {
        Metadata2 *meta = buffer_meta->gstmetadata->metadata;
        /* Add target telemetry to metadata
         * This gets sent upstream with the image buffer so any downstream elements
         * can make use of the targeting data. */
        if (meta)
        {
            meta->tracks.num_targets = 1;
            meta->tracks.state = filter->tracker_config.state;
            meta->tracks.confidence[0] = 1.0;
            meta->tracks.x[0] = 1.0;
            meta->tracks.y[0] = 1.0;
            meta->tracks.w[0] = 0.0;
            meta->tracks.h[0] = 0.0;
            meta->tracks.x_dot[0] = 0.0;
            meta->tracks.y_dot[0] = 0.0;
        }
    }

    /* push out the incoming buffer */
    return gst_pad_push (filter->srcpad_any, buf);
}
