#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>
#include <string.h>
#include <stdio.h>

#include "gstaeryonbuffermeta.h"
#include "gstaeryonevent.h"
#include "gstaeryonmessage.h"
#include "yuvdrawutils.h"
#include <gst/video/video.h>
#include <sys/time.h>
#include <stdio.h>

#include "gstaeryonoverlaytee.h"

static guint32 MAX_QUEUE_LEN = 16;


GST_DEBUG_CATEGORY_STATIC (gst_aeryon_overlay_tee_debug);
#define GST_CAT_DEFAULT gst_aeryon_overlay_tee_debug

#define GST_TYPE_AERYON_OVERLAY_TEE_PULL_MODE (gst_aeryon_overlay_tee_pull_mode_get_type())
static GType
gst_aeryon_overlay_tee_pull_mode_get_type (void)
{
    static GType type = 0;
    static const GEnumValue data[] = {
        {GST_AERYON_OVERLAY_TEE_PULL_MODE_NEVER, "Never activate in pull mode", "never"},
        {GST_AERYON_OVERLAY_TEE_PULL_MODE_SINGLE, "Only one src pad can be active in pull mode",
                "single"},
        {0, NULL, NULL},
    };

    if (!type) {
        type = g_enum_register_static ("GstAeryonOverlayTeePullMode", data);
    }
    return type;
}

#define DEFAULT_PROP_NUM_SRC_PADS 0
#define DEFAULT_PROP_HAS_CHAIN    TRUE
#define DEFAULT_PROP_SILENT   TRUE
#define DEFAULT_PROP_LAST_MESSAGE NULL
#define DEFAULT_PULL_MODE   GST_AERYON_OVERLAY_TEE_PULL_MODE_NEVER
#define DEFAULT_PROP_ALLOW_NOT_LINKED FALSE

enum
{
    PROP_0,
    PROP_NUM_SRC_PADS,
    PROP_HAS_CHAIN,
    PROP_SILENT,
    PROP_LAST_MESSAGE,
    PROP_PULL_MODE,
    PROP_ALLOC_PAD,
    PROP_ALLOW_NOT_LINKED,
    PROP_DRAW
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sinktemplate = GST_STATIC_PAD_TEMPLATE ("sink",
        GST_PAD_SINK,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS ("ANY")
        );

static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE ("src_%u",
        GST_PAD_SRC,
        GST_PAD_REQUEST,
        GST_STATIC_CAPS ("ANY"));

//----------------------------aeryonoverlaytee---------------------------------------------//

#define gst_aeryon_overlay_tee_parent_class parent_class
G_DEFINE_TYPE (GstAeryonOverlayTee, gst_aeryon_overlay_tee, GST_TYPE_ELEMENT);

static gboolean gst_aeryon_overlay_tee_sink_event (GstPad * pad, GstObject * parent, GstEvent * event);
static GstFlowReturn gst_aeryon_overlay_tee_chain (GstPad * pad, GstObject * parent, GstBuffer * buf);


static GstPad *gst_aeryon_overlay_tee_request_new_pad (GstElement * element,
        GstPadTemplate * temp, const gchar * unused, const GstCaps * caps);

static void gst_aeryon_overlay_tee_release_pad (GstElement * element, GstPad * pad);

static void gst_aeryon_overlay_tee_finalize (GObject * object);
static void gst_aeryon_overlay_tee_set_property (GObject * object, guint prop_id,
        const GValue * value, GParamSpec * pspec);
static void gst_aeryon_overlay_tee_get_property (GObject * object, guint prop_id,
        GValue * value, GParamSpec * pspec);
static void gst_aeryon_overlay_tee_dispose (GObject * object);

static gboolean gst_aeryon_overlay_tee_sink_query (GstPad * pad, GstObject * parent,
        GstQuery * query);
static gboolean gst_aeryon_overlay_tee_sink_activate_mode (GstPad * pad, GstObject * parent,
        GstPadMode mode, gboolean active);
static gboolean gst_aeryon_overlay_tee_src_query (GstPad * pad, GstObject * parent,
        GstQuery * query);
static gboolean gst_aeryon_overlay_tee_src_activate_mode (GstPad * pad, GstObject * parent,
        GstPadMode mode, gboolean active);
static GstFlowReturn gst_aeryon_overlay_tee_src_get_range (GstPad * pad, GstObject * parent,
        guint64 offset, guint length, GstBuffer ** buf);
//----------------------------aeryonoverlaytee ends ---------------------------------------------//

//-------------------------------------tee pad begins--------------------------------------------//

GType gst_aeryon_overlay_tee_pad_get_type (void);

#define GST_TYPE_AERYON_OVERLAY_TEE_PAD \
    (gst_aeryon_overlay_tee_pad_get_type())
#define GST_AERYON_OVERLAY_TEE_PAD(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_AERYON_OVERLAY_TEE_PAD, GstAeryonOverlayTeePad))
#define GST_AERYON_OVERLAY_TEE_PAD_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_AERYON_OVERLAY_TEE_PAD, GstAeryonOverlayTeePadClass))
#define GST_IS_AERYON_OVERLAY_TEE_PAD(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_AERYON_OVERLAY_TEE_PAD))
#define GST_IS_AERYON_OVERLAY_TEE_PAD_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_AERYON_OVERLAY_TEE_PAD))
#define GST_AERYON_OVERLAY_TEE_PAD_CAST(obj) \
    ((GstAeryonOverlayTeePad *)(obj))

/*pad structs */
static GParamSpec *pspec_last_message = NULL;
static GParamSpec *pspec_alloc_pad = NULL;


typedef struct _GstAeryonOverlayTeePad GstAeryonOverlayTeePad;
typedef struct _GstAeryonOverlayTeePadClass GstAeryonOverlayTeePadClass;

struct _GstAeryonOverlayTeePad
{
    GstPad parent;
    guint index;
    gboolean pushed;
    GstFlowReturn result;
    gboolean removed;
    gboolean always_overlay;
};

struct _GstAeryonOverlayTeePadClass
{
    GstPadClass parent;
};

G_DEFINE_TYPE (GstAeryonOverlayTeePad, gst_aeryon_overlay_tee_pad, GST_TYPE_PAD);

static void
gst_aeryon_overlay_tee_pad_class_init (__attribute__((unused)) GstAeryonOverlayTeePadClass * klass)
{
}


static void
gst_aeryon_overlay_tee_pad_reset (GstAeryonOverlayTeePad * pad)
{
    pad->pushed = FALSE;
    pad->result = GST_FLOW_NOT_LINKED;
    pad->removed = FALSE;
    pad->always_overlay = FALSE;
}

static void
gst_aeryon_overlay_tee_pad_init (GstAeryonOverlayTeePad * pad)
{
    gst_aeryon_overlay_tee_pad_reset (pad);
}

static void
gst_aeryon_overlay_tee_dispose (GObject * object)
{
    GList *item;

    restart:
        for (item = GST_ELEMENT_PADS (object); item; item = g_list_next (item)) {
            GstPad *pad = GST_PAD (item->data);
            if (GST_PAD_IS_SRC (pad)) {
                    gst_element_release_request_pad (GST_ELEMENT (object), pad);
                    goto restart;
            }
        }

    G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_aeryon_overlay_tee_finalize (GObject * object)
{
    GstAeryonOverlayTee *tee;


    tee = GST_AERYON_OVERLAY_TEE (object);

    GstBuffer *buffer;
    g_mutex_lock(&tee->data_mutex);
    while ((buffer = g_queue_pop_head (tee->pending_buffers))) {
        gst_buffer_unref (buffer);
    }
    g_mutex_unlock(&tee->data_mutex);

    g_queue_free (tee->pending_buffers);

    g_hash_table_unref (tee->pad_indexes);
    g_free (tee->last_message);

    g_hash_table_unref (tee->srcpads_bypass);
    g_hash_table_unref (tee->srcpads_overlay);
    g_mutex_clear(&tee->data_mutex);

    G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_aeryon_overlay_tee_class_init (GstAeryonOverlayTeeClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;

    gobject_class = G_OBJECT_CLASS (klass);
    gstelement_class = GST_ELEMENT_CLASS (klass);

    gobject_class->finalize = gst_aeryon_overlay_tee_finalize;
    gobject_class->set_property = gst_aeryon_overlay_tee_set_property;
    gobject_class->get_property = gst_aeryon_overlay_tee_get_property;
    gobject_class->dispose = gst_aeryon_overlay_tee_dispose;

    g_object_class_install_property (gobject_class, PROP_NUM_SRC_PADS,
            g_param_spec_int ("num-src-pads", "Num Src Pads",
                    "The number of source pads", 0, G_MAXINT, DEFAULT_PROP_NUM_SRC_PADS,
                    G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, PROP_HAS_CHAIN,
            g_param_spec_boolean ("has-chain", "Has Chain",
                    "If the element can operate in push mode", DEFAULT_PROP_HAS_CHAIN,
                    G_PARAM_CONSTRUCT | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, PROP_SILENT,
            g_param_spec_boolean ("silent", "Silent",
                    "Don't produce last_message events", DEFAULT_PROP_SILENT,
                    G_PARAM_CONSTRUCT | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    pspec_last_message = g_param_spec_string ("last-message", "Last Message",
            "The message describing current status", DEFAULT_PROP_LAST_MESSAGE,
            G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

    g_object_class_install_property (gobject_class, PROP_LAST_MESSAGE,
            pspec_last_message);

    g_object_class_install_property(gobject_class, PROP_DRAW,
            g_param_spec_boolean("draw", "Draw", "Draw overlays ?",
                    FALSE, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, PROP_PULL_MODE,
            g_param_spec_enum ("pull-mode", "Pull mode",
                    "Behavior of tee in pull mode", GST_TYPE_AERYON_OVERLAY_TEE_PULL_MODE,
                    DEFAULT_PULL_MODE,
                    G_PARAM_CONSTRUCT | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    pspec_alloc_pad = g_param_spec_object ("alloc-pad", "Allocation Src Pad",
            "The pad ALLOCATION queries will be proxied to (unused)", GST_TYPE_PAD,
            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
    g_object_class_install_property (gobject_class, PROP_ALLOC_PAD,
            pspec_alloc_pad);

    /**
     * GstTee:allow-not-linked
     *
     * This property makes sink pad return GST_FLOW_OK even if there are no
     * source pads or any of them is linked.
     *
     * This is useful to avoid errors when you have a dynamic pipeline and during
     * a reconnection you can have all the pads unlinked or removed.
     *
     * Since: 1.6
     */
     g_object_class_install_property (gobject_class, PROP_ALLOW_NOT_LINKED,
        g_param_spec_boolean ("allow-not-linked", "Allow not linked",
            "Return GST_FLOW_OK even if there are no source pads or they are "
            "all unlinked", DEFAULT_PROP_ALLOW_NOT_LINKED,
            G_PARAM_CONSTRUCT | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

     gst_element_class_set_static_metadata (gstelement_class,
        "AeryonOverlayTee",
        "Combined element of aeryonoverlay and gstreamer tee to avoid memcpy during overlaying",
        "1-to-N pipe fitting and overlay boxes, circles, and reticles on the video for target tracking",
        "Erik Walthinsen <omega@cse.ogi.edu>, " "Wim Taymans <wim@fluendo.com>," " April Blaylock <april@aeryon.com>, " "Jun Zhang <jzhang@aeryon.com>");

    gst_element_class_add_pad_template(gstelement_class,
        gst_static_pad_template_get(&sinktemplate));

    gst_element_class_add_pad_template(gstelement_class,
        gst_static_pad_template_get(&src_template));


    gstelement_class->request_new_pad =
            GST_DEBUG_FUNCPTR (gst_aeryon_overlay_tee_request_new_pad);
    gstelement_class->release_pad = GST_DEBUG_FUNCPTR (gst_aeryon_overlay_tee_release_pad);

    GST_DEBUG_CATEGORY_INIT(gst_aeryon_overlay_tee_debug, "aeryonoverlaytee", 0, "Aeryon overlay and tee element");

}

static void
gst_aeryon_overlay_tee_init (GstAeryonOverlayTee * tee)
{
    tee->sinkpad = gst_pad_new_from_static_template (&sinktemplate, "sink");
    tee->sink_mode = GST_PAD_MODE_NONE;

    gst_pad_set_event_function (tee->sinkpad,
            GST_DEBUG_FUNCPTR (gst_aeryon_overlay_tee_sink_event));

    gst_pad_set_query_function (tee->sinkpad,
            GST_DEBUG_FUNCPTR (gst_aeryon_overlay_tee_sink_query));

    gst_pad_set_activatemode_function (tee->sinkpad,
            GST_DEBUG_FUNCPTR (gst_aeryon_overlay_tee_sink_activate_mode));

    gst_pad_set_chain_function (tee->sinkpad, GST_DEBUG_FUNCPTR (gst_aeryon_overlay_tee_chain));

    GST_OBJECT_FLAG_SET (tee->sinkpad, GST_PAD_FLAG_PROXY_CAPS);
    gst_element_add_pad (GST_ELEMENT (tee), tee->sinkpad);

    tee->pad_indexes = g_hash_table_new (NULL, NULL);
    tee->srcpads_bypass = g_hash_table_new (NULL, NULL);
    tee->srcpads_overlay = g_hash_table_new (NULL, NULL);

    tee->last_message = NULL;
    tee->pending_buffers = g_queue_new ();
    tee->gating_timestamp = 0;
    tee->draw = TRUE;
    g_mutex_init(&tee->data_mutex);
}

static void
gst_aeryon_overlay_tee_notify_alloc_pad (GstAeryonOverlayTee * tee)
{
    g_object_notify_by_pspec ((GObject *) tee, pspec_alloc_pad);
}

static gboolean
forward_sticky_events (__attribute__((unused)) GstPad * pad, GstEvent ** event, gpointer user_data)
{
    GstPad *srcpad = GST_PAD_CAST (user_data);
    GstFlowReturn ret;

    ret = gst_pad_store_sticky_event (srcpad, *event);
    if (ret != GST_FLOW_OK) {
        GST_DEBUG_OBJECT (srcpad, "storing sticky event %p (%s) failed: %s", *event,
                GST_EVENT_TYPE_NAME (*event), gst_flow_get_name (ret));
    }

    return TRUE;
}

static GstPad *
gst_aeryon_overlay_tee_request_new_pad (GstElement * element, GstPadTemplate * templ,
        const gchar * name_templ, __attribute__((unused)) const GstCaps * caps)
{
    gchar *name;
    GstPad *srcpad;
    GstAeryonOverlayTee *tee;
    GstPadMode mode;
    gboolean res;
    guint index = 0;

    tee = GST_AERYON_OVERLAY_TEE (element);
    
    GST_OBJECT_LOCK (tee);

    if (name_templ && sscanf (name_templ, "src_%u", &index) == 1) {
        GST_LOG_OBJECT (element, "name: %s (index %d)", name_templ, index);
        if (g_hash_table_contains (tee->pad_indexes, GUINT_TO_POINTER (index))) {
            GST_ERROR_OBJECT (element, "pad name %s is not unique", name_templ);
            GST_OBJECT_UNLOCK (tee);
            return NULL;
        }

        if (index >= tee->next_pad_index)
            tee->next_pad_index = index + 1;
    } else {
        index = tee->next_pad_index;

        while (g_hash_table_contains (tee->pad_indexes, GUINT_TO_POINTER (index)))
            index++;

        tee->next_pad_index = index + 1;
    }

    g_hash_table_insert (tee->pad_indexes, GUINT_TO_POINTER (index), NULL);

    name = g_strdup_printf ("src_%u", index);

    srcpad = GST_PAD_CAST (g_object_new (GST_TYPE_AERYON_OVERLAY_TEE_PAD,
                    "name", name, "direction", templ->direction, "template", templ,
                    NULL));

    GST_AERYON_OVERLAY_TEE_PAD_CAST (srcpad)->index = index;
    GST_AERYON_OVERLAY_TEE_PAD_CAST (srcpad)->always_overlay = TRUE;

    //by default insert the new pad into overlay hash
    g_hash_table_insert(tee->srcpads_overlay, srcpad, NULL);
 
 
    g_free (name);
    mode = tee->sink_mode;

    GST_OBJECT_UNLOCK (tee);

    switch (mode) {
        case GST_PAD_MODE_PULL:
            /* we already have a src pad in pull mode, and our pull mode can only be
                 SINGLE, so fall through to activate this new pad in push mode */
        case GST_PAD_MODE_PUSH:
            res = gst_pad_activate_mode (srcpad, GST_PAD_MODE_PUSH, TRUE);
            break;
        default:
            res = TRUE;
            break;
    }

    if (!res)
        goto activate_failed;

    gst_pad_set_activatemode_function (srcpad,
            GST_DEBUG_FUNCPTR (gst_aeryon_overlay_tee_src_activate_mode));

    gst_pad_set_query_function (srcpad, GST_DEBUG_FUNCPTR (gst_aeryon_overlay_tee_src_query));
    gst_pad_set_getrange_function (srcpad,
            GST_DEBUG_FUNCPTR (gst_aeryon_overlay_tee_src_get_range));
    /* Forward sticky events to the new srcpad */
    gst_pad_sticky_events_foreach (tee->sinkpad, forward_sticky_events, srcpad);
    GST_OBJECT_FLAG_SET (srcpad, GST_PAD_FLAG_PROXY_CAPS);
    gst_element_add_pad (GST_ELEMENT_CAST (tee), srcpad);

    return srcpad;

    /* ERRORS */
activate_failed:
    {
        gboolean changed = FALSE;

        GST_OBJECT_LOCK (tee);
        GST_ERROR ("warning failed to activate request pad");
        if (tee->allocpad == srcpad) {
            tee->allocpad = NULL;
            changed = TRUE;
        }
        GST_OBJECT_UNLOCK (tee);
        gst_object_unref (srcpad);
        if (changed) {
            gst_aeryon_overlay_tee_notify_alloc_pad (tee);
        }
        return NULL;
    }
}



static void
gst_aeryon_overlay_tee_release_pad (GstElement * element, GstPad * pad)
{
    GstAeryonOverlayTee *tee;
    gboolean changed = FALSE;
    guint index;

    tee = GST_AERYON_OVERLAY_TEE (element);

    GST_LOG_OBJECT(tee, "releasing pad");

    GST_OBJECT_LOCK (tee);

    index = GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->index;
    /* mark the pad as removed so that future pad_alloc fails with NOT_LINKED. */
    GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->removed = TRUE;
    if (tee->allocpad == pad) {
        tee->allocpad = NULL;
        changed = TRUE;
    }

    GST_OBJECT_UNLOCK (tee);

    gst_object_ref (pad);
    gst_element_remove_pad (GST_ELEMENT_CAST (tee), pad);

    if(g_hash_table_contains(tee->srcpads_bypass, pad)) 
        g_hash_table_remove(tee->srcpads_bypass, pad);
    

    if(g_hash_table_contains(tee->srcpads_overlay, pad))
        g_hash_table_remove(tee->srcpads_overlay, pad);
    
    gst_pad_set_active (pad, FALSE);

    gst_object_unref (pad);

    if (changed) {
        gst_aeryon_overlay_tee_notify_alloc_pad (tee);
    }

    GST_OBJECT_LOCK (tee);
    g_hash_table_remove (tee->pad_indexes, GUINT_TO_POINTER (index));
    GST_OBJECT_UNLOCK (tee);
}

static void
gst_aeryon_overlay_tee_set_property (GObject * object, guint prop_id, const GValue * value,
        GParamSpec * pspec)
{
    GstAeryonOverlayTee *tee = GST_AERYON_OVERLAY_TEE (object);

    GST_OBJECT_LOCK (tee);
    switch (prop_id) {
        case PROP_HAS_CHAIN:
            tee->has_chain = g_value_get_boolean (value);
            break;
        case PROP_SILENT:
            tee->silent = g_value_get_boolean (value);
            break;
        case PROP_PULL_MODE:
            tee->pull_mode = (GstAeryonOverlayTeePullMode) g_value_get_enum (value);
            break;
        case PROP_ALLOC_PAD:
        {
            GstPad *pad = g_value_get_object (value);
            GST_OBJECT_LOCK (pad);
            if (GST_OBJECT_PARENT (pad) == GST_OBJECT_CAST (object))
                tee->allocpad = pad;
            else
                GST_WARNING_OBJECT (object, "Tried to set alloc pad %s which"
                        " is not my pad", GST_OBJECT_NAME (pad));
            GST_OBJECT_UNLOCK (pad);
            break;
        }
        case PROP_ALLOW_NOT_LINKED:
            tee->allow_not_linked = g_value_get_boolean (value);
            break;
        case PROP_DRAW:
            tee->draw = g_value_get_boolean (value);
            break;            
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
    GST_OBJECT_UNLOCK (tee);
}

static void
gst_aeryon_overlay_tee_get_property (GObject * object, guint prop_id, GValue * value,
        GParamSpec * pspec)
{
    GstAeryonOverlayTee *tee = GST_AERYON_OVERLAY_TEE (object);

    GST_OBJECT_LOCK (tee);
    switch (prop_id) {
        case PROP_NUM_SRC_PADS:
            g_value_set_int (value, GST_ELEMENT (tee)->numsrcpads);
            break;
        case PROP_HAS_CHAIN:
            g_value_set_boolean (value, tee->has_chain);
            break;
        case PROP_SILENT:
            g_value_set_boolean (value, tee->silent);
            break;
        case PROP_LAST_MESSAGE:
            g_value_set_string (value, tee->last_message);
            break;
        case PROP_PULL_MODE:
            g_value_set_enum (value, tee->pull_mode);
            break;
        case PROP_ALLOC_PAD:
            g_value_set_object (value, tee->allocpad);
            break;
        case PROP_ALLOW_NOT_LINKED:
            g_value_set_boolean (value, tee->allow_not_linked);
            break;
        case PROP_DRAW:
            g_value_set_boolean (value, tee->draw);
            break;            
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
    GST_OBJECT_UNLOCK (tee);
}

static gboolean
gst_aeryon_overlay_tee_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
    gboolean res;
    GstAeryonOverlayTee *tee = GST_AERYON_OVERLAY_TEE (parent);


    switch (GST_EVENT_TYPE (event)) {
        case GST_EVENT_CAPS:
        case GST_EVENT_RECONFIGURE:
        case GST_EVENT_EOS:
            //Flush the queue during those events
            g_mutex_lock(&tee->data_mutex);
            while(!g_queue_is_empty(tee->pending_buffers)) {
                GstBuffer *buffer = GST_BUFFER_CAST(g_queue_pop_head(tee->pending_buffers)); 
                gst_buffer_unref(buffer);
            }
            g_mutex_unlock(&tee->data_mutex);

        default:
            res = gst_pad_event_default (pad, parent, event);
            break;
    }

    return res;
}

static gboolean
gst_aeryon_overlay_tee_sink_query (GstPad * pad, GstObject * parent, GstQuery * query)
{
    gboolean res;

    switch (GST_QUERY_TYPE (query)) {
        default:
            res = gst_pad_query_default (pad, parent, query);
            break;
    }
    return res;
}

static void
gst_aeryon_overlay_tee_do_message (GstAeryonOverlayTee * tee, GstPad * pad, gpointer data)
{
    GST_OBJECT_LOCK (tee);
    g_free (tee->last_message);

    tee->last_message =
            g_strdup_printf ("chain        ******* (%s:%s)t (%" G_GSIZE_FORMAT
            " bytes, %" G_GUINT64_FORMAT ") %p", GST_DEBUG_PAD_NAME (pad),
            gst_buffer_get_size (data), GST_BUFFER_TIMESTAMP (data), data);
    
    GST_OBJECT_UNLOCK (tee);

    g_object_notify_by_pspec ((GObject *) tee, pspec_last_message);
}


static void
aeryon_overlay_draw( Metadata2 *meta, GstVideoInfo *info, GstVideoFrame *frame)
{
    if (!meta) {
        return;
    }
        
    yuv_frame_info_t f;    
    f.h = info->height;
    f.w = info->width;
    f.s = info->stride[0];
    f.ptr = frame->data[0];
    int nt = meta->tracks.num_targets;
    int lw = 1;
    if (info->width > 1024)
        lw = 3;
    
    // Handle possible scale changes between vision and overlay
    float sx = 1.0;
    float sy = 1.0;
    if (meta->tracks.image_w != 0)
        sx = (float)info->width / meta->tracks.image_w;
    if (meta->tracks.image_h != 0)
        sy = (float)info->height / meta->tracks.image_h;
    
    int i;
    for (i = 0; i < nt; i++) {
        int xmin = sx * (meta->tracks.x[i] - meta->tracks.w[i] / 2);
        int xmax = sx * (xmin + (int)meta->tracks.w[i]);
        int ymin = sy * (meta->tracks.y[i] - meta->tracks.h[i] / 2);
        int ymax = sy * (ymin + (int)meta->tracks.h[i]);
        float conf = meta->tracks.confidence[i];
        
        if (meta->tracks.state == TRACKER_STATE_UDOT)
        {
            recticle_yuy420(&f, xmin, ymin, xmax,
                    ymax, lw, (conf >= 1.0)?
                    GREEN:(conf > 0.1)?ORANGE:RED);
        }
        else
        {
            rect2_yuy420(&f, xmin, ymin, xmax, 
                    ymax, lw, (conf >= 1.0)?
                    GREEN:(conf > 0.1)?ORANGE:RED);            
        }
    }

}

static gboolean
gst_aeryon_overlay_buffer_is_drawn(GstAeryonOverlayTee *filter, GstBuffer * buf)
{
    gboolean ret = true;

    GstAeryonBufferMeta *buffer_meta =  NULL;
    if (filter->draw) {    
        // Open buffer's metadata (if present)
        buffer_meta = GST_AERYON_BUFFER_META_GET(buf);
        if (buffer_meta && buffer_meta->gstmetadata && buffer_meta->gstmetadata->metadata)
            ret = (buffer_meta->drawn) ? true: false;
    }

    return ret;
}
/* chain function
 * this function does the actual drawing
 */
static void
gst_aeryon_overlay_tee_draw(GstPad * pad, GstAeryonOverlayTee *filter, GstBuffer * buf)
{  
    gchar* name = gst_element_get_name(GST_ELEMENT(filter));

    GstAeryonBufferMeta *buffer_meta =  NULL;
    gboolean draw_failed = false;

    if (filter->draw) {    
        // Open buffer's metadata (if present)
        buffer_meta = GST_AERYON_BUFFER_META_GET(buf);
        // Get Metadata2
        // Get Metadata2
        Metadata2 *meta = NULL;
        if (buffer_meta && buffer_meta->gstmetadata && buffer_meta->gstmetadata->metadata) {
            gst_aeryon_metadata_ref(buffer_meta->gstmetadata);
            meta = buffer_meta->gstmetadata->metadata;
        }
        else {
            fprintf(stderr, "%s does not have meta\n", name);
            draw_failed = true;
            goto exit_overlay;
        }

        // Get the video frame 
        GstCaps *caps = gst_pad_get_current_caps(pad);
        if (caps == NULL){
            fprintf(stderr, "%s could not get caps\n", name);
            draw_failed = true;            
            goto exit_overlay;
        }

        GstVideoInfo info;
        if (gst_video_info_from_caps(&info, caps) == FALSE) {
            fprintf(stderr, "%s could not get info\n", name);
            draw_failed = true;            
            goto exit_overlay;
        }

        gst_caps_unref(caps);
        
        // Handle the case where there is  more than one references to this 
        // buffer out there. This should never happen in aeryonoverlaytee
        if (!gst_buffer_is_writable(buf)) {
            GST_ERROR_OBJECT (filter, "current buffer %p PTS %" GST_TIME_FORMAT " is not writable\n", 
                buf, GST_TIME_ARGS(GST_BUFFER_PTS(buf)));
            buf = gst_buffer_make_writable(buf);
        }

        GstVideoFrame frame;
        if (gst_video_frame_map(&frame, &info, buf, GST_MAP_READ | GST_MAP_WRITE) == FALSE) {
            GST_WARNING_OBJECT(filter, "%s could not map frame\n", name);
            draw_failed = true; 
            goto exit_overlay;
        }

        aeryon_overlay_draw(meta, &info, &frame);
        buffer_meta->drawn = true;

        gst_video_frame_unmap(&frame);
    }
    
exit_overlay:
                     
    if (buffer_meta && buffer_meta->gstmetadata && buffer_meta->gstmetadata->metadata)
        gst_aeryon_metadata_unref(buffer_meta->gstmetadata);

    if(draw_failed) {
        GST_WARNING_OBJECT(filter, "Overlay failed on frame %" GST_TIME_FORMAT "\n", 
            GST_TIME_ARGS(GST_BUFFER_PTS(buf)));
    }
    g_free(name);
}

void gst_aeryon_overlay_tee_update_ts(GstAeryonOverlayTee *tee, GstPad *srcpad_bypass, guint64 timestamp)
{

  GST_OBJECT_LOCK (tee); 

  if(GST_AERYON_OVERLAY_TEE_PAD_CAST(srcpad_bypass)->always_overlay) {
        GST_DEBUG_OBJECT(tee, "Gating timestamp was updated by a always_overlay srcpad");
        goto done;
  }

  tee->gating_timestamp = (timestamp > tee->gating_timestamp) ? timestamp : tee->gating_timestamp;

  GST_LOG_OBJECT (tee, "Updating gating ts by pad %s to be %" GST_TIME_FORMAT " number of bypass pad %d number of overay pad %d \n", 
    GST_OBJECT_NAME (srcpad_bypass), 
    GST_TIME_ARGS(timestamp),
    g_hash_table_size(tee->srcpads_bypass),
    g_hash_table_size(tee->srcpads_overlay));

done:
  GST_OBJECT_UNLOCK(tee);
  return;
}

gboolean gst_aeryon_overlay_tee_set_overlay(GstAeryonOverlayTee *tee, GstPad *pad, gboolean flag) 
{
    if(!GST_PAD_IS_SRC(pad)){
        GST_ERROR_OBJECT(tee, "Trying to set overlay flag on srcpad, not supported");
        return FALSE;
    }

    GstBuffer *buffer;

    // Flush all the buffers in the queue to:
    //    1. maintain continuous timestamp in filestream
    //    2. avoid pushing the same buffer to filestream

    g_mutex_lock(&tee->data_mutex);
    while(!g_queue_is_empty(tee->pending_buffers)) {
        buffer = GST_BUFFER_CAST(g_queue_pop_head(tee->pending_buffers)); 
        GST_INFO_OBJECT(pad, "set_overlay unreffing buffer %p PTS %" GST_TIME_FORMAT " \n", 
            buffer, GST_TIME_ARGS(GST_BUFFER_PTS(buffer)));
        gst_buffer_unref(buffer);
    }
    g_mutex_unlock(&tee->data_mutex);

    GST_OBJECT_LOCK (tee);

    if(flag) { 
        if(g_hash_table_contains(tee->srcpads_bypass, pad) &&  !g_hash_table_contains(tee->srcpads_overlay, pad)) {

            g_hash_table_remove(tee->srcpads_bypass, pad);
            g_hash_table_insert(tee->srcpads_overlay, pad, NULL); 
            GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->always_overlay = TRUE;

            GST_WARNING_OBJECT(pad, "Moving a pad from bypass into overlay hash ");
    	} else {
        	GST_WARNING_OBJECT(pad, "Current srcpad is already in overlay hash \n"); 
    	}
	} else {
	    if(!g_hash_table_contains(tee->srcpads_bypass, pad) && g_hash_table_contains(tee->srcpads_overlay, pad)) {

	        g_hash_table_remove(tee->srcpads_overlay, pad);
            g_hash_table_insert(tee->srcpads_bypass, pad, NULL);
            GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->always_overlay = FALSE;

            GST_WARNING_OBJECT(pad, "Moving pad from overlay into bypass hash ");  
		} else {
		    GST_WARNING_OBJECT(pad, "Current srcpad is already in bypass hash \n");      
		}
	}
	GST_OBJECT_UNLOCK (tee);

	return TRUE;
}

static void
clear_pads (GstPad * pad, __attribute__((unused)) GstAeryonOverlayTee * tee)
{
    GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->pushed = FALSE;
    GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->result = GST_FLOW_NOT_LINKED;
}

static GstFlowReturn
gst_aeryon_overlay_tee_do_push(GstAeryonOverlayTee * tee, GstPad * pad, gpointer data, 
                                guint32 num_bypass, guint32 num_overlay, gboolean is_overlay_srcpad)
{
    GstFlowReturn res = GST_FLOW_OK;

    /*data type depends on srcpad overlay status:
     **If current pad is a bypass srcpad (filestream): data is with GstsBuffer type
     **If current pad is a overlay requiring srcpad, data is a list of GstsBuffer
    */
    if(data == NULL)
        return res;
    
    if(!is_overlay_srcpad) { //bypass srcpad, ref buffer and forward it immediately 
        res = gst_pad_push (pad, gst_buffer_ref (GST_BUFFER_CAST(data)));

        GST_LOG_OBJECT(tee, "bypass pad do_push: buffer: %p writable %s gating PTS %" GST_TIME_FORMAT " data PTS %" GST_TIME_FORMAT "\n", 
            GST_BUFFER_CAST(data), (gst_buffer_is_writable(GST_BUFFER_CAST(data)) ? "TRUE" : "FALSE"),
            GST_TIME_ARGS(tee->gating_timestamp),
            GST_TIME_ARGS((GST_BUFFER_PTS(GST_BUFFER_CAST(data))))
            );

        return res;        
    } else {
        GList *cur;
        //If all srcpads require overlaying, buffer list here must only have one item
        for(cur = g_list_first((GList *)data); cur != NULL; cur = g_list_next(cur)){
            GstBuffer *buffer = (GstBuffer *) cur->data;

            if(num_bypass == 0)
                res = gst_pad_push (pad,  gst_buffer_ref(buffer));                
            else if(num_overlay == 1) //If all srcpads require overlaying, buffer list here must only have one item
                res = gst_pad_push (pad, buffer); //buffer has beeing reffed when we pushing them into pending_buffers queue
            else 
               GST_ERROR_OBJECT(tee, "Unsupported mode: one bypass and more than one overlay srcpads"); 

           GST_LOG_OBJECT(tee, "overlay pad do_push: buffer: %p writable %s gating PTS %" GST_TIME_FORMAT " data PTS %" GST_TIME_FORMAT "\n", 
                buffer, (gst_buffer_is_writable(buffer) ? "TRUE" : "FALSE"),
                GST_TIME_ARGS(tee->gating_timestamp),
                GST_TIME_ARGS((GST_BUFFER_PTS(buffer)))
                );

        }
        return res;
    }
}

void fetch_overlayed_buffers(GstAeryonOverlayTee * tee, GstPad *pad, GList** outlist, gpointer data, gboolean is_no_bypass)
{
    GList* overlayed_buffers = NULL;;
    guint64 gating_timestamp = tee->gating_timestamp; 
    GstBuffer *in_buffer = GST_BUFFER_CAST (data);

    GstBuffer *out_buffer = NULL;;

    //if all the srcpads are changed to overlay, we will just flush all outstanding buffers in current queue and 
    //always drawing on the in_buffer before reffing it. 
    if(is_no_bypass) {

        g_mutex_lock(&tee->data_mutex);
        while(!g_queue_is_empty(tee->pending_buffers)) {

            out_buffer = GST_BUFFER_CAST(g_queue_pop_head(tee->pending_buffers)); 
            //only draw once 
            if(!gst_aeryon_overlay_buffer_is_drawn(tee, out_buffer))
                gst_aeryon_overlay_tee_draw(pad, tee, out_buffer);

                GST_LOG_OBJECT(tee, "No bypass srcpads: poping queue buffer: %p writable %s gating PTS %" GST_TIME_FORMAT " data PTS %" GST_TIME_FORMAT " q_len = %d", 
                    out_buffer, (gst_buffer_is_writable(out_buffer) ? "TRUE" : "FALSE"),
                    GST_TIME_ARGS(gating_timestamp),
                    GST_TIME_ARGS((GST_BUFFER_PTS(out_buffer))),
                    g_queue_get_length(tee->pending_buffers)
                    );

            /* adding qualified buffer into list*/
            overlayed_buffers = g_list_append(overlayed_buffers, out_buffer);
        }        
        g_mutex_unlock(&tee->data_mutex);
        
        GST_LOG_OBJECT(tee, "No bypass srcpads: prepare pushing buffer: %p writable %s gating PTS %" GST_TIME_FORMAT " data PTS %" GST_TIME_FORMAT " q_len = %d", 
            in_buffer, (gst_buffer_is_writable(in_buffer) ? "TRUE" : "FALSE"),
            GST_TIME_ARGS(gating_timestamp),
            GST_TIME_ARGS((GST_BUFFER_PTS(in_buffer))),
            g_queue_get_length(tee->pending_buffers)
            );

        //draw will only happen once as setting 'drawn' flag on buffer
        if(!gst_aeryon_overlay_buffer_is_drawn(tee, in_buffer))
            gst_aeryon_overlay_tee_draw(pad, tee, in_buffer); 

        overlayed_buffers  = g_list_append(overlayed_buffers, in_buffer);
        *outlist = overlayed_buffers;

    } else {

        g_mutex_lock(&tee->data_mutex);

        g_queue_push_tail(tee->pending_buffers, gst_buffer_ref(in_buffer));

         while(!g_queue_is_empty(tee->pending_buffers)) {

            GstBuffer *top_item = g_queue_peek_head(tee->pending_buffers);

            if(!top_item || !GST_IS_BUFFER(top_item)) {
                GST_ERROR_OBJECT(tee, "Buffers in pending queue might have been released \n");
                break;
            }
            
            gboolean no_push = 
                (g_queue_get_length(tee->pending_buffers) < MAX_QUEUE_LEN) &&
                (GST_BUFFER_PTS(top_item) >= gating_timestamp && !gst_buffer_is_writable(top_item)); 

            if(no_push) {
                GST_LOG_OBJECT(tee, "No buffer is avaliable for drawing: writable %s gating ts %" GST_TIME_FORMAT " top buffer PTS %" GST_TIME_FORMAT " top buffer_ptr %p q_len %d ", 
                    (gst_buffer_is_writable(top_item) ? "TRUE" : "FALSE"),
                    GST_TIME_ARGS(gating_timestamp), 
                    GST_TIME_ARGS((GST_BUFFER_PTS(top_item))),
                    top_item,
                    g_queue_get_length(tee->pending_buffers));
                break;   
            }

            out_buffer = GST_BUFFER_CAST(g_queue_pop_head(tee->pending_buffers)); 

            GST_LOG_OBJECT(tee, "Overlay srcpad: prepare pushing buffer buffer_ptr %p writable %s gating PTS %" GST_TIME_FORMAT " data PTS %" GST_TIME_FORMAT " q_len %d", 
                out_buffer, (gst_buffer_is_writable(out_buffer) ? "TRUE" : "FALSE"),
                GST_TIME_ARGS(gating_timestamp),
                GST_TIME_ARGS((GST_BUFFER_PTS(out_buffer))),
                g_queue_get_length(tee->pending_buffers)
                );

            if(!gst_aeryon_overlay_buffer_is_drawn(tee, out_buffer))
                gst_aeryon_overlay_tee_draw(pad, tee, out_buffer);

            overlayed_buffers = g_list_append(overlayed_buffers, out_buffer);
        }
        g_mutex_unlock(&tee->data_mutex);        
    }

    *outlist = overlayed_buffers;
}

static GstFlowReturn
gst_aeryon_overlay_tee_handle_data (GstAeryonOverlayTee * tee, gpointer data)
{    
    GList *pads;
    guint32 cookie;
    GstFlowReturn ret = GST_FLOW_OK;
    GstFlowReturn cret = GST_FLOW_OK;

    if (G_UNLIKELY (!tee->silent))
        gst_aeryon_overlay_tee_do_message (tee, tee->sinkpad, data);

    GST_OBJECT_LOCK (tee);

    pads = GST_ELEMENT_CAST (tee)->srcpads;

    /* special case for zero pads */
    if (G_UNLIKELY (!pads))
        goto no_pads;

    /* special case for just one pad that avoids reffing the buffer */
    if (!pads->next) {
        GstPad *pad = GST_PAD_CAST (pads->data);

        GstBuffer *out_buffer = NULL;

        /* Keep another ref around, a pad probe
         * might release and destroy the pad */
        gst_object_ref (pad);
        GST_OBJECT_UNLOCK (tee);

        g_mutex_lock(&tee->data_mutex);
        while(!g_queue_is_empty(tee->pending_buffers)) {

            out_buffer = GST_BUFFER_CAST(g_queue_pop_head(tee->pending_buffers)); 

            if(!gst_aeryon_overlay_buffer_is_drawn(tee, out_buffer))
                gst_aeryon_overlay_tee_draw(pad, tee, out_buffer);
            /* Push */
            ret = gst_pad_push (pad, gst_buffer_ref (out_buffer));
        }
        g_mutex_unlock(&tee->data_mutex);

        out_buffer = GST_BUFFER_CAST (data);
        if(!gst_aeryon_overlay_buffer_is_drawn(tee, out_buffer))
            gst_aeryon_overlay_tee_draw(pad, tee, out_buffer);

        ret = gst_pad_push (pad, out_buffer);

        gst_object_unref (pad);

        if (ret == GST_FLOW_NOT_LINKED && tee->allow_not_linked) {
            ret = GST_FLOW_OK;
        }

        return ret;
    }

    /* mark all pads as 'not pushed on yet' */
    g_list_foreach (pads, (GFunc) clear_pads, tee);

    guint32 num_bypass = g_hash_table_size(tee->srcpads_bypass);
    guint32 num_overlay = g_hash_table_size(tee->srcpads_overlay);
    /*store srcpads into a list to avoid race condition*/
    GList *bypass_srcpads_list = g_hash_table_get_keys(tee->srcpads_bypass);
    GList *overlay_srcpads_list = g_hash_table_get_keys(tee->srcpads_overlay);
    GList *bufferlist = NULL;   
    GList *cur = NULL;

restart_overlay:
    if(num_overlay > 0) {
        if (tee->allow_not_linked) {
            cret = GST_FLOW_OK;
        } else {
            cret = GST_FLOW_NOT_LINKED;
        }
    }

    cookie = GST_ELEMENT_CAST (tee)->pads_cookie;

    //push data to overlay srcpads
    if(overlay_srcpads_list) {
        for(cur = g_list_first(overlay_srcpads_list); cur != NULL; cur = g_list_next(cur)) {
            gpointer pad_overlay  = cur->data;

            if(!pad_overlay)
                break;

            GstPad *pad = GST_PAD_CAST (pad_overlay);

            if (G_LIKELY (!GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->pushed)) {
                /* not yet pushed, release lock and start pushing */
                gst_object_ref (pad);

                GST_OBJECT_UNLOCK (tee);

                /*This has to call after unlocking object o/w might call deadlock due to gst_element_get_name()
                 *If all the srcpads require overlaying, fetch_overlayed_buffers() will simply draw buffer on arrivated buffer
                 *If we a srcpad is bypassing, we will delay buffers for overlay srcpad until bypass srcpad branch release the 
                    buffer ref counter. fetch_overlayed_buffers() might return nothing or return a list of buffers. 
                */
                GST_LOG_OBJECT (pad, "Starting to push buffer %p to overlay srcpads", data);

                fetch_overlayed_buffers(tee, pad, &bufferlist, data, (num_bypass == 0));

                ret = gst_aeryon_overlay_tee_do_push(tee, pad, bufferlist, num_bypass, num_overlay, TRUE);

                GST_LOG_OBJECT (pad, "Pushing item %p yielded result %s", data,
                    gst_flow_get_name (ret));

                GST_OBJECT_LOCK (tee);
                /* keep track of which pad we pushed and the result value */
                GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->pushed = TRUE;
                GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->result = ret;
                gst_object_unref (pad);
                pad = NULL;

            } else {
                /* already pushed, use previous return value */
                ret = GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->result;
                GST_LOG_OBJECT (pad, "pad already pushed with %s", gst_flow_get_name (ret));
            }

            /* before we go combining the return value, check if the pad list is still
             * the same. It could be possible that the pad we just pushed was removed
             * and the return value it not valid anymore */
             if (G_UNLIKELY (GST_ELEMENT_CAST (tee)->pads_cookie != cookie)) {
                GST_LOG_OBJECT (tee, "pad list changed");
                /* the list of pads changed, restart iteration. Pads that we already
                 * pushed on and are still in the new list, will not be pushed on
                 * again. */
                 goto restart_overlay;
             }

            /* stop pushing more buffers when we have a fatal error */
             if (G_UNLIKELY (ret != GST_FLOW_OK && ret != GST_FLOW_NOT_LINKED))
                goto error;

            /* keep all other return values, overwriting the previous one. */
            if (G_LIKELY (ret != GST_FLOW_NOT_LINKED)) {
                GST_LOG_OBJECT (tee, "Replacing ret val %d with %d", cret, ret);
                cret = ret;
            }
        }
    }

    if(bufferlist) g_list_free(bufferlist);
    if(overlay_srcpads_list) g_list_free(overlay_srcpads_list);

    cur = NULL;

    restart_bypass:
    if(num_bypass != 0) {
        if (tee->allow_not_linked) {
            cret = GST_FLOW_OK;      
        } else {
            cret = GST_FLOW_NOT_LINKED;
        }        
    }

    cookie = GST_ELEMENT_CAST (tee)->pads_cookie;

    if(bypass_srcpads_list) {
        for(cur = g_list_first(bypass_srcpads_list); cur != NULL; cur = g_list_next(cur)) {
            gpointer pad_bypass  = cur->data;

            if(!pad_bypass)
                break;

            GstPad *pad = GST_PAD_CAST (pad_bypass);

            if (G_LIKELY (!GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->pushed)) {
                /* not yet pushed, release lock and start pushing */
                gst_object_ref (pad);
                GST_OBJECT_UNLOCK (tee);

                GST_LOG_OBJECT (pad, "Starting to push buffer %p to bypass srcpads", data);

                ret = gst_aeryon_overlay_tee_do_push(tee, pad, data, num_bypass, num_overlay, FALSE);

                GST_LOG_OBJECT (pad, "Pushing item %p yielded result %s", data,
                    gst_flow_get_name (ret));

                GST_OBJECT_LOCK (tee);
                /* keep track of which pad we pushed and the result value */
                GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->pushed = TRUE;
                GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->result = ret;
                gst_object_unref (pad);
                pad = NULL;
            } else {
                /* already pushed, use previous return value */
                ret = GST_AERYON_OVERLAY_TEE_PAD_CAST (pad)->result;
                GST_LOG_OBJECT (pad, "pad already pushed with %s",
                    gst_flow_get_name (ret));
            }

            /* before we go combining the return value, check if the pad list is still
             * the same. It could be possible that the pad we just pushed was removed
             * and the return value it not valid anymore */
             if (G_UNLIKELY (GST_ELEMENT_CAST (tee)->pads_cookie != cookie)) {
                GST_LOG_OBJECT (tee, "pad list changed");
                /* the list of pads changed, restart iteration. Pads that we already
                 * pushed on and are still in the new list, will not be pushed on
                 * again. */
                 goto restart_bypass;
             }

            /* stop pushing more buffers when we have a fatal error */
             if (G_UNLIKELY (ret != GST_FLOW_OK && ret != GST_FLOW_NOT_LINKED))
                goto error;

            /* keep all other return values, overwriting the previous one. */
            if (G_LIKELY (ret != GST_FLOW_NOT_LINKED)) {
                GST_LOG_OBJECT (tee, "Replacing ret val %d with %d", cret, ret);
                cret = ret;
            }
        }
    }

    if(bypass_srcpads_list) g_list_free(bypass_srcpads_list);


    GST_OBJECT_UNLOCK (tee);
    gst_mini_object_unref (GST_MINI_OBJECT_CAST (data));

    /* no need to unset gvalue */
    return cret;

    /* ERRORS */
    no_pads:
    {
        if (tee->allow_not_linked) {
            GST_ERROR_OBJECT (tee, "there are no pads, dropping buffer");
            ret = GST_FLOW_OK;
        } else {
            GST_DEBUG_OBJECT (tee, "there are no pads, return not-linked");
            ret = GST_FLOW_NOT_LINKED;
        }
        goto end;
    }
    error:
    {
        GST_ERROR_OBJECT (tee, "received error %s", gst_flow_get_name (ret));
        goto end;
    }
    end:
    {
        GST_OBJECT_UNLOCK (tee);
        gst_mini_object_unref (GST_MINI_OBJECT_CAST (data));
        return ret;
    }
}


static GstFlowReturn
gst_aeryon_overlay_tee_chain (__attribute__((unused)) GstPad * pad, GstObject * parent, GstBuffer * buffer)
{
    GstFlowReturn res;
    GstAeryonOverlayTee *tee;

    tee = GST_AERYON_OVERLAY_TEE_CAST (parent);

    GST_LOG_OBJECT (tee, "received buffer %p", buffer);

    res = gst_aeryon_overlay_tee_handle_data (tee, buffer);

    GST_LOG_OBJECT (tee, "handled buffer %s", gst_flow_get_name (res));

    return res;
}


static gboolean
gst_aeryon_overlay_tee_sink_activate_mode (__attribute__((unused)) GstPad * pad, GstObject * parent, GstPadMode mode,
        gboolean active)
{
    gboolean res;
    GstAeryonOverlayTee *tee;

    tee = GST_AERYON_OVERLAY_TEE (parent);

    switch (mode) {
        case GST_PAD_MODE_PUSH:
        {
            GST_OBJECT_LOCK (tee);
            tee->sink_mode = active ? mode : GST_PAD_MODE_NONE;

            if (active && !tee->has_chain)
                goto no_chain;
            GST_OBJECT_UNLOCK (tee);
            res = TRUE;
            break;
        }
        default:
            res = FALSE;
            break;
    }
    return res;

    /* ERRORS */
no_chain:
    {
        GST_OBJECT_UNLOCK (tee);
        GST_INFO_OBJECT (tee,
                "Tee cannot operate in push mode with has-chain==FALSE");
        return FALSE;
    }
}

static gboolean
gst_aeryon_overlay_tee_src_activate_mode (GstPad * pad, GstObject * parent, GstPadMode mode,
        gboolean active)
{
    GstAeryonOverlayTee *tee;
    gboolean res;
    GstPad *sinkpad;

    tee = GST_AERYON_OVERLAY_TEE (parent);

    switch (mode) {
        case GST_PAD_MODE_PULL:
        {
            GST_OBJECT_LOCK (tee);

            if (tee->pull_mode == GST_AERYON_OVERLAY_TEE_PULL_MODE_NEVER)
                goto cannot_pull;

            if (tee->pull_mode == GST_AERYON_OVERLAY_TEE_PULL_MODE_SINGLE && active && tee->pull_pad)
                goto cannot_pull_multiple_srcs;

            sinkpad = gst_object_ref (tee->sinkpad);

            GST_OBJECT_UNLOCK (tee);

            res = gst_pad_activate_mode (sinkpad, GST_PAD_MODE_PULL, active);
            gst_object_unref (sinkpad);

            if (!res)
                goto sink_activate_failed;

            GST_OBJECT_LOCK (tee);
            if (active) {
                if (tee->pull_mode == GST_AERYON_OVERLAY_TEE_PULL_MODE_SINGLE)
                    tee->pull_pad = pad;
            } else {
                if (pad == tee->pull_pad)
                    tee->pull_pad = NULL;
            }
            tee->sink_mode = (active ? GST_PAD_MODE_PULL : GST_PAD_MODE_NONE);
            GST_OBJECT_UNLOCK (tee);
            break;
        }
        default:
            res = TRUE;
            break;
    }

    return res;

    /* ERRORS */
cannot_pull:
    {
        GST_OBJECT_UNLOCK (tee);
        GST_INFO_OBJECT (tee, "Cannot activate in pull mode, pull-mode "
                "set to NEVER");
        return FALSE;
    }
cannot_pull_multiple_srcs:
    {
        GST_OBJECT_UNLOCK (tee);
        GST_INFO_OBJECT (tee, "Cannot activate multiple src pads in pull mode, "
                "pull-mode set to SINGLE");
        return FALSE;
    }
sink_activate_failed:
    {
        GST_INFO_OBJECT (tee, "Failed to %sactivate sink pad in pull mode",
                active ? "" : "de");
        return FALSE;
    }
}



static gboolean
gst_aeryon_overlay_tee_src_query (GstPad * pad, GstObject * parent, GstQuery * query)
{
    GstAeryonOverlayTee *tee;
    gboolean res;
    GstPad *sinkpad;

    tee = GST_AERYON_OVERLAY_TEE (parent);

    switch (GST_QUERY_TYPE (query)) {
        case GST_QUERY_SCHEDULING:
        {
            gboolean pull_mode;

            GST_OBJECT_LOCK (tee);
            pull_mode = TRUE;
            if (tee->pull_mode == GST_AERYON_OVERLAY_TEE_PULL_MODE_NEVER) {
                GST_INFO_OBJECT (tee, "Cannot activate in pull mode, pull-mode "
                        "set to NEVER");
                pull_mode = FALSE;
            } else if (tee->pull_mode == GST_AERYON_OVERLAY_TEE_PULL_MODE_SINGLE && tee->pull_pad) {
                GST_INFO_OBJECT (tee, "Cannot activate multiple src pads in pull mode, "
                        "pull-mode set to SINGLE");
                pull_mode = FALSE;
            }

            sinkpad = gst_object_ref (tee->sinkpad);
            GST_OBJECT_UNLOCK (tee);

            if (pull_mode) {
                /* ask peer if we can operate in pull mode */
                res = gst_pad_peer_query (sinkpad, query);
            } else {
                res = TRUE;
            }
            gst_object_unref (sinkpad);
            break;
        }
        default:
            res = gst_pad_query_default (pad, parent, query);
            break;
    }

    return res;
}

static void
gst_aeryon_overlay_tee_push_eos (const GValue * vpad, GstAeryonOverlayTee * tee)
{
    GstPad *pad = g_value_get_object (vpad);

    if (pad != tee->pull_pad)
        gst_pad_push_event (pad, gst_event_new_eos ());
}

static void
gst_aeryon_overlay_tee_pull_eos (GstAeryonOverlayTee * tee)
{
    GstIterator *iter;

    iter = gst_element_iterate_src_pads (GST_ELEMENT (tee));
    gst_iterator_foreach (iter, (GstIteratorForeachFunction) gst_aeryon_overlay_tee_push_eos,
            tee);
    gst_iterator_free (iter);
}

static GstFlowReturn
gst_aeryon_overlay_tee_src_get_range (__attribute__((unused)) GstPad * pad, GstObject * parent, guint64 offset,
        guint length, GstBuffer ** buf)
{
    GstAeryonOverlayTee *tee;
    GstFlowReturn ret;

    tee = GST_AERYON_OVERLAY_TEE (parent);

    ret = gst_pad_pull_range (tee->sinkpad, offset, length, buf);

    if (ret == GST_FLOW_OK)
        ret = gst_aeryon_overlay_tee_handle_data (tee, gst_buffer_ref (*buf));

    else if (ret == GST_FLOW_EOS)
        gst_aeryon_overlay_tee_pull_eos (tee);

    return ret;
}
