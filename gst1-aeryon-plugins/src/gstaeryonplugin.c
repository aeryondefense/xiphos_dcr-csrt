
#include "gstptpsrc.h"
#include "gstaeryonmeta.h"
#include "gstaeryondbg.h"
#include "gstaeryonparser.h"
#include "gstaeryongimbal.h"
#include "gstaeryonvision.h"
#include "gstxiphosvision.h"
#include "gstaeryonoverlay.h"
#include "gstaeryonbufferalloc.h"
#include "gstaeryonoverlaytee.h"
#include "gstvideowriter.h"
#include "gstcudajpegdec.h"
#include "gsth264meta.h"
#include "gstframeduplicator.h"


static gboolean plugin_init(GstPlugin *plugin)
{
    if (!gst_element_register(plugin, "aeryonmeta",         GST_RANK_NONE, GST_TYPE_AERYON_META       ) ||
        !gst_element_register(plugin, "aeryonparser",       GST_RANK_NONE, GST_TYPE_AERYON_PARSER     ) ||
        !gst_element_register(plugin, "ptpsrc",             GST_RANK_NONE, GST_TYPE_PTP_SRC           ) ||
        !gst_element_register(plugin, "aeryondbg",          GST_RANK_NONE, GST_TYPE_AERYON_DBG        ) ||
        !gst_element_register(plugin, "aeryongimbal",       GST_RANK_NONE, GST_TYPE_AERYON_GIMBAL     ) ||
        !gst_element_register(plugin, "aeryonvision",       GST_RANK_NONE, GST_TYPE_AERYON_VISION     ) ||
        !gst_element_register(plugin, "xiphosvision",       GST_RANK_NONE, GST_TYPE_XIPHOS_VISION     ) ||
        !gst_element_register(plugin, "aeryonoverlay",      GST_RANK_NONE, GST_TYPE_AERYON_OVERLAY    ) ||
        !gst_element_register(plugin, "aeryonbufferalloc",  GST_RANK_NONE, GST_TYPE_AERYONBUFFERALLOC ) ||
        !gst_element_register(plugin, "aeryonoverlaytee",   GST_RANK_NONE, GST_TYPE_AERYON_OVERLAY_TEE) ||
        !gst_element_register(plugin, "videowriter",        GST_RANK_NONE, GST_TYPE_VIDEO_WRITER      ) ||
        !gst_element_register(plugin, "cudajpegdec",        GST_RANK_NONE, GST_TYPE_CUDA_JPEG_DEC     ) || 
        !gst_element_register(plugin, "h264meta",           GST_RANK_NONE, GST_TYPE_H264_META         ) ||    
        !gst_element_register(plugin, "frameduplicator",    GST_RANK_NONE, GST_TYPE_FRAME_DUPLICATOR  ) ||
        FALSE)
        return FALSE;
    
    return TRUE;
}

#ifndef PACKAGE
#define PACKAGE "Aeryon plugins"
#endif

GST_PLUGIN_DEFINE(
        GST_VERSION_MAJOR,
        GST_VERSION_MINOR,
        aeryonplugins,
        "Aeryon Gstreamer elements",
        plugin_init,
        "1.0",
        "LGPL",
        "GStreamer",
        "http://gstreamer.net/")

