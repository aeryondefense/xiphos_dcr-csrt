/* GStreamer
 * Copyright (C) 2016 Robb Nandlall <robb.nandlall@aeryon.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstvideowriter
 *
 * The videowriter element does FIXME stuff.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v fakesrc ! videowriter file="MOV_0001.TS"
 * ]|
 * FIXME Describe what the pipeline does.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/video/video-event.h>
#include "gstvideowriter.h"

#include <inttypes.h>
#include <string.h>
#include <unistd.h>

#include "video_writer.h"
#include "video_writer_csv.h"

GST_DEBUG_CATEGORY_STATIC (gst_video_writer_debug_category);
#define GST_CAT_DEFAULT gst_video_writer_debug_category

/* prototypes */

static void gst_video_writer_set_property(GObject * object, guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_video_writer_get_property(GObject * object, guint property_id, GValue * value, GParamSpec * pspec);
static void gst_video_writer_dispose(GObject * object);
static void gst_video_writer_finalize(GObject * object);
static gboolean gst_video_writer_video_event(GstPad *pad, GstObject *parent, GstEvent * event);
static GstFlowReturn gst_video_writer_video_chain(GstPad *pad, GstObject *parent, GstBuffer *buf);
static GstFlowReturn gst_video_writer_klv_chain(GstPad *pad, GstObject *parent, GstBuffer *buf);

/* interface to the video_writer library */
static void gvw_record_start(GstVideoWriter *video_writer, char *filename);
static void gvw_record_stop(GstVideoWriter *video_writer);
static void gvw_record_rollover(GstVideoWriter *video_writer);

#define DEFAULT_LOCATION      (NULL)
#define DEFAULT_BUFFER_SIZE   (0)
#define DEFAULT_WRITE_KLV     (TRUE)
#define DEFAULT_RECORD        (TRUE) /* Enable auto-record when run from bash. */

#define KLV_LOG_RATE        (5) // Hz
#define SAFE_FILE_SIZE      ((38ll << 30) / 10)   // 3.8 GB
#define MAX_FILE_SIZE       (UINT32_MAX)          // 4.0 GB - 1 byte
#define FRAME_DURATION      (40000000)            // 40ms

enum
{
  PROP_0,
  PROP_LOCATION,
  PROP_BUFFER_SIZE,
  PROP_WRITE_KLV,
  PROP_RECORD,
  PROP_LAST_PTS,  
};

/* pad templates */

/* We may extend this to support more than just "video/x-h264" in the future,
   but this is all that's supported by our videowriter library at the moment. */
static GstStaticPadTemplate gst_video_writer_video_template =
GST_STATIC_PAD_TEMPLATE ("video",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-h264")
    );

/* TODO: Accept a single stream with embedded KLV? E.g. gstaeryonparser? */
static GstStaticPadTemplate gst_video_writer_klv_template =
GST_STATIC_PAD_TEMPLATE ("klv",
    GST_PAD_SINK,
    GST_PAD_SOMETIMES,
    GST_STATIC_CAPS ("meta/x-klv")
    );


/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstVideoWriter, gst_video_writer, GST_TYPE_ELEMENT,
  GST_DEBUG_CATEGORY_INIT (gst_video_writer_debug_category, "videowriter", 0,
  "debug category for videowriter element"));

static void
gst_video_writer_class_init (GstVideoWriterClass * klass)
{
  GObjectClass    *gobject_class  = G_OBJECT_CLASS   (klass);
  GstElementClass *gelement_class = GST_ELEMENT_CLASS(klass);
  
  gobject_class->set_property = gst_video_writer_set_property;
  gobject_class->get_property = gst_video_writer_get_property;
  gobject_class->dispose      = gst_video_writer_dispose;
  gobject_class->finalize     = gst_video_writer_finalize;
  
  vw_mux_init();
  
  g_object_class_install_property(gobject_class, PROP_LOCATION,
      g_param_spec_string ("location", "File Location",
          "Location of the file to write", DEFAULT_LOCATION,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property(gobject_class, PROP_BUFFER_SIZE,
      g_param_spec_int ("buffer-size", "Buffering size",
          "Size of buffer in number of bytes for line or full buffer-mode",
          0, G_MAXINT, DEFAULT_BUFFER_SIZE,
          G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_WRITE_KLV,
      g_param_spec_boolean ("klv", "Write KLV",
          "Write KLV metadata to the .TS file", DEFAULT_WRITE_KLV,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_RECORD,
      g_param_spec_boolean ("record", "Record to file",
          "Record to file now.", DEFAULT_RECORD,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_LAST_PTS,
      g_param_spec_uint64 ("last-pts", "Last Frame Timestamp",
          "Timestamp of last processed frame.", GST_CLOCK_TIME_NONE, G_MAXUINT64, GST_CLOCK_TIME_NONE,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  gst_element_class_set_details_simple (gelement_class,
      "Video Writer", "Sink/File", "Mux H.264 and KLV streams into a .TS file",
      "Robb Nandlall <robb.nandlall@aeryon.com>");

  gst_element_class_add_pad_template (gelement_class,
      gst_static_pad_template_get (&gst_video_writer_video_template));
  gst_element_class_add_pad_template (gelement_class,
      gst_static_pad_template_get (&gst_video_writer_klv_template));
}

static void
gst_video_writer_init (GstVideoWriter *video_writer)
{
  memset(&video_writer->props, 0, sizeof(video_writer->props));
  memset(&video_writer->state, 0, sizeof(video_writer->state));
  
  video_writer->last_pts = GST_CLOCK_TIME_NONE;
  video_writer->first_key_frame = FALSE;
  
  video_writer->videopad = gst_pad_new_from_static_template(
      &gst_video_writer_video_template, "video");
  
  gst_pad_set_event_function(video_writer->videopad,
      GST_DEBUG_FUNCPTR(gst_video_writer_video_event));
  gst_pad_set_chain_function(video_writer->videopad,
      GST_DEBUG_FUNCPTR(gst_video_writer_video_chain));
  GST_PAD_SET_PROXY_CAPS(video_writer->videopad);
  gst_element_add_pad(GST_ELEMENT(video_writer), video_writer->videopad);
  
  video_writer->klvpad   = gst_pad_new_from_static_template(
      &gst_video_writer_klv_template, "klv");
  gst_pad_set_chain_function(video_writer->klvpad,
      GST_DEBUG_FUNCPTR(gst_video_writer_klv_chain));
  GST_PAD_SET_PROXY_CAPS(video_writer->klvpad);
  gst_element_add_pad(GST_ELEMENT(video_writer), video_writer->klvpad);
}

static void
gst_video_writer_set_property_location (GstVideoWriter *video_writer,
   const gchar *location)
{
  GstVideoWriterProps *props = &video_writer->props;
  
  /* Stop recording whenever the location is updated */
  gvw_record_stop(video_writer);
  
  /* Remove old location */
  if (props->location)
  {
    g_free(props->location);
    props->location = NULL;
  }
  
  /* Add new location if it is a non-empty string */
  if (location && strlen(location) > 0)
  {
    props->location = g_strdup(location);
  }
}

static void
gst_video_writer_set_property_record (GstVideoWriter *video_writer,
   gboolean record)
{
  GstVideoWriterProps *props = &video_writer->props;
  GstVideoWriterState *state = &video_writer->state;
  
  props->record = record;
  
  if (!record)
  {
    gvw_record_stop(video_writer);
  }
  else if (props->location && !state->is_recording)
  {
    video_writer->last_pts  = GST_CLOCK_TIME_NONE;
    state->file_index       = 1;
    gvw_record_start(video_writer, props->location);
  }
}

void
gst_video_writer_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstVideoWriter *video_writer = GST_VIDEO_WRITER (object);

  GST_DEBUG_OBJECT (video_writer, "set_property");

  GST_OBJECT_LOCK(video_writer);
  switch (property_id) {
    case PROP_LOCATION:
      gst_video_writer_set_property_location(video_writer,
        g_value_get_string(value));
      break;
    case PROP_BUFFER_SIZE:
      if (!video_writer->state.is_recording) {
        video_writer->props.buffer_size = g_value_get_int(value);
      } else {
        GST_WARNING_OBJECT(video_writer, "ignoring buffer-size prop: %d\n",
          g_value_get_int(value));
      }
      break;
    case PROP_WRITE_KLV:
      if (!video_writer->state.is_recording) {
        video_writer->props.write_klv = g_value_get_boolean(value);
      } else {
        GST_WARNING_OBJECT(video_writer, "ignoring klv prop: %d\n",
          g_value_get_boolean(value));
      }
      break;
    case PROP_RECORD:
      gst_video_writer_set_property_record(video_writer,
        g_value_get_boolean(value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
  GST_OBJECT_UNLOCK(video_writer);
}

void
gst_video_writer_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstVideoWriter *video_writer = GST_VIDEO_WRITER (object);

  GST_DEBUG_OBJECT (video_writer, "get_property");

  GST_OBJECT_LOCK(video_writer);
  switch (property_id) {
    case PROP_LOCATION:
      g_value_set_string(value, video_writer->props.location);
      break;
    case PROP_BUFFER_SIZE:
      g_value_set_int(value, video_writer->props.buffer_size);
      break;
    case PROP_WRITE_KLV:
      g_value_set_boolean(value, video_writer->props.write_klv);
      break;
    case PROP_RECORD:
      /* Read the actual file recording state instead of whether it should be
         recording. */
      g_value_set_boolean(value, video_writer->state.is_recording);
      break;
    case PROP_LAST_PTS:
      g_value_set_uint64(value, video_writer->last_pts);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
  GST_OBJECT_UNLOCK(video_writer);
}

void
gst_video_writer_dispose (GObject * object)
{
  GstVideoWriter *video_writer = GST_VIDEO_WRITER (object);

  GST_DEBUG_OBJECT (video_writer, "dispose");

  /* clean up as possible.  may be called multiple times */

  gvw_record_stop(video_writer);
  if (video_writer->props.location)
  {
    g_free(video_writer->props.location);
    video_writer->props.location = NULL;
  }

  G_OBJECT_CLASS (gst_video_writer_parent_class)->dispose (object);
}

void
gst_video_writer_finalize (GObject * object)
{
  GstVideoWriter *video_writer = GST_VIDEO_WRITER (object);

  GST_DEBUG_OBJECT (video_writer, "finalize");

  /* clean up object here */

  G_OBJECT_CLASS (gst_video_writer_parent_class)->finalize (object);
}

static gboolean
gst_video_writer_video_event(GstPad *UNUSED(pad), GstObject *parent, GstEvent *event)
{
  GstVideoWriter *video_writer = GST_VIDEO_WRITER(parent);
  
  GST_OBJECT_LOCK(video_writer);
  
  switch (GST_EVENT_TYPE(event))
  {
    case GST_EVENT_CUSTOM_DOWNSTREAM:
    {
      GstClockTime timestamp;
      GstClockTime duration;
      GstClockTime running_time;
      GstClockTime stream_time;
      gboolean     all_headers;
      guint        count;
      
      if(gst_video_event_parse_downstream_force_key_unit(event, &timestamp,
          &stream_time, &running_time, &all_headers, &count)) {
        // Don't really care...
        (void) timestamp;
        (void) duration;
        (void) running_time;
        (void) stream_time;
        (void) all_headers;
        
        GST_WARNING_OBJECT(video_writer, "downstream event: %u\n", count);
        gst_video_writer_set_property_record(video_writer, TRUE);
      }
      break;
    }
    
    // TODO: Handle EOS?
    
    default:
      break;
  }
  
  GST_OBJECT_UNLOCK(video_writer);
  
  return TRUE;
}

// scan the buffer and discard the first found start code (23 zero bits, followed by a 1.. byte aligned)
// data: start of the buffer
// end:  last byte of valid data
// return: - NULL when no start code is found, 
//         - pointer to first byte of data after the start code
//         - note: this may point to one past the end of the buffer if the start coded was at the end of the buffer
guint8 * ParseStartCode(guint8 * data, guint8 * end) {
  while(data+2 <= end) {
    if(data[0] == 0 && data[1] == 0 && data[2] == 1)
    {
      return data+3;
    }
    data++;
  }
  return NULL; //no start code found
}

// Check if the data here is a valid NAL unit
// TODO: for our purposes, we assume the data in the RBSP is valid
// and we just check the forbidden_zero_bit
gboolean IsValidNalUnit(guint8 * data, guint bufsize) {
  if(bufsize > 0 && (data[0] & 0x80) == 0) return TRUE;
  return FALSE;
}

//Check if we have a PPS or SPS in this buffer
gboolean IsPPSorSPS(guint8 * data, gint bufsize) {
  guint8 * end = data + bufsize-1;
  while((data = ParseStartCode(data, end))) {
    bufsize = end - data + 1;
    if(IsValidNalUnit(data, bufsize)) {
      gint8 nal_unit_type = data[0] & 0x1f; 
      if(nal_unit_type == 7 || nal_unit_type == 8) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

static GstFlowReturn
gst_video_writer_video_chain(GstPad *UNUSED(pad), GstObject *parent, GstBuffer *buf)
{
  GstVideoWriter       *video_writer = GST_VIDEO_WRITER(parent);
  GstVideoWriterState  *state        = &video_writer->state;
  gboolean              is_key_frame = GST_BUFFER_FLAG_IS_SET(buf, GST_BUFFER_FLAG_DELTA_UNIT) == 0;
  GstClockTime          pts          = GST_BUFFER_PTS(buf);
  GstMapInfo            map;
  
  GST_OBJECT_LOCK(video_writer);
  
  // Skip this frame if we're not currently in recording mode
  if (!state->is_recording)
  {
    goto gst_video_writer_video_chain_exit;
  }
  
  // Workaround the recording issue of very first frame 1:
  // Drop the PTS = 0 case, as Gstreamer will pre-roll one frame
  // during start up and we usually will not start recording at that time
  // This pre-rolled frame got propagated into video file after we start recording 
  // video muxer will use the very PTS as offet to move all the timestamp to around zero
  // This bad first frame will mess up all the ts of following frame 
  if(pts == 0 && is_key_frame)
    goto gst_video_writer_video_chain_exit;
  
  // Workaround the recording issue of very first frame 2:
  // Drop another GOP of images, after recording started, the 2nd frame from omx264enc
  // is always corrupted/undecodable (Probably due to a long gap between pipeline starts and recording starts)?
  if(!video_writer->first_key_frame)
  {
    video_writer->first_key_frame = TRUE;
    goto gst_video_writer_video_chain_exit;
  }

  if (!GST_CLOCK_TIME_IS_VALID(pts))
  {
    gboolean bSendWithoutPTS = FALSE;
    GST_WARNING_OBJECT(video_writer, "invalid pts for frame %d, %c-frame\n",
        state->num_frames, (is_key_frame ? 'i' : 'p'));

    // The SPS/PPS required to decode the first frame comes before the first I frame.
    // Parse the NAL unit to see if it is an SPS or PPS
    if (state->num_frames == 0 && is_key_frame)
    {
      if (!gst_buffer_map(buf, &map, GST_MAP_READ))
      {
        GST_ERROR_OBJECT(video_writer, "unable to map buffer for frame %d\n",
                         state->num_frames);
        goto gst_video_writer_video_chain_exit;
      }

      if (IsPPSorSPS(map.data, map.size))
      {
        bSendWithoutPTS = true;
      }

      gst_buffer_unmap(buf, &map);
    }

    if(!bSendWithoutPTS) {    
      if (GST_CLOCK_TIME_IS_VALID(video_writer->last_pts))
      {
        // Re-use the last good PTS we encountered.
        // 
        // TODO: Advance this by some know interval?
        pts = video_writer->last_pts;
      }
      else
      {
        // We don't have a good base-time stamp to use, so discard the frame.
        GST_WARNING_OBJECT(video_writer, "discarding frame\n");
        goto gst_video_writer_video_chain_exit;
      }
    }
  } // if (!GST_CLOCK_TIME_IS_VALID(pts))

  // Start recording on I-Frame to avoid data corruption at the start
  // of a video file.
  if (state->num_frames == 0 && !is_key_frame)
  {
    goto gst_video_writer_video_chain_exit;
  }
  
  // Rollover if we are close to the max file-size, and this looks like a good
  // spot to stop the file (i.e. an I-Frame)
  if (is_key_frame && state->file_size >= SAFE_FILE_SIZE)
  {
    gvw_record_rollover(video_writer);
  }
  
  if (!gst_buffer_map(buf, &map, GST_MAP_READ))
  {
    GST_ERROR_OBJECT(video_writer, "unable to map buffer for frame %d\n",
      state->num_frames);
    goto gst_video_writer_video_chain_exit;
  }
  
  // Force a rollover if there simply isn't any way to fit the next packet into
  // the current file.
  if ((state->file_size + map.size) >= MAX_FILE_SIZE)
  {
    // Log this case because we really don't want it occuring. This would
    // indicate that we need to lower the SAFE_FILE_SIZE.
    GST_WARNING_OBJECT(video_writer, "Rolling over at %zu (+%zu)\n",
      state->file_size, map.size);
    gvw_record_rollover(video_writer);
  }

  if (state->is_recording)
  {
    int64_t pts_ms = -1;
    GstClockTime last_pts = video_writer->last_pts;

    if (GST_CLOCK_TIME_IS_VALID(pts))
    {
      if (GST_CLOCK_TIME_IS_VALID(last_pts))
      {
        // The PTS send to mux be non-decresing, otherwise many players cannot play it
        // Note: The FRAME_DURATION is hardcoded based on 24fps pipeline, it might be better to 
        // use the average frame duration here. However, decresing PTS issue usually happens 
        // on the very first 3 frames, so the estimation might also be off. 
        if (pts <= last_pts)
        {
          GST_WARNING_OBJECT(video_writer, "PTS is non-incremented");
          GST_WARNING_OBJECT(video_writer, "Faking PTS %lld based on last PTS %lld \n", 
            (long long)GST_TIME_AS_MSECONDS(pts), (long long)GST_TIME_AS_MSECONDS(last_pts));
          pts = last_pts + FRAME_DURATION;
        }
      }
      pts_ms = GST_TIME_AS_MSECONDS(pts);
      // save the PTS of last recorded frame
      video_writer->last_pts = pts;
    }

    // We do not supprot B frames. DTS and PTS should always be the same
    vw_mux_write_h264(state->mux_file, map.data, map.size,
      pts_ms, pts_ms, is_key_frame);
    state->file_size = vw_mux_size(state->mux_file);
    state->num_frames++;
  }
  
  gst_buffer_unmap(buf, &map);
gst_video_writer_video_chain_exit:
  gst_buffer_unref(buf);
  GST_OBJECT_UNLOCK(video_writer);
  return GST_FLOW_OK;
}

static GstFlowReturn
gst_video_writer_klv_chain(GstPad *UNUSED(pad), GstObject *parent, GstBuffer *buf)
{
  GstVideoWriter       *video_writer = GST_VIDEO_WRITER(parent);
  GstVideoWriterProps  *props        = &video_writer->props;
  GstVideoWriterState  *state        = &video_writer->state;
  GstClockTime          pts          = GST_BUFFER_PTS(buf);
  GstMapInfo            map;
  
  GST_OBJECT_LOCK(video_writer);
  
  if (!props->write_klv)
  {
    goto gst_video_writer_klv_chain_exit;
  }
  
  if (!GST_CLOCK_TIME_IS_VALID(pts))
  {
    // Discard KLV without a valid timestamp. Nobody has time to deal with this.
    GST_WARNING_OBJECT(video_writer, "invalid klv pts\n");
    goto gst_video_writer_klv_chain_exit;
  }
  
  if (!state->is_recording)
  {
    goto gst_video_writer_klv_chain_exit;
  }

  // waiting for the video being recorded first
  // mux will always use the PTS of first recieved package (either klv or video)
  // as the offset to move all the timestamp to zero based 
  if (state->num_frames == 0)
  {
    goto gst_video_writer_klv_chain_exit;
  }

  if (!gst_buffer_map(buf, &map, GST_MAP_READ))
  {
    GST_ERROR_OBJECT(video_writer, "unable to map buffer for klv frame %d\n",
      state->num_frames);
    goto gst_video_writer_klv_chain_exit;
  }
  
  // We don't really want to rollover when only writing metadata, but we will
  // do so if we can't fit the entire packet in the file without hitting the
  // file-system limits.
  if ((state->file_size + map.size) >= MAX_FILE_SIZE)
  {
    gvw_record_rollover(video_writer);
  }
  
  if (state->is_recording)
  {
    int64_t pts_ms = -1;
    int64_t dts_ms = -1;
    GstClockTime dts = GST_BUFFER_DTS(buf);
    if(GST_CLOCK_TIME_IS_VALID(pts)) {
        pts_ms = GST_TIME_AS_MSECONDS(pts);
    }
    if(GST_CLOCK_TIME_IS_VALID(dts)) {
        dts_ms = GST_TIME_AS_MSECONDS(dts);
    }

    vw_mux_write_klva(state->mux_file, map.data, map.size, pts_ms, dts_ms);
    state->file_size = vw_mux_size(state->mux_file);
    state->num_klv++;
  }
  
  gst_buffer_unmap(buf, &map);
gst_video_writer_klv_chain_exit:
  gst_buffer_unref(buf);
  GST_OBJECT_UNLOCK(video_writer);
  return GST_FLOW_OK;
}

static void
gvw_record_start(GstVideoWriter *video_writer, char *filename)
{
  GstVideoWriterProps  *props = &video_writer->props;
  GstVideoWriterState  *state = &video_writer->state;
  int                   status;
  
  if (!state->is_recording)
  {
    state->file_size   = 0;
    state->num_frames  = 0;
    state->num_klv     = 0;
    
    g_strlcpy(state->filename, filename, sizeof(state->filename));
    GST_WARNING_OBJECT(video_writer, "starting recording: %s\n", state->filename);
    
    status = vw_mux_open(&state->mux_file, state->filename, props->buffer_size);
    if (status == 0)
    {
      state->is_recording = TRUE;
    }
    else
    {
      GST_ERROR_OBJECT(video_writer, "unable to enable recording\n");
    }
  }
  else
  {
    GST_WARNING_OBJECT(video_writer, "ignoring record request, already recording\n");
  }
}

static void
gvw_record_stop(GstVideoWriter *video_writer)
{
  GstVideoWriterState *state = &video_writer->state;

  if (state->is_recording)
  {
    vw_mux_close(state->mux_file);

    state->mux_file = NULL;
    state->is_recording = FALSE;
    video_writer->first_key_frame = FALSE;
    GST_WARNING_OBJECT(video_writer, "stopped recording %s, %zu bytes, %d frames, %d klv\n",
                       state->filename, state->file_size, state->num_frames, state->num_klv);
  }
}

static void
gvw_record_rollover(GstVideoWriter *video_writer)
{
  GstVideoWriterProps *props = &video_writer->props;
  GstVideoWriterState *state = &video_writer->state;
  char new_filename[sizeof(state->filename)];
  
  gvw_record_stop(video_writer);
  state->file_index++;
  
  if (props->location)
  {
    video_writer_rotate_filename(props->location, new_filename,
      sizeof(new_filename), state->file_index);
    gvw_record_start(video_writer, new_filename);
  }
}

