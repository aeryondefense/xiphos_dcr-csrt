#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "gstaeryonparser.h"
#include "types.h"

GST_DEBUG_CATEGORY (gst_aeryon_parser_debug);
#define GST_CAT_DEFAULT gst_aeryon_parser_debug
#define GST_TYPE_SECURITY_CLASS (gst_security_class_get_type ())
static GType
gst_security_class_get_type (void)
{
  static GType security_class_type = 0;

  if (!security_class_type) {
    static GEnumValue security_classes[] = {
        { KLV_SECURITY_UNCLASSIFIED, "KLV_SECURITY_UNCLASSIFIED", "unclassified" },
        { KLV_SECURITY_RESTRICTED  , "KLV_SECURITY_RESTRICTED"  , "restricted"  },
        { KLV_SECURITY_CONFIDENTIAL, "KLV_SECURITY_CONFIDENTIAL", "confidential" },
        { KLV_SECURITY_SECRET      , "KLV_SECURITY_SECRET"      , "secret" },
        { KLV_SECURITY_TOP_SECRET  , "KLV_SECURITY_TOP_SECRET"  , "top-secret" },
        { 0, NULL, NULL },
    };

    security_class_type =
    g_enum_register_static ("GstKlvSecurityClass",
                security_classes);
  }

  return security_class_type;
}
#define DEFAULT_KLV_DECIMATION_RATE 5
enum
{
    PROP_0,
    PROP_KLV_DECIMATION_RATE,
    PROP_KLV_ENABLED,
    
    // Metadata2 properties
    PROP_KLV_SECURITY_CLASS,
    PROP_KLV_MISSION_ID,
    PROP_KLV_PLATFORM_TAIL_NUMBER,
    PROP_KLV_PLATFORM_DESIGNATION,
    PROP_KLV_SOURCE_SENSOR,
};

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE("sink", 
        GST_PAD_SINK,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS("ANY"));

static GstStaticPadTemplate srcany_factory = GST_STATIC_PAD_TEMPLATE("any",
        GST_PAD_SRC,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS("ANY"));

static GstStaticPadTemplate srcklv_factory = GST_STATIC_PAD_TEMPLATE("klv_%04d",
        GST_PAD_SRC,
        GST_PAD_SOMETIMES,
        GST_STATIC_CAPS("meta/x-klv, parsed=true"));

#define gst_aeryon_parser_parent_class  parent_class
G_DEFINE_TYPE(GstAeryonParser, gst_aeryon_parser, GST_TYPE_ELEMENT);

static void gst_aeryon_parser_finalize(GObject *object);
static GstFlowReturn gst_aeryon_parser_chain(GstPad *pad, GstObject *parent, GstBuffer *buf);
static void gst_aeryon_parser_set_property(GObject *obj, guint prop_id, const GValue *value, GParamSpec *pspec);
static void gst_aeryon_parser_get_property(GObject *obj, guint prop_id, GValue *value, GParamSpec *pspec);
static gboolean gst_aeryon_parser_sink_event(GstPad *pad, GstObject *parent, GstEvent *event);
static gpointer gst_aeryon_parser_klvsrc_loop(gpointer parser);
static void aeryon_parser_stop_klv_producer(GstAeryonParser *parser);

static GstStateChangeReturn gst_aeryon_parser_change_state(GstElement *element, GstStateChange transition)
{
    GstStateChangeReturn ret;
    GstAeryonParser *parser  = GST_AERYON_PARSER(element);

    GST_WARNING_OBJECT(parser, "change state to %d", transition);

    switch (transition)
    {
        case GST_STATE_CHANGE_NULL_TO_READY:         
            break;
        case GST_STATE_CHANGE_READY_TO_PAUSED:
            break;
        case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
            GST_WARNING_OBJECT(parser, "change state to PLAYING ");
        break;
        default:
            break;
    }

    ret = GST_ELEMENT_CLASS(parent_class)->change_state(element, transition);


    switch (transition) {
        case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
            GST_WARNING_OBJECT(parser, "change state to PAUSED ");

            g_mutex_lock(&parser->klv_cond_mutex);
            parser->klv_cond_ready = false;
            g_cond_signal(&parser->klv_cond);
            g_mutex_unlock(&parser->klv_cond_mutex);

            g_mutex_lock(&parser->queue_mutex);
            g_queue_clear(parser->queue);
            g_mutex_unlock(&parser->queue_mutex);
            break;
        case GST_STATE_CHANGE_PAUSED_TO_READY:
            break;
        case GST_STATE_CHANGE_READY_TO_NULL:
            aeryon_parser_stop_klv_producer(parser);
            break;
        default:
            break;
    }

    return ret;
}

static void gst_aeryon_parser_class_init(GstAeryonParserClass *klass)
{
    GObjectClass *gobject_class;
    GstElementClass *element_class;
        
    gobject_class = G_OBJECT_CLASS(klass);
    element_class = GST_ELEMENT_CLASS(klass);

    gobject_class->finalize = gst_aeryon_parser_finalize;
    gobject_class->set_property = gst_aeryon_parser_set_property;
    gobject_class->get_property = gst_aeryon_parser_get_property;

    g_object_class_install_property(gobject_class, PROP_KLV_DECIMATION_RATE,
            g_param_spec_uint("klv-decimator", "klv-decimator", 
                "frame->klv decimation rate (0 = default)",
                0,      //min
                30,     //max
                DEFAULT_KLV_DECIMATION_RATE,
                G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property(gobject_class, PROP_KLV_ENABLED,
            g_param_spec_boolean("klv-enabled", "klv-enabled",
                "frame->klv enabled (false = default)", false,
                G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property(gobject_class, PROP_KLV_SECURITY_CLASS,
            g_param_spec_enum ("klv-security-class", "klv-security-class",
               "Security class to mark klv data",
               GST_TYPE_SECURITY_CLASS, KLV_SECURITY_UNCLASSIFIED,
               G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property(gobject_class, PROP_KLV_MISSION_ID,
            g_param_spec_string ("klv-mission-id", "klv-mission-id",
                "Flight data and flight number", NULL,
                G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property(gobject_class, PROP_KLV_PLATFORM_TAIL_NUMBER,
            g_param_spec_string ("klv-platform-tail-number", "klv-platform-tail-number",
                "Flyer identifier (serial number/hostname)", NULL,
                G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property(gobject_class, PROP_KLV_PLATFORM_DESIGNATION,
            g_param_spec_string ("klv-platform-designation", "klv-platform-designation",
                "Flyer type", NULL,
                G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property(gobject_class, PROP_KLV_SOURCE_SENSOR,
            g_param_spec_string ("klv-source-sensor", "klv-source-sensor",
                "Camera sensor model", NULL,
                G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    gst_element_class_set_static_metadata(element_class,
            "Aeryon metadata parser",
            "Aeryon metadata parser",
            "parse aeryon buffer metadata embedded in gstbuffer to produce KLV and tracking data",
            "nhandojo@aeryon.com");

    gst_element_class_add_pad_template(element_class,
            gst_static_pad_template_get(&sink_factory));
    gst_element_class_add_pad_template(element_class,
            gst_static_pad_template_get(&srcany_factory));
    gst_element_class_add_pad_template(element_class,
            gst_static_pad_template_get(&srcklv_factory));
    
    element_class->change_state = gst_aeryon_parser_change_state;
}

static void gst_aeryon_parser_finalize(GObject *object)
{
    GST_WARNING("gst_aeryon_parser_finalize!");
    GstAeryonParser *parser = GST_AERYON_PARSER(object);
    g_free(parser->klv_mission_id);
    g_free(parser->klv_platform_tail_number);
    g_free(parser->klv_platform_designation);
    g_free(parser->klv_source_sensor);
    g_mutex_clear(&parser->queue_mutex);
    g_mutex_clear(&parser->klv_cond_mutex);
    G_OBJECT_CLASS(parent_class)->finalize(object);
}

static void gst_aeryon_parser_init(GstAeryonParser *parser)
{
    parser->klv_decimator = DEFAULT_KLV_DECIMATION_RATE;
    parser->sinkpad = gst_pad_new_from_static_template(&sink_factory, "sink");
    gst_pad_set_chain_function(parser->sinkpad, gst_aeryon_parser_chain);
    gst_element_add_pad(GST_ELEMENT(parser), parser->sinkpad);
    gst_pad_set_event_function(parser->sinkpad, gst_aeryon_parser_sink_event);

    parser->srcpad_any = gst_pad_new_from_static_template(&srcany_factory, "any");
    GST_PAD_SET_PROXY_CAPS(parser->srcpad_any);
    gst_element_add_pad(GST_ELEMENT(parser), parser->srcpad_any);

    parser->srcpad_klv = NULL;
    parser->queue = g_queue_new();
    g_mutex_init(&parser->queue_mutex);
    g_mutex_init(&parser->klv_cond_mutex);

    parser->klv_running = false;
    parser->klv_enabled = false;

    parser->klv_timestamp = 0;
    parser->klv_cond_ready = false;
    parser->klv_decimator_count = 0;
    
    parser->klv_thread = NULL;
    parser->klv_security_class = KLV_SECURITY_UNCLASSIFIED;
    parser->klv_mission_id = NULL;
    parser->klv_platform_tail_number = NULL;
    parser->klv_platform_designation = NULL;
    parser->klv_source_sensor = NULL;

    memset(&parser->klv_gps_ctx, 0, sizeof(klv_gps_ctx_t));
    
    GST_DEBUG_CATEGORY_INIT(gst_aeryon_parser_debug, "aeryonparser", 0, "Aeryon buffer metadata parser element");
}

static void gst_aeryon_parser_pad_linked(GstPad *pad)
{
    GstAeryonParser *parser = GST_AERYON_PARSER(gst_pad_get_parent(pad));

    guint group_id = 0;
    GstEvent *event = gst_pad_get_sticky_event(parser->sinkpad, GST_EVENT_STREAM_START, 0);
    if (event)
        if (!gst_event_parse_group_id(event, &group_id))
            group_id = gst_util_group_id_next();

    gchar *stream_id = 
        gst_pad_create_stream_id_printf (parser->srcpad_klv, GST_ELEMENT_CAST (parser), "%08x", 2);

    event = gst_event_new_stream_start(stream_id);
    gst_event_set_group_id(event, group_id);
    // setting sparse flag causes a deadlock bug in gst_collect_pads used in mpegtsmux. STREAM_LOCK
    // is held by the collect_pads whilst it continously tries to wait for data on the KLV pad, but to
    // push data, one must hold said STREAM_LOCK. Hence deadlock.
    // UPDATE: Our build of gstreamer was patched to fix this issue, see 0002-aeryon-klv-collectpads.patch
    gst_event_set_stream_flags(event, GST_STREAM_FLAG_SPARSE);
    gst_pad_push_event(parser->srcpad_klv, event);
    g_free(stream_id);

    GstCaps *caps = gst_caps_new_simple("meta/x-klv", "parsed", G_TYPE_BOOLEAN, TRUE, NULL);
    gst_pad_set_caps(parser->srcpad_klv, caps);
    gst_caps_unref(caps);

    GstSegment segment;
    gst_segment_init(&segment, GST_FORMAT_TIME);
    segment.start = 0; //abs_time - GST_ELEMENT(parser)->base_time;
    segment.stop = GST_CLOCK_TIME_NONE;
    segment.position = segment.start;
    segment.rate = 1.0;
    segment.base = 0; //abs_time - GST_ELEMENT(parser)->base_time;
        

    GST_WARNING("pushing new segment for KLV");

    event = gst_event_new_segment(&segment);
    gst_pad_push_event(parser->srcpad_klv, event);
    
    parser->klv_running = true;
    GError *err = NULL;
    parser->klv_thread = g_thread_try_new("klv_thread", gst_aeryon_parser_klvsrc_loop, parser, &err);
    if (err) {
        GST_ERROR("Failed to create klv_thread: %s ", err->message);
        parser->klv_running = false;
        g_error_free(err);
    }

    GST_WARNING("klv pad linked!");
}

static void aeryon_parser_stop_klv_producer(GstAeryonParser *parser)
{
    if(!parser)
        return;
    if (parser->klv_running) {
        parser->klv_running = false;
        g_thread_join(parser->klv_thread);
    }
}

static void gst_aeryon_parser_pad_unlinked(GstPad *pad)
{
    GstAeryonParser *parser = GST_AERYON_PARSER(gst_pad_get_parent(pad));
    aeryon_parser_stop_klv_producer(parser);
    GST_WARNING("klv pad unlinked!");
}

static void add_klv_stream(GstAeryonParser *parser)
{
    GST_WARNING_OBJECT(parser, "adding KLV pad!");
    parser->srcpad_klv = gst_pad_new_from_static_template(&srcklv_factory, "klv");
    parser->klv_linked_sig_id = g_signal_connect_after(parser->srcpad_klv, "linked", G_CALLBACK(gst_aeryon_parser_pad_linked), NULL);
    parser->klv_unlinked_sig_id = g_signal_connect_after(parser->srcpad_klv, "unlinked", G_CALLBACK(gst_aeryon_parser_pad_unlinked), NULL);

    gst_pad_set_active(parser->srcpad_klv, TRUE);
    gst_pad_use_fixed_caps(parser->srcpad_klv);


    gst_element_add_pad(GST_ELEMENT(parser), parser->srcpad_klv);
    gst_element_no_more_pads(GST_ELEMENT(parser));
}

static GstFlowReturn produce_klv(GstAeryonParser *parser, GstAeryonMetadata *meta)
{
    klv_uas_metadata_t klv;
    memset(&klv, 0, sizeof(klv));

    if (!gst_pad_is_linked(parser->srcpad_klv))
    {
        GST_ERROR_OBJECT(parser, "klv pad not linked!");
        return GST_FLOW_OK;
    }

    GST_OBJECT_LOCK(parser);
    
    klv.mission_id                     = parser->klv_mission_id;
    klv.platform_tail_number           = parser->klv_platform_tail_number;
    klv.platform_designation           = parser->klv_platform_designation;
    klv.image_source_sensor            = parser->klv_source_sensor;
    klv.image_coordinate_system        = "WGS-84";
    klv.platform_call_sign             = parser->klv_platform_tail_number;
    klv.security.classification        = parser->klv_security_class;
    klv.security.classifying_country   = NULL;
    klv.security.object_country_codes  = NULL;
    klv.security.comments              = NULL;
    
    klv_calculate(&(parser->klv_gps_ctx), &klv, &(meta->metadata->frame), &(meta->metadata->position));
    gpointer data = g_malloc0(512);
    int size = klv_encode(&klv, data, 512);
    
    GST_OBJECT_UNLOCK(parser);
    
    GstBuffer *gstbuf = gst_buffer_new_wrapped_full(0, data, 512, 0, size, data, g_free);
    GST_BUFFER_DTS(gstbuf) = meta->ts;
    GST_BUFFER_PTS(gstbuf) = meta->ts;
    GST_BUFFER_DURATION(gstbuf) = gst_util_uint64_scale_int(GST_SECOND, 1, 30);

    GST_BUFFER_OFFSET(gstbuf) = GST_BUFFER_OFFSET_NONE;
    GST_BUFFER_OFFSET_END(gstbuf) = GST_BUFFER_OFFSET_NONE;
    return gst_pad_push(parser->srcpad_klv, gstbuf);
}

static gpointer gst_aeryon_parser_klvsrc_loop(gpointer data) {
    GstAeryonParser *parser = GST_AERYON_PARSER(data);
    GstAeryonMetadata *gstmetadata = NULL;

    while (parser && parser->klv_running) {
        //Wait for condition variable to be signalled before producing klv data.
        //Also use a timeout in case we get the kill signal
        g_mutex_lock(&parser->klv_cond_mutex);
        while (!parser->klv_cond_ready) {
            gint64 end_time = g_get_monotonic_time () + 2 * G_TIME_SPAN_SECOND;
            if (!g_cond_wait_until(&parser->klv_cond, &parser->klv_cond_mutex, end_time)) {
                if (!parser->klv_running) {
                    g_mutex_unlock(&parser->klv_cond_mutex);
                    goto exit;
                }
            }
        }
        parser->klv_cond_ready = false;
        g_mutex_unlock(&parser->klv_cond_mutex);

        g_mutex_lock(&parser->queue_mutex);
        gstmetadata = g_queue_pop_head(parser->queue);
        g_mutex_unlock(&parser->queue_mutex);

        if (gstmetadata && (parser->klv_decimator_count % parser->klv_decimator) == 0 && parser->klv_enabled) {
            produce_klv(parser, gstmetadata);
        }/* else {
            gst_pad_push_event(parser->srcpad_klv, 
                gst_event_new_gap(parser->klv_timestamp,  
                gst_util_uint64_scale_int(GST_SECOND, 1, 30)));
        }*/
        if (gstmetadata)
            gst_aeryon_metadata_unref(gstmetadata);

        ++parser->klv_decimator_count;
    }
exit:


    GST_WARNING("exit klv thread");
    GstPad *peer_pad = gst_pad_get_peer(parser->srcpad_klv);
    if (!gst_pad_unlink(parser->srcpad_klv, peer_pad)) {
        GST_ERROR_OBJECT(parser, "unlinking klv pad failed \n");
    }
    return NULL;
}

static gboolean gst_aeryon_parser_sink_event(GstPad *pad, GstObject *parent, GstEvent *event)
{
    gboolean ret = FALSE;
    GstAeryonParser *parser = GST_AERYON_PARSER(parent);
    
    switch (GST_EVENT_TYPE(event))
    {
        case GST_EVENT_CAPS:
        {
            GstCaps *caps = NULL;
            GstStructure *structure = NULL;
            gst_event_parse_caps(event, &caps);
            structure = gst_caps_get_structure(caps, 0);

            gst_structure_get_fraction(structure, "framerate", &parser->fps_n, &parser->fps_d);
            ret = gst_pad_push_event(parser->srcpad_any, event);
            break;
        }
        case GST_EVENT_CUSTOM_DOWNSTREAM:
        {
            GstEvent* e = gst_event_copy(event);
            if (parser && parser->srcpad_klv) gst_pad_push_event(parser->srcpad_klv, event);
            ret = gst_pad_event_default(pad, parent, e);
            break;
        }
        default:
            ret = gst_pad_event_default(pad, parent, event);
    }

    return ret;
}

static GstFlowReturn gst_aeryon_parser_chain(GstPad *UNUSED(pad), GstObject *parent, GstBuffer *buf)
{
    GstAeryonParser *parser = GST_AERYON_PARSER(parent);
    GstAeryonBufferMeta *buffer_meta = GST_AERYON_BUFFER_META_GET(buf);

    if (!buffer_meta || !buffer_meta->gstmetadata)
    {
        // Not bad enough as a Warning message 
        GST_INFO("no metadata!");

        // don't allow pipeline to complete preroll as the mpegtsmux element
        // only create new streams upon transitioning from PAUSED->PLAYING
        // and for some reason, linking the KLV pad after pipeline has started
        // playing does not trigger this, even with reconfigure event being sent
        // back to the video source.
        if(GST_IS_BUFFER(buf)) {
            gst_buffer_unref(buf);
        }
        return GST_FLOW_OK;
    }
    else if (parser->srcpad_klv == NULL && buffer_meta->gstmetadata)
    {
        add_klv_stream(parser);
        if(GST_IS_BUFFER(buf)) {
            gst_buffer_unref(buf);
        }
        return GST_FLOW_OK;
    }

    if (buffer_meta->gstmetadata)
    {
        gst_aeryon_metadata_ref(buffer_meta->gstmetadata);
        buffer_meta->gstmetadata->ts = GST_BUFFER_PTS(buf);
        g_mutex_lock(&parser->queue_mutex);
        g_queue_push_tail(parser->queue, buffer_meta->gstmetadata);
        g_mutex_unlock(&parser->queue_mutex);
    }

    // Signal worker thread to produce klv data
    if (parser->klv_running) {
        g_mutex_lock(&parser->klv_cond_mutex);
        parser->klv_cond_ready = true;
        parser->klv_timestamp = GST_BUFFER_PTS(buf);
        g_cond_signal(&parser->klv_cond);
        g_mutex_unlock(&parser->klv_cond_mutex);
    }

    return gst_pad_push(parser->srcpad_any, buf);
}

static void gst_aeryon_parser_set_property(GObject *obj, guint prop_id, const GValue *value, GParamSpec *pspec)
{
    GstAeryonParser *parser = GST_AERYON_PARSER(obj);
    GST_OBJECT_LOCK(parser);
    switch (prop_id)
    {
        case PROP_KLV_DECIMATION_RATE:
            parser->klv_decimator = g_value_get_uint(value);
            break;
        case PROP_KLV_ENABLED:
            parser->klv_enabled = g_value_get_boolean(value);
            break;
        case PROP_KLV_SECURITY_CLASS:
            parser->klv_security_class = g_value_get_enum(value);
            break;
        case PROP_KLV_MISSION_ID:
            g_free(parser->klv_mission_id);
            parser->klv_mission_id = g_strdup(g_value_get_string(value));
            break;
        case PROP_KLV_PLATFORM_TAIL_NUMBER:
            g_free(parser->klv_platform_tail_number);
            parser->klv_platform_tail_number = g_strdup(g_value_get_string(value));
            break;
        case PROP_KLV_PLATFORM_DESIGNATION:
            g_free(parser->klv_platform_designation);
            parser->klv_platform_designation = g_strdup(g_value_get_string(value));
            break;
        case PROP_KLV_SOURCE_SENSOR:
            g_free(parser->klv_source_sensor);
            parser->klv_source_sensor = g_strdup(g_value_get_string(value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, prop_id, pspec);
            break;
    }
    GST_OBJECT_UNLOCK(parser);
}

static void gst_aeryon_parser_get_property(GObject *obj, guint prop_id, GValue *value, GParamSpec *pspec)
{
    GstAeryonParser *parser = GST_AERYON_PARSER(obj);
    GST_OBJECT_LOCK(parser);
    switch (prop_id)
    {
        case PROP_KLV_DECIMATION_RATE:
            g_value_set_uint(value, parser->klv_decimator);
            break;
        case PROP_KLV_ENABLED:
            g_value_set_boolean(value, parser->klv_enabled);
            break;
        case PROP_KLV_SECURITY_CLASS:
            g_value_set_enum(value, parser->klv_security_class);
            break;
        case PROP_KLV_MISSION_ID:
            g_value_set_string(value, parser->klv_mission_id);
            break;
        case PROP_KLV_PLATFORM_TAIL_NUMBER:
            g_value_set_string(value, parser->klv_platform_tail_number);
            break;
        case PROP_KLV_PLATFORM_DESIGNATION:
            g_value_set_string(value, parser->klv_platform_designation);
            break;
        case PROP_KLV_SOURCE_SENSOR:
            g_value_set_string(value, parser->klv_source_sensor);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, prop_id, pspec);
            break;
    }
    GST_OBJECT_UNLOCK(parser);
}

