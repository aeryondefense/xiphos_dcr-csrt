#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>
#include <gst/gstelement.h>
#include "gstaeryonmeta.h"

GST_DEBUG_CATEGORY (gst_aeryon_meta_debug);
#define GST_CAT_DEFAULT gst_aeryon_meta_debug

#define DEFAULT_NUM_EVENTS 1
enum 
{
    PROP_0,
    PROP_NUM_EVENTS
};

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE("sink", 
        GST_PAD_SINK,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS("ANY"));

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE("src",
        GST_PAD_SRC,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS("ANY"));

#define gst_aeryon_meta_parent_class    parent_class
G_DEFINE_TYPE(GstAeryonMeta, gst_aeryon_meta, GST_TYPE_ELEMENT);

static void gst_aeryon_meta_finalize(GObject *object);
static gboolean gst_aeryon_meta_send_event(GstElement *element, GstEvent *event);
static GstStateChangeReturn gst_aeryon_meta_change_state(GstElement *element, GstStateChange transition);
static GstFlowReturn gst_aeryon_meta_chain(GstPad *pad, GstObject *parent, GstBuffer*buf);

static void gst_aeryon_meta_set_property(GObject *obj, guint prop_id, const GValue *value, GParamSpec *pspec);
static void gst_aeryon_meta_get_property(GObject *obj, guint prop_id, GValue *value, GParamSpec  *pspec);
static void ringbuffer_add_data(GstAeryonMeta *aeryon_meta, GstAeryonMetadata *metadata);

static int metadata_timestamp_compare(void *item, void *key)
{
    GstAeryonMetadata *gst_item = *((GstAeryonMetadata **)item);
    GstClockTime *ts = (GstClockTime *)key;
    return (*ts > gst_item->ts) ? GST_CLOCK_DIFF(gst_item->ts, *ts) : -((int)GST_CLOCK_DIFF(*ts, gst_item->ts));
}

static void gst_aeryon_meta_class_init(GstAeryonMetaClass *klass)
{
    GObjectClass *gobject_class;
    GstElementClass *element_class;

    gobject_class = G_OBJECT_CLASS(klass);
    element_class = GST_ELEMENT_CLASS(klass);

    gobject_class->finalize = gst_aeryon_meta_finalize;
    gobject_class->set_property = gst_aeryon_meta_set_property;
    gobject_class->get_property = gst_aeryon_meta_get_property;

    g_object_class_install_property(gobject_class, PROP_NUM_EVENTS,
            g_param_spec_uint("num-events", "num-events",
                "Number of cached Aeryon metadata events (0 = default)",
                0,                  //min
                600,                //max
                DEFAULT_NUM_EVENTS, //default
                G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    gst_element_class_set_static_metadata(element_class,
            "Aeryon metadata plugin",
            "Aeryon metadata plugin",
            "receives aeryon event and decorates gstbuffer with aeryon metadata",
            "nhandojo@aeryon.com");

    gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&src_factory));
    gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&sink_factory));

    element_class->send_event = gst_aeryon_meta_send_event;
    element_class->change_state = gst_aeryon_meta_change_state;
}

static void gst_aeryon_meta_init(GstAeryonMeta *aeryon_meta)
{
    aeryon_meta->num_events = DEFAULT_NUM_EVENTS;
    
    // free in finalize()
    aeryon_meta->ringbuffer_backing = g_malloc0(aeryon_meta->num_events * sizeof(gpointer));

    aeryon_meta->sinkpad = gst_pad_new_from_static_template(&sink_factory, "sink");
    
    gst_pad_set_chain_function(
            aeryon_meta->sinkpad, 
            GST_DEBUG_FUNCPTR(gst_aeryon_meta_chain));
    GST_PAD_SET_PROXY_CAPS(aeryon_meta->sinkpad);
    gst_element_add_pad(GST_ELEMENT(aeryon_meta), aeryon_meta->sinkpad);

    aeryon_meta->srcpad = gst_pad_new_from_static_template(&src_factory, "src");
    GST_PAD_SET_PROXY_CAPS(aeryon_meta->srcpad);
    gst_element_add_pad(GST_ELEMENT(aeryon_meta), aeryon_meta->srcpad);

    g_rw_lock_init(&aeryon_meta->ringbuffer_rwlock);

    int ret = searchable_rbuf_init(
            &aeryon_meta->ringbuffer,
            sizeof(gpointer),
            aeryon_meta->num_events,
            metadata_timestamp_compare,
            aeryon_meta->ringbuffer_backing);

    assert (ret == SRBUF_OK);

    memset(aeryon_meta->ringbuffer_backing, 0, aeryon_meta->num_events * sizeof(gpointer));
    aeryon_meta->pipeline_to_meta_offset = 0LL;
    GST_DEBUG_CATEGORY_INIT(gst_aeryon_meta_debug, "aeryonmeta", 0, "Aeryon metadata/event handler element");
}

static void gst_aeryon_meta_finalize(GObject *object)
{
    GstAeryonMeta *aeryon_meta = GST_AERYON_META(object);    

    g_free(aeryon_meta->ringbuffer_backing);
    
    G_OBJECT_CLASS(parent_class)->finalize(object);
}

static void ringbuffer_add_data(GstAeryonMeta *aeryon_meta, GstAeryonMetadata *metadata)
{
    int rc;
    GstAeryonMetadata **bufitem = NULL;

    metadata->ts = metadata->metadata->arm_time;
    
    g_rw_lock_reader_lock(&aeryon_meta->ringbuffer_rwlock);
    rc = searchable_rbuf_get_tail(&aeryon_meta->ringbuffer, (void **)&bufitem);
    g_rw_lock_reader_unlock(&aeryon_meta->ringbuffer_rwlock);
    if (rc == SRBUF_EMPTY)
    {
        g_rw_lock_writer_lock(&aeryon_meta->ringbuffer_rwlock);
        rc = searchable_rbuf_push(&aeryon_meta->ringbuffer, &metadata);
        if (rc != SRBUF_OK)
            GST_WARNING_OBJECT(aeryon_meta, "failed to log event into ringbuffer, rc: %d", rc);
        goto unlock_writer;
    }

    // check if we received an event out of order
    if (metadata->ts <= (*bufitem)->ts) 
    {
        GST_DEBUG_OBJECT(aeryon_meta, "received an out-of-order event, not logging");
        goto exit;
    }

    g_rw_lock_writer_lock(&aeryon_meta->ringbuffer_rwlock);
    rc = searchable_rbuf_push(&aeryon_meta->ringbuffer, &metadata);
    if (rc == SRBUF_FULL)
    {
        searchable_rbuf_get_head(&aeryon_meta->ringbuffer, (void **)&bufitem);
        gst_aeryon_metadata_unref(*bufitem);
        searchable_rbuf_pop(&aeryon_meta->ringbuffer);
        searchable_rbuf_push(&aeryon_meta->ringbuffer, &metadata);
    }
    
unlock_writer:
    g_rw_lock_writer_unlock(&aeryon_meta->ringbuffer_rwlock);
exit:
    return;
}

static gboolean gst_aeryon_meta_send_event(GstElement *element, GstEvent *event)
{
    // intercept metadata event and place it in the ring buffer to decorate
    // buffer in the chain function
    GstAeryonMeta *aeryon_meta = GST_AERYON_META(element);
    gboolean ret = TRUE;
    GstAeryonMetadata *metadata = NULL;

    switch ((GstAeryonEventType)GST_EVENT_TYPE(event))
    {
        case GST_EVENT_AERYON_META:
            ret = gst_event_get_aeryon_metadata(event, &metadata);
            if (ret)
            {
                ringbuffer_add_data(aeryon_meta, metadata);
                gst_aeryon_metadata_unref(metadata);
            }

            break;

        default:
            return GST_ELEMENT_CLASS(parent_class)->send_event(element, event);
    }

    return ret;
}

static GstStateChangeReturn gst_aeryon_meta_change_state(GstElement *element, GstStateChange transition)
{
    GstAeryonMeta *aeryon_meta = GST_AERYON_META(element);

    switch (transition)
    {
        case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
        {
            GstAeryonMetadata **metadata = NULL;
            aeryon_meta->pipeline_to_meta_offset = 0LL;
            aeryon_meta->base_time = element->base_time;
            searchable_rbuf_get_tail(&aeryon_meta->ringbuffer, (void **)&metadata);
            if (metadata)
                aeryon_meta->pipeline_to_meta_offset = 
                    (GstClockTimeDiff)(element->base_time - (*metadata)->ts);
            break;
        }

        default:
            break;
    }

    return GST_ELEMENT_CLASS(parent_class)->change_state(element, transition);
}

static GstFlowReturn gst_aeryon_meta_chain(GstPad *pad, GstObject *parent, GstBuffer *buf)
{
    (void)pad;

    // Need to make writable for h264 file source
    if (!gst_buffer_is_writable(buf))
    {
        buf = gst_buffer_make_writable(buf);
    }

    GstAeryonMetadata **metadata = NULL;
    GstAeryonMeta *aeryon_meta = GST_AERYON_META(parent);
    GstAeryonBufferMeta *buffer_meta = GST_AERYON_BUFFER_META_GET(buf);
    if (!buffer_meta)
        buffer_meta = GST_AERYON_BUFFER_META_ADD(buf);

    if (buffer_meta->gstmetadata)
    {
        gst_aeryon_metadata_unref(buffer_meta->gstmetadata);
        buffer_meta->gstmetadata = NULL;
    }

    GstClockTime pts = GST_BUFFER_PTS(buf);
    GstClockTime key = pts;
    g_rw_lock_reader_lock(&aeryon_meta->ringbuffer_rwlock);
    int ret = searchable_rbuf_find_closest(
            &aeryon_meta->ringbuffer,
            &key,
            (void **)&metadata);
    
    if (ret == SRBUF_OK && metadata)
    {
#ifndef X86_64
        GstClockTimeDiff ts_diff_ms = (GstClockTimeDiff)(key - (*metadata)->ts) / 1000000LL;
        if (ts_diff_ms > 50)
        {
            GST_WARNING_OBJECT(aeryon_meta, "nearest metadata ts is %" G_GINT64_FORMAT " ms old!:", ts_diff_ms);
            GST_WARNING_OBJECT(aeryon_meta, "test: %"G_GINT64_FORMAT", %"G_GINT64_FORMAT"\n" ,key, (*metadata)->ts);
        }
#endif
        //gst_aeryon_metadata_ref(*metadata);
        buffer_meta->gstmetadata = gst_aeryon_metadata_dup(*metadata);
    }
    else
    {
        GST_DEBUG_OBJECT(aeryon_meta, "did not find metadata in ringbuffer, rc: %d", ret);
        // TODO: Generate empty one because downstream elements need this in the headless case
        // add an element for now
        Metadata2* meta = calloc(1, sizeof(Metadata2));
        buffer_meta->gstmetadata = gst_aeryon_metadata_new(meta);
        free (meta);
    }
    g_rw_lock_reader_unlock(&aeryon_meta->ringbuffer_rwlock);

    return gst_pad_push(aeryon_meta->srcpad, buf);
}

static void gst_aeryon_meta_set_property(GObject *obj, guint prop_id, const GValue *value, GParamSpec *pspec)
{
    GstAeryonMeta *aeryon_meta = GST_AERYON_META(obj);

    switch (prop_id)
    {
        case PROP_NUM_EVENTS:
            aeryon_meta->num_events = g_value_get_uint(value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, prop_id, pspec);
            break;
    }
}

static void gst_aeryon_meta_get_property(GObject *obj, guint prop_id, GValue *value, GParamSpec  *pspec)
{
    GstAeryonMeta *aeryon_meta = GST_AERYON_META(obj);

    switch (prop_id)
    {
        case PROP_NUM_EVENTS:
            g_value_set_uint(value, aeryon_meta->num_events);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, prop_id, pspec);
            break;
    }
}


