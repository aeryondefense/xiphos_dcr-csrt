#include "gstframeduplicator.h"

GST_DEBUG_CATEGORY (gst_frame_duplicator_debug);
#define GST_CAT_DEFAULT gst_frame_duplicator_debug

/* Filter signals and args */
enum
{
    /* FILL ME */
    LAST_SIGNAL
};

enum
{
    PROP_0,
    PROP_TIMEOUT,
    PROP_FREQUENCY,
    PROP_ENABLE,
    PROP_MAX_FILL
};

#define DEFAULT_MAX_FILL  10000
#define DEFAULT_TIMEOUT   100
#define DEFAULT_FREQUENCY 12

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */

static gboolean timeout_send_pic(gpointer user_data);
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE("sink",
                                                                   GST_PAD_SINK,
                                                                   GST_PAD_ALWAYS,
                                                                   GST_STATIC_CAPS("ANY"));

static GstStaticPadTemplate srcany_factory = GST_STATIC_PAD_TEMPLATE("src",
                                                                     GST_PAD_SRC,
                                                                     GST_PAD_ALWAYS,
                                                                     GST_STATIC_CAPS("ANY"));

#define gst_frame_duplicator_parent_class parent_class

static void gst_frame_duplicator_set_property (GObject * object, guint prop_id,
                                           const GValue * value, GParamSpec * pspec);
static void gst_frame_duplicator_get_property (GObject * object, guint prop_id,
                                           GValue * value, GParamSpec * pspec);


static void gst_frame_duplicator_finalize (GObject * object);

static gboolean gst_frame_duplicator_sink_event (GstPad * pad, GstObject * parent, GstEvent * event);
static GstFlowReturn gst_frame_duplicator_chain (GstPad * pad, GstObject * parent, GstBuffer * buf);
static GstStateChangeReturn gst_frame_duplicator_change_state (GstElement *element, GstStateChange transition);
/* GObject vmethod implementations */

/* initialize the frame_duplicator's class */

G_DEFINE_TYPE_WITH_CODE (GstFrameDuplicator, gst_frame_duplicator, GST_TYPE_ELEMENT,
  GST_DEBUG_CATEGORY_INIT (gst_frame_duplicator_debug, "frameduplicator", 0,
  "debug category for frameduplicator element"));

static void
gst_frame_duplicator_class_init (GstFrameDuplicatorClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;

    gobject_class = (GObjectClass *) klass;
    gstelement_class = (GstElementClass *) klass;

    gobject_class->set_property = gst_frame_duplicator_set_property;
    gobject_class->get_property = gst_frame_duplicator_get_property;
    gobject_class->finalize     = gst_frame_duplicator_finalize;
    gstelement_class->change_state     = GST_DEBUG_FUNCPTR (gst_frame_duplicator_change_state);

    g_object_class_install_property(gobject_class, PROP_TIMEOUT,
        g_param_spec_int("timeout", "Timeout", "Timeout (ms) before repeating a picture",
        0, 2000, DEFAULT_TIMEOUT, G_PARAM_READWRITE));
    g_object_class_install_property(gobject_class, PROP_FREQUENCY,
        g_param_spec_int("frequency", "Frequency", "Scheduled frequency (Hz) of frame duplication task",
        1, 100, DEFAULT_FREQUENCY, G_PARAM_READWRITE));
    g_object_class_install_property(gobject_class, PROP_ENABLE,
        g_param_spec_boolean("enable", "Enable", "Enable",
        false, G_PARAM_READWRITE));
    g_object_class_install_property(gobject_class, PROP_MAX_FILL,
        g_param_spec_int("max_fill", "MaxFill", "Maximum duration (ms) for which to duplicate frames. Bus error signalled after timeout. 0 repeats forever.",
        1, 100000, DEFAULT_MAX_FILL, G_PARAM_READWRITE));

    gst_element_class_set_details_simple(gstelement_class,
        "Frame Duplicator filter",
        "Frame duplicator filter",
        "Fill in gaps in the input stream by duplicating frames.",
        "phabsch@aeryon.com");

    gst_element_class_add_pad_template(gstelement_class,
        gst_static_pad_template_get(&srcany_factory));
    gst_element_class_add_pad_template(gstelement_class,
        gst_static_pad_template_get(&sink_factory));
}

/* initialize the new frame_duplicator element
 * instantiate pads and add them to element
 * set pad callback functions
 * initialize instance structure
 */
static void
gst_frame_duplicator_init (GstFrameDuplicator * filter)
{
    filter->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
    gst_pad_set_event_function (filter->sinkpad,
        GST_DEBUG_FUNCPTR (gst_frame_duplicator_sink_event));
    gst_pad_set_chain_function (filter->sinkpad,
        GST_DEBUG_FUNCPTR (gst_frame_duplicator_chain));
    GST_PAD_SET_PROXY_CAPS (filter->sinkpad);
    gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);

    filter->srcpad_any = gst_pad_new_from_static_template(&srcany_factory, "any");
    GST_PAD_SET_PROXY_CAPS (filter->srcpad_any);
    gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad_any);

    filter->send_timeout_ms = DEFAULT_TIMEOUT;
    filter->frequency       = DEFAULT_FREQUENCY;
    filter->enable          = false;
    filter->last_sync_pts   = GST_CLOCK_TIME_NONE;
    filter->stored_buffer   = NULL;
    filter->max_gap_fill_ms = DEFAULT_MAX_FILL;

    g_mutex_init(&filter->data_mutex);

    filter->callback_id = 0; 
}

static GstStateChangeReturn gst_frame_duplicator_change_state (GstElement * element, 
    GstStateChange transition) {
    GstFrameDuplicator *filter = GST_FRAME_DUPLICATOR (element);
    GstStateChangeReturn ret;
    ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

    switch (transition) {
        case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
            break;
        case GST_STATE_CHANGE_PAUSED_TO_READY:
            break;
        case GST_STATE_CHANGE_READY_TO_NULL:
            if(filter->enable && filter->callback_id) {
                g_source_remove(filter->callback_id);
                filter->callback_id = 0;
            }
            break;
        default:
            break;
    }
    
    return ret;
}

static void
gst_frame_duplicator_finalize (GObject * object) {
    GstFrameDuplicator * filter = GST_FRAME_DUPLICATOR (object);
    g_mutex_lock(&filter->data_mutex);
    filter->last_sync_pts = GST_CLOCK_TIME_NONE;
    if(filter->stored_buffer) {
        gst_buffer_unref(filter->stored_buffer);
    }
    filter->stored_buffer = NULL;
    g_mutex_unlock(&filter->data_mutex);
    g_mutex_clear(&filter->data_mutex);
}

static void
gst_frame_duplicator_set_property (GObject * object, guint prop_id,
                               const GValue * value, GParamSpec * pspec)
{
    GstFrameDuplicator *filter = GST_FRAME_DUPLICATOR (object);

    GST_OBJECT_LOCK(filter);
    switch (prop_id)
    {
        case PROP_TIMEOUT:
            filter->send_timeout_ms = g_value_get_int (value);
            break;
        case PROP_FREQUENCY:
            {
                filter->frequency = g_value_get_int (value);
                if(filter->enable) {
                    int frame_interval = 1000/filter->frequency;
                    g_source_remove(filter->callback_id);
                    filter->callback_id = g_timeout_add(frame_interval, timeout_send_pic, filter);
                }
            }
            break;
        case PROP_ENABLE:
            if(filter->enable) {
                g_source_remove(filter->callback_id);
                filter->callback_id = 0;
            }
            filter->enable = g_value_get_boolean (value);
            if(filter->enable) {
                int frame_interval = 1000/filter->frequency;
                filter->callback_id = g_timeout_add(frame_interval, timeout_send_pic, filter);
            }
            break;
        case PROP_MAX_FILL:
            filter->max_gap_fill_ms = g_value_get_int (value);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
    GST_OBJECT_UNLOCK(filter);
}

static void
gst_frame_duplicator_get_property (GObject * object, guint prop_id,
                               GValue * value, GParamSpec * pspec)
{
    GstFrameDuplicator *filter = GST_FRAME_DUPLICATOR (object);

    GST_OBJECT_LOCK(filter);
    switch (prop_id)
    {
        case PROP_TIMEOUT:
            g_value_set_int (value, filter->send_timeout_ms);
            break;
        case PROP_FREQUENCY:
            g_value_set_int (value, filter->frequency);
            break;
        case PROP_ENABLE:
            g_value_set_boolean (value, filter->enable);
            break;
        case PROP_MAX_FILL:
            g_value_set_int (value, filter->max_gap_fill_ms);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
    GST_OBJECT_UNLOCK(filter);
}

/* GstElement method implementations */

/* this function handles sink events */
static gboolean
gst_frame_duplicator_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
    gboolean ret;
    GstFrameDuplicator *filter;

    filter = GST_FRAME_DUPLICATOR (parent);
    
    switch (GST_EVENT_TYPE (event))
    {
        case GST_EVENT_EOS:
        case GST_EVENT_CAPS:
            //reset buffers and pts values
            g_mutex_lock(&filter->data_mutex);
            filter->last_sync_pts = GST_CLOCK_TIME_NONE;
            if(filter->stored_buffer) {
                gst_buffer_unref(filter->stored_buffer);
            }
            filter->stored_buffer = NULL;
            g_mutex_unlock(&filter->data_mutex);
        default:
            ret = gst_pad_event_default (pad, parent, event);
            break;
    }
    return ret;
}

static void 
fill_pts_gap(GstFrameDuplicator * filter,  GstClockTime current, GstClockTime duration) {
    GstClockTime next_pts = filter->last_sent_pts + duration;
    while(next_pts < current-duration) {
        GstBuffer * buffer = gst_buffer_copy(filter->stored_buffer);
        if(!buffer) {
            GST_WARNING_OBJECT(filter, "Could not copy buffer %p\n", filter->stored_buffer);
            return;
        }
        GST_BUFFER_PTS(buffer) = next_pts;
        GST_DEBUG_OBJECT(filter, "SEND FILL: %" GST_TIME_FORMAT " (dur: %d)\n", GST_TIME_ARGS(GST_BUFFER_PTS(buffer)), (int)duration);
        gst_pad_push(filter->srcpad_any, buffer);
        filter->last_sent_pts = next_pts;
        next_pts += duration;
    }
}

static gboolean 
timeout_send_pic(gpointer user_data) {
    GstFrameDuplicator *filter = (GstFrameDuplicator*)user_data;
    if(!g_mutex_trylock(&filter->data_mutex)) {
        GST_DEBUG_OBJECT(filter, "locked by other task, unable to fill buffer \n");
        return TRUE;
    }

    // don't repeat pictures if none have been recieved yet
    if(filter->last_sync_pts == GST_CLOCK_TIME_NONE || !filter->stored_buffer) {
        g_mutex_unlock(&filter->data_mutex);
        return TRUE;
    }

    gint64 current_time = g_get_monotonic_time()*1000;
    GstClockTime delta = GST_TIME_AS_NSECONDS(current_time) - GST_TIME_AS_NSECONDS(filter->last_sync_pts_time);
    GstClockTime current_pts = filter->last_sync_pts + delta;

    // if the filter has been duplicating pictures for more than the 
    // allowed amount, send a bus error
    if(filter->max_gap_fill_ms && GST_TIME_AS_MSECONDS(delta) > (uint64_t)filter->max_gap_fill_ms) {
        g_mutex_unlock(&filter->data_mutex);
        GST_ELEMENT_ERROR (filter, STREAM, FAILED, ("Frame duplicator input timeout"), ("Frame duplicator input timeout"));
        return FALSE;
    }
    // wait for user configured timeout before starting to repeat
    if(GST_TIME_AS_MSECONDS(delta) < (uint64_t)filter->send_timeout_ms) {
        g_mutex_unlock(&filter->data_mutex);
        return TRUE;
    }

    fill_pts_gap(filter, current_pts, filter->duration);

    if(current_pts - filter->last_sent_pts > filter->duration) {
        GstBuffer * buffer = gst_buffer_copy(filter->stored_buffer);
        GST_BUFFER_PTS(buffer) = filter->last_sent_pts + filter->duration;
        GST_DEBUG_OBJECT(filter, "SEND timeout: %" GST_TIME_FORMAT " delta: %d)\n", GST_TIME_ARGS(GST_BUFFER_PTS(buffer)), (int)delta);
        gst_pad_push(filter->srcpad_any, buffer);
        filter->last_sent_pts = GST_BUFFER_PTS(buffer);
    }
    g_mutex_unlock(&filter->data_mutex);
    return TRUE;
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_frame_duplicator_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
    USE(pad);
    GstFrameDuplicator *filter;
    
    filter = GST_FRAME_DUPLICATOR (parent);

    g_mutex_lock(&filter->data_mutex);
    if(GST_CLOCK_TIME_IS_VALID(filter->duration) && !filter->enable) {
        g_mutex_unlock(&filter->data_mutex);
        return gst_pad_push (filter->srcpad_any, buf);
    }

    GstClockTime pts = GST_BUFFER_PTS(buf);

    // a picture of this PTS has already been sent. Use this as the most recent picture to repeat, but don't forward it.
    if(filter->last_sync_pts != GST_CLOCK_TIME_NONE && pts < filter->last_sent_pts) {
        if(filter->stored_buffer) {
            gst_buffer_unref(filter->stored_buffer);
        }
        // no need to add a ref to this buffer because the chain function already has ownership
        filter->stored_buffer = buf;

        g_mutex_unlock(&filter->data_mutex);
        return GST_FLOW_OK;
    }

    if(filter->enable && filter->last_sync_pts != GST_CLOCK_TIME_NONE) {
        fill_pts_gap(filter, pts, filter->duration);
    }
    
    filter->duration = GST_BUFFER_DURATION(buf);
    if(filter->stored_buffer) {
        gst_buffer_unref(filter->stored_buffer);
    }
    filter->stored_buffer = gst_buffer_ref(buf);

    gint64 current_time = g_get_monotonic_time()*1000;

    filter->last_sent_pts = pts;
    filter->last_sync_pts = pts;
    filter->last_sync_pts_time = current_time;

    /* push out the incoming buffer */
    GST_DEBUG_OBJECT(filter, "SEND orig: %" GST_TIME_FORMAT "\n", GST_TIME_ARGS(GST_BUFFER_PTS(buf)));
    g_mutex_unlock(&filter->data_mutex);
    return gst_pad_push (filter->srcpad_any, buf);
}

