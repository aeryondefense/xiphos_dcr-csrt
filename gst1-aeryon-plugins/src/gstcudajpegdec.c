/* GStreamer
 * Copyright (C) 2016 Jun Zhang <jzhang@aeryon.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstcudajpegdec
 *
 * The cudajpegdec element does jpeg decoding using NVIDIA GPU
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v videotestsrc ! jpegenc ! cudajpegdec  ! autovideosink -v 
 * ]|
 * jpeg encoder and decoder video test pattern
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideodecoder.h>
#include "gstcudajpegdec.h"
#include "types.h"

GST_DEBUG_CATEGORY_STATIC (gst_cuda_jpeg_dec_debug_category);
#define GST_CAT_DEFAULT gst_cuda_jpeg_dec_debug_category

// Low-pass first-order IIR filter
// Time constant is ~ (N * T_sample), N=0 for no filter
static void 
gst_cuda_jpeg_dec_lp_filt(gdouble * val, gdouble new_val, int n)
{
    *val = (*val * (float)n + new_val) / ((float)n+1);
}

static GType
gst_cuda_jpeg_dec_temporal_tradeoff_get_type (void)
{
    static volatile gsize temporal_tradeoff_type_type = 0;

    static const GEnumValue temporal_tradeoff_type[] = {
        {CudaJpegDecTemporalTradeoffLevel_DropNone,
            "GST_CUDA_JPEG_DEC_DROP_NO_FRAMES", "Do not drop frames"},
        {CudaJpegDecTemporalTradeoffLevel_Drop1in5,
            "GST_CUDA_JPEG_DEC_DROP_1_IN_5_FRAMES", "Drop 1 in 5 frames"},
        {CudaJpegDecTemporalTradeoffLevel_Drop1in3,
            "GST_CUDA_JPEG_DEC_DROP_1_IN_3_FRAMES", "Drop 1 in 3 frames"},
        {0, NULL, NULL}
    };

    if (g_once_init_enter (&temporal_tradeoff_type_type)) {
        GType tmp = g_enum_register_static ("GstCudaJpegDecTemporalTradeoffType", temporal_tradeoff_type);
      g_once_init_leave (&temporal_tradeoff_type_type, tmp);
    }

    return (GType) temporal_tradeoff_type_type;
}

#define GST_TYPE_CUDA_JPEG_DEC_TEMPORAL_TRADEOFF (gst_cuda_jpeg_dec_temporal_tradeoff_get_type ())
#define DEFAULT_TEMPORAL_TRADEOFF_TYPE   CudaJpegDecTemporalTradeoffLevel_DropNone

// Define this 16/9 ratio slightly smaller 1.77 
// to make sure 1920x1088 is also detected as 16:9
#define ASPECT_RATIO_16_9 (1.76)

/* prototypes */
static void gst_cuda_jpeg_dec_set_property (GObject * object, guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_cuda_jpeg_dec_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec);
static void gst_cuda_jpeg_dec_finalize (GObject * object);
static gboolean gst_cuda_jpeg_dec_set_format (GstVideoDecoder * decoder, GstVideoCodecState * state);
static GstFlowReturn gst_cuda_jpeg_dec_handle_frame (GstVideoDecoder * decoder, GstVideoCodecFrame * frame);
static gboolean gst_cuda_jpeg_dec_decide_allocation (GstVideoDecoder * decoder, GstQuery * query);

enum
{
  PROP_0,
  PROP_TEMPORAL_TRADEOFF,
  PROP_MIN_RATE,
  PROP_FORCE_AR_16_9
};

#define VIDEO_CAPS \
    "width = (int) [ 16, 1920 ], " \
    "height = (int) [ 16, 1088 ], " \
    "framerate = " \
    " (fraction) [ 0/1, 60/1 ]"

/* pad templates */
static GstStaticPadTemplate gst_cuda_jpeg_dec_sink_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("image/jpeg")
    );

static GstStaticPadTemplate gst_cuda_jpeg_dec_src_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS("video/x-raw, format = (string) I420, " VIDEO_CAPS));


/* class initialization */
G_DEFINE_TYPE_WITH_CODE (GstCudaJpegDec, gst_cuda_jpeg_dec, GST_TYPE_VIDEO_DECODER,
  GST_DEBUG_CATEGORY_INIT (gst_cuda_jpeg_dec_debug_category, "cudajpegdec", 0,
  "jpeg decoder based on NVIDIA NPP (only support up to 1920x1088 for now)"));

static void
gst_cuda_jpeg_dec_class_init (GstCudaJpegDecClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstVideoDecoderClass *video_decoder_class = GST_VIDEO_DECODER_CLASS (klass);

  gobject_class->set_property = gst_cuda_jpeg_dec_set_property;
  gobject_class->get_property = gst_cuda_jpeg_dec_get_property;
  gobject_class->finalize = gst_cuda_jpeg_dec_finalize;

  /* Setting up pads and setting metadata should be moved to
     base_class_init if you intend to subclass this class. */
  gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
      gst_static_pad_template_get (&gst_cuda_jpeg_dec_src_template));

  gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
      gst_static_pad_template_get (&gst_cuda_jpeg_dec_sink_template));

  g_object_class_install_property (gobject_class, PROP_TEMPORAL_TRADEOFF,
      g_param_spec_enum ("temporal-tradeoff", "Temporal Tradeoff for jpeg decoder",
          "Temporal Tradeoff value for jpeg decoder",
          GST_TYPE_CUDA_JPEG_DEC_TEMPORAL_TRADEOFF,
          DEFAULT_TEMPORAL_TRADEOFF_TYPE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
          GST_PARAM_MUTABLE_READY));

  g_object_class_install_property (gobject_class, PROP_MIN_RATE,
      g_param_spec_uint ("min-rate", "Min framerate for jpeg decoder",
          "The jpeg decoder will not drop frames if the minimum framerate is not met",
          0, 60, 60, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_FORCE_AR_16_9,
      g_param_spec_boolean ("force-ar-16-9", "Force aspect ratio 16x9",
          "Force output image size to be aspect ratio 16x9 by resizing the image ",
          FALSE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
      "cudajpegdec", "jpeg decoder accelerated by GPU", "cudajpegdec",
      "Jun Zhang <jzhang@aeryon.com>");

  video_decoder_class->set_format = GST_DEBUG_FUNCPTR (gst_cuda_jpeg_dec_set_format);
  video_decoder_class->handle_frame = GST_DEBUG_FUNCPTR (gst_cuda_jpeg_dec_handle_frame);
  video_decoder_class->decide_allocation = GST_DEBUG_FUNCPTR (gst_cuda_jpeg_dec_decide_allocation);
}


static void
gst_cuda_jpeg_dec_init (GstCudaJpegDec *cudajpegdec)
{
    cuda_jpeg_dec_alloc(&cudajpegdec->instance_p);
    cudajpegdec->pFrameHeader      = cudajpegdec->instance_p->pFrameHeader;
    cudajpegdec->temporal_tradeoff = DEFAULT_TEMPORAL_TRADEOFF_TYPE;
    cudajpegdec->index             = 0;
    cudajpegdec->drop_factor       = 0xFFFF;
    cudajpegdec->min_rate          = 60;
    cudajpegdec->min_duration      = 1000.f/(float)(cudajpegdec->min_rate);
    cudajpegdec->filt_duration     = cudajpegdec->min_duration;
    cudajpegdec->last_frame_ms     = get_time_ms();
    cudajpegdec->force_ar_16_9     = false;
}

void
gst_cuda_jpeg_dec_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstCudaJpegDec *cudajpegdec = GST_CUDA_JPEG_DEC (object);
  

  switch (property_id) {
    case PROP_TEMPORAL_TRADEOFF:
      cudajpegdec->temporal_tradeoff = g_value_get_enum (value);
      switch(cudajpegdec->temporal_tradeoff){
        case CudaJpegDecTemporalTradeoffLevel_Drop1in5:
            cudajpegdec->drop_factor = 5;
          break;
        case CudaJpegDecTemporalTradeoffLevel_Drop1in3:
            cudajpegdec->drop_factor = 3;
          break;
        default:
            cudajpegdec->drop_factor = 0xFFFF;
          break;          
      }
      break;
    case PROP_MIN_RATE:
      cudajpegdec->min_rate = g_value_get_uint(value);
      cudajpegdec->min_duration = 1000.f/(float)(cudajpegdec->min_rate);
      cudajpegdec->filt_duration = cudajpegdec->min_duration;
      break;
    case PROP_FORCE_AR_16_9:
      cudajpegdec->force_ar_16_9 = g_value_get_boolean(value);
      break;      
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_cuda_jpeg_dec_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstCudaJpegDec *cudajpegdec = GST_CUDA_JPEG_DEC (object);

  switch (property_id) {
    case PROP_TEMPORAL_TRADEOFF:
      g_value_set_enum (value, cudajpegdec->temporal_tradeoff);
      break;
    case PROP_MIN_RATE:
      g_value_set_uint (value, cudajpegdec->min_rate);
      break;
    case PROP_FORCE_AR_16_9:
      g_value_set_boolean(value, cudajpegdec->force_ar_16_9);
      break;         
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_cuda_jpeg_dec_finalize (GObject * object)
{
  GstCudaJpegDec *cudajpegdec = GST_CUDA_JPEG_DEC (object);

  if (cudajpegdec->input_state)
    gst_video_codec_state_unref (cudajpegdec->input_state);

  /* release cuda memory allocate for decoder */
  cuda_jpeg_dec_dealloc(cudajpegdec->instance_p);

  G_OBJECT_CLASS (gst_cuda_jpeg_dec_parent_class)->finalize (object);
}


static gboolean
gst_cuda_jpeg_dec_set_format (GstVideoDecoder * decoder, GstVideoCodecState * state)
{
  GstCudaJpegDec *cudajpegdec = GST_CUDA_JPEG_DEC (decoder);

  //We are not implementing the parse vmethod, so always set 'packetized' to be true
   gst_video_decoder_set_packetized (decoder, TRUE);

  if (cudajpegdec->input_state)
    gst_video_codec_state_unref (cudajpegdec->input_state);

  cudajpegdec->input_state = gst_video_codec_state_ref (state);

  return TRUE;
}

static gboolean
gst_cuda_jpeg_dec_negotiate (GstCudaJpegDec * decoder, gint width, gint height, gint clrspc)
{
  GstVideoCodecState *outstate;
  GstVideoInfo *info;
  GstVideoFormat format;
  //Only allow YUY2 or I420 
  format = clrspc;

  // Compare to currently configured output state 
  outstate = gst_video_decoder_get_output_state (GST_VIDEO_DECODER (decoder));
  if (outstate) {
    info = &outstate->info;

    if (width == GST_VIDEO_INFO_WIDTH (info) &&
        height == GST_VIDEO_INFO_HEIGHT (info) &&
        format == GST_VIDEO_INFO_FORMAT (info)) {
      gst_video_codec_state_unref (outstate);
      return TRUE;
    }
    gst_video_codec_state_unref (outstate);
  }

  outstate =
    gst_video_decoder_set_output_state (GST_VIDEO_DECODER (decoder), format, width, height, decoder->input_state);

  outstate->info.colorimetry.range     = GST_VIDEO_COLOR_RANGE_0_255;
  outstate->info.colorimetry.matrix    = GST_VIDEO_COLOR_MATRIX_BT601;
  outstate->info.colorimetry.transfer  = GST_VIDEO_TRANSFER_UNKNOWN;
  outstate->info.colorimetry.primaries = GST_VIDEO_COLOR_PRIMARIES_UNKNOWN;

  gst_video_codec_state_unref (outstate);

  gst_video_decoder_negotiate (GST_VIDEO_DECODER (decoder));

  return TRUE;
}

static gboolean
gst_cuda_jpeg_dec_decide_allocation (GstVideoDecoder * decoder, GstQuery * query)
{
  GstBufferPool *pool = NULL;
  GstStructure *config;

  if (!GST_VIDEO_DECODER_CLASS (gst_cuda_jpeg_dec_parent_class)->decide_allocation (decoder, query))
    return FALSE;

  if (gst_query_get_n_allocation_pools (query) > 0)
    gst_query_parse_nth_allocation_pool (query, 0, &pool, NULL, NULL, NULL);

  if (pool == NULL)
    return FALSE;

  config = gst_buffer_pool_get_config (pool);
  if (gst_query_find_allocation_meta (query, GST_VIDEO_META_API_TYPE, NULL)) {
    gst_buffer_pool_config_add_option (config,
        GST_BUFFER_POOL_OPTION_VIDEO_META);
  }

  gst_buffer_pool_set_config (pool, config);
  gst_object_unref (pool);

  return TRUE;
}

// Decompress one jpeg image and push the data to downstream
static GstFlowReturn
gst_cuda_jpeg_dec_handle_frame (GstVideoDecoder *decoder, GstVideoCodecFrame *frame)
{
  GstCudaJpegDec *cudajpegdec = GST_CUDA_JPEG_DEC (decoder);
  GstFlowReturn ret = GST_FLOW_OK;
  gint result = 0;
  GstVideoCodecState *state = NULL;
  guint out_width = 0;
  guint out_height = 0;
  guint bytes_in_buffer = 0;
  GstMapInfo in_map;
  GstMapInfo out_map;

  cudajpegdec->current_frame = frame;

  if(!gst_buffer_map (frame->input_buffer, &in_map, GST_MAP_READ)){
    GST_ERROR_OBJECT (decoder, "Invalid input jpeg data");
    return ret;
  }

  gst_cuda_jpeg_dec_lp_filt(&(cudajpegdec->filt_duration), get_time_ms() - cudajpegdec->last_frame_ms, 5);
  
  if(cudajpegdec->index && ((cudajpegdec->index % cudajpegdec->drop_factor) == 0)
        && cudajpegdec->min_duration > cudajpegdec->filt_duration) {
    cudajpegdec->index = 0;
    goto drop_frame;
  }
  
  cudajpegdec->last_frame_ms = get_time_ms();
  cudajpegdec->index++;
  cudajpegdec->index &= 0xFF; //reset after 255

  bytes_in_buffer = in_map.size;

  result = cuda_jpeg_dec_parse_header(cudajpegdec->instance_p, 
      (guint8 *)in_map.data, 
      bytes_in_buffer);

  if(result < 0) {
    gst_buffer_unmap (frame->input_buffer, &in_map);
    GST_ERROR_OBJECT (decoder, "Invalid or unsupported jpeg format");
    return ret;
  }

  out_width  = cudajpegdec->pFrameHeader->nWidth; 
  out_height = cudajpegdec->pFrameHeader->nHeight;

  if(cudajpegdec->pFrameHeader->nHeight == 0) {
    GST_ERROR_OBJECT (decoder, "Invalid image resolution width = %d height = %d \n", 
        cudajpegdec->pFrameHeader->nWidth,
        cudajpegdec->pFrameHeader->nHeight);
    return ret;
  }

  if(cudajpegdec->force_ar_16_9) {

    float aspect_ratio = (float)cudajpegdec->pFrameHeader->nWidth / (float) cudajpegdec->pFrameHeader->nHeight;

    if(aspect_ratio < ASPECT_RATIO_16_9) {
      out_width  = cudajpegdec->pFrameHeader->nWidth; // Keep the width the same 
      out_height = (cudajpegdec->pFrameHeader->nWidth * 9) / 16;

      GST_DEBUG_OBJECT (decoder, "Aspect ratio is not 16:9, converting to 4:3 by resizing [%d x %d] to [%d x %d] \n",
        cudajpegdec->pFrameHeader->nWidth,
        cudajpegdec->pFrameHeader->nHeight,
        out_width, out_height);
    }
  }

  // Notify buffer allocator about our image format
  gst_cuda_jpeg_dec_negotiate (cudajpegdec, 
                               out_width, 
                               out_height, 
                               GST_VIDEO_FORMAT_I420); // Only support I420 for now

  GST_DEBUG_OBJECT (decoder, "starting decompress");

 
  state = gst_video_decoder_get_output_state (decoder);

  ret = gst_video_decoder_allocate_output_frame (decoder, frame);

  if (G_UNLIKELY (ret != GST_FLOW_OK)) {
    GST_ERROR_OBJECT (decoder, "allocation failed ");
    goto exit_handle_data;
  }

  if(!gst_buffer_map (frame->output_buffer, &out_map, GST_MAP_WRITE)){
    GST_ERROR_OBJECT (decoder, "map output data failed ");
     goto exit_handle_data;
  }

  cuda_jpeg_dec_decompress(cudajpegdec->instance_p, 
                          (guint8 *)in_map.data, 
                          bytes_in_buffer, 
                          out_map.data,
                          out_width,
                          out_height);

  gst_buffer_unmap (frame->input_buffer, &in_map);
  gst_buffer_unmap (frame->output_buffer, &out_map);

  // Push decoded frame to downstream
  ret = gst_video_decoder_finish_frame (decoder, frame);

  if (state)
    gst_video_codec_state_unref (state);

  return ret;

exit_handle_data:

  if (state)
    gst_video_codec_state_unref (state);
  
  gst_buffer_unmap (frame->input_buffer, &in_map);

  return ret;

drop_frame:
  gst_buffer_unmap (frame->input_buffer, &in_map);
  gst_video_decoder_drop_frame (decoder, frame);

  return ret;
}
