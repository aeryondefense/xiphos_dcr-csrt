/*
 * GStreamer
 * Copyright (C) 2006 Stefan Kost <ensonic@users.sf.net>
 * Copyright (C) 2016 Jun Zhang <<user@hostname.org>>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-aeryonbufferalloc
 *
 * FIXME:Describe aeryonbufferalloc here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! aeryonbufferalloc ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/base/base.h>
#include <gst/controller/controller.h>

#include "gstaeryonbufferalloc.h"

GST_DEBUG_CATEGORY_STATIC (gst_aeryonbufferalloc_debug);
#define GST_CAT_DEFAULT gst_aeryonbufferalloc_debug

#define DEFAULT_NUM_BUFFERS   4
#define DEFAULT_ALIGNMENT     127

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_NUM_BUFFERS,
  PROP_ALIGNMENT
};

/* the capabilities of the inputs and outputs.
 *
 * FIXME:describe the real formats here.
 */
static GstStaticPadTemplate sink_template =
GST_STATIC_PAD_TEMPLATE (
  "sink",
  GST_PAD_SINK,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ I420, YV12, YUY2, UYVY }"))
);

static GstStaticPadTemplate src_template =
GST_STATIC_PAD_TEMPLATE (
  "src",
  GST_PAD_SRC,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ I420, YV12, YUY2, UYVY }"))
);

#define gst_aeryonbufferalloc_parent_class parent_class
G_DEFINE_TYPE (Gstaeryonbufferalloc, gst_aeryonbufferalloc, GST_TYPE_BASE_TRANSFORM);

static void gst_aeryonbufferalloc_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_aeryonbufferalloc_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static GstFlowReturn gst_aeryonbufferalloc_transform_ip (GstBaseTransform * base,
    GstBuffer * outbuf);
static gboolean
gst_aeryonbufferalloc_propose_allocation (GstBaseTransform * trans,
    GstQuery * decide_query, GstQuery * query);
static gboolean
gst_aeryonbufferalloc_decide_allocation (GstBaseTransform * trans, GstQuery * query);


/* GObject vmethod implementations */

/* initialize the aeryonbufferalloc's class */
static void
gst_aeryonbufferalloc_class_init (GstaeryonbufferallocClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->set_property = gst_aeryonbufferalloc_set_property;
  gobject_class->get_property = gst_aeryonbufferalloc_get_property;


  g_object_class_install_property (gobject_class, PROP_NUM_BUFFERS,
    g_param_spec_uint ("numBuffers", "NumBuffers", "Number of buffers allocated for pipeline",
           0, 256, DEFAULT_NUM_BUFFERS, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_ALIGNMENT,
    g_param_spec_uint ("alignment", "Alignment", "Alignment of allocated buffers ",
           0, 1023, DEFAULT_ALIGNMENT, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  gst_element_class_set_details_simple (gstelement_class,
    "aeryonbufferalloc",
    "aeryonbufferalloc",
    "inject user space buffers for v4l2src userptr mode",
    "Jun Zhang <<jzhang@aeryon.com>>");

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&src_template));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&sink_template));

  GST_BASE_TRANSFORM_CLASS (klass)->transform_ip =
      GST_DEBUG_FUNCPTR (gst_aeryonbufferalloc_transform_ip);


  GST_BASE_TRANSFORM_CLASS (klass)->propose_allocation =
      GST_DEBUG_FUNCPTR (gst_aeryonbufferalloc_propose_allocation);

  GST_BASE_TRANSFORM_CLASS (klass)->decide_allocation =
      GST_DEBUG_FUNCPTR (gst_aeryonbufferalloc_decide_allocation);

  /* debug category for fltering log messages
   *
   * FIXME:exchange the string 'Template aeryonbufferalloc' with your description
   */
  GST_DEBUG_CATEGORY_INIT (gst_aeryonbufferalloc_debug, "aeryonbufferalloc", 0, "aeryonbufferalloc");
}

/* initialize the new element
 * initialize instance structure
 */
static void
gst_aeryonbufferalloc_init (Gstaeryonbufferalloc *filter)
{
  filter->numBuffers = DEFAULT_NUM_BUFFERS;
  filter->alignment = DEFAULT_ALIGNMENT;
}

static void
gst_aeryonbufferalloc_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gstaeryonbufferalloc *filter = GST_AERYONBUFFERALLOC (object);
  
  switch (prop_id) {
    case PROP_NUM_BUFFERS:
      filter->numBuffers = g_value_get_uint (value);
      break;
    case PROP_ALIGNMENT:
      filter->alignment = g_value_get_uint (value);
      break;
    default:
     G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }  
}

static void
gst_aeryonbufferalloc_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  Gstaeryonbufferalloc *filter = GST_AERYONBUFFERALLOC (object);

  switch (prop_id) {
    case PROP_NUM_BUFFERS:
      g_value_set_uint(value, filter->numBuffers);
      break;
    case PROP_ALIGNMENT:
      g_value_set_uint(value, filter->alignment);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

/* Answer the allocation query downstream. */
static gboolean
gst_aeryonbufferalloc_propose_allocation (GstBaseTransform * trans,
    GstQuery * decide_query, GstQuery * query)
{
  Gstaeryonbufferalloc *filter = GST_AERYONBUFFERALLOC (trans);

  GstVideoInfo info;
  GstBufferPool *pool;
  GstCaps *caps;
  guint size;
  gboolean need_pool;



  if (!GST_BASE_TRANSFORM_CLASS (parent_class)->propose_allocation (trans,
          decide_query, query)) {
    //do nothing 
  }
    
  gst_query_parse_allocation (query, &caps, &need_pool);

  if (caps == NULL)
    return FALSE;

  if (!gst_video_info_from_caps (&info, caps))
    return FALSE;

  size = GST_VIDEO_INFO_SIZE (&info);

  if (gst_query_get_n_allocation_pools (query) == 0) {
    GstStructure *structure;
    GstAllocator *allocator = NULL;

    GstAllocationParams params;
    gst_allocation_params_init (&params);
    params.align = filter->alignment;


    if (gst_query_get_n_allocation_params (query) > 0)
      gst_query_parse_nth_allocation_param (query, 0, &allocator, &params);
    else
      gst_query_add_allocation_param (query, allocator, &params);

    pool = gst_video_buffer_pool_new ();

    structure = gst_buffer_pool_get_config (pool);
    gst_buffer_pool_config_set_params (structure, caps, size, 0, 0);
    gst_buffer_pool_config_set_allocator (structure, allocator, &params);

    if (allocator)
      gst_object_unref (allocator);

    if (!gst_buffer_pool_set_config (pool, structure))
      goto config_failed;

    gst_query_add_allocation_pool (query, pool, size, 0, 0);
    gst_object_unref (pool);
    
    gst_query_add_allocation_meta (query, GST_VIDEO_META_API_TYPE, NULL);
  }

  return TRUE;

  /* ERRORS */
config_failed:
  {
    GST_ERROR_OBJECT (filter, "failed to set config");
    gst_object_unref (pool);
    return FALSE;
  }
}

/* configure the allocation query that was answered downstream, we can configure
 * some properties on it. Only called when not in passthrough mode. */
static gboolean
gst_aeryonbufferalloc_decide_allocation (GstBaseTransform * trans, GstQuery * query)
{

  Gstaeryonbufferalloc *filter = GST_AERYONBUFFERALLOC (trans);

  GstBufferPool *pool = NULL;
  GstStructure *config;
  guint min = 0, max = 0, total = filter->numBuffers, size;
  gboolean update_pool;
  GstCaps *outcaps = NULL;

  GST_DEBUG_OBJECT(filter, "decide_allocation \n");

  if (gst_query_get_n_allocation_pools (query) > 0) {
    gst_query_parse_nth_allocation_pool (query, 0, &pool, &size, &min, &max);
    update_pool = TRUE;
  } else {
    GstVideoInfo vinfo;

    gst_query_parse_allocation (query, &outcaps, NULL);
    gst_video_info_init (&vinfo);
    gst_video_info_from_caps (&vinfo, outcaps);
    size = vinfo.size;
    update_pool = FALSE;
  }

  if(max > total) total = max;

  if (!pool)
    pool = gst_video_buffer_pool_new ();

  config = gst_buffer_pool_get_config (pool);

  gst_buffer_pool_config_add_option (config, GST_BUFFER_POOL_OPTION_VIDEO_META);

  if (outcaps)
    gst_buffer_pool_config_set_params (config, outcaps, size, 0, 0);

  gst_buffer_pool_set_config (pool, config);

  if (update_pool)
    gst_query_set_nth_allocation_pool (query, 0, pool, size, total, total);
  else
    gst_query_add_allocation_pool (query, pool, size, total, total);

  gst_object_unref (pool);

  return GST_BASE_TRANSFORM_CLASS (parent_class)->decide_allocation (trans,
      query);
}

/* GstBaseTransform vmethod implementations */

/* this function does the actual processing
 */
static GstFlowReturn
gst_aeryonbufferalloc_transform_ip (GstBaseTransform * base, GstBuffer * outbuf)
{
  Gstaeryonbufferalloc *filter = GST_AERYONBUFFERALLOC (base);

  if (GST_CLOCK_TIME_IS_VALID (GST_BUFFER_TIMESTAMP (outbuf)))
    gst_object_sync_values (GST_OBJECT (filter), GST_BUFFER_TIMESTAMP (outbuf));

  /* FIXME: do something interesting here.  This simply copies the source
   * to the destination. */

  return GST_FLOW_OK;
}


