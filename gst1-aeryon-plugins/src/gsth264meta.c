/* GStreamer
 * Copyright (C) 2017 Jun Zhang <jzhang@aeryon.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gsth264meta
 *
 * The h264meta element does FIXME stuff.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v videotestsrc ! x264enc ! video/x-h264, stream-format=byte-stream ! h264meta ! fakesink 
 * ]|
 * FIXME Describe what the pipeline does.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <string.h>

#include <gst/gst.h>
#include <gst/base/gstbaseparse.h>
#include <gst/base/gstbytewriter.h>
#include "gsth264meta.h"
#include "types.h"
#include "Timesync.hpp"

#define GST_H264_META_NAL_SEI (6) 
#define GST_H264_META_SEI_USER_DATA_UNREGISTERED (5) 

// 16 bytes universally unique identifier (uuid) generated according to ISO-11578
// "Aeryon Labs Inc"
static const uint8_t aeryon_uuid[16] = {
   0x41, 0x65, 0x72, 0x79, 0x6f, 0x6e, 0x20, 0x4c,
   0x61, 0x62, 0x73, 0x20, 0x49, 0x6e, 0x63, 0x3A
};

//reference 
//codec_data format from ISO 14496-15 Section 5.2.4.1.1:
/*
aligned(8) class AVCDecoderConfigurationRecord { 
  unsigned int(8) configurationVersion = 1; 
  unsigned int(8) AVCProfileIndication; 
  unsigned int(8) profile_compatibility; 
  unsigned int(8) AVCLevelIndication; 
  bit(6) reserved = ‘111111’b;   
  unsigned int(2) lengthSizeMinusOne; 
  bit(3) reserved = ‘111’b;   
  unsigned int(5) numOfSequenceParameterSets; 
  for (i=0; i< numOfSequenceParameterSets; i++) { 
    unsigned int(16) sequenceParameterSetLength ; 
    bit(8*sequenceParameterSetLength) sequenceParameterSetNALUnit; 
  } 
unsigned int(8) numOfPictureParameterSets; 
  for (i=0; i< numOfPictureParameterSets; i++) {   
    unsigned int(16) pictureParameterSetLength; 
    bit(8*pictureParameterSetLength) pictureParameterSetNALUnit; 
  } 
}
*/

enum
{
  GST_H264_META_FORMAT_NONE,
  GST_H264_META_FORMAT_AVC, // We do not support AVC format for now 
  GST_H264_META_FORMAT_BYTE
};

enum
{
  GST_H264_META_ALIGN_NONE = 0,
  GST_H264_META_ALIGN_NAL,
  GST_H264_META_ALIGN_AU
};

static const gchar *nal_names[] = {
  "Unknown",
  "Slice",
  "Slice DPA",
  "Slice DPB",
  "Slice DPC",
  "Slice IDR",
  "SEI",
  "SPS",
  "PPS",
  "AU delimiter",
  "Sequence End",
  "Stream End",
  "Filler Data",
  "SPS extension",
  "Prefix",
  "SPS Subset",
  "Depth Parameter Set",
  "Reserved", "Reserved",
  "Slice Aux Unpartitioned",
  "Slice Extension",
  "Slice Depth/3D-AVC Extension"
};

static const gchar *
_nal_name (GstH264NalUnitType nal_type)
{
  if (nal_type <= GST_H264_NAL_SLICE_DEPTH)
    return nal_names[nal_type];
  return "Invalid";
}

GST_DEBUG_CATEGORY_STATIC (gst_h264_meta_debug_category);
#define GST_CAT_DEFAULT gst_h264_meta_debug_category
#define parent_class gst_h264_meta_parent_class

/* prototypes */

static void gst_h264_meta_set_property (GObject *object, guint property_id, 
  const GValue *value, GParamSpec *pspec);
static void gst_h264_meta_get_property (GObject *object, guint property_id, 
  GValue *value, GParamSpec *pspec);
static void     gst_h264_meta_finalize (GObject *object);

static gboolean gst_h264_meta_start (GstBaseParse *parse);
static gboolean gst_h264_meta_stop (GstBaseParse *parse);

static gboolean gst_h264_meta_set_caps (GstBaseParse *parse, GstCaps *caps);
static GstCaps *gst_h264_meta_get_caps (GstBaseParse *parse, GstCaps *filter);

static gboolean gst_h264_meta_sink_event (GstBaseParse *parse, GstEvent *event);
static gboolean gst_h264_meta_src_event (GstBaseParse *parse, GstEvent *event);


static GstFlowReturn gst_h264_meta_handle_frame (GstBaseParse *parse, 
  GstBaseParseFrame *frame, gint *skipsize);
static GstFlowReturn gst_h264_meta_pre_push_frame (GstBaseParse *parse, 
  GstBaseParseFrame *frame);

enum
{
  PROP_0,
  PROP_SEI_PARSING,
  PROP_HW_TIME_OFFSET
};

/* pad templates */
static GstStaticPadTemplate gst_h264_meta_src_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-h264, stream-format=byte-stream") // Only support byte-stream for now
    );

static GstStaticPadTemplate gst_h264_meta_sink_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-h264, stream-format=byte-stream")
    );


/* class initialization */
G_DEFINE_TYPE_WITH_CODE (GstH264meta, gst_h264_meta, GST_TYPE_BASE_PARSE,
  GST_DEBUG_CATEGORY_INIT (gst_h264_meta_debug_category, "h264meta", 0,
  "debug category for h264meta element"));

static void
gst_h264_meta_class_init (GstH264metaClass *klass) {
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstBaseParseClass *base_parse_class = GST_BASE_PARSE_CLASS (klass);

  /* Setting up pads and setting metadata should be moved to
     base_class_init if you intend to subclass this class. */
  gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
      gst_static_pad_template_get (&gst_h264_meta_src_template));
  gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
      gst_static_pad_template_get (&gst_h264_meta_sink_template));

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass), "H.264 meta parser and injector",
      "Codec/Parser/Converter/Video", "Parse or inject metadata for H.264 elementary stream",
      "Jun Zhang <jzhang@aeryon.com>");

  gobject_class->set_property = gst_h264_meta_set_property;
  gobject_class->get_property = gst_h264_meta_get_property;

  g_object_class_install_property(gobject_class, PROP_SEI_PARSING,
      g_param_spec_boolean("sei-parsing", "sei-parsing", "Parse out SEI ?",
      FALSE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property(gobject_class, PROP_HW_TIME_OFFSET,
      g_param_spec_int64("hw-time-offset", "hw-time-offset", "Time offset against time sync server",
      G_MININT64, G_MAXINT64, 0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  base_parse_class->start = GST_DEBUG_FUNCPTR (gst_h264_meta_start);
  base_parse_class->stop  = GST_DEBUG_FUNCPTR (gst_h264_meta_stop);
  gobject_class->finalize = gst_h264_meta_finalize;
  base_parse_class->sink_event = GST_DEBUG_FUNCPTR (gst_h264_meta_sink_event);
  base_parse_class->src_event  = GST_DEBUG_FUNCPTR (gst_h264_meta_src_event);

  base_parse_class->set_sink_caps = GST_DEBUG_FUNCPTR (gst_h264_meta_set_caps);
  base_parse_class->get_sink_caps = GST_DEBUG_FUNCPTR (gst_h264_meta_get_caps);

  base_parse_class->handle_frame  = GST_DEBUG_FUNCPTR (gst_h264_meta_handle_frame);
  base_parse_class->pre_push_frame = GST_DEBUG_FUNCPTR (gst_h264_meta_pre_push_frame);
}

static gboolean 
timeout_calculate_stats(gpointer user_data) {

  GstH264meta *h264meta = GST_H264_META(user_data);

  gint64 current_time = g_get_monotonic_time();
  g_mutex_lock (&h264meta->stats_mutex);
  if(!GST_CLOCK_TIME_IS_VALID(h264meta->last_stats_collect_time)) {
      h264meta->last_stats_collect_time = current_time;
      g_mutex_unlock (&h264meta->stats_mutex);
      return TRUE;
  }
  GstClockTimeDiff time_diff = GST_CLOCK_DIFF(h264meta->last_stats_collect_time, current_time);
  gint64 time_diff_ms        = time_diff / 1000; // convert from microseconds to milliseconds 
  if(time_diff_ms > 0) {
    h264meta->average_bitrate   = (h264meta->stats_bytes_sum * 1000.0 * 8) / time_diff_ms;
    h264meta->average_framerate = h264meta->stats_frame_sum * 1000.0 / time_diff_ms;    
    h264meta->last_stats_collect_time = current_time; 
    GST_DEBUG_OBJECT (h264meta, "bps %d, fps %.1f last_valid_pts %" GST_TIME_FORMAT "\n", 
      (gint)h264meta->average_bitrate, h264meta->average_framerate, 
      GST_TIME_ARGS(h264meta->last_valid_pts));

    h264meta->stats_bytes_sum = 0;
    h264meta->stats_frame_sum = 0;   
  }
  g_mutex_unlock (&h264meta->stats_mutex);
  return TRUE;
}

static void
gst_h264_meta_init (GstH264meta *h264meta) {
  h264meta->sei_parsing     = false;
  h264meta->nal_length_size = 4; // Set default sync bytes length as 4, will be updated later 
  h264meta->nalparser       = NULL;  
  g_mutex_init(&h264meta->meta_mutex);
  g_mutex_init(&h264meta->stats_mutex);

  memset(&h264meta->system_meta, 0, sizeof(h264meta->system_meta));
  memset(&h264meta->orig_sei_data, 0, sizeof(h264meta->orig_sei_data));

  h264meta->frame_meta_queue = g_queue_new();

  h264meta->stats_callback_id = 
    g_timeout_add_seconds(STATS_INTERVAL_SECONDS, timeout_calculate_stats, h264meta);
  h264meta->stats_bytes_sum   = 0;
  h264meta->stats_frame_sum   = 0;
  h264meta->average_framerate = 0.0;
  h264meta->average_bitrate   = 0.0;
  h264meta->last_valid_pts          = GST_CLOCK_TIME_NONE;
  h264meta->last_stats_collect_time = GST_CLOCK_TIME_NONE;

  GST_PAD_SET_ACCEPT_INTERSECT (GST_BASE_PARSE_SINK_PAD (h264meta));
  GST_PAD_SET_ACCEPT_TEMPLATE (GST_BASE_PARSE_SINK_PAD (h264meta));
}

void
gst_h264_meta_finalize (GObject *object) {  
  GstH264meta *h264meta = GST_H264_META (object);
  g_source_remove(h264meta->stats_callback_id);
  // clean up object here 
  g_mutex_lock (&h264meta->meta_mutex);
  g_queue_foreach (h264meta->frame_meta_queue, (GFunc) gst_buffer_unref, NULL);
  g_queue_clear (h264meta->frame_meta_queue);
  g_mutex_unlock (&h264meta->meta_mutex);

  g_mutex_clear (&h264meta->meta_mutex);
  g_mutex_clear (&h264meta->stats_mutex);
  g_queue_free(h264meta->frame_meta_queue);
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_h264_meta_reset_frame (GstH264meta *h264meta) {
  GST_DEBUG_OBJECT (h264meta, "reset frame");

  // done parsing; reset state 
  h264meta->current_off            = -1;
  h264meta->sei_insertion_position = -1;
  h264meta->is_key_frame           = FALSE;
  h264meta->have_sps               = FALSE;
  h264meta->have_pps               = FALSE;
  h264meta->have_sei               = FALSE; 
  h264meta->orig_sei_offset        = -1;
  h264meta->orig_sei_size          = 0;
}

static void
gst_h264_meta_reset_stream_info (GstH264meta *h264meta) {
  h264meta->width  = 0; 
  h264meta->height = 0;
  h264meta->align  = GST_H264_META_ALIGN_NONE;
  h264meta->format = GST_H264_META_FORMAT_NONE;

  h264meta->nal_length_size = 4; // Reset to default value 
  h264meta->packetized      = FALSE;

  gst_h264_meta_reset_frame(h264meta);
  gst_buffer_replace (&h264meta->codec_data_in, NULL);
}

static void
gst_h264_meta_reset (GstH264meta *h264meta) {
  h264meta->last_report = GST_CLOCK_TIME_NONE;
  gst_h264_meta_reset_stream_info (h264meta);
}

static gboolean
gst_h264_meta_start (GstBaseParse *parse) {
  GstH264meta *h264meta = GST_H264_META (parse);
  gst_h264_meta_reset(h264meta);

  h264meta->nalparser = gst_h264_nal_parser_new ();

  // Require at at least 6 bytes before starting parsing 
  gst_base_parse_set_min_frame_size (parse, 6);
  return TRUE;
}

static gboolean
gst_h264_meta_stop (GstBaseParse *parse) {

  GstH264meta *h264meta = GST_H264_META (parse);
  gst_h264_meta_reset (h264meta);

  return TRUE;
}


void
gst_h264_meta_set_property (GObject *object, guint property_id,
    const GValue *value, GParamSpec *pspec) {
  
  GstH264meta *h264meta = GST_H264_META (object);
  switch (property_id) {
    case PROP_SEI_PARSING:
      h264meta->sei_parsing = g_value_get_boolean (value);
      break;
    case PROP_HW_TIME_OFFSET:
      h264meta->timesync_offset = g_value_get_int64 (value);
      break;     
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_h264_meta_get_property (GObject *object, guint property_id,
    GValue *value, GParamSpec *pspec) {

  GstH264meta *h264meta = GST_H264_META (object);
  
  switch (property_id) {
    case PROP_SEI_PARSING:
      g_value_set_boolean (value, h264meta->sei_parsing);
      break;
    case PROP_HW_TIME_OFFSET:
      g_value_set_int64 (value, h264meta->timesync_offset);
      break;      
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
gst_h264_meta_format_from_caps (GstCaps *caps, guint *format, guint *align) {

  g_return_if_fail (gst_caps_is_fixed (caps));

  GST_DEBUG ("parsing caps: %" GST_PTR_FORMAT, caps);

  if (caps && gst_caps_get_size (caps) > 0) {
    GstStructure *s = gst_caps_get_structure (caps, 0);
    const gchar *str = NULL;

    if (format) {
      if ((str = gst_structure_get_string (s, "stream-format"))) {
        if (strcmp (str, "avc") == 0)
          *format = GST_H264_META_FORMAT_AVC;
        else if (strcmp (str, "byte-stream") == 0)
          *format = GST_H264_META_FORMAT_BYTE;
        else
          *format = GST_H264_META_FORMAT_NONE;
      }
    }

    if (align) {
      if ((str = gst_structure_get_string (s, "alignment"))) {
        if (strcmp (str, "au") == 0)
          *align = GST_H264_META_ALIGN_AU;
        else if (strcmp (str, "nal") == 0)
          *align = GST_H264_META_ALIGN_NAL;
      }
    }
  }
}


static gboolean
gst_h264_meta_set_caps (GstBaseParse *parse, GstCaps *caps) {
  GstH264meta *h264meta = GST_H264_META (parse);
  GstCaps *old_caps;
  GstStructure *str;
  const GValue *codec_data_value;
  GstBuffer *codec_data = NULL;
  guint format = GST_H264_META_FORMAT_NONE;
  guint align  = GST_H264_META_ALIGN_NONE;

  GST_DEBUG_OBJECT (h264meta, "parsing caps: %" GST_PTR_FORMAT, caps);

  old_caps = gst_pad_get_current_caps (GST_BASE_PARSE_SINK_PAD (parse));

  if (old_caps) {
    if (!gst_caps_is_equal (old_caps, caps))
      gst_h264_meta_reset_stream_info (h264meta);
    gst_caps_unref (old_caps);
  }

  str = gst_caps_get_structure (caps, 0);
  // Parse the caps and get h264 width and height 
  gst_structure_get_int (str, "width", &h264meta->width);
  gst_structure_get_int (str, "height", &h264meta->height);
   
  // Get upstream format and align from caps 
  gst_h264_meta_format_from_caps (caps, &format, &align);
  codec_data_value = gst_structure_get_value (str, "codec_data");

  if (format == GST_H264_META_FORMAT_NONE) {
    // codec_data implies avc 
    if (codec_data_value != NULL) {
      GST_ERROR ("video/x-h264 caps with codec_data but no stream-format=avc");
      format = GST_H264_META_FORMAT_AVC;
    } else {
      // otherwise assume bytestream input
      GST_ERROR ("video/x-h264 caps without codec_data or stream-format");
      format = GST_H264_META_FORMAT_BYTE;
    }
  }
  /* avc caps sanity checks */
  if (format == GST_H264_META_FORMAT_AVC) {
    if (codec_data_value == NULL)
      goto refuse_caps;

    /* AVC implies alignment=au, everything else is not allowed */
    if (align == GST_H264_META_ALIGN_NONE)
      align = GST_H264_META_ALIGN_AU;
    else if (align != GST_H264_META_ALIGN_AU)
      goto refuse_caps;
  }

  /* bytestream caps sanity checks */
  if (format == GST_H264_META_FORMAT_BYTE) {
    /* should have SPS/PSS in-band (and/or oob in streamheader field) */
    if (codec_data_value != NULL)
      goto refuse_caps;
  }

  if (codec_data_value != NULL) {
    GstMapInfo map;
    guint8 *data;
    gsize size;

    GST_DEBUG_OBJECT (h264meta, "have packetized h264");
    /* make note for optional split processing */
    h264meta->packetized = TRUE;

    /* codec_data field should hold a buffer */
    if (!GST_VALUE_HOLDS_BUFFER (codec_data_value))
      goto refuse_caps;

    codec_data = gst_value_get_buffer (codec_data_value);
    if (!codec_data)
      goto refuse_caps;
    gst_buffer_map (codec_data, &map, GST_MAP_READ);
    data = map.data;
    size = map.size;

    /* parse the avcC data */
    if (size < 7) {             /* when numSPS==0 and numPPS==0, length is 7 bytes */
      gst_buffer_unmap (codec_data, &map);
      goto refuse_caps;
    }

    // Check codec_data format from ISO 14496-15 Section 5.2.4.1.1
    // Parse the version, this must be 1 
    if (data[0] != 1) {
      gst_buffer_unmap (codec_data, &map);
      goto refuse_caps;
    }

    h264meta->nal_length_size = (data[4] & 0x03) + 1;

    GST_WARNING_OBJECT (h264meta, "nal length size %u", h264meta->nal_length_size);
    gst_buffer_unmap (codec_data, &map);
    // Store this codec data 
    gst_buffer_replace (&h264meta->codec_data_in, codec_data);

  } else if (format == GST_H264_META_FORMAT_BYTE) {
     /* nothing to pre-process */
    h264meta->packetized = FALSE;
    // byte stream is using 4 sync bytes 
    h264meta->nal_length_size = 4;
  }

  h264meta->format = format;
  h264meta->align  = align;

  // We are not transform the caps and buffer 
  gst_pad_set_caps (GST_BASE_PARSE_SRC_PAD (h264meta), caps);
  GST_WARNING_OBJECT (h264meta, "setting sink caps %" GST_PTR_FORMAT, caps);
  return TRUE;

refuse_caps:
  GST_WARNING_OBJECT (h264meta, "refused caps %" GST_PTR_FORMAT, caps);
  return FALSE;
}


static GstCaps *
gst_h264_meta_get_caps (GstBaseParse *parse, GstCaps *filter) {
  GstCaps *peercaps, *templ;
  GstCaps *res;

  templ = gst_pad_get_pad_template_caps (GST_BASE_PARSE_SINK_PAD (parse));
  peercaps = gst_pad_peer_query_caps (GST_BASE_PARSE_SRC_PAD (parse), filter);

  if (peercaps) {
    res = gst_caps_intersect_full (peercaps, templ, GST_CAPS_INTERSECT_FIRST);
    gst_caps_unref (peercaps);
    gst_caps_unref (templ);
  } else {
    res = templ;
  }

  if (filter) {
    GstCaps *tmp = gst_caps_intersect_full (res, filter, GST_CAPS_INTERSECT_FIRST);
    gst_caps_unref (res);
    res = tmp;
  }

  GST_DEBUG_OBJECT (parse, "sink getcaps returning caps %" GST_PTR_FORMAT, res);

  return res;
}

static gboolean 
gst_h264_meta_sei_insert_unregistered_user_data(GstH264meta *h264meta, 
                                                GstBaseParseFrame *frame) {
  GstBuffer *old_buffer; 
  GstBuffer *new_buffer;
  GstByteWriter bw;
  gboolean ok;
  gint32 i, sei_pos; 
  gsize sei_size; 
  gint32 uuid_size; 
  GstMapInfo map;

  GstBuffer *frame_meta_buffer =  NULL;
  guint8 *frame_metadata       =  NULL;
  gsize frame_metasize        = 0;
  
  const gboolean bs = (h264meta->format == GST_H264_META_FORMAT_BYTE);
  const gint nls    = (4 - h264meta->nal_length_size); 

  old_buffer = frame->buffer;
  sei_pos    = h264meta->sei_insertion_position;

  // No valid position 
  if(sei_pos <= 0 || (gsize)sei_pos >= gst_buffer_get_size (old_buffer)){
    GST_WARNING_OBJECT(h264meta, "Invalid SEI candidate position %d ", sei_pos);
    return FALSE;
  }

  g_mutex_lock(&h264meta->meta_mutex);
  sei_size   = h264meta->system_meta.pos;
  
  // Insert timemeta after system meta
  guint32 now = timesync_time_since_start();
  gchar *hw_time_now    = g_strdup_printf("%d",now);
  gchar *hw_time_offset = g_strdup_printf("%"G_GINT64_FORMAT, h264meta->timesync_offset);
  gchar *hw_time_meta   = g_strconcat("/hwtime:", hw_time_now,",",hw_time_offset, NULL);

  g_free(hw_time_now);
  g_free(hw_time_offset);
  
  // Fetch one frame meta 
  if(!g_queue_is_empty(h264meta->frame_meta_queue)) {
    frame_meta_buffer = g_queue_pop_head(h264meta->frame_meta_queue);
    gst_buffer_map (frame_meta_buffer, &map, GST_MAP_READ);  
    frame_metadata = (guint8 *)map.data;
    frame_metasize = map.size;  
  }

  g_mutex_unlock(&h264meta->meta_mutex);

  uuid_size = sizeof(aeryon_uuid);
  // Add the aeryon_uuid into the begining of meta 
  sei_size += uuid_size;
  sei_size += frame_metasize;
  sei_size += (int)strlen(hw_time_meta);

  // Create a byte writer to insert extra SEI data into video es 
  gst_byte_writer_init_with_size (&bw, gst_buffer_get_size (old_buffer) + sei_size, FALSE);

  // Copy upto pos to byte writer
  ok = gst_byte_writer_put_buffer (&bw, old_buffer, 0, sei_pos);
  
  GST_DEBUG_OBJECT (h264meta, "inserting metadata at pos %d ", sei_pos);
  GST_DEBUG_OBJECT (h264meta, "metadata %s ", h264meta->system_meta.data);
  
  // Write NAL start code 
  if (bs) {
    ok &= gst_byte_writer_put_uint32_be (&bw, 0x1);
  } else {
    ok &= gst_byte_writer_put_uint32_be (&bw, (sei_size << (nls * 8)));
    ok &= gst_byte_writer_set_pos (&bw, gst_byte_writer_get_pos (&bw) - nls);
  }

  // NAL type
  ok &= gst_byte_writer_put_uint8 (&bw, GST_H264_META_NAL_SEI);
  // SEI type
  ok &= gst_byte_writer_put_uint8 (&bw, GST_H264_META_SEI_USER_DATA_UNREGISTERED); 

  // SEI payload size 
  for( i = 0; i <= (gint)(sei_size-255); i += 255 )
    gst_byte_writer_put_uint8( &bw, 0xFF );

  if((gint)(sei_size-i) > 0)
    gst_byte_writer_put_uint8( &bw, sei_size-i);
  
  // Embed 16 bytes Aeryon UUID first 
  for(i = 0; i < uuid_size; i++) {
    gst_byte_writer_put_uint8( &bw, aeryon_uuid[i]);  
  }
  g_mutex_lock(&h264meta->meta_mutex);
  // Embed system metadata  
  for( i = 0; i < h264meta->system_meta.pos; i++) {
    ok &= gst_byte_writer_put_uint8(&bw, h264meta->system_meta.data[i]);
  }
  // Embed time metadata  
  for( i = 0; i < (int)strlen(hw_time_meta); i++) {
    ok &= gst_byte_writer_put_uint8(&bw, hw_time_meta[i]);
  }

  // Embed frame meta 
  if(frame_metadata && frame_metasize > 0) {
    for( i = 0; i < (gint32)frame_metasize; i++) {
      ok &= gst_byte_writer_put_uint8(&bw, frame_metadata[i]);
    }
  }
  g_mutex_unlock(&h264meta->meta_mutex);
  // Write rbsp trailing bytes 
  ok &= gst_byte_writer_put_uint8(&bw, 0x80);
  
  // Copy the remaining of original frame into new buffer 
  ok &= gst_byte_writer_put_buffer (&bw, old_buffer, sei_pos, -1);
  new_buffer = gst_byte_writer_reset_and_get_buffer (&bw);
  // Copy the meta into new buffer 
  ok &= gst_buffer_copy_into (new_buffer, old_buffer, GST_BUFFER_COPY_METADATA, 0, -1);
  GST_BUFFER_FLAG_UNSET (new_buffer, GST_BUFFER_FLAG_DELTA_UNIT);
  ok &= gst_buffer_replace (&frame->out_buffer, new_buffer);

#if 0
  GstMapInfo map;
  gsize size; 
  guint8 *data;

  gst_buffer_map (new_buffer, &map, GST_MAP_READ);
  data = map.data;
  size = map.size;     

  for (i = 0; i < (int)size; i++){
    if((i != 0) && (i % 32 == 0))
      fprintf(stderr,"\n");
    fprintf(stderr, "%02X ", (guint8)(data[i]));
  }
  
  gst_buffer_unmap (new_buffer, &map);
#endif

  gst_buffer_unref (new_buffer);
  if(frame_meta_buffer) {
    gst_buffer_unmap (frame_meta_buffer, &map);  
    gst_buffer_unref(frame_meta_buffer);
  }

  g_free(hw_time_meta);
  return ok;
}


static gboolean
gst_h264_meta_process_nal (GstH264meta *h264meta, GstH264NalUnit *nalu)
{
  guint nal_type;
  GstH264NalParser *nalparser = h264meta->nalparser;
  GstH264ParserResult pres;
  GstH264PPS pps;
  GstH264SPS sps;

  GST_DEBUG_OBJECT (h264meta, "parsing collected nal");

  // Nothing to do for broken input
  if (G_UNLIKELY (nalu->size < 2)) {
    GST_DEBUG_OBJECT (h264meta, "not processing nal size %u", nalu->size);
    return TRUE;
  }

  memset(&pps, 0, sizeof(pps));
  memset(&sps, 0, sizeof(sps));

  nal_type = nalu->type;
  GST_DEBUG_OBJECT (h264meta, "processing nal of type %u %s, size %u", nal_type, _nal_name (nal_type), nalu->size);

  switch (nal_type) {
    case GST_H264_NAL_SUBSET_SPS:
      if(!h264meta->have_sps) //expect to receive SPS first 
        return FALSE;
      pres = gst_h264_parser_parse_subset_sps (nalparser, nalu, &sps, TRUE);
      goto process_sps;
      break;
    case GST_H264_NAL_SPS:
      pres = gst_h264_parser_parse_sps (nalparser, nalu, &sps, TRUE);
    process_sps:
      if (pres != GST_H264_PARSER_OK) {
        GST_ERROR_OBJECT(h264meta, "failed to parse SPS");
        return FALSE;
      }
      h264meta->have_sps = TRUE;
      break;
    case GST_H264_NAL_PPS:
      // Expect PPS is after SPS
      if(!h264meta->have_sps) //expect to receive SPS first 
        return FALSE;
      pres = gst_h264_parser_parse_pps (nalparser, nalu, &pps);
      // Invalid PPS, exiting 
      if (pres != GST_H264_PARSER_OK) {
        GST_ERROR_OBJECT (h264meta, "failed to parse PPS:");
        if (pres != GST_H264_PARSER_BROKEN_LINK)
          return FALSE;
      }
      h264meta->have_pps = TRUE;
      break;
    case GST_H264_NAL_SEI:
      if(!h264meta->have_sps) //expect to receive SPS first 
        return FALSE;
      h264meta->have_sei = TRUE;
      h264meta->orig_sei_offset = nalu->offset;
      h264meta->orig_sei_size = nalu->size;   
      break;
    default:
      break;
  }

  return TRUE;
}

// Check whether our parsing has been complete 
static gboolean
gst_h264_meta_parse_complete(GstH264meta *h264meta, const guint8 * data, guint size, GstH264NalUnit *nalu) {
  gboolean complete = FALSE;
  GstH264ParserResult parse_res;
  GstH264NalUnitType nal_type = nalu->type;
  GstH264NalUnit nnalu;
  gboolean picture_start = FALSE;

  parse_res = gst_h264_parser_identify_nalu_unchecked (h264meta->nalparser,
      data, nalu->offset + nalu->size, size, &nnalu);

  if (parse_res != GST_H264_PARSER_OK)
    return FALSE;

  picture_start |= (nal_type == GST_H264_NAL_SLICE ||
      nal_type == GST_H264_NAL_SLICE_DPA || nal_type == GST_H264_NAL_SLICE_IDR);  

  nal_type = nnalu.type;
  complete = picture_start &&
             ((nal_type >= GST_H264_NAL_SEI && nal_type <= GST_H264_NAL_AU_DELIMITER) ||
            (nal_type >= GST_H264_NAL_PREFIX_UNIT && nal_type <= 18)); // 16-18 is reserved NAL type

  GST_LOG_OBJECT (h264meta, "next nal type: %d %s", nal_type, _nal_name (nal_type));

  complete |= picture_start && (nal_type == GST_H264_NAL_SLICE
      || nal_type == GST_H264_NAL_SLICE_DPA
      || nal_type == GST_H264_NAL_SLICE_IDR) &&
      // first_mb_in_slice == 0 considered start of frame 
      (nnalu.data[nnalu.offset + nnalu.header_bytes] & 0x80);
  
  return complete;
}

// The actual function which will parse NALs
static GstFlowReturn
gst_h264_meta_handle_frame (GstBaseParse *parse, GstBaseParseFrame *frame, gint *skipsize) {

  GstH264meta *h264meta;
  guint8 *data;
  gsize size;
  GstFlowReturn ret = GST_FLOW_OK;
  GstMapInfo map;
  GstBuffer *buffer;
  gint current_off = 0;
  gboolean drain, no_next;
  GstH264ParserResult parse_res;
  GstH264NalParser *nalparser;
  GstH264NalUnit nalu;
  gboolean found_sei_position = false;
  
  h264meta = GST_H264_META (parse);
  
  nalparser = h264meta->nalparser;
  buffer    = frame->buffer;
  gst_buffer_map (buffer, &map, GST_MAP_READ);

  data = map.data;
  size = map.size;

  // Expect at least 3 bytes startcode and 2 bytes NAL payload
  if (G_UNLIKELY (size < 5)) {
    gst_buffer_unmap (buffer, &map);
    *skipsize = 1;
    return GST_FLOW_OK;
  }

  if (G_UNLIKELY (h264meta->format == GST_H264_META_FORMAT_NONE)) {
    GST_ERROR_OBJECT(h264meta, "Unknown buffer format");
    *skipsize = 0;
    goto handle_frame_done;
  }
  // Avoid stale cached parsing state 
  if (frame->flags & GST_BASE_PARSE_FRAME_FLAG_NEW_FRAME) {
    GST_LOG_OBJECT (h264meta, "parsing new frame");
    gst_h264_meta_reset_frame (h264meta);
  } else {
    GST_LOG_OBJECT (h264meta, "resuming frame parsing");
  }

  h264meta->is_key_frame = 
    (GST_BUFFER_FLAG_IS_SET(buffer, GST_BUFFER_FLAG_DELTA_UNIT) == 0);
  
  if(!h264meta->is_key_frame) {
    GST_DEBUG_OBJECT(h264meta, "Done parsing: not a key frame");
    *skipsize = 0;
    goto handle_frame_done;
  }
  // drain? Does this frame is close to the end of stream? 
  // Always consume the entire input buffer when in_align == ALIGN_AU 
  drain = GST_BASE_PARSE_DRAINING (parse)
      || h264meta->align == GST_H264_META_ALIGN_AU;

  no_next = FALSE;
  current_off = h264meta->current_off;

  if (current_off < 0)
    current_off = 0;

  if (current_off >= (gint)size) {
     GST_ERROR_OBJECT(h264meta, "Buffer overflow during parsing");
    goto handle_frame_done;
  }
  GST_DEBUG_OBJECT (h264meta, "last parse position %d", current_off);

  // Check for initial skip
  if (h264meta->current_off == -1) {
    parse_res = gst_h264_parser_identify_nalu_unchecked (nalparser, data, current_off, size, &nalu);

    switch (parse_res) {
      case GST_H264_PARSER_OK:
        if (nalu.sc_offset > 0) {
          // Skip the unimportant data before the first valid NAL 
          *skipsize = nalu.sc_offset;
          goto skip;
        }
        break;
      case GST_H264_PARSER_NO_NAL: 
        *skipsize = size - 3; // Skip the whole frame
        goto skip;
        break;
      default:
        goto handle_frame_done;
        break;
    }
  }
  
  while (!found_sei_position) {

    parse_res = gst_h264_parser_identify_nalu (nalparser, data, current_off, size, &nalu);

    switch (parse_res) {
      case GST_H264_PARSER_OK:
        GST_DEBUG_OBJECT (h264meta, "complete nal (offset, size): (%u, %u) : key_frame: %d SPS:%d PPS:%d ",
            nalu.offset, 
            nalu.size, 
            h264meta->is_key_frame,
            h264meta->have_sps,
            h264meta->have_pps);
        break;
      case GST_H264_PARSER_NO_NAL_END:
        GST_DEBUG_OBJECT (h264meta, "not a complete nal found at offset %u", nalu.offset);
        // if draining, accept it as complete nal 
        if (drain) {
          no_next = TRUE;
          nalu.size = size - nalu.offset;
          GST_DEBUG_OBJECT (h264meta, "draining, accepting with size %u", nalu.size);

          if (nalu.size < 2) {
            if(current_off == 0) {
              GST_DEBUG_OBJECT (h264meta, "skipping broken nal");
              *skipsize = nalu.offset;
              goto skip;
            } else {
              GST_DEBUG_OBJECT (h264meta, "terminating au");
              nalu.size = 0;
              nalu.offset = nalu.sc_offset;
              goto handle_frame_done;
            }
          } 
        }
        // Otherwise need more
        goto more;
        break;
      default:
        goto handle_frame_done;
        break;
    }
    
    // simulate no next nal if none needed
    no_next = no_next || (h264meta->align == GST_H264_META_ALIGN_NAL);
    
    GST_DEBUG_OBJECT (h264meta, "%p complete nal found. Off: %u, Size: %u, no_next: %d ",
        data, nalu.offset, nalu.size, no_next);

    if (!no_next) {
      if (nalu.offset + nalu.size + 4 + 2 > size) {
        GST_DEBUG_OBJECT (h264meta, "not enough data for next NALU");
        if (drain) {
          GST_DEBUG_OBJECT (h264meta, "but draining anyway");
          no_next = TRUE;
        } else {
          goto handle_frame_done;
        }
      }
    }

    if(!gst_h264_meta_process_nal(h264meta, &nalu)) {
      GST_WARNING_OBJECT (h264meta, "Unexpected error during parsing NALs ");
      goto handle_frame_done;
    }

    if(h264meta->have_sps && h264meta->have_pps) {
      if(h264meta->sei_insertion_position <= 0) {
        h264meta->sei_insertion_position = nalu.offset + nalu.size;
        GST_DEBUG_OBJECT (h264meta, "sei_insertion_position = %d ", h264meta->sei_insertion_position);
      }
      // Continue the parsing until we found the sei offset and data 
      if(!h264meta->sei_parsing || h264meta->have_sei) {
        found_sei_position = TRUE;
        break;
      }  
    }

    // no next NAl, parsing h264 stream is done 
    if (no_next)
      break;
    
    // Still next NAL, check whether we are the 2nd last NAL
    if (gst_h264_meta_parse_complete (h264meta, data, size, &nalu))
      break;

    current_off = nalu.offset + nalu.size;
    GST_DEBUG_OBJECT (h264meta, "Looking for more, current offset = %d ", current_off);
  } // while (!found_sei_position)

  // Restart parsing from here next time  
  if (current_off > 0)
    h264meta->current_off = current_off;


handle_frame_done:
  gst_buffer_unmap (buffer, &map);
  ret = gst_base_parse_finish_frame (parse, frame, map.size);
  return ret;

more:
  *skipsize = 0;
  gst_buffer_unmap (buffer, &map);
  return GST_FLOW_OK;

skip:
  GST_DEBUG_OBJECT (h264meta, "skipping %d", *skipsize);
  gst_buffer_unmap (buffer, &map);
  return GST_FLOW_OK;
}

static GstFlowReturn
gst_h264_meta_pre_push_frame (GstBaseParse * parse, GstBaseParseFrame *frame) {
  GstH264meta *h264meta = GST_H264_META (parse);
  GstBuffer *buffer = frame->buffer;

  GST_DEBUG_OBJECT (h264meta, "pre_push_frame");
  
  //Collect h264 stats 
  g_mutex_lock (&h264meta->stats_mutex);
  if(buffer) {
    h264meta->stats_frame_sum++;
    h264meta->stats_bytes_sum += gst_buffer_get_size(buffer); 
    if(GST_CLOCK_TIME_IS_VALID(GST_BUFFER_PTS(buffer))) 
      h264meta->last_valid_pts  = GST_BUFFER_PTS(buffer);
  }

  g_mutex_unlock (&h264meta->stats_mutex);

  if(h264meta->sei_parsing && h264meta->have_sei && frame->buffer) {
    GstMapInfo map;

    guint8 *data;
    gsize size;
    gst_buffer_map (buffer, &map, GST_MAP_READ);
    data = map.data;
    size = map.size;

    if(data && h264meta->orig_sei_offset >=0 && h264meta->orig_sei_offset < (gint)size  
      && h264meta->orig_sei_size >0 && h264meta->orig_sei_size < size) {
      data += h264meta->orig_sei_offset;
      if (h264meta->orig_sei_size > MAX_META_SIZE)
        h264meta->orig_sei_size = MAX_META_SIZE;
      guint32 payload_size = 0, i = 0;
      
      data += 2; // Skip NAL type and SEI type

      while(i < h264meta->orig_sei_size && data[i] == 0xFF) {
        payload_size += 0xFF;
        i++;
      }
      payload_size += data[i];
      i++;
      // move the data to start of payload 
      data += i;

      // Simply copy the original meta
      memcpy(&h264meta->orig_sei_data, data, h264meta->orig_sei_size);
      GST_INFO_OBJECT(h264meta, "SEI from upstream begins: payload_size = %d", payload_size);
      gchar *sei_str = g_strndup((gchar*)data, payload_size); 
      if(sei_str) {
        GST_INFO_OBJECT(h264meta, "%s", sei_str);
        g_free(sei_str);
      }
      GST_INFO_OBJECT(h264meta, "SEI from upstream ends");

#if 0
      data = map.data + h264meta->orig_sei_offset;
      for (i = 0; i < h264meta->orig_sei_size; i++) {
        if((i != 0) && (i % 32 == 0))
          fprintf(stderr,"\n");
        fprintf(stderr, "%02X ", (guint8)(data[i]));
      }
      fprintf(stderr,"\n");
#endif
    }

    gst_buffer_unmap (buffer, &map);  
  }


  // Put custom meta only on key frames 
  if(h264meta->is_key_frame && h264meta->have_sps)  
    gst_h264_meta_sei_insert_unregistered_user_data(h264meta, frame);

  gst_h264_meta_reset_frame (h264meta);

  return GST_FLOW_OK;
}


static gboolean
gst_h264_meta_sink_event (GstBaseParse *parse, GstEvent *event) {
  GstH264meta *h264meta = GST_H264_META (parse);

  GST_DEBUG_OBJECT (h264meta, "sink_event");
  // Simply use the sink event handler from parent class 
  return GST_BASE_PARSE_CLASS (parent_class)->sink_event (parse, event);;
}

static gboolean
gst_h264_meta_src_event (GstBaseParse *parse, GstEvent *event) {
  GstH264meta *h264meta = GST_H264_META (parse);

  GST_DEBUG_OBJECT (h264meta, "src_event");
  // Simply use the src event handler from parent class 
  return GST_BASE_PARSE_CLASS (parent_class)->src_event (parse, event);;
}

void gst_h264_meta_set_system_meta(GstH264meta *h264meta, 
  const char *buffer, gint32 size) {
  if(!buffer || size <= 0 || size > MAX_META_SIZE) {
    GST_ERROR_OBJECT (h264meta, "Set meta failed");
    return; 
  }

  gchar *meta_str = g_strndup((gchar*)buffer, size); 
  if(meta_str) {
    GST_INFO_OBJECT(h264meta, "%s", meta_str);
    g_free(meta_str);
  }

  g_mutex_lock(&h264meta->meta_mutex);
  memset(&(h264meta->system_meta), 0, sizeof(h264meta->system_meta));
  memcpy(&(h264meta->system_meta.data), buffer, size);
  h264meta->system_meta.pos = size;
  g_mutex_unlock(&h264meta->meta_mutex);
}

void gst_h264_meta_set_frame_meta(GstH264meta *h264meta, const char *buffer, gint32 size){
  if(!buffer || size <= 0 || size > MAX_META_SIZE) {
      GST_DEBUG_OBJECT (h264meta, "Set meta failed, size %d", size);
      return;
  }
  GstBuffer *frame_meta_buffer = gst_buffer_new_allocate (NULL, size, NULL);
  if(!frame_meta_buffer) {
    GST_ERROR_OBJECT (h264meta, "Create buffer failed");
    return;
  }
  //Copy frame meta
  gst_buffer_fill(frame_meta_buffer, 0, buffer, size);
  gst_buffer_set_size(frame_meta_buffer, size);

  g_mutex_lock(&h264meta->meta_mutex);
  g_queue_push_tail(h264meta->frame_meta_queue, frame_meta_buffer);
  g_mutex_unlock(&h264meta->meta_mutex);
}
