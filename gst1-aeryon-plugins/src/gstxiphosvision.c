#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <aeryon_logging.h>
#include "gstxiphosvision.h"
#include <gst/base/base.h>
#include <gst/controller/controller.h>
#include "msg_types.h"
#include "cam_messages.h"
#include "gstaeryonbuffermeta.h"
#include "gstaeryonevent.h"
#include "gstaeryonmessage.h"
#include <gst/video/video.h>
#include <sys/time.h>
#include "xiphos.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <inttypes.h>
#include "app_status.h"
#include <math.h>
#include "targetinjector.h"
#include "errno.h"
#include "string.h"
#include "gmbl_math.h"
#include "aeryon_math.h"
#include "gps_converter.h"
#include "gen_utils.h"

GST_DEBUG_CATEGORY(gst_xiphos_vision_debug);
#define GST_CAT_DEFAULT gst_xiphos_vision_debug
#define MAX_SEARCH_TIME_MS 8000
#define MAX_CPU_TEMP_DEG 80.0
#define TEMP_HYSTERESIS_DEG 3.0
#define VISION_BUFFER_SECONDS 0.1
#define IR_PALETTE_COUNT 2
#define MAX_TRACK_PITCH DEG_TO_RAD(80)

static GMutex xiphos_update_mutex;

#if 1
#define VISION_LOG(level, stream, obj, format, ...)	GST_CAT_LEVEL_LOG (GST_CAT_DEFAULT, level, obj,  "Stream [%d] - " format "\n", stream, ##__VA_ARGS__)
#else
#define VISION_LOG(level, stream, obj, format, ...)	fprintf(stderr, "Stream [%d] - " format "\n", stream, ##__VA_ARGS__)
#endif

#if 0
static uint32_t getCurrentTimeMs()
{
  struct timeval currentTimeval;
  uint32_t currentTimestamp; // milliseconds
  gettimeofday(&currentTimeval, NULL);
  currentTimestamp = currentTimeval.tv_sec * 1000 + currentTimeval.tv_usec/1000;
  return currentTimestamp;
}
#endif

/* Filter signals and args */
enum
{
    /* FILL ME */
    LAST_SIGNAL
};

enum
{
    PROP_0,
    PROP_SILENT,
    PROP_DRAW,
    PROP_STREAM_ID,
    PROP_STAB,
    PROP_SENSOR,
    PROP_TARGET_INJECTOR,
    PROP_TARGET_EJECTOR,
    PROP_STABLE_EJECTOR,
    PROP_UPDATE_DELAY,
    PROP_APC_APP_ID,
    PROP_STATS
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE("sink",
                                                                   GST_PAD_SINK,
                                                                   GST_PAD_ALWAYS,
                                                                   GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ I420, UYVY, YUY2 }")));

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE("src",
                                                                 GST_PAD_SRC,
                                                                 GST_PAD_ALWAYS,
                                                                 GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ I420}")));


static void
gst_xiphos_vision_write_track(GstObject * parent, int ts, int num, int id, int lock, xipvis_bb *bbs)
{
    GstXiphosVision *filter;
    filter = GST_XIPHOS_VISION(parent);

    // track entry header
    if (filter->target_ejector_cnt > 0)
    {
        fprintf(filter->target_ejector_fd, ",\n");
    }
    fprintf(filter->target_ejector_fd, "    {\n");
    fprintf(filter->target_ejector_fd, "      \"time\": %d,\n"
                                       "      \"state\": %d,\n"
                                       "      \"error_flags\": %d,\n"
                                       "      \"num_targets\": %d,\n"
                                       "      \"image_id\": %d,\n"
                                       "      \"image_w\": %d,\n"
                                       "      \"image_h\": %d,\n"
                                       "      \"lock\": %d,\n"
                                       "      \"targets\": [\n",
            ts,
            filter->tracker_config.state,
            filter->error_flags,
            num,
            id,
            filter->in_info.width,
            filter->in_info.height,
            lock);
    int i = 0;
    for (i = 0; i < num; i++)
    {
        // target entry
        int w = bbs[i].xmax - bbs[i].xmin;
        int h = bbs[i].ymax - bbs[i].ymin;
        fprintf(filter->target_ejector_fd, "        {\n"
                                           "          \"id\": %d,\n"
                                           "          \"confidence\": %0.1f,\n"
                                           "          \"x\": %d,\n"
                                           "          \"y\": %d,\n"
                                           "          \"w\": %d,\n"
                                           "          \"h\": %d\n"
                                           "        }%s\n",
                bbs[i].id,
                bbs[i].confidence,
                bbs[i].xmin + w / 2,
                bbs[i].ymin + h / 2,
                w,
                h,
                (i + 1 < num) ? "," : "");
    }
    // track entry footer
    fprintf(filter->target_ejector_fd, "      ]\n    }");
    filter->target_ejector_cnt++;
}

static void
gst_xiphos_vision_write_stabilize_solution(GstObject * parent, int id)
{
    GstXiphosVision *filter;
    filter = GST_XIPHOS_VISION(parent);

    xipvis_print_stabilization_solution(filter->xipvis_, filter->stable_ejector_fd, id);
}

static void
gst_xiphos_vision_set_cpu_freq(GstObject * parent)
{
    GstXiphosVision *filter;
    filter = GST_XIPHOS_VISION(parent);
    perf_mon_cpu_freq_t freq;
    freq.cpu_freq = 1530000;
    freq.app_id = filter->apc_app_id;
    filter->hi_power = TRUE;

    GstMessage *msg = gst_message_new_apc(
        parent,
        MSG_PERF_MON_SET_CPU_FREQ_APC_CLASS,
        MSG_PERF_MON_SET_CPU_FREQ_APC_ID,
        &freq,
        sizeof (perf_mon_cpu_freq_t));
    gst_element_post_message(GST_ELEMENT(parent), msg);
}

static void
gst_xiphos_vision_unset_cpu_freq(GstObject * parent)
{
    GstXiphosVision *filter;
    filter = GST_XIPHOS_VISION(parent);
    perf_mon_cpu_freq_t freq;
    freq.cpu_freq = 0;
    freq.app_id = filter->apc_app_id;
    filter->hi_power = FALSE;

    GstMessage *msg = gst_message_new_apc(
        parent,
        MSG_PERF_MON_UNSET_CPU_FREQ_APC_CLASS,
        MSG_PERF_MON_UNSET_CPU_FREQ_APC_ID,
        &freq,
        sizeof (perf_mon_cpu_freq_t));
    gst_element_post_message(GST_ELEMENT(parent), msg);
}

static void
gst_xiphos_vision_free_pts(gpointer *pts_id_head)
{
    if (*pts_id_head == NULL)
        return;
    g_free(*pts_id_head);
    *pts_id_head = NULL;
}

static void
gst_xiphos_vision_init_pts(gpointer *pts_id_head, gint num_pst_id_elems)
{
    if (*pts_id_head != NULL)
        gst_xiphos_vision_free_pts(pts_id_head);
    *pts_id_head = g_malloc0(num_pst_id_elems * sizeof (GstXiphosPtsIdElement));
}

static gint
gst_xiphos_vision_get_id_from_pts(GstXiphosVision * filter, long pts, int * id)
{
    int i;
    GstXiphosPtsIdElement* elem = (GstXiphosPtsIdElement*) filter->pts_id_head;
    int itr = filter->pts_id_cur;
    for (i = 0; i < filter->num_pst_id_elems; i++)
    {
        if (itr < 0)
            itr = filter->num_pst_id_elems;
        *id = elem[itr].image_id;
        if (elem[itr].pipe_ts == pts)
            return 1;
        itr--;
    }
    return 0;
}

static void
gst_xiphos_vision_push_pts_id(GstXiphosVision * filter, long pts, int id)
{
    if (filter->pts_id_head == NULL)
    {
        VISION_LOG(GST_LEVEL_ERROR, filter->stream_id, filter, "Attempted to push image id into non-existent buffer.");
        return;
    }

    if (((GstXiphosPtsIdElement*) filter->pts_id_head)[filter->pts_id_cur].image_id >= id)
    {
        // Reset the buffer because vision has re-initialized
        memset(filter->pts_id_head, 0, filter->num_pst_id_elems * sizeof (GstXiphosPtsIdElement));
        filter->pts_id_cur = 0;
    }
    else
    {
        filter->pts_id_cur++;
    }
    if (filter->pts_id_cur >= filter->num_pst_id_elems)
        filter->pts_id_cur = 0;
    ((GstXiphosPtsIdElement*) filter->pts_id_head)[filter->pts_id_cur].image_id = id;
    ((GstXiphosPtsIdElement*) filter->pts_id_head)[filter->pts_id_cur].pipe_ts = pts;
}

static int
gst_xiphos_vision_start_udot_from_ts(void* xipvis, GstXiphosVision* filter, long ts,
    GstVideoFrame *inframe, GstVideoFrame *outframe, GstVideoInfo *info, int draw, int nowarp)
{
    int id;
    int num;
    xipvis_bb bb;
    xipvis_bb *bbs;

    bb.xmin = (filter->request_tracker_config.pixel_x - filter->request_tracker_config.pixel_w / 2);
    bb.ymin = (filter->request_tracker_config.pixel_y - filter->request_tracker_config.pixel_h / 2);
    bb.xmax = (filter->request_tracker_config.pixel_x + filter->request_tracker_config.pixel_w / 2);
    bb.ymax = (filter->request_tracker_config.pixel_y + filter->request_tracker_config.pixel_h / 2);


    long track_ts = (long) (filter->request_tracker_config.target_ts);
    g_mutex_lock(&xiphos_update_mutex);
    int rc = xipvis_update( xipvis,
                            draw,
                            inframe->data[0] + info->offset[0],
                            info->stride[0],
                            &id, &num, &bbs,
                            outframe->data[0],
                            UDOT_NORMAL);
    g_mutex_unlock(&xiphos_update_mutex);
    if(rc == 0)
    {
        gst_xiphos_vision_push_pts_id(filter, ts, id);
    }
    else
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Xipvis udot initial update failed");
        return -1;
    }

    int track_id = 0;
    if (gst_xiphos_vision_get_id_from_pts(filter, track_ts, &track_id))
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Found Image ID [%d] with TS [%ld]. Current Image ID[%d]" , track_id, track_ts, id);
    }
    else
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Could not find Image ID with TS [%ld]. Using Image ID [%d]. Current Image ID[%d]", track_ts, track_id, id);
    }

    if (xipvis_udot_start_track(xipvis, track_id, nowarp, &bb) < 0)
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Xipvis udot init failed");
        return -1;
    }
    if (xipvis_csrt_start_track(xipvis, id, 0, &bb, inframe->data[0] + info->offset[0], info->stride[0]) < 0)
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Xipvis csrt init failed");
        return -1;
    }
    if (xipvis_kcft_start_track(xipvis, id, 0, &bb, inframe->data[0] + info->offset[0], info->stride[0]) < 0)
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Xipvis kcft init failed");
        return -1;
    }
    return 0;
}

static int
gst_xiphos_vision_start_udot_from_id(void* xipvis, GstXiphosVision* filter, long ts, GstVideoFrame *inframe,
                                    GstVideoFrame *outframe, GstVideoInfo *info, int draw)
{
    int id;
    int num;
    xipvis_bb bb;
    xipvis_bb *bbs;
    int image_id = filter->request_tracker_config.image_id;

    bb.xmin = (filter->request_tracker_config.pixel_x - filter->request_tracker_config.pixel_w / 2);
    bb.ymin = (filter->request_tracker_config.pixel_y - filter->request_tracker_config.pixel_h / 2);
    bb.xmax = (filter->request_tracker_config.pixel_x + filter->request_tracker_config.pixel_w / 2);
    bb.ymax = (filter->request_tracker_config.pixel_y + filter->request_tracker_config.pixel_h / 2);

    g_mutex_lock(&xiphos_update_mutex);
    int rc = xipvis_update( xipvis,
                            draw,
                            inframe->data[0] + info->offset[0],
                            info->stride[0],
                            &id, &num, &bbs,
                            outframe->data[0],
                            UDOT_NORMAL);
    g_mutex_unlock(&xiphos_update_mutex);
    if (rc == 0)
    {
        gst_xiphos_vision_push_pts_id(filter, ts, id);
    }
    else
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Xipvis udot initial update failed");
    }

    VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Using MTI Image ID [%d]", image_id);
    if (xipvis_udot_start_track(xipvis, image_id, 0, &bb) < 0)
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Xipvis udot init failed");
        return -1;
    }
    if (xipvis_csrt_start_track(xipvis, image_id, 0, &bb, inframe->data[0] + info->offset[0], info->stride[0]) < 0)
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Xipvis csrt init failed");
        return -1;
    }
    VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Current Image ID [%d]", id);
    return 0;
}

static int
gst_xiphos_vision_start_mti(void* xipvis, GstXiphosVision* filter, long ts, GstVideoFrame *inframe,
        GstVideoFrame *outframe, GstVideoInfo *info, int draw)
{
    int id;
    int num;
    xipvis_bb *bbs;
    g_mutex_lock(&xiphos_update_mutex);
    int rc = xipvis_update( xipvis,
                            draw,
                            inframe->data[0] + info->offset[0],
                            info->stride[0],
                            &id, &num, &bbs,
                            outframe->data[0],
                            UDOT_NORMAL);
    g_mutex_unlock(&xiphos_update_mutex);
    if(rc == 0)
    {
        gst_xiphos_vision_push_pts_id(filter, ts, id);
    }
    else
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Xipvis mti initial update failed");
    }

    if (xipvis_enable_mti(xipvis) < 0)
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Xipvis mti init failed");
        return -1;
    }
    return 0;
}

// Get the vision parameters from xipvis
static void gst_xiphos_vision_get_vision_params(void* const xipvis, GstXiphosVision *filter)
{
    if (filter->xipvis_)
    {
        filter->vision_params.mti_decay_rate      = xipvis_mti_get_decay_rate(xipvis);
        filter->vision_params.mti_filter_rate     = xipvis_mti_get_filter_rate(xipvis);
        filter->vision_params.mti_max_age         = xipvis_mti_get_max_age(xipvis);
        filter->vision_params.mti_max_speed       = xipvis_mti_get_max_speed(xipvis);
        filter->vision_params.mti_min_age         = xipvis_mti_get_min_age(xipvis);
        filter->vision_params.mti_min_rad         = xipvis_mti_get_min_radius(xipvis);
        filter->vision_params.mti_sweep_size      = xipvis_mti_get_sweep_size(xipvis);
        filter->vision_params.mti_min_box_size    = xipvis_mti_get_min_box_size(xipvis);
        filter->vision_params.mti_blanking        = xipvis_mti_get_blanking(xipvis);

        filter->vision_params.udot_learn_rate      = xipvis_udot_get_learning_rate(xipvis);
        filter->vision_params.udot_max_fb_error    = xipvis_udot_get_max_fb_error(xipvis);
        filter->vision_params.udot_max_rad         = xipvis_udot_get_max_radius(xipvis);
        filter->vision_params.udot_rad_inc         = xipvis_udot_get_radius_increment(xipvis);
        filter->vision_params.udot_velocity_filter = xipvis_udot_get_velocity_filter(xipvis);
        filter->vision_params.udot_max_ssd         = xipvis_udot_get_max_ssd(xipvis);

        filter->vision_params.stbl_split_screen    = xipvis_stbl_get_split_screen(xipvis);

        filter->vision_params.stabilization       = xipvis_stabilization_enabled(xipvis);
    }
}

// Set the vision parameters to xipvis
static void gst_xiphos_vision_set_vision_params(void* const xipvis, GstXiphosVision *filter)
{
    if (filter->xipvis_)
    {
        xipvis_mti_set_decay_rate     (xipvis, filter->vision_params.mti_decay_rate);
        xipvis_mti_set_filter_rate    (xipvis, filter->vision_params.mti_filter_rate);
        xipvis_mti_set_max_age        (xipvis, filter->vision_params.mti_max_age);
        xipvis_mti_set_max_speed      (xipvis, filter->vision_params.mti_max_speed);
        xipvis_mti_set_min_age        (xipvis, filter->vision_params.mti_min_age);
        xipvis_mti_set_min_radius     (xipvis, filter->vision_params.mti_min_rad);
        xipvis_mti_set_sweep_size     (xipvis, filter->vision_params.mti_sweep_size);
        xipvis_mti_set_min_box_size   (xipvis, filter->vision_params.mti_min_box_size);
        xipvis_mti_set_blanking       (xipvis, filter->vision_params.mti_blanking);

        xipvis_udot_set_learning_rate   (xipvis, filter->vision_params.udot_learn_rate);
        xipvis_udot_set_max_fb_error    (xipvis, filter->vision_params.udot_max_fb_error);
        xipvis_udot_set_max_radius      (xipvis, filter->vision_params.udot_max_rad);
        xipvis_udot_set_radius_increment(xipvis, filter->vision_params.udot_rad_inc);
        xipvis_udot_set_velocity_filter (xipvis, filter->vision_params.udot_velocity_filter);
        xipvis_udot_set_max_ssd         (xipvis, filter->vision_params.udot_max_ssd);

        xipvis_stbl_set_split_screen(xipvis, filter->vision_params.stbl_split_screen);

        filter->stab = filter->vision_params.stabilization;
        if (filter->stab)
        {
            xipvis_enable_stabilization(filter->xipvis_);
        }
        else
        {
            xipvis_disable_stabilization(filter->xipvis_);
        }
    }
}

static void
gst_xiphos_vision_finalize(GObject *object);

#define gst_xiphos_vision_parent_class parent_class
G_DEFINE_TYPE (GstXiphosVision, gst_xiphos_vision, GST_TYPE_BASE_TRANSFORM);

static void gst_xiphos_vision_set_property(GObject * object, guint prop_id,
                                           const GValue * value, GParamSpec * pspec);
static void gst_xiphos_vision_get_property(GObject * object, guint prop_id,
                                           GValue * value, GParamSpec * pspec);
static void gst_xiphos_send_cam_tracker_targets(Metadata2 * meta, GstObject * parent);

static gint gst_xiphos_vision_fps_from_info(GstVideoInfo in_info);

static GstStateChangeReturn gst_xiphos_vision_change_state (GstElement * element, GstStateChange transition);

static gboolean gst_xiphos_vision_sink_event(GstBaseTransform * parent, GstEvent * event);
static gboolean gst_xiphos_vision_start(GstBaseTransform * parent);
static gboolean gst_xiphos_vision_stop(GstBaseTransform * parent);


static gboolean gst_xiphos_vision_set_caps (GstBaseTransform * trans, GstCaps * incaps, GstCaps * outcaps);

static GstCaps* gst_xiphos_vision_transform_caps (GstBaseTransform * btrans, GstPadDirection direction,
                                                GstCaps * caps, GstCaps * filter);

static gboolean gst_xiphos_vision_filter_meta (GstBaseTransform * trans, GstQuery * query,
                                            GType api, const GstStructure * params);
static gboolean gst_xiphos_vision_transform_size (GstBaseTransform * btrans, GstPadDirection direction,
    GstCaps * caps, gsize size, GstCaps * othercaps, gsize * othersize);

static GstFlowReturn gst_xiphos_vision_transform(GstBaseTransform * base, GstBuffer * inbuf, GstBuffer * outbuf);

static gboolean gst_xiphos_vision_reset(GstXiphosVision *filter);

static GstStructure* gst_xiphos_vision_create_stats(GstXiphosVision *filter);

static gboolean gst_xiphos_vision_get_unit_size (GstBaseTransform * btrans, GstCaps * caps, gsize * size)
{
    GstVideoInfo info;

    if (!gst_video_info_from_caps (&info, caps)) {
        GST_WARNING_OBJECT (btrans, "Failed to parse caps %" GST_PTR_FORMAT, caps);
        return FALSE;
    }

    *size = info.size;

    GST_DEBUG_OBJECT (btrans, "Returning size %" G_GSIZE_FORMAT " bytes"
      "for caps %" GST_PTR_FORMAT, *size, caps);

    return TRUE;
}
static gboolean gst_xiphos_vision_transform_meta (GstBaseTransform * trans, GstBuffer * inbuf,
    GstMeta * meta, GstBuffer * outbuf)
{
    const GstMetaInfo *info = meta->info;
    const gchar *const *tags;

    tags = gst_meta_api_type_get_tags (info->api);

    if (!tags || (g_strv_length ((gchar **) tags) == 1
      && gst_meta_api_type_has_tag (info->api,
          g_quark_from_string (GST_META_TAG_VIDEO_STR))))
        return TRUE;


    return GST_BASE_TRANSFORM_CLASS (parent_class)->transform_meta (trans, inbuf, meta, outbuf);
}

static GstStateChangeReturn gst_xiphos_vision_change_state (GstElement * element,
    GstStateChange transition) {
    GstXiphosVision *filter = GST_XIPHOS_VISION(element);
    GstStateChangeReturn ret;
    ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

    switch (transition) {
        case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
            g_mutex_lock (&filter->queue_lock);
            g_queue_foreach (&filter->img_queue, (GFunc) gst_buffer_unref, NULL);
            g_queue_clear (&filter->img_queue);
            g_mutex_unlock (&filter->queue_lock);
            break;
        case GST_STATE_CHANGE_PAUSED_TO_READY:
            break;
        case GST_STATE_CHANGE_READY_TO_NULL:
            if (filter->xipvis_ != NULL)
            {
                VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "freeing xipvis");
                xipvis_free(filter->xipvis_);
                VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "freed xipvis");
            }
            gst_xiphos_vision_free_pts(&filter->pts_id_head);
            break;
        default:
            break;
    }

    return ret;
}


static GstFlowReturn
gst_xiphos_vision_generate_output (GstBaseTransform * trans, GstBuffer ** outbuf)
{
    GstBaseTransformClass *bclass = GST_BASE_TRANSFORM_GET_CLASS (trans);
    GstXiphosVision *filter = GST_XIPHOS_VISION(trans);

    GstFlowReturn ret = GST_FLOW_OK;
    GstBuffer *inbuf;

  /* Retrieve stashed input buffer, if the default submit_input_buffer
   * was run. Takes ownership back from there */
    inbuf = trans->queued_buf;
    trans->queued_buf = NULL;

    if (inbuf == NULL)
        return GST_FLOW_OK;

    /*This should never happen */
    if (bclass->prepare_output_buffer == NULL)
        goto no_prepare;

    GST_DEBUG_OBJECT (trans, "calling prepare buffer");
    ret = bclass->prepare_output_buffer (trans, inbuf, outbuf);

    if (ret != GST_FLOW_OK || *outbuf == NULL)
        goto no_buffer;

    GST_DEBUG_OBJECT (trans, "using allocated buffer in %p, out %p", inbuf, *outbuf);

    ret = bclass->transform (trans, inbuf, *outbuf);


    if (*outbuf != inbuf) {
        GstBuffer *tmpbuf;
        g_mutex_lock (&filter->queue_lock);
        while((gint)(g_queue_get_length(&filter->img_queue)) >= filter->num_pst_id_elems) {
            tmpbuf = g_queue_pop_head (&filter->img_queue);
            gst_buffer_unref (tmpbuf);
        }
        GST_DEBUG_OBJECT (trans, "pushing %p into queue %d \n", inbuf, g_queue_get_length(&filter->img_queue));

        g_queue_push_tail (&filter->img_queue, inbuf);
        g_mutex_unlock (&filter->queue_lock);
    }
    else
        GST_ERROR_OBJECT (trans, "input and outbuffer is the same \n");


    return ret;

/* ERRORS */
no_prepare:
    {
        gst_buffer_unref (inbuf);
        return GST_FLOW_NOT_SUPPORTED;
    }

no_buffer:
    {
        gst_buffer_unref (inbuf);
        *outbuf = NULL;
        GST_WARNING_OBJECT (trans, "could not get buffer from pool: %s", gst_flow_get_name (ret));
        return ret;
    }
}

/* GObject method implementations */

/* initialize the xiphos_vision's class */
static void
gst_xiphos_vision_class_init(GstXiphosVisionClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;
    GstBaseTransformClass *gstbasetransform_class;

    gobject_class = (GObjectClass *) klass;
    gstelement_class = (GstElementClass *) klass;
    gstbasetransform_class = (GstBaseTransformClass *) klass;

    gobject_class->finalize     = gst_xiphos_vision_finalize;
    gobject_class->set_property = gst_xiphos_vision_set_property;
    gobject_class->get_property = gst_xiphos_vision_get_property;

    g_object_class_install_property(gobject_class, PROP_SILENT,
        g_param_spec_boolean("silent", "Silent", "Produce verbose output ?",
        FALSE, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, PROP_DRAW,
        g_param_spec_boolean("draw", "Draw", "Draw targets ?",
        FALSE, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, PROP_STREAM_ID,
        g_param_spec_uint("stream_id", "Stream ID",
            "The usecase stream ID. This is used to identify which stream the vision"
            "element is working with.  This is used to direct commands to the correct"
            "vision element when more than one is in use.",
            0, 2, 0, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, PROP_UPDATE_DELAY,
        g_param_spec_uint("update_delay", "Update Delay",
            "Average update delay of vision processing in the last 1 second ",
            0, G_MAXUINT, 0, G_PARAM_READABLE));

    g_object_class_install_property(gobject_class, PROP_APC_APP_ID,
        g_param_spec_uint("apc_app_id", "Apc application ID",
            "Apc application ID where this used to talk to APCS ",
            0, 32, 0, G_PARAM_READABLE));

    g_object_class_install_property(gobject_class, PROP_STAB,
        g_param_spec_uint("stab", "Stabilization", "Perform software image stabilization ?",
        0, 1, 0, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, PROP_SENSOR,
        g_param_spec_uint("sensor", "Sensor type",
            "The sensor type of the camera. EO (Daylight) = 0, IR (infrared) = 1."
            "The vision algorithm will tailor itself to this property if provided.",
            0, 1, 0, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, PROP_TARGET_INJECTOR,
        g_param_spec_string("injector", "Target Injector Path",
            "The full path to the json file to read from for automation of vision commands.",
            "", G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, PROP_TARGET_EJECTOR,
        g_param_spec_string("target_ejector", "Target Ejector Path",
            "The full path to the json file that will be used to save UDOT and MTI target log.",
            "", G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, PROP_STABLE_EJECTOR,
        g_param_spec_string("stable_ejector", "Stabilization Ejector Path",
            "The full path to the json file that will be used to save image stabilization log.",
            "", G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, PROP_STATS,
        g_param_spec_boxed("stats", "Outgoing camera intrinsics",
            "The camera intrinsics after the stabilization operation that are sent down stream.",
           GST_TYPE_STRUCTURE, G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

    gst_element_class_set_details_simple(gstelement_class,
        "Xiphos vision filter",
        "Xiphos vision filter",
        "This is the Xiphos Vision Plugin.",
        "april@aeryon.com");

    gst_element_class_add_pad_template(gstelement_class,
        gst_static_pad_template_get(&src_factory));

    gst_element_class_add_pad_template(gstelement_class,
        gst_static_pad_template_get(&sink_factory));
    gstelement_class->change_state     = GST_DEBUG_FUNCPTR (gst_xiphos_vision_change_state);

    gstbasetransform_class->sink_event = GST_DEBUG_FUNCPTR (gst_xiphos_vision_sink_event);
    gstbasetransform_class->start      = GST_DEBUG_FUNCPTR (gst_xiphos_vision_start);
    gstbasetransform_class->stop       = GST_DEBUG_FUNCPTR (gst_xiphos_vision_stop);

    gstbasetransform_class->transform_caps = GST_DEBUG_FUNCPTR (gst_xiphos_vision_transform_caps);

    gstbasetransform_class->filter_meta = GST_DEBUG_FUNCPTR (gst_xiphos_vision_filter_meta);
    gstbasetransform_class->set_caps = GST_DEBUG_FUNCPTR (gst_xiphos_vision_set_caps);
    gstbasetransform_class->transform = GST_DEBUG_FUNCPTR (gst_xiphos_vision_transform);

    gstbasetransform_class->passthrough_on_same_caps = FALSE;

    gstbasetransform_class->transform_size = GST_DEBUG_FUNCPTR (gst_xiphos_vision_transform_size);
    gstbasetransform_class->get_unit_size = GST_DEBUG_FUNCPTR (gst_xiphos_vision_get_unit_size);
    gstbasetransform_class->transform_meta =GST_DEBUG_FUNCPTR (gst_xiphos_vision_transform_meta);
    gstbasetransform_class->generate_output = GST_DEBUG_FUNCPTR (gst_xiphos_vision_generate_output);

    GST_DEBUG_CATEGORY_INIT(gst_xiphos_vision_debug, "xiphosvision", 0, "Xiphos Vision element");
}

/* initialize the new aeryon_vision element
 * instantiate pads and add them to element
 * set pad callback functions
 * initialize instance structure
 */
static void
gst_xiphos_vision_init(GstXiphosVision * filter)
{
    gst_xiphos_vision_reset(filter);

    filter->pts_id_head = NULL;
    filter->xipvis_ = NULL;
    filter->newconfig_ = 0;
    filter->last_lock = 0;
    filter->last_lock_time_ms = 0.0;
    filter->ctr = 0;
    filter->current_frame_pts = 0;
    filter->hi_power = FALSE;
    filter->update_accum = 0.0;
    filter->palette = 0;
    filter->palette_ctr = 0;
    filter->palette_changed = FALSE;
    filter->isotherm_changed = FALSE;
    filter->retrack = FALSE;
    filter->update_delay = 0;
    filter->apc_app_id = 0;
    filter->target_ejector_path = NULL;
    filter->target_ejector_fd = NULL;
    filter->target_ejector_cnt = 0;
    filter->stable_ejector_path = NULL;
    filter->stable_ejector_fd = NULL;
    memset(&filter->gps_ctx, 0 , sizeof(klv_gps_ctx_t));
    injector_init(&filter->injector);
    g_queue_init (&filter->img_queue);
    g_mutex_init (&filter->queue_lock);
    g_mutex_init (&filter->stats_mutex);
    memset(&filter->stats, 0 , sizeof(vision_stats_t));
}

/* copies the given caps */
static GstCaps *
gst_xiphos_vision_caps_remove_format_info (GstCaps * caps)
{
    GstStructure *st;
    GstCapsFeatures *f;
    gint i, n;
    GstCaps *res;

    res = gst_caps_new_empty ();

    n = gst_caps_get_size (caps);
    for (i = 0; i < n; i++) {
        st = gst_caps_get_structure (caps, i);
        f = gst_caps_get_features (caps, i);

        /* If this is already expressed by the existing caps
         * skip this structure */
        if (i > 0 && gst_caps_is_subset_structure_full (res, st, f))
            continue;

        st = gst_structure_copy (st);
         /* Only remove format info for the cases when we can actually convert */
        if (!gst_caps_features_is_any (f)
            && gst_caps_features_is_equal (f,GST_CAPS_FEATURES_MEMORY_SYSTEM_MEMORY))
            gst_structure_remove_fields (st, "format", "colorimetry", "chroma-site", NULL);
        gst_caps_append_structure_full (res, st, gst_caps_features_copy (f));
    }

    return res;
}

static gint gst_xiphos_vision_fps_from_info(GstVideoInfo in_info)
{
    gint fps = in_info.fps_n / in_info.fps_d;
    // If not valid, make it reasonable
    fps = (fps < 1)? 24 : fps;
    fps = (fps > 60)? 60 : fps;
    return fps;
}

static gboolean gst_xiphos_vision_set_caps (GstBaseTransform * trans, GstCaps * incaps, GstCaps * outcaps){

    GstXiphosVision *filter = GST_XIPHOS_VISION(trans);
    GstVideoInfo in_info, out_info;

    /* input caps */
    if (!gst_video_info_from_caps (&in_info, incaps))
        goto invalid_caps;

    /* output caps */
    if (!gst_video_info_from_caps (&out_info, outcaps))
        goto invalid_caps;

    if((in_info.width != filter->in_info.width) || (in_info.height != filter->in_info.height)) {
        filter->caps_changed = TRUE;
    }

    filter->in_info    = in_info;
    filter->out_info   = out_info;
    filter->fps        = gst_xiphos_vision_fps_from_info(in_info);
    filter->f_duration = 1001 / (filter->fps + 1);

    gst_base_transform_set_in_place (trans, FALSE);



    filter->negotiated   = TRUE;

    GST_DEBUG_OBJECT (trans, "in_caps %" GST_PTR_FORMAT " out_caps %" GST_PTR_FORMAT, incaps, outcaps);
    return TRUE;

    /* ERRORS */
invalid_caps:
    {
        GST_ERROR_OBJECT (filter, "invalid caps");
        filter->negotiated = FALSE;
        return FALSE;
    }
}


 static GstCaps *gst_xiphos_vision_transform_caps (GstBaseTransform * btrans,
                                                GstPadDirection direction,
                                                GstCaps * caps, GstCaps * filtercaps)
 {
    GstCaps *tmp, *tmp2;
    GstCaps *result;

    USE(direction);
    /* Get all possible caps that we can transform to */
    tmp = gst_xiphos_vision_caps_remove_format_info (caps);

    if (filtercaps) {
        tmp2 = gst_caps_intersect_full (filtercaps, tmp, GST_CAPS_INTERSECT_FIRST);
        gst_caps_unref (tmp);
        tmp = tmp2;
    }

    result = tmp;

    GST_DEBUG_OBJECT (btrans, "transformed %" GST_PTR_FORMAT "into %"
      GST_PTR_FORMAT, caps, result);

    return result;
}

static gboolean
gst_xiphos_vision_filter_meta (GstBaseTransform * UNUSED(trans), GstQuery * UNUSED(query),
                            GType UNUSED(api), const GstStructure * UNUSED(params))
{
    /* propose all metadata upstream */
    return TRUE;
}

static void
gst_xiphos_vision_finalize(GObject * object)
{
    GstXiphosVision *filter = GST_XIPHOS_VISION(object);

    injector_free(&filter->injector);
    if (filter->target_ejector_fd != NULL)
    {
        // write track session footer and close
        fprintf(filter->target_ejector_fd, "\n  ]\n}\n");
        fclose(filter->target_ejector_fd);
        filter->target_ejector_fd = NULL;
    }
    g_free(filter->target_ejector_path);

    if (filter->stable_ejector_fd != NULL)
    {
        fclose(filter->stable_ejector_fd);
        filter->stable_ejector_fd = NULL;
    }
    g_free(filter->stable_ejector_path);

    g_mutex_lock (&filter->queue_lock);
    g_queue_foreach (&filter->img_queue, (GFunc) gst_buffer_unref, NULL);
    g_queue_clear (&filter->img_queue);
    g_mutex_unlock (&filter->queue_lock);
    g_mutex_clear(&filter->queue_lock);

    VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "cuda device reset");
    xipvis_device_reset(NULL);

    G_OBJECT_CLASS(parent_class)->finalize(object);
}

static void
gst_xiphos_vision_set_property(GObject * object, guint prop_id,
                               const GValue * value, GParamSpec * pspec)
{
    GstXiphosVision *filter = GST_XIPHOS_VISION(object);

    switch (prop_id)
    {
        case PROP_SILENT:
            filter->silent = g_value_get_boolean(value);
            break;
        case PROP_DRAW:
            filter->draw = g_value_get_boolean(value);
            break;
        case PROP_STREAM_ID:
            filter->stream_id = g_value_get_uint(value);
            break;
        case PROP_STAB:
            filter->stab = g_value_get_uint(value);
            break;
        case PROP_SENSOR:
            filter->sensor = g_value_get_uint(value);
            break;
        case PROP_TARGET_INJECTOR:
        {
            filter->injector_path = g_value_dup_string(value);
            if (strcmp(filter->injector_path, ""))
            {
                injector_load(&filter->injector, filter->injector_path);
                filter->injector_cmd = injector_next(&filter->injector);
            }
            break;
        }
        case PROP_TARGET_EJECTOR:
        {
            filter->target_ejector_path = g_value_dup_string(value);
            if (strcmp(filter->target_ejector_path, ""))
            {
                filter->target_ejector_fd = fopen(filter->target_ejector_path, "w");
                if (filter->target_ejector_fd == NULL)
                {
                    char msg[256] = "";
                    VISION_LOG(GST_LEVEL_ERROR, filter->stream_id, filter, "Open target ejector file failed: %s\n", strerror_r(errno, msg, sizeof(msg)));
                }
                else
                {
                    // Write track session header
                    fprintf(filter->target_ejector_fd, "{\n  \"track_session\": [\n");
                }
            }
            break;
        }
        case PROP_STABLE_EJECTOR:
        {
            filter->stable_ejector_path = g_value_dup_string(value);
            if (strcmp(filter->stable_ejector_path, ""))
            {
                filter->stable_ejector_fd = fopen(filter->stable_ejector_path, "w");
                if (filter->stable_ejector_fd == NULL)
                {
                    char msg[256] = "";
                    VISION_LOG(GST_LEVEL_ERROR, filter->stream_id, filter, "Open stabilization ejector file failed: %s\n", strerror_r(errno, msg, sizeof(msg)));
                }
            }
            break;
        }
       case PROP_APC_APP_ID:
        {
            filter->apc_app_id = g_value_get_uint(value);
            break;
        }
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
gst_xiphos_vision_get_property(GObject * object, guint prop_id,
                               GValue * value, GParamSpec * pspec)
{
    GstXiphosVision *filter = GST_XIPHOS_VISION(object);

    switch (prop_id)
    {
        case PROP_SILENT:
            g_value_set_boolean(value, filter->silent);
            break;
        case PROP_DRAW:
            g_value_set_boolean(value, filter->draw);
            break;
        case PROP_STREAM_ID:
            g_value_set_uint(value, filter->stream_id);
            break;
        case PROP_STAB:
            g_value_set_uint(value, filter->stab);
            break;
        case PROP_SENSOR:
            g_value_set_uint(value, filter->sensor);
            break;
        case PROP_UPDATE_DELAY:
            g_value_set_uint(value, filter->update_delay);
            break;
        case PROP_APC_APP_ID:
            g_value_set_uint(value, filter->apc_app_id);
            break;
        case PROP_STATS:
            g_value_take_boxed(value, gst_xiphos_vision_create_stats(filter));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static gboolean gst_xiphos_vision_start(GstBaseTransform * parent) {
    USE(parent);
    return TRUE;
};

static gboolean gst_xiphos_vision_reset(GstXiphosVision *filter) {
    memset(&filter->request_tracker_config, 0, sizeof(TrackerConfig));
    memset(&filter->tracker_config, 0, sizeof(TrackerConfig));
    memset(&filter->isotherm, 0, sizeof(cam_isotherm_config_t));
    filter->tracker_config.state = TRACKER_STATE_INIT;
    filter->error_flags = 0;
    filter->cool_down = 0;
    filter->silent = FALSE;
    filter->draw = FALSE;
    filter->stab = 1;
    filter->negotiated = FALSE;
    filter->stream_id = 0;
    filter->sensor = EO;
    filter->fps = 0;
    filter->f_duration = 0;
    filter->caps_changed = FALSE;

    // Load default vision parameters
    filter->vision_params.time_ms = 0;
    filter->vision_params.mti_reserved_1 = 0;
    filter->vision_params.udot_reserved_1 = 0;
    filter->num_pst_id_elems = 0;
    filter->pts_id_cur = 0;

    return TRUE;
};

static gboolean gst_xiphos_vision_stop(GstBaseTransform * parent) {
    GstXiphosVision *filter = GST_XIPHOS_VISION(parent);
    gst_xiphos_vision_reset(filter);
    return TRUE;
};


/* GstElement method implementations */
/* this function handles sink events */
static gboolean
gst_xiphos_vision_sink_event(GstBaseTransform * parent, GstEvent * event)
{
    gboolean ret;
    GstXiphosVision *filter;

    GstTrackerConfig *gstconfig;
    GstPerfData *gstperfdata;
    GstVisionParams *gstvisionparams;

    filter = GST_XIPHOS_VISION(parent);

    // Handle any Aeryon sink events first and destory them afterwards
    switch ((GstAeryonEventType)GST_EVENT_TYPE (event)) {

        case GST_EVENT_TRACKER_CONFIG:
        {
            ret = gst_event_get_tracker_config(event, &gstconfig);
            if (ret)
            {
                // Ensure config is only used if this stream is in the recipient
                // bitfield
                if (gstconfig->config->stream_bf & (1 << filter->stream_id))
                {
                    // Debug
                    VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Received cam tracker config. STATE:[%d]",
                        gstconfig->config->state);
                    // Debug
                    GST_WARNING_OBJECT(filter, "Received cam tracker config. STATE:[%d]\n",
                        gstconfig->config->state);

                    VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Location: [%d,%d]\nSize: [%d,%d] "
                        "timestamp: [%" PRIu64 "] image_id: [%d] "
                        "res: [%d,%d] feedback_delay %d ",
                        gstconfig->config->pixel_x,
                        gstconfig->config->pixel_y,
                        gstconfig->config->pixel_w,
                        gstconfig->config->pixel_h,
                        gstconfig->config->target_ts,
                        gstconfig->config->image_id,
                        gstconfig->config->image_w,
                        gstconfig->config->image_h,
                        (gint)(filter->current_frame_pts - gstconfig->config->target_ts)
                        );

                    GST_WARNING_OBJECT(filter, "Location: [%d,%d]\nSize: [%d,%d] "
                        "timestamp: [%" PRIu64 "] image_id: [%d] "
                        "res: [%d,%d] feedback_delay %d\n",
                        gstconfig->config->pixel_x,
                        gstconfig->config->pixel_y,
                        gstconfig->config->pixel_w,
                        gstconfig->config->pixel_h,
                        gstconfig->config->target_ts,
                        gstconfig->config->image_id,
                        gstconfig->config->image_w,
                        gstconfig->config->image_h,
                        (gint)(filter->current_frame_pts - gstconfig->config->target_ts)
                        );

                    /* Parse the config where we receive frames.
                    */
                    filter->request_tracker_config = *(gstconfig->config);
                    // Do not let the user tear down vision
                    if (filter->request_tracker_config.state < TRACKER_STATE_STANDBY)
                    {
                        filter->request_tracker_config.state = TRACKER_STATE_READY;
                        // bump to stabilization if stab = TRUE
                        if (filter->stab)
                        {
                            filter->request_tracker_config.state = TRACKER_STATE_STANDBY;
                        }
                    }
                    filter->newconfig_ = 1;
                }
                gst_tracker_config_unref(gstconfig);
            }
            gst_event_unref (event);
            break;
        } // case GST_EVENT_TRACKER_CONFIG:

        case GST_EVENT_PERF_DATA:
        {
            ret = gst_event_get_perf_data(event, &gstperfdata);

            if (ret)
            {
                filter->perfdata = *(gstperfdata->perfdata);

                // Debug
                VISION_LOG(GST_LEVEL_DEBUG, filter->stream_id, filter, "CPU Temp [%0.2f] GPU [%d] CPU [%d]\n",
                    filter->perfdata.cpu_temp,
                    filter->perfdata.gpu_freq,
                    filter->perfdata.cpu_freq[0]);

                gst_perf_data_unref(gstperfdata);
            }
            gst_event_unref (event);
            break;
        } // case GST_EVENT_PERF_DATA:

        case GST_EVENT_VISION_PARAMS:
        {
            ret = gst_event_get_vision_params(event, &gstvisionparams);
            if (ret)
            {
                // Only accept parameters sent to this stream
                if (gstvisionparams->params->stream_id == filter->stream_id)
                {
                    filter->vision_params = *(gstvisionparams->params);
                    // Debug
                    VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Received new vision params.");
                    // set the new parameters
                    gst_xiphos_vision_set_vision_params(filter->xipvis_, filter);
                    // read them back in case anything was out of bounds
                    gst_xiphos_vision_get_vision_params(filter->xipvis_, filter);
                }
            }
            gst_event_unref (event);
            break;
        } // case GST_EVENT_VISION_PARAMS:
        case GST_EVENT_PALETTE_DATA:
        {
            GstPaletteData *gstpalettedata;
            ret = gst_event_get_palette_data(event, &gstpalettedata);

            if (ret)
            {
                guint data = *(gstpalettedata->palettedata);
                if (data != filter->palette)
                {
                    filter->palette_changed = TRUE;
                }
                filter->palette = data;

                VISION_LOG(GST_LEVEL_DEBUG, filter->stream_id, filter, "Received IR Palette Data. Palette [%d].", filter->palette);
                gst_palette_data_unref(gstpalettedata);
            }
            gst_event_unref (event);
            break;
        } // case GST_EVENT_PALETTE_DATA:

        case GST_EVENT_ISOTHERM_DATA:
        {
            GstIsothermData *gstisothermdata;
            ret = gst_event_get_isotherm_data(event, &gstisothermdata);

            if (ret)
            {
                cam_isotherm_config_t data = *(gstisothermdata->isothermdata);

                // Only start a re-track if mode or units change (units can affect colorization)
                if (filter->isotherm.mode != data.mode || filter->isotherm.units != data.units)
                {
                    filter->isotherm_changed = TRUE;
                }
                filter->isotherm = data;

                VISION_LOG(GST_LEVEL_DEBUG, filter->stream_id, filter, "Received IR Isotherm Data. Mode [%d]. Units[%d]", data.mode, data.units);
                gst_isotherm_data_unref(gstisothermdata);
            }
            gst_event_unref (event);
            break;
        } // GST_EVENT_ISOTHERM_DATA
        // End of processing aeryon custom events
        // now forward other standard events to base transform to process
        default :
            ret = GST_BASE_TRANSFORM_CLASS (parent_class)->sink_event (parent, event);
            break;
    }
    return ret;
}

// Send the vision parameters to apc
static void gst_xiphos_vision_send_vision_params(GstObject *parent){
    gst_vision_params_t params;
    GstXiphosVision *filter;
    filter = GST_XIPHOS_VISION(parent);
    // refresh xiphos params
    gst_xiphos_vision_get_vision_params(filter->xipvis_, filter);
    params = (filter->vision_params);
    params.stream_id = filter->stream_id;
    params.time_ms = get_time_ms();

    GstMessage *msg = gst_message_new_apc(
            parent,
            MSG_GST_VISION_PARAMS_APC_CLASS,
            MSG_GST_VISION_PARAMS_APC_ID,
            &params,
            sizeof (gst_vision_params_t));
    gst_element_post_message(GST_ELEMENT(parent), msg);
}

static void gst_xiphos_add_world_coordinates(Metadata2 *meta, GstObject *parent)
{
    GstXiphosVision *filter;
    filter = GST_XIPHOS_VISION(parent);
    int i;
    for (i = 0; i < (int)meta->tracks.num_targets; i++)
    {
        float gimbal_rpy[3] = { (float)meta->position.camera_roll_est,
                                   (float)meta->position.camera_pitch_est,
                                   (float)meta->position.camera_yaw_est};

        if (gimbal_rpy[1] > MAX_TRACK_PITCH)
        {
            meta->tracks.lat[i] = 0.0;
            meta->tracks.lon[i] = 0.0;
            meta->tracks.alt[i] = 0.0;
            return;
        }

        // Rotate camera into world rotation matrix
        float Rcw[9];
        gmbl_rpy_to_Rcw( gimbal_rpy, Rcw );


        // Find a unit Ray towards target relative to camera
        const float target_x_rad = meta->frame.fov_x
                                 * (meta->tracks.gimbal_target_x[i]
                                 - meta->tracks.image_w / 2.0)
                                 / meta->tracks.image_w;
        const float target_y_rad = meta->frame.fov_y
                                 * (meta->tracks.gimbal_target_y[i]
                                 - meta->tracks.image_h / 2.0)
                                 / meta->tracks.image_h;
        float target_ray[3] = { tan( target_x_rad ),
                                tan( target_y_rad ),
                                1.0 };
        norm_v3( target_ray, target_ray );

        // Position of the camera in NED
        float cam_ned[3] = { (float)meta->position.est_state.x_n,
                             (float)meta->position.est_state.x_e,
                             (float)meta->position.est_state.x_d};

        // Find pose of unit Ray in NED
        float target_rel_ned[3];
        mul_m3_v3( Rcw, target_ray, target_rel_ned );
        target_rel_ned[0] += cam_ned[0];
        target_rel_ned[1] += cam_ned[1];
        target_rel_ned[2] += cam_ned[2];

        // Cast unit Ray onto ground plane in NED
        float u[3];
        sub_v3(target_rel_ned, cam_ned, u);
        float su[3];
        float scale = -cam_ned[2]/(target_rel_ned[2]-cam_ned[2]);
        mul_v3(scale, u, su);
        float target_ground_ned[3];
        add_v3(cam_ned, su, target_ground_ned);

        // Initialize the gps base if it has changed or was not initialized
        if (!filter->gps_ctx.init ||
             filter->gps_ctx.base[0] != meta->position.base_ecef_x ||
             filter->gps_ctx.base[1] != meta->position.base_ecef_y ||
             filter->gps_ctx.base[2] != meta->position.base_ecef_z )
        {
            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Set GPS base: [%d, %d, %d] to [%d, %d, %d]",
                filter->gps_ctx.base[0],
                filter->gps_ctx.base[1],
                filter->gps_ctx.base[2],
                meta->position.base_ecef_x,
                meta->position.base_ecef_y,
                meta->position.base_ecef_z
                );
            filter->gps_ctx.base[0] = meta->position.base_ecef_x;
            filter->gps_ctx.base[1] = meta->position.base_ecef_y;
            filter->gps_ctx.base[2] = meta->position.base_ecef_z;
            ts_gps_init_base(&(filter->gps_ctx.datum), filter->gps_ctx.transform, filter->gps_ctx.base);
            filter->gps_ctx.init = 1;
        }

        // Convert NED to LLH
        float64_t target_llh[3];
        float64_t target_ned[3] = {target_ground_ned[0],target_ground_ned[1],target_ground_ned[2]};
        ts_gps_ned_to_llh(target_llh, filter->gps_ctx.base, &(filter->gps_ctx.datum), target_ned, filter->gps_ctx.transform);

        meta->tracks.lat[i] = target_llh[0];
        meta->tracks.lon[i] = target_llh[1];
        meta->tracks.alt[i] = target_llh[2];
    }
}

static void gst_xiphos_send_cam_tracker_targets(Metadata2 *meta, GstObject *parent){
    cam_tracker_targets_t tracks;
    int i;
    memset(&tracks, 0, sizeof(tracks));
    tracks.ts = meta->tracks.ts;
    tracks.stream_id = meta->tracks.stream_id;
    tracks.state = meta->tracks.state;
    tracks.lock = meta->tracks.lock;
    tracks.num_targets = meta->tracks.num_targets;
    tracks.image_id = meta->tracks.image_id;
    tracks.image_w = meta->tracks.image_w;
    tracks.image_h = meta->tracks.image_h;
    tracks.error_flags = meta->tracks.error_flags;
    for (i = 0; i < (int)tracks.num_targets; i++)
    {
        tracks.id[i]        = meta->tracks.id[i];
        tracks.pixel_x[i]   = meta->tracks.x[i];
        tracks.pixel_y[i]   = meta->tracks.y[i];
        tracks.pixel_w[i]   = meta->tracks.w[i];
        tracks.pixel_h[i]   = meta->tracks.h[i];
        tracks.confidence[i] = (uint32_t)(meta->tracks.confidence[i]*100);
        tracks.lat[i] = meta->tracks.lat[i];
        tracks.lon[i] = meta->tracks.lon[i];
        tracks.height[i] = meta->tracks.alt[i];
    }

    GstMessage *msg = gst_message_new_apc(
            parent,
            MSG_CAM_TRACKER_TARGETS_APC_CLASS,
            MSG_CAM_TRACKER_TARGETS_APC_ID,
            &tracks,
            sizeof (cam_tracker_targets_t));
    gst_element_post_message(GST_ELEMENT(parent), msg);
}

/* our output size only depends on the caps, not on the input caps */
static gboolean gst_xiphos_vision_transform_size (GstBaseTransform * UNUSED(btrans), GstPadDirection UNUSED(direction),
    GstCaps * UNUSED(caps), gsize size, GstCaps * othercaps, gsize * othersize)
{
    gboolean ret = TRUE;
    GstVideoInfo info;

    g_assert (size);

    ret = gst_video_info_from_caps (&info, othercaps);
    if (ret)
        *othersize = info.size;

    return ret;
}


/* chain function
 * this function does the actual processing
 */
 static GstFlowReturn
 gst_xiphos_vision_transform(GstBaseTransform * parent, GstBuffer * inbuf, GstBuffer * outbuf)
 {
    // CPU will increase at this state and above
    unsigned int hi_state = TRACKER_STATE_MAX;
    Metadata2 *meta = NULL;
    GstAeryonBufferMeta *buffer_meta = NULL;

    GstXiphosVision *filter;
    GstVideoFrame inframe;
    GstVideoFrame outframe;

    unsigned int lock = TRACKER_LOCK_NONE;
    unsigned int send = 0;

    filter = GST_XIPHOS_VISION(parent);
    TrackerConfig new_config = filter->tracker_config;

    filter->error_flags = 0;

    // Get the gst buffer timestamp
    long ts = 0;

    if (GST_BUFFER_PTS(inbuf) != GST_CLOCK_TIME_NONE)
    {
        ts = (long) (GST_BUFFER_PTS(inbuf) / 1000 / 1000);
        LOG(TRACE_DEBUG, "Pad PTS = %d \n", (int) (ts));
        filter->current_frame_pts = ts;
    }

    if(!filter->negotiated)
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "could not get info");
        goto exit_vision_chain;
    }

    /* Open buffer's metadata (if present) */
    buffer_meta = GST_AERYON_BUFFER_META_GET(inbuf);

    // Initialize tracks
    if (buffer_meta && buffer_meta->gstmetadata && buffer_meta->gstmetadata->metadata)
    {
        meta = buffer_meta->gstmetadata->metadata;
        memset(&meta->tracks, 0, sizeof(Tracks));
    }


    if (gst_video_frame_map(&inframe, &filter->in_info, inbuf, GST_MAP_READ ) == FALSE)
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "could not map inframe");
        goto exit_vision_chain;
    }

    if (gst_video_frame_map(&outframe, &filter->out_info, outbuf, GST_MAP_WRITE ) == FALSE)
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "could not map outframe");
        goto exit_vision_chain;
    }


    if(filter->caps_changed)
    {
        // TODO: Fix Xipvis so it can handle dynamic upstream resolution changes without
        // having to be reset entirely.
        if(filter->xipvis_)
        {
            // Tear down xipvis to handle new resolution
            xipvis_free(filter->xipvis_);
            filter->xipvis_     = NULL;
            filter->error_flags = 0;
            new_config.state = TRACKER_STATE_INIT;

            // Resume last state if it was not udot
            if (filter->tracker_config.state < TRACKER_STATE_UDOT)
            {
                filter->request_tracker_config.state = filter->tracker_config.state;
                filter->newconfig_ = 1;
            }
            else
            {
                // Re-request UDOT with last track and new caps
                filter->request_tracker_config.state = TRACKER_STATE_UDOT;
                filter->request_tracker_config.pixel_x = filter->last_x;
                filter->request_tracker_config.pixel_y = filter->last_y;
                filter->request_tracker_config.image_id = 0;
                filter->request_tracker_config.target_ts = filter->current_frame_pts;
            }
        }

        filter->caps_changed = FALSE;
        g_mutex_lock (&filter->queue_lock);
        g_queue_foreach (&filter->img_queue, (GFunc) gst_buffer_unref, NULL);
        g_queue_clear (&filter->img_queue);
        g_mutex_unlock (&filter->queue_lock);
    }

    //
    // Handle over temperature
    //
    if (filter->perfdata.cpu_temp >= MAX_CPU_TEMP_DEG)
    {
        // Disregard new configurations
        filter->newconfig_ = 0;
        if (filter->cool_down == FALSE)
        {
            VISION_LOG(GST_LEVEL_ERROR, filter->stream_id, filter, "Xipvis Overtemp");
            filter->cool_down = TRUE;
            filter->error_flags |= (1<<TRACKER_ERROR_OVERTEMP);
            send = 1;
        }

        // Stop egomotion
        if (filter->xipvis_ && filter->tracker_config.state > TRACKER_STATE_READY)
        {
            VISION_LOG(GST_LEVEL_ERROR, filter->stream_id, filter, "Disable Egomotion");
            xipvis_disable_egomotion(filter->xipvis_);
            new_config.state = TRACKER_STATE_READY;
            send = 1;
        }
    }

    //
    // Handle cool down period
    //
    if (filter->cool_down == TRUE)
    {
        // Check for cooldown over
        if (filter->perfdata.cpu_temp < MAX_CPU_TEMP_DEG - TEMP_HYSTERESIS_DEG)
        {
            filter->cool_down = FALSE;
            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Cooldown over");
            filter->request_tracker_config.state = TRACKER_STATE_READY;
            if (filter->stab)
                filter->request_tracker_config.state = TRACKER_STATE_STANDBY;
            filter->newconfig_ = 1;
            send = 1;
        }
        else
        {
            filter->error_flags |= (1<<TRACKER_ERROR_COOLDOWN);
            filter->newconfig_ = 0;
        }
    }

    // Higher resolution requires pushing the frequency up
    if (filter->in_info.width > 1024)
    {
        hi_state = TRACKER_STATE_UDOT;
    }

    // Track Injector TODO: Replace hard coding with a script reader

    if (filter->injector_cmd != null)
    {
        if (ts > filter->injector_cmd->time)
        {
            // Get state command
            filter->request_tracker_config.state = filter->injector_cmd->state;
            filter->request_tracker_config.cmd_src = 1;
            if (filter->injector_cmd->state == TRACKER_STATE_UDOT)
            {
                filter->request_tracker_config.image_w = filter->in_info.width;
                filter->request_tracker_config.image_h = filter->in_info.height;
                filter->request_tracker_config.pixel_x = filter->injector_cmd->x;
                filter->request_tracker_config.pixel_y = filter->injector_cmd->y;
                filter->request_tracker_config.pixel_w = filter->injector_cmd->w;
                filter->request_tracker_config.pixel_h = filter->injector_cmd->h;
                filter->request_tracker_config.target_ts = ts;
                filter->request_tracker_config.image_id = 0;
            }
            // Retrieve the next injector command
            filter->injector_cmd = injector_next(&filter->injector);
            filter->newconfig_ = 1;
        }
    }

    // Debug MTI
#if 0
    static int injector_on = 0;
    if (!injector_on && ts > 1000)
    {
        injector_on = 1;
        filter->request_tracker_config.state = TRACKER_STATE_MTI;
        if (filter->xipvis_)
        {
            // override some settings
//            xipvis_mti_set_max_speed(filter->xipvis_, 0.015);
//            xipvis_mti_set_min_radius(filter->xipvis_, 0.015);
//            xipvis_mti_set_min_box_size(filter->xipvis_, 0.020);
//            xipvis_mti_set_sweep_size(filter->xipvis_, 0.019);
//            xipvis_mti_set_min_age(filter->xipvis_, 2);
//            xipvis_mti_set_decay_rate(filter->xipvis_, 0.02);
//            xipvis_mti_set_filter_rate(filter->xipvis_, 0.2);
        }
        filter->newconfig_ = 1;
    }
#endif

    // Uncomment turn on tracker for the following UDOT injectors (pick one)
#if 0
    static int injector_on = 0;
    if (!injector_on)
    {
        injector_on = 1;
        filter->request_tracker_config.state = TRACKER_STATE_STANDBY;
        filter->newconfig_ = 1;
    }

    // inject a UDOT target at the correct time
    static int injector_udot_on = 0;
#endif

    // Human 001 1920x1080 30
    // ~/aeryon/static/contrib/gstreamer-1.0/bin/gst-launch-1.0 filesrc location=/home/april/Dropbox/aeryon/ComputerVisionStockVideos/Humans/HUMAN_001_1920_30.MP4 ! qtdemux ! avdec_h264 ! video/x-raw, format=I420, framerate=30/1 ! aeryonmeta ! xiphosvision stream_id=0 stab=0 draw=1 ! autovideosink
#if 0
    if (!injector_udot_on && ts >= 3399)
    {
        injector_udot_on = 1;
        filter->request_tracker_config.state = TRACKER_STATE_UDOT;
        filter->request_tracker_config.image_w = 1920;
        filter->request_tracker_config.image_h = 1080;
        filter->request_tracker_config.pixel_x = 874;
        filter->request_tracker_config.pixel_y = 695;
        filter->request_tracker_config.pixel_w = 24;
        filter->request_tracker_config.pixel_h = 50;
        filter->request_tracker_config.target_ts = ts;
        filter->request_tracker_config.image_id = 0;
        filter->fps = 30;
        filter->newconfig_ = 1;
    }
#endif

    // Human 003 1920x1080 30
    // ~/aeryon/static/contrib/gstreamer-1.0/bin/gst-launch-1.0 filesrc location=/home/april/Dropbox/aeryon/ComputerVisionStockVideos/Humans/HUMAN_003_1920_30.MP4 ! qtdemux ! avdec_h264 ! video/x-raw, format=I420, framerate=30/1 ! aeryonmeta ! xiphosvision stream_id=0 stab=0 draw=1 ! autovideosink
#if 0
    if (!injector_udot_on && ts >= 10000)
    {
        injector_udot_on = 1;
        filter->request_tracker_config.state = TRACKER_STATE_UDOT;
        filter->request_tracker_config.image_w = 1920;
        filter->request_tracker_config.image_h = 1080;
        filter->request_tracker_config.pixel_x = 728;
        filter->request_tracker_config.pixel_y = 510;
        filter->request_tracker_config.pixel_w = 25;
        filter->request_tracker_config.pixel_h = 76;
        filter->request_tracker_config.target_ts = ts;
        filter->request_tracker_config.image_id = 0;
        filter->fps = 30;
        filter->newconfig_ = 1;
    }
#endif

    // Human 005 1920x1080 30
    // ~/aeryon/static/contrib/gstreamer-1.0/bin/gst-launch-1.0 filesrc location=/home/april/Dropbox/aeryon/ComputerVisionStockVideos/Humans/HUMAN_005_1920_30.MP4 ! qtdemux ! avdec_h264 ! video/x-raw, format=I420, framerate=30/1 ! aeryonmeta ! xiphosvision stream_id=0 stab=0 draw=1 ! autovideosink
#if 0
    if (!injector_udot_on && ts >= 16665)
    {
        injector_udot_on = 1;
        filter->request_tracker_config.state = TRACKER_STATE_UDOT;
        filter->request_tracker_config.image_w = 1920;
        filter->request_tracker_config.image_h = 1080;
        filter->request_tracker_config.pixel_x = 838;
        filter->request_tracker_config.pixel_y = 696;
        filter->request_tracker_config.pixel_w = 75;
        filter->request_tracker_config.pixel_h = 102;
        filter->request_tracker_config.target_ts = ts;
        filter->request_tracker_config.image_id = 0;
        filter->fps = 30;
        filter->newconfig_ = 1;
    }
#endif

#if 0
    // Human 006 1920x1080 30
    // ~/aeryon/static/contrib/gstreamer-1.0/bin/gst-launch-1.0 filesrc location=/home/april/Dropbox/aeryon/ComputerVisionStockVideos/Humans/HUMAN_006_1920_30.MP4 ! qtdemux ! avdec_h264 ! video/x-raw, format=I420, framerate=30/1 ! aeryonmeta ! xiphosvision stream_id=0 stab=0 draw=1 ! autovideosink
    if (!injector_udot_on && ts >= 3332)
    {
        //if (filter->xipvis_)
        //    xipvis_udot_set_learning_rate(filter->xipvis_, 0.01); // Used to work if set to 0.01
        injector_udot_on = 1;
        filter->request_tracker_config.state = TRACKER_STATE_UDOT;
        filter->request_tracker_config.image_w = 1920;
        filter->request_tracker_config.image_h = 1080;
        filter->request_tracker_config.pixel_x = 901;
        filter->request_tracker_config.pixel_y = 318;
        filter->request_tracker_config.pixel_w = 52;
        filter->request_tracker_config.pixel_h = 72;
        filter->request_tracker_config.target_ts = ts;
        filter->request_tracker_config.image_id = 0;
        filter->fps = 30;
        filter->newconfig_ = 1;
    }
#endif

    // Vehicle 002 #1 1920x1080 30
    // ~/aeryon/static/contrib/gstreamer-1.0/bin/gst-launch-1.0 filesrc location=/home/april/Dropbox/aeryon/ComputerVisionStockVideos/Vehicles/VEHICLE_002_1920_30.MP4 ! qtdemux ! avdec_h264 ! video/x-raw, format=I420, framerate=30/1 ! aeryonmeta ! xiphosvision stream_id=0 stab=0 draw=1 ! autovideosink
#if 0
    if (!injector_udot_on && ts >= 2366)
    {
        injector_udot_on = 1;
        filter->request_tracker_config.state = TRACKER_STATE_UDOT;
        filter->request_tracker_config.image_w = 1920;
        filter->request_tracker_config.image_h = 1080;
        filter->request_tracker_config.pixel_x = 1653;
        filter->request_tracker_config.pixel_y = 510;
        filter->request_tracker_config.pixel_w = 80;
        filter->request_tracker_config.pixel_h = 37;
        filter->request_tracker_config.target_ts = ts;
        filter->request_tracker_config.image_id = 0;

        filter->newconfig_ = 1;
    }
    // Vehicle 002 #2  1920x1080 30
    static int injector_udot_on_1 = 0;
    if (!injector_udot_on_1 && ts >= 23233)
    {
        injector_udot_on_1 = 1;
        filter->request_tracker_config.state = TRACKER_STATE_UDOT;
        filter->request_tracker_config.image_w = 1920;
        filter->request_tracker_config.image_h = 1080;
        filter->request_tracker_config.pixel_x = 556;
        filter->request_tracker_config.pixel_y = 332;
        filter->request_tracker_config.pixel_w = 51;
        filter->request_tracker_config.pixel_h = 32;
        filter->request_tracker_config.target_ts = ts;
        filter->request_tracker_config.image_id = 0;

        filter->newconfig_ = 1;
    }
#endif

    // Vehicle 003  1920x1080 30
    // ~/aeryon/static/contrib/gstreamer-1.0/bin/gst-launch-1.0 filesrc location=/home/april/Dropbox/aeryon/ComputerVisionStockVideos/Vehicles/VEHICLE_003_1920_30.MP4 ! qtdemux ! avdec_h264 ! video/x-raw, format=I420, framerate=30/1 ! aeryonmeta ! xiphosvision stream_id=0 stab=0 draw=1 ! autovideosink
#if 0
    if (!injector_udot_on && ts >= 332)
    {
        injector_udot_on = 1;
        filter->request_tracker_config.state = TRACKER_STATE_UDOT;
        filter->request_tracker_config.image_w = 1920;
        filter->request_tracker_config.image_h = 1080;
        filter->request_tracker_config.pixel_x = 633;
        filter->request_tracker_config.pixel_y = 415;
        filter->request_tracker_config.pixel_w = 42;
        filter->request_tracker_config.pixel_h = 65;
        filter->request_tracker_config.target_ts = ts;
        filter->request_tracker_config.image_id = 0;

        filter->newconfig_ = 1;
    }
#endif

    // READY - Initialize the camera parameters - Ideally this is only done
    // once at startup
    if (filter->xipvis_ == NULL)
    {
        filter->newconfig_ = 1;
        xipvis_cam cam;
        memset(&cam, 0, sizeof(cam));

        // Xiphos defaults (web cam?)
        // cam.flen = 0.027;
        // cam.pixel_pitch = 0.000005;

        // Start with defaults that match QX30U at zoom level 1
        if (filter->in_info.width > 0)
            cam.pixel_pitch = 6.17 / filter->in_info.width;
        cam.flen = 4.3;

        // From pipeline filter->in_info
        cam.width = filter->in_info.width;
        cam.height = filter->in_info.height;
        cam.centre_x = cam.width / 2.0 - 0.5;
        cam.centre_y = cam.height / 2.0 - 0.5;

        cam.sensor = filter->sensor;
        cam.fps = filter->fps;
        cam.format = I420;
        if (filter->in_info.finfo->format ==  GST_VIDEO_FORMAT_UYVY)
            cam.format = UYVY;
        if (filter->in_info.finfo->format ==  GST_VIDEO_FORMAT_YUY2)
            cam.format = YUY2;

        // Only populate if we have something useful in metadata
        if (meta != 0 && cam.width > 0 && meta->frame.sensor_size_x > 0.0)
        {
            cam.pixel_pitch = meta->frame.sensor_size_x / cam.width;
        }

        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Initialize Xipvis: "
            "cam.pixel_pitch [%f], "
            "cam.flen [%f], "
            "cam.width [%d], "
            "cam.height [%d], "
            "cam.format [%d], "
            "cam.sensor [%d], "
            "cam.fps [%.1f]",
            cam.pixel_pitch, cam.flen, cam.width, cam.height, cam.format, cam.sensor, cam.fps);

        filter->num_pst_id_elems = (gint)(VISION_BUFFER_SECONDS * filter->fps);

        if (xipvis_init(&filter->xipvis_, &cam, filter->stream_id, filter->num_pst_id_elems) < 0)
        {
            VISION_LOG(GST_LEVEL_ERROR, filter->stream_id, filter, "Xipvis initialization failed");
            if(filter->xipvis_) xipvis_free(filter->xipvis_);
            filter->xipvis_ = NULL;
            filter->error_flags |= (1<<TRACKER_ERROR_INIT);
            new_config.state = TRACKER_STATE_INIT;
            goto exit_vision_chain;
        }
        else
        {
            gst_xiphos_vision_init_pts(&filter->pts_id_head, filter->num_pst_id_elems);

            filter->tracker_config.state = TRACKER_STATE_READY;
            gst_xiphos_vision_get_vision_params(filter->xipvis_, filter);
            if (filter->request_tracker_config.state < TRACKER_STATE_READY)
            {
                if (filter->stab)
                    filter->request_tracker_config.state = TRACKER_STATE_STANDBY;
                else
                    filter->request_tracker_config.state = TRACKER_STATE_READY;
            }
            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Xipvis initialization successful. Queue size [%d]", filter->num_pst_id_elems);
        }
    }

    //
    // Handle new tracker configuration.
    //
    if (filter->newconfig_)
    {
        filter->newconfig_ = 0;
        send = 1;

        // INIT - Should not be allowed to get here
        if (filter->request_tracker_config.state == TRACKER_STATE_INIT
            && filter->xipvis_ != NULL)
        {
            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Exit Xipvis");
            xipvis_free(filter->xipvis_);
            filter->xipvis_ = NULL;
            new_config.state = TRACKER_STATE_INIT;
            goto exit_vision_chain;
        }

        // STOP UDOT
        if (filter->request_tracker_config.state != TRACKER_STATE_UDOT
            && filter->tracker_config.state == TRACKER_STATE_UDOT)
        {
            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Exit UDOT");
            if (filter->xipvis_)
                xipvis_disable_udot(filter->xipvis_);
        }

        // STOP MTI
        if (filter->request_tracker_config.state != TRACKER_STATE_MTI
            && filter->tracker_config.state == TRACKER_STATE_MTI)
        {
            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Exit MTI");
            if (filter->xipvis_)
                xipvis_disable_mti(filter->xipvis_);
        }

        // READY - Convert Only (if needed)
        if (filter->xipvis_ != NULL
                && filter->request_tracker_config.state == TRACKER_STATE_READY)
        {
            xipvis_disable_egomotion(filter->xipvis_);
            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Entered READY state");
            new_config.state = TRACKER_STATE_READY;
        }

        // Do not allow higher states in cooldown
        if (filter->cool_down == TRUE)
        {
            goto update;
        }

        // STANDBY - Update and Video Stabilization
        if (filter->xipvis_ != NULL && filter->tracker_config.state != TRACKER_STATE_STANDBY
                && filter->request_tracker_config.state > TRACKER_STATE_READY)
        {
            xipvis_enable_egomotion(filter->xipvis_);
            if (filter->stab)
            {
                xipvis_enable_stabilization(filter->xipvis_);
            }

            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Entered STANDBY state");
            new_config.state = TRACKER_STATE_STANDBY;
        }

        // START UDOT
        if (filter->xipvis_ != NULL && filter->request_tracker_config.state == TRACKER_STATE_UDOT)
        {
            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Start UDOT");

            float sx = 1.0;
            float sy = 1.0;
            int min_size = 16;
            TrackerConfig* cfg = &(filter->request_tracker_config);

            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "UDOT input res[%d, %d] location[%d, %d] size[%d, %d]",
                    cfg->image_w, cfg->image_h, cfg->pixel_x, cfg->pixel_y, cfg->pixel_w, cfg->pixel_h);

            // Scale and resize if needed
            if (cfg->image_id) // MTI target
            {
                // Reduce MTI targets by 50%, they are created with vision element resolution.
                cfg->pixel_w /= 2;
                cfg->pixel_h /= 2;
            }
            else // UDOT Target
            {
                // Correct for possible scale change between vision element and GUI
                if (cfg->image_w != 0)
                    sx = (float) filter->in_info.width / cfg->image_w;
                if (cfg->image_h != 0)
                    sy = (float) filter->in_info.height / cfg->image_h;

                cfg->image_w = filter->in_info.width;
                cfg->image_h = filter->in_info.height;
                cfg->pixel_x *= sx;
                cfg->pixel_y *= sy;
                cfg->pixel_w *= sx;
                cfg->pixel_h *= sy;
            }

            // Clamp to minimum box size
            cfg->pixel_w = (cfg->pixel_w >= min_size ) ? cfg->pixel_w : min_size;
            cfg->pixel_h = (cfg->pixel_h >= min_size) ? cfg->pixel_h  : min_size;

            // Clamp box to image dimensions
            cfg->pixel_x = (cfg->pixel_x + cfg->pixel_w / 2 < filter->in_info.width) ? cfg->pixel_x : filter->in_info.width - 1 - cfg->pixel_w / 2;
            cfg->pixel_y = (cfg->pixel_y + cfg->pixel_h / 2 < filter->in_info.height) ? cfg->pixel_y : filter->in_info.height - 1 - cfg->pixel_h / 2;
            cfg->pixel_x = (cfg->pixel_x - cfg->pixel_w / 2 > 0) ? cfg->pixel_x : cfg->pixel_w / 2;
            cfg->pixel_y = (cfg->pixel_y - cfg->pixel_h / 2 > 0) ? cfg->pixel_y : cfg->pixel_h / 2;

            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "UDOT output res[%d, %d] location[%d, %d] size[%d, %d]",
                    filter->in_info.width, filter->in_info.height, cfg->pixel_x, cfg->pixel_y, cfg->pixel_w, cfg->pixel_h);

            int ret = 0;
            if (cfg->image_id)
            {
                ret = gst_xiphos_vision_start_udot_from_id(filter->xipvis_, filter, ts, &inframe,
                                                        &outframe, &filter->in_info, filter->draw);
            }
            else
            {
                ret = gst_xiphos_vision_start_udot_from_ts(filter->xipvis_, filter, ts, &inframe,
                                                        &outframe, &filter->in_info, filter->draw, cfg->cmd_src);
            }

            if (ret != 0)
            {
                VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Failed to start UDOT. Lack of features?");
                xipvis_disable_udot(filter->xipvis_);
                filter->error_flags |= (1<<TRACKER_ERROR_UDOT);
                new_config.state = filter->tracker_config.state; // don't change the state
                if (filter->tracker_config.state == TRACKER_STATE_MTI)
                {
                    // Re-enable MTI
                    if (xipvis_enable_mti(filter->xipvis_) < 0)
                    {
                        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Failed to restart MTI.");
                        new_config.state = TRACKER_STATE_STANDBY;
                    }
                }
            }
            else
            {
                new_config.state = TRACKER_STATE_UDOT;
                VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Entered UDOT state");
            }

            goto exit_vision_chain;
        }

        // START MTI
        if (filter->xipvis_ != NULL && filter->request_tracker_config.state == TRACKER_STATE_MTI &&
            filter->tracker_config.state != TRACKER_STATE_MTI)
        {
            VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Start MTI");
            if (gst_xiphos_vision_start_mti(filter->xipvis_, filter, ts, &inframe, &outframe, &filter->in_info, filter->draw))
            {
                VISION_LOG(GST_LEVEL_ERROR, filter->stream_id, filter, "Failed to start MTI");
                filter->error_flags |= (1<<TRACKER_ERROR_MTI);
                new_config.state = filter->tracker_config.state; // don't change the state
            }
            else
            {
                new_config.state = TRACKER_STATE_MTI;
                VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Entered MTI state");
            }
            goto exit_vision_chain;
        }
    }

update:
    //
    // Update xipvis with new image and tracks (if any))
    //
    if (filter->xipvis_ != NULL)
    {
        int id;
        int num = 0;
        xipvis_bb *bbs = NULL;
        int udotflag = UDOT_NORMAL;

        // Handle IR palette and Isotherm changes
        if (filter->palette_changed || filter->isotherm_changed)
        {
            VISION_LOG(GST_LEVEL_DEBUG, filter->stream_id, filter, "Palette Changed.");
            filter->palette_changed = FALSE;
            filter->isotherm_changed = FALSE;
            filter->palette_ctr = IR_PALETTE_COUNT;
        }
        if (filter->palette_ctr >= 1) // Wait for palette/isotherm change to take effect
        {
            udotflag = UDOT_SKIP;
            filter->retrack = TRUE;
            filter->palette_ctr--;
        }
        else if (filter->retrack == TRUE) // Re-start the udot track
        {
            udotflag = UDOT_RETRACK;
            filter->retrack = FALSE;
        }

        double lasttime = get_time_ms();
        g_mutex_lock(&xiphos_update_mutex);
        int rc = xipvis_update( filter->xipvis_,
                                filter->draw,
                                inframe.data[0] + filter->in_info.offset[0],
                                filter->in_info.stride[0],
                                &id, &num, &bbs,
                                outframe.data[0] + filter->out_info.offset[0],
                                udotflag);
        g_mutex_unlock(&xiphos_update_mutex);
        if (rc == 0)
        {
            gst_xiphos_vision_push_pts_id(filter, ts, id);
        }

        double delay = get_time_ms() - lasttime;

        filter->update_accum += delay;

        if (delay > (gint)filter->f_duration || delay > 40)
        {
            VISION_LOG(GST_LEVEL_DEBUG, filter->stream_id, filter, "Xipvis is falling behind! [%.3f] ms\n", delay);
        }
        if ((filter->fps) && (filter->ctr % (filter->fps * 5) == 0))
        {
            VISION_LOG(GST_LEVEL_DEBUG, filter->stream_id, filter, "Average update delay [%.3f] ms.", filter->update_accum / (filter->fps * 5));
            filter->update_accum = 0.0;
            filter->update_delay = (uint8_t)delay;
        }

        if (filter->stable_ejector_fd != NULL)
        {
            // Log stabilization solution
            gst_xiphos_vision_write_stabilize_solution((GstObject*)parent, id);
        }

        if (bbs != NULL)
        {
            // Save last target for caps change
            if (num > 0)
            {
                filter->last_x = bbs[0].x_unwarped;
                filter->last_y = bbs[0].y_unwarped;
            }

            //
            // Update the lock status
            //
            if (filter->tracker_config.state == TRACKER_STATE_UDOT && num > 0 && bbs[0].confidence < 1.0)
            {
                lock = TRACKER_LOCK_SEARCHING;
            }
            else if (filter->tracker_config.state == TRACKER_STATE_UDOT && num > 0)
            {
                lock = TRACKER_LOCK_ON;
            }
            else
            {
                lock = TRACKER_LOCK_NONE;
            }

            //
            // Handle occlusion timeout
            //
            if (lock == TRACKER_LOCK_ON)
            {
                filter->last_lock_time_ms = get_time_ms();
            }
            if (lock == TRACKER_LOCK_SEARCHING)
            {
                float search_time = get_time_ms() - filter->last_lock_time_ms;

                VISION_LOG(GST_LEVEL_DEBUG, filter->stream_id, filter,
                        "Lost Time [%.1f] s, Confidence: [%f]", search_time, bbs[0].confidence );

                if (search_time > MAX_SEARCH_TIME_MS)
                {
                    VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Exit UDOT - Occlusion timeout [%.f]", search_time);
                    xipvis_disable_udot(filter->xipvis_);
                    if (filter->stab)
                        new_config.state = TRACKER_STATE_STANDBY;
                    else
                        new_config.state = TRACKER_STATE_READY;
                    lock = TRACKER_LOCK_NONE;
                }
            }
            if (lock != filter->last_lock)
            {
                VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Lock [%d] -> Lock [%d]", filter->last_lock, lock);
                filter->last_lock = lock;
            }

            if (filter->target_ejector_fd != NULL && 0 < num)
            {
                // Log track
                gst_xiphos_vision_write_track((GstObject*)parent, ts, num, id, lock, bbs);
            }

            if (meta)
            {
                if (0 < num)
                {
                    int i;
                    if (num > MAX_TARGETS)
                        num = MAX_TARGETS;
                    meta->tracks.num_targets = num;
                    meta->tracks.image_id = id;
                    meta->tracks.image_w = filter->in_info.width;
                    meta->tracks.image_h = filter->in_info.height;
                    for (i = 0; i < num; i++)
                    {
                        meta->tracks.gimbal_target_x[i] = bbs[i].x_unwarped;
                        meta->tracks.gimbal_target_y[i] = bbs[i].y_unwarped;
                        int w = bbs[i].xmax - bbs[i].xmin;
                        int h = bbs[i].ymax - bbs[i].ymin;
                        meta->tracks.id[i] = bbs[i].id;
                        meta->tracks.confidence[i] = bbs[i].confidence;
                        meta->tracks.x[i] = bbs[i].xmin + w / 2;
                        meta->tracks.y[i] = bbs[i].ymin + h / 2;
                        meta->tracks.w[i] = w;
                        meta->tracks.h[i] = h;
                        meta->tracks.x_dot[i] = 0.0;
                        meta->tracks.y_dot[i] = 0.0;
                    }
                }
            }
        }
    }

exit_vision_chain:
    gst_video_frame_unmap(&inframe);
    gst_video_frame_unmap(&outframe);

    // Copy over the new config
    filter->tracker_config = new_config;

    if (send)
    {
        VISION_LOG(GST_LEVEL_WARNING, filter->stream_id, filter, "Request STATE [%d] -> Set STATE [%d]\n",
            filter->request_tracker_config.state,
            filter->tracker_config.state
            );
    }

    //
    // Adjust CPU Frequency here
    //
    if (filter->hi_power != TRUE && filter->tracker_config.state >= hi_state)
    {
        gst_xiphos_vision_set_cpu_freq((GstObject *)parent);
    }
    if (filter->hi_power == TRUE && filter->tracker_config.state < hi_state)
    {
        gst_xiphos_vision_unset_cpu_freq((GstObject *)parent);
    }

    if (meta)
    {
        meta->tracks.ts = ts;
        meta->tracks.stream_id = filter->stream_id;
        meta->tracks.state = filter->tracker_config.state;
        meta->tracks.lock = lock;
        meta->tracks.send = send;
        meta->tracks.error_flags = filter->error_flags;

        gst_xiphos_add_world_coordinates(meta, (GstObject *)parent);

        // Send the cam_tracker_targets message to GUI at ~ 5 Hz
        if (((filter->fps) && filter->ctr % ((int)ceil(filter->fps/5.0)) == 0)
                || meta->tracks.send > 0)
        {
            gst_xiphos_send_cam_tracker_targets(meta, (GstObject *)parent);
        }
    }

    if (filter->fps)
    {
        if (filter->ctr % filter->fps == 0) // 1 Hz
            gst_xiphos_vision_send_vision_params((GstObject *)parent);
    }

    filter->ctr++;

    GstAeryonBufferMeta *out_buffer_meta = GST_AERYON_BUFFER_META_GET(outbuf);

    // Adjust outgoing camera metadata for stabilization scale effect
    if (meta)
    {
        if (filter->xipvis_ != NULL)
        {
            float scale = 1.0;
            if (xipvis_stabilization_enabled(filter->xipvis_))
            {
                    scale = xipvis_stbl_get_marginal_scale(filter->xipvis_);
                    scale = (scale > 1e-3) ? scale : 1.0;
                    // theta = 2*arctan(d/2f), artificially hold
                    // sensor size constant, scale the focal length and
                    // solve for new fov.
                    meta->frame.focal_length *= scale;
                    meta->frame.fov_x = 2*atan(tan(meta->frame.fov_x/2)/scale);
                    meta->frame.fov_y = 2*atan(tan(meta->frame.fov_y/2)/scale);
            }
        }
        g_mutex_lock(&filter->stats_mutex);
        filter->stats.fov_x = meta->frame.fov_x;
        filter->stats.fov_y = meta->frame.fov_y;
        filter->stats.f_length = meta->frame.focal_length;
        g_mutex_unlock(&filter->stats_mutex);
    }

    //copy the meta to outbuffer
    if(buffer_meta) {
        if(!out_buffer_meta){
            out_buffer_meta = GST_AERYON_BUFFER_META_ADD(outbuf);

            memcpy(out_buffer_meta, buffer_meta, sizeof(GstAeryonBufferMeta));
            out_buffer_meta->gstmetadata = NULL;

            if(buffer_meta->gstmetadata){
                gst_aeryon_metadata_ref(buffer_meta->gstmetadata);
                out_buffer_meta->gstmetadata = buffer_meta->gstmetadata;
            }
        }
    }

    GST_DEBUG_OBJECT (filter,
      "doing colorspace conversion from %s -> to %s %d -> %d ",
      GST_VIDEO_INFO_NAME (&filter->in_info),
      GST_VIDEO_INFO_NAME (&filter->out_info),
      GST_VIDEO_INFO_N_PLANES(&filter->in_info),
      GST_VIDEO_INFO_N_PLANES(&filter->out_info)
      );

    return GST_FLOW_OK;
}

static GstStructure*
gst_xiphos_vision_create_stats(GstXiphosVision *filter)
{
    g_mutex_lock(&filter->stats_mutex);
    GstStructure *s;
    s = gst_structure_new ("aeryon/xiphos_stats",
        "fov_x", G_TYPE_FLOAT, filter->stats.fov_x,
        "fov_y", G_TYPE_FLOAT, filter->stats.fov_y,
        "f_length", G_TYPE_FLOAT, filter->stats.f_length,
        NULL);
    g_mutex_unlock(&filter->stats_mutex);
    return s;
}