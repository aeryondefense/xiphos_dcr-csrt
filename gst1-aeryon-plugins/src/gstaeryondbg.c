/* GStreamer
 * Copyright (C) 1999,2000 Erik Walthinsen <omega@cse.ogi.edu>
 *                    2000 Wim Taymans <wtay@chello.be>
 *                    2005 Wim Taymans <wim@fluendo.com>
 *                    2015 Aeryon Labs
 *
 * gstaeryon_dbg.c:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
/**
 * SECTION:element-aeryon_dbg
 *
 * Extension of the identity element from gst-plugins-base which supports
 * profiling the time a buffer spends in a particular section of the pipeline.
 * 
 * Dummy element that passes incoming data through unmodified. It has some
 * useful diagnostic functions, such as offset and timestamp checking.
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include <stdint.h>
#include <time.h>

#include "gstaeryondbg.h"
#include "gstaeryonbuffermeta.h"

static GstStaticPadTemplate sinktemplate = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

static GstStaticPadTemplate srctemplate = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

GST_DEBUG_CATEGORY_STATIC (gst_aeryon_dbg_debug);
#define GST_CAT_DEFAULT gst_aeryon_dbg_debug

/* Identity signals and args */
enum
{
  SIGNAL_HANDOFF,
  /* FILL ME */
  LAST_SIGNAL
};

#define DEFAULT_SLEEP_TIME              0
#define DEFAULT_DUPLICATE               1
#define DEFAULT_ERROR_AFTER             -1
#define DEFAULT_DROP_PROBABILITY        0.0
#define DEFAULT_DATARATE                0
#define DEFAULT_SINGLE_SEGMENT          FALSE
#define DEFAULT_DUMP                    FALSE
#define DEFAULT_SYNC                    FALSE
#define DEFAULT_CHECK_IMPERFECT_TIMESTAMP FALSE
#define DEFAULT_TIMESTAMP_DUMP_FILE       NULL
#define DEFAULT_CHECK_IMPERFECT_OFFSET    FALSE
#define DEFAULT_SIGNAL_HANDOFFS           TRUE

enum
{
  PROP_0,
  PROP_SLEEP_TIME,
  PROP_ERROR_AFTER,
  PROP_DROP_PROBABILITY,
  PROP_DATARATE,
  PROP_SINGLE_SEGMENT,
  PROP_DUMP,
  PROP_SYNC,
  PROP_CHECK_IMPERFECT_TIMESTAMP,
  PROP_TIMESTAMP_DUMP_FILE,
  PROP_CHECK_IMPERFECT_OFFSET,
  PROP_SIGNAL_HANDOFFS
};

/* Copy of glib's g_fopen due to win32 libc/cross-DLL brokenness: we can't
 * use the 'file pointer' opened in glib (and returned from this function)
 * in this library, as they may have unrelated C runtimes. */
static FILE *
gst_fopen (const gchar * filename, const gchar * mode)
{
#ifdef G_OS_WIN32
  wchar_t *wfilename = g_utf8_to_utf16 (filename, -1, NULL, NULL, NULL);
  wchar_t *wmode;
  FILE *retval;
  int save_errno;

  if (wfilename == NULL) {
    errno = EINVAL;
    return NULL;
  }

  wmode = g_utf8_to_utf16 (mode, -1, NULL, NULL, NULL);

  if (wmode == NULL) {
    g_free (wfilename);
    errno = EINVAL;
    return NULL;
  }

  retval = _wfopen (wfilename, wmode);
  save_errno = errno;

  g_free (wfilename);
  g_free (wmode);

  errno = save_errno;
  return retval;
#else
  return fopen (filename, mode);
#endif
}

#define _do_init \
    GST_DEBUG_CATEGORY_INIT (gst_aeryon_dbg_debug, "aeryon_dbg", 0, "aeryon_dbg element");
#define gst_aeryon_dbg_parent_class parent_class
G_DEFINE_TYPE_WITH_CODE (GstAeryonDbg, gst_aeryon_dbg, GST_TYPE_BASE_TRANSFORM,
    _do_init);

static void gst_aeryon_dbg_finalize (GObject * object);
static void gst_aeryon_dbg_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_aeryon_dbg_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static gboolean gst_aeryon_dbg_sink_event (GstBaseTransform * trans,
    GstEvent * event);
static GstFlowReturn gst_aeryon_dbg_transform_ip (GstBaseTransform * trans,
    GstBuffer * buf);
static gboolean gst_aeryon_dbg_start (GstBaseTransform * trans);
static gboolean gst_aeryon_dbg_stop (GstBaseTransform * trans);
static GstStateChangeReturn gst_aeryon_dbg_change_state (GstElement * element,
    GstStateChange transition);
static gboolean gst_aeryon_dbg_accept_caps (GstBaseTransform * base,
    GstPadDirection direction, GstCaps * caps);
static gboolean gst_aeryon_dbg_query (GstBaseTransform * base,
    GstPadDirection direction, GstQuery * query);

static guint gst_aeryon_dbg_signals[LAST_SIGNAL] = { 0 };

static GParamSpec *pspec_last_message = NULL;

static void
gst_aeryon_dbg_finalize (GObject * object)
{
  GstAeryonDbg *aeryon_dbg;

  aeryon_dbg = GST_AERYON_DBG (object);

  g_free (aeryon_dbg->ts_dump_filename);
  g_cond_clear (&aeryon_dbg->blocked_cond);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_aeryon_dbg_class_init (GstAeryonDbgClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstBaseTransformClass *gstbasetrans_class;

  gobject_class = G_OBJECT_CLASS (klass);
  gstelement_class = GST_ELEMENT_CLASS (klass);
  gstbasetrans_class = GST_BASE_TRANSFORM_CLASS (klass);

  gobject_class->set_property = gst_aeryon_dbg_set_property;
  gobject_class->get_property = gst_aeryon_dbg_get_property;

  g_object_class_install_property (gobject_class, PROP_SLEEP_TIME,
      g_param_spec_uint ("sleep-time", "Sleep time",
          "Microseconds to sleep between processing", 0, G_MAXUINT,
          DEFAULT_SLEEP_TIME, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_ERROR_AFTER,
      g_param_spec_int ("error-after", "Error After", "Error after N buffers",
          G_MININT, G_MAXINT, DEFAULT_ERROR_AFTER,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_DROP_PROBABILITY,
      g_param_spec_float ("drop-probability", "Drop Probability",
          "The Probability a buffer is dropped", 0.0, 1.0,
          DEFAULT_DROP_PROBABILITY,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_DATARATE,
      g_param_spec_int ("datarate", "Datarate",
          "(Re)timestamps buffers with number of bytes per second (0 = inactive)",
          0, G_MAXINT, DEFAULT_DATARATE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_SINGLE_SEGMENT,
      g_param_spec_boolean ("single-segment", "Single Segment",
          "Timestamp buffers and eat segments so as to appear as one segment",
          DEFAULT_SINGLE_SEGMENT, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  pspec_last_message = g_param_spec_string ("last-message", "last-message",
      "last-message", NULL, G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (gobject_class, PROP_DUMP,
      g_param_spec_boolean ("dump", "Dump", "Dump buffer contents to stdout",
          DEFAULT_DUMP, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_SYNC,
      g_param_spec_boolean ("sync", "Synchronize",
          "Synchronize to pipeline clock", DEFAULT_SYNC,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class,
      PROP_CHECK_IMPERFECT_TIMESTAMP,
      g_param_spec_boolean ("check-imperfect-timestamp",
          "Check for discontiguous timestamps",
          "Send element messages if timestamps and durations do not match up",
          DEFAULT_CHECK_IMPERFECT_TIMESTAMP,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class,
      PROP_TIMESTAMP_DUMP_FILE,
      g_param_spec_string ("received-timestamp-file",
          "Dump the time each buffer was received to this file.",
          "Dump the time each buffer was received to this file.",
          DEFAULT_TIMESTAMP_DUMP_FILE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
          GST_PARAM_MUTABLE_READY));
  g_object_class_install_property (gobject_class, PROP_CHECK_IMPERFECT_OFFSET,
      g_param_spec_boolean ("check-imperfect-offset",
          "Check for discontiguous offset",
          "Send element messages if offset and offset_end do not match up",
          DEFAULT_CHECK_IMPERFECT_OFFSET,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  /**
   * GstAeryonDbg:signal-handoffs
   *
   * If set to #TRUE, the aeryon_dbg will emit a handoff signal when handling a buffer.
   * When set to #FALSE, no signal will be emitted, which might improve performance.
   */
  g_object_class_install_property (gobject_class, PROP_SIGNAL_HANDOFFS,
      g_param_spec_boolean ("signal-handoffs",
          "Signal handoffs", "Send a signal before pushing the buffer",
          DEFAULT_SIGNAL_HANDOFFS, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  /**
   * GstAeryonDbg::handoff:
   * @aeryon_dbg: the aeryon_dbg instance
   * @buffer: the buffer that just has been received
   * @pad: the pad that received it
   *
   * This signal gets emitted before passing the buffer downstream.
   */
  gst_aeryon_dbg_signals[SIGNAL_HANDOFF] =
      g_signal_new ("handoff", G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstAeryonDbgClass, handoff), NULL, NULL,
      g_cclosure_marshal_generic, G_TYPE_NONE, 1,
      GST_TYPE_BUFFER | G_SIGNAL_TYPE_STATIC_SCOPE);

  gobject_class->finalize = gst_aeryon_dbg_finalize;

  gst_element_class_set_static_metadata (gstelement_class,
      "Aeryon debug plugin",
      "Aeryon debug plugin",
      "Pass data without modification. Will calculate the time a buffer spends "
      "in a section of the pipeline when the received-timestamp-file property "
      "is set for a matching pair of aeryon_dbg elements at the start and end "
      "of the pipeline section of interest.", 
      "cmackenzie@aeryon.com");
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&srctemplate));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&sinktemplate));

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_aeryon_dbg_change_state);

  gstbasetrans_class->sink_event = GST_DEBUG_FUNCPTR (gst_aeryon_dbg_sink_event);
  gstbasetrans_class->transform_ip =
      GST_DEBUG_FUNCPTR (gst_aeryon_dbg_transform_ip);
  gstbasetrans_class->start = GST_DEBUG_FUNCPTR (gst_aeryon_dbg_start);
  gstbasetrans_class->stop = GST_DEBUG_FUNCPTR (gst_aeryon_dbg_stop);
  gstbasetrans_class->accept_caps =
      GST_DEBUG_FUNCPTR (gst_aeryon_dbg_accept_caps);
  gstbasetrans_class->query = gst_aeryon_dbg_query;
}

static void
gst_aeryon_dbg_init (GstAeryonDbg * aeryon_dbg)
{
  aeryon_dbg->sleep_time = DEFAULT_SLEEP_TIME;
  aeryon_dbg->error_after = DEFAULT_ERROR_AFTER;
  aeryon_dbg->drop_probability = DEFAULT_DROP_PROBABILITY;
  aeryon_dbg->datarate = DEFAULT_DATARATE;
  aeryon_dbg->single_segment = DEFAULT_SINGLE_SEGMENT;
  aeryon_dbg->sync = DEFAULT_SYNC;
  aeryon_dbg->check_imperfect_timestamp = DEFAULT_CHECK_IMPERFECT_TIMESTAMP;
  aeryon_dbg->check_imperfect_offset = DEFAULT_CHECK_IMPERFECT_OFFSET;
  aeryon_dbg->ts_dump_filename = DEFAULT_TIMESTAMP_DUMP_FILE;
  aeryon_dbg->ts_dump_fd = NULL;
  aeryon_dbg->dump = DEFAULT_DUMP;
  aeryon_dbg->signal_handoffs = DEFAULT_SIGNAL_HANDOFFS;
  g_cond_init (&aeryon_dbg->blocked_cond);

  gst_base_transform_set_gap_aware (GST_BASE_TRANSFORM_CAST (aeryon_dbg), TRUE);
}

static GstFlowReturn
gst_aeryon_dbg_do_sync (GstAeryonDbg * aeryon_dbg, GstClockTime running_time)
{
  GstFlowReturn ret = GST_FLOW_OK;

  if (aeryon_dbg->sync &&
      GST_BASE_TRANSFORM_CAST (aeryon_dbg)->segment.format == GST_FORMAT_TIME) {
    GstClock *clock;

    GST_OBJECT_LOCK (aeryon_dbg);

    while (aeryon_dbg->blocked)
      g_cond_wait (&aeryon_dbg->blocked_cond, GST_OBJECT_GET_LOCK (aeryon_dbg));


    if ((clock = GST_ELEMENT (aeryon_dbg)->clock)) {
      GstClockReturn cret;
      GstClockTime timestamp;

      timestamp = running_time + GST_ELEMENT (aeryon_dbg)->base_time +
          aeryon_dbg->upstream_latency;

      /* save id if we need to unlock */
      aeryon_dbg->clock_id = gst_clock_new_single_shot_id (clock, timestamp);
      GST_OBJECT_UNLOCK (aeryon_dbg);

      cret = gst_clock_id_wait (aeryon_dbg->clock_id, NULL);

      GST_OBJECT_LOCK (aeryon_dbg);
      if (aeryon_dbg->clock_id) {
        gst_clock_id_unref (aeryon_dbg->clock_id);
        aeryon_dbg->clock_id = NULL;
      }
      if (cret == GST_CLOCK_UNSCHEDULED)
        ret = GST_FLOW_EOS;
    }
    GST_OBJECT_UNLOCK (aeryon_dbg);
  }

  return ret;
}

static gboolean
gst_aeryon_dbg_sink_event (GstBaseTransform * trans, GstEvent * event)
{
  GstAeryonDbg *aeryon_dbg;
  gboolean ret = TRUE;

  aeryon_dbg = GST_AERYON_DBG (trans);

  if (aeryon_dbg->single_segment && (GST_EVENT_TYPE (event) == GST_EVENT_SEGMENT)) {
    if (!trans->have_segment) {
      GstEvent *news;
      GstSegment segment;

      gst_event_copy_segment (event, &segment);
      gst_event_copy_segment (event, &trans->segment);
      trans->have_segment = TRUE;

      /* This is the first segment, send out a (0, -1) segment */
      gst_segment_init (&segment, segment.format);
      news = gst_event_new_segment (&segment);

      gst_pad_event_default (trans->sinkpad, GST_OBJECT_CAST (trans), news);
    } else {
      /* need to track segment for proper running time */
      gst_event_copy_segment (event, &trans->segment);
    }
  }

  if (GST_EVENT_TYPE (event) == GST_EVENT_GAP &&
      trans->have_segment && trans->segment.format == GST_FORMAT_TIME) {
    GstClockTime start, dur;

    gst_event_parse_gap (event, &start, &dur);
    if (GST_CLOCK_TIME_IS_VALID (start)) {
      start = gst_segment_to_running_time (&trans->segment,
          GST_FORMAT_TIME, start);

      gst_aeryon_dbg_do_sync (aeryon_dbg, start);

      /* also transform GAP timestamp similar to buffer timestamps */
      if (aeryon_dbg->single_segment) {
        gst_event_unref (event);
        event = gst_event_new_gap (start, dur);
      }
    }
  }

  /* Reset previous timestamp, duration and offsets on SEGMENT
   * to prevent false warnings when checking for perfect streams */
  if (GST_EVENT_TYPE (event) == GST_EVENT_SEGMENT) {
    aeryon_dbg->prev_timestamp = aeryon_dbg->prev_duration = GST_CLOCK_TIME_NONE;
    aeryon_dbg->prev_offset = aeryon_dbg->prev_offset_end = GST_BUFFER_OFFSET_NONE;
  }

  if (aeryon_dbg->single_segment && GST_EVENT_TYPE (event) == GST_EVENT_SEGMENT) {
    /* eat up segments */
    gst_event_unref (event);
    ret = TRUE;
  } else {
    if (GST_EVENT_TYPE (event) == GST_EVENT_FLUSH_START) {
      GST_OBJECT_LOCK (aeryon_dbg);
      if (aeryon_dbg->clock_id) {
        GST_DEBUG_OBJECT (aeryon_dbg, "unlock clock wait");
        gst_clock_id_unschedule (aeryon_dbg->clock_id);
      }
      GST_OBJECT_UNLOCK (aeryon_dbg);
    }

    ret = GST_BASE_TRANSFORM_CLASS (parent_class)->sink_event (trans, event);
  }

  return ret;
}

static void
gst_aeryon_dbg_check_imperfect_timestamp (GstAeryonDbg * aeryon_dbg, GstBuffer * buf)
{
  GstClockTime timestamp = GST_BUFFER_TIMESTAMP (buf);

  /* invalid timestamp drops us out of check.  FIXME: maybe warn ? */
  if (timestamp != GST_CLOCK_TIME_NONE) {
    /* check if we had a previous buffer to compare to */
    if (aeryon_dbg->prev_timestamp != GST_CLOCK_TIME_NONE &&
        aeryon_dbg->prev_duration != GST_CLOCK_TIME_NONE) {
      GstClockTime t_expected;
      GstClockTimeDiff dt;

      t_expected = aeryon_dbg->prev_timestamp + aeryon_dbg->prev_duration;
      dt = GST_CLOCK_DIFF (t_expected, timestamp);
      if (dt != 0) {
        /*
         * "imperfect-timestamp" bus message:
         * @aeryon_dbg:        the aeryon_dbg instance
         * @delta:           the GST_CLOCK_DIFF to the prev timestamp
         * @prev-timestamp:  the previous buffer timestamp
         * @prev-duration:   the previous buffer duration
         * @prev-offset:     the previous buffer offset
         * @prev-offset-end: the previous buffer offset end
         * @cur-timestamp:   the current buffer timestamp
         * @cur-duration:    the current buffer duration
         * @cur-offset:      the current buffer offset
         * @cur-offset-end:  the current buffer offset end
         *
         * This bus message gets emitted if the check-imperfect-timestamp
         * property is set and there is a gap in time between the
         * last buffer and the newly received buffer.
         */
        gst_element_post_message (GST_ELEMENT (aeryon_dbg),
            gst_message_new_element (GST_OBJECT (aeryon_dbg),
                gst_structure_new ("imperfect-timestamp",
                    "delta", G_TYPE_INT64, dt,
                    "prev-timestamp", G_TYPE_UINT64,
                    aeryon_dbg->prev_timestamp, "prev-duration", G_TYPE_UINT64,
                    aeryon_dbg->prev_duration, "prev-offset", G_TYPE_UINT64,
                    aeryon_dbg->prev_offset, "prev-offset-end", G_TYPE_UINT64,
                    aeryon_dbg->prev_offset_end, "cur-timestamp", G_TYPE_UINT64,
                    timestamp, "cur-duration", G_TYPE_UINT64,
                    GST_BUFFER_DURATION (buf), "cur-offset", G_TYPE_UINT64,
                    GST_BUFFER_OFFSET (buf), "cur-offset-end", G_TYPE_UINT64,
                    GST_BUFFER_OFFSET_END (buf), NULL)));
      }
    } else {
      GST_DEBUG_OBJECT (aeryon_dbg, "can't check data-contiguity, no "
          "offset_end was set on previous buffer");
    }
  }
}

static void
gst_aeryon_dbg_dump_timestamp_to_file (GstAeryonDbg * aeryon_dbg, GstBuffer * buf)
{
  GstAeryonBufferMeta *buffer_meta = GST_AERYON_BUFFER_META_GET(buf);
  if (!buffer_meta) {
    buf = gst_buffer_make_writable (buf);
    buffer_meta = GST_AERYON_BUFFER_META_ADD(buf);
  }

  struct timespec tspec;
  clock_gettime(CLOCK_MONOTONIC, &tspec);

  int64_t received_ns = (int64_t)tspec.tv_sec*1e9 + tspec.tv_nsec;
  int64_t enter_ns = -1;
  int64_t exit_ns = -1;
  int64_t diff_ns = -1;
  double diff_ms = -1.0;

  /* If the received_ns entry is invalid, this is the element at the start of
   * section of the pipeline being profiled */
  if (buffer_meta->received_ns == GST_AERYON_BUFFER_META_INVALID_NS) {
    buffer_meta->received_ns = received_ns;
    enter_ns = received_ns;
  }
  else {
    enter_ns = buffer_meta->received_ns;
    exit_ns = received_ns;
    diff_ns = exit_ns - buffer_meta->received_ns;
    diff_ms = diff_ns / 1.0e6;
  }

  fprintf (aeryon_dbg->ts_dump_fd, "%"PRId64",%"PRId64",%"PRId64",%.5f\n", 
      enter_ns, exit_ns, diff_ns, diff_ms);
}

static void
gst_aeryon_dbg_check_imperfect_offset (GstAeryonDbg * aeryon_dbg, GstBuffer * buf)
{
  guint64 offset;

  offset = GST_BUFFER_OFFSET (buf);

  if (aeryon_dbg->prev_offset_end != offset &&
      aeryon_dbg->prev_offset_end != GST_BUFFER_OFFSET_NONE &&
      offset != GST_BUFFER_OFFSET_NONE) {
    /*
     * "imperfect-offset" bus message:
     * @aeryon_dbg:        the aeryon_dbg instance
     * @prev-timestamp:  the previous buffer timestamp
     * @prev-duration:   the previous buffer duration
     * @prev-offset:     the previous buffer offset
     * @prev-offset-end: the previous buffer offset end
     * @cur-timestamp:   the current buffer timestamp
     * @cur-duration:    the current buffer duration
     * @cur-offset:      the current buffer offset
     * @cur-offset-end:  the current buffer offset end
     *
     * This bus message gets emitted if the check-imperfect-offset
     * property is set and there is a gap in offsets between the
     * last buffer and the newly received buffer.
     */
    gst_element_post_message (GST_ELEMENT (aeryon_dbg),
        gst_message_new_element (GST_OBJECT (aeryon_dbg),
            gst_structure_new ("imperfect-offset", "prev-timestamp",
                G_TYPE_UINT64, aeryon_dbg->prev_timestamp, "prev-duration",
                G_TYPE_UINT64, aeryon_dbg->prev_duration, "prev-offset",
                G_TYPE_UINT64, aeryon_dbg->prev_offset, "prev-offset-end",
                G_TYPE_UINT64, aeryon_dbg->prev_offset_end, "cur-timestamp",
                G_TYPE_UINT64, GST_BUFFER_TIMESTAMP (buf), "cur-duration",
                G_TYPE_UINT64, GST_BUFFER_DURATION (buf), "cur-offset",
                G_TYPE_UINT64, GST_BUFFER_OFFSET (buf), "cur-offset-end",
                G_TYPE_UINT64, GST_BUFFER_OFFSET_END (buf), NULL)));
  } else {
    GST_DEBUG_OBJECT (aeryon_dbg, "can't check offset contiguity, no offset "
        "and/or offset_end were set on previous buffer");
  }
}

static GstFlowReturn
gst_aeryon_dbg_transform_ip (GstBaseTransform * trans, GstBuffer * buf)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstAeryonDbg *aeryon_dbg = GST_AERYON_DBG (trans);
  GstClockTime rundts = GST_CLOCK_TIME_NONE;
  GstClockTime runpts = GST_CLOCK_TIME_NONE;
  GstClockTime ts, duration, runtimestamp;
  gsize size;

  size = gst_buffer_get_size (buf);

  if (aeryon_dbg->check_imperfect_timestamp)
    gst_aeryon_dbg_check_imperfect_timestamp (aeryon_dbg, buf);
  if (aeryon_dbg->check_imperfect_offset)
    gst_aeryon_dbg_check_imperfect_offset (aeryon_dbg, buf);
  if (aeryon_dbg->ts_dump_fd)
    gst_aeryon_dbg_dump_timestamp_to_file (aeryon_dbg, buf);

  /* update prev values */
  aeryon_dbg->prev_timestamp = GST_BUFFER_TIMESTAMP (buf);
  aeryon_dbg->prev_duration = GST_BUFFER_DURATION (buf);
  aeryon_dbg->prev_offset_end = GST_BUFFER_OFFSET_END (buf);
  aeryon_dbg->prev_offset = GST_BUFFER_OFFSET (buf);

  if (aeryon_dbg->error_after >= 0) {
    aeryon_dbg->error_after--;
    if (aeryon_dbg->error_after == 0)
      goto error_after;
  }

  if (aeryon_dbg->drop_probability > 0.0) {
    if ((gfloat) (1.0 * rand () / (RAND_MAX)) < aeryon_dbg->drop_probability)
      goto dropped;
  }

  if (aeryon_dbg->dump) {
    GstMapInfo info;

    gst_buffer_map (buf, &info, GST_MAP_READ);
    gst_util_dump_mem (info.data, info.size);
    gst_buffer_unmap (buf, &info);
  }

  if (aeryon_dbg->datarate > 0) {
    GstClockTime time = gst_util_uint64_scale_int (aeryon_dbg->offset,
        GST_SECOND, aeryon_dbg->datarate);

    GST_BUFFER_PTS (buf) = GST_BUFFER_DTS (buf) = time;
    GST_BUFFER_DURATION (buf) = size * GST_SECOND / aeryon_dbg->datarate;
  }

  if (aeryon_dbg->signal_handoffs)
    g_signal_emit (aeryon_dbg, gst_aeryon_dbg_signals[SIGNAL_HANDOFF], 0, buf);

  if (trans->segment.format == GST_FORMAT_TIME) {
    rundts = gst_segment_to_running_time (&trans->segment,
        GST_FORMAT_TIME, GST_BUFFER_DTS (buf));
    runpts = gst_segment_to_running_time (&trans->segment,
        GST_FORMAT_TIME, GST_BUFFER_PTS (buf));
  }

  if (GST_CLOCK_TIME_IS_VALID (rundts))
    runtimestamp = rundts;
  else if (GST_CLOCK_TIME_IS_VALID (runpts))
    runtimestamp = runpts;
  else
    runtimestamp = 0;
  ret = gst_aeryon_dbg_do_sync (aeryon_dbg, runtimestamp);

  aeryon_dbg->offset += size;

  if (aeryon_dbg->sleep_time && ret == GST_FLOW_OK)
    g_usleep (aeryon_dbg->sleep_time);

  if (aeryon_dbg->single_segment && (trans->segment.format == GST_FORMAT_TIME)
      && (ret == GST_FLOW_OK)) {
    GST_BUFFER_DTS (buf) = rundts;
    GST_BUFFER_PTS (buf) = runpts;
    GST_BUFFER_OFFSET (buf) = GST_CLOCK_TIME_NONE;
    GST_BUFFER_OFFSET_END (buf) = GST_CLOCK_TIME_NONE;
  }

  return ret;

  /* ERRORS */
error_after:
  {
    GST_ELEMENT_ERROR (aeryon_dbg, CORE, FAILED,
        ("Failed after iterations as requested."), (NULL));
    return GST_FLOW_ERROR;
  }
dropped:
  {
    ts = GST_BUFFER_TIMESTAMP (buf);
    if (GST_CLOCK_TIME_IS_VALID (ts)) {
      duration = GST_BUFFER_DURATION (buf);
      gst_pad_push_event (GST_BASE_TRANSFORM_SRC_PAD (aeryon_dbg),
          gst_event_new_gap (ts, duration));
    }

    /* return DROPPED to basetransform. */
    return GST_BASE_TRANSFORM_FLOW_DROPPED;
  }
}

static gboolean
gst_aeryon_dbg_set_ts_dump_file (GstAeryonDbg *aeryon_dbg, const gchar * filename,
    GError ** error)
{
  if (aeryon_dbg->ts_dump_fd) {
    g_warning ("Changing the `timestamp-dump-file' property on aeryon_dbg when "
        "a file is open is not supported.");
    g_set_error (error, GST_URI_ERROR, GST_URI_ERROR_BAD_STATE,
        "Changing the 'timestamp-dump-file' property on aeryon_dbg when "
        "a file is open is not supported");
    return FALSE;
  }

  g_free (aeryon_dbg->ts_dump_filename);

  if (filename != NULL) {
    /* we store the filename as we received it from the application. On Windows
     * this should be in UTF8 */
    aeryon_dbg->ts_dump_filename = g_strdup (filename);
    GST_INFO_OBJECT (aeryon_dbg, "filename : %s", aeryon_dbg->ts_dump_filename);
  } else {
    aeryon_dbg->ts_dump_filename = NULL;
  }

  return TRUE;
}

static void
gst_aeryon_dbg_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstAeryonDbg *aeryon_dbg;

  aeryon_dbg = GST_AERYON_DBG (object);

  switch (prop_id) {
    case PROP_SLEEP_TIME:
      aeryon_dbg->sleep_time = g_value_get_uint (value);
      break;
    case PROP_SINGLE_SEGMENT:
      aeryon_dbg->single_segment = g_value_get_boolean (value);
      break;
    case PROP_DUMP:
      aeryon_dbg->dump = g_value_get_boolean (value);
      break;
    case PROP_ERROR_AFTER:
      aeryon_dbg->error_after = g_value_get_int (value);
      break;
    case PROP_DROP_PROBABILITY:
      aeryon_dbg->drop_probability = g_value_get_float (value);
      break;
    case PROP_DATARATE:
      aeryon_dbg->datarate = g_value_get_int (value);
      break;
    case PROP_SYNC:
      aeryon_dbg->sync = g_value_get_boolean (value);
      break;
    case PROP_CHECK_IMPERFECT_TIMESTAMP:
      aeryon_dbg->check_imperfect_timestamp = g_value_get_boolean (value);
      break;
    case PROP_TIMESTAMP_DUMP_FILE:
      gst_aeryon_dbg_set_ts_dump_file (aeryon_dbg, g_value_get_string (value), NULL);
      break;
    case PROP_CHECK_IMPERFECT_OFFSET:
      aeryon_dbg->check_imperfect_offset = g_value_get_boolean (value);
      break;
    case PROP_SIGNAL_HANDOFFS:
      aeryon_dbg->signal_handoffs = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
  if (aeryon_dbg->datarate > 0 || aeryon_dbg->single_segment)
    gst_base_transform_set_passthrough (GST_BASE_TRANSFORM (aeryon_dbg), FALSE);
  else
    gst_base_transform_set_passthrough (GST_BASE_TRANSFORM (aeryon_dbg), TRUE);
}

static void
gst_aeryon_dbg_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  GstAeryonDbg *aeryon_dbg;

  aeryon_dbg = GST_AERYON_DBG (object);

  switch (prop_id) {
    case PROP_SLEEP_TIME:
      g_value_set_uint (value, aeryon_dbg->sleep_time);
      break;
    case PROP_ERROR_AFTER:
      g_value_set_int (value, aeryon_dbg->error_after);
      break;
    case PROP_DROP_PROBABILITY:
      g_value_set_float (value, aeryon_dbg->drop_probability);
      break;
    case PROP_DATARATE:
      g_value_set_int (value, aeryon_dbg->datarate);
      break;
    case PROP_SINGLE_SEGMENT:
      g_value_set_boolean (value, aeryon_dbg->single_segment);
      break;
    case PROP_DUMP:
      g_value_set_boolean (value, aeryon_dbg->dump);
      break;
    case PROP_SYNC:
      g_value_set_boolean (value, aeryon_dbg->sync);
      break;
    case PROP_CHECK_IMPERFECT_TIMESTAMP:
      g_value_set_boolean (value, aeryon_dbg->check_imperfect_timestamp);
      break;
    case PROP_TIMESTAMP_DUMP_FILE:
      g_value_set_string (value, aeryon_dbg->ts_dump_filename);
      break;
    case PROP_CHECK_IMPERFECT_OFFSET:
      g_value_set_boolean (value, aeryon_dbg->check_imperfect_offset);
      break;
    case PROP_SIGNAL_HANDOFFS:
      g_value_set_boolean (value, aeryon_dbg->signal_handoffs);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static gboolean
gst_aeryon_dbg_start (GstBaseTransform * trans)
{
  GstAeryonDbg *aeryon_dbg;

  aeryon_dbg = GST_AERYON_DBG (trans);

  aeryon_dbg->offset = 0;
  aeryon_dbg->prev_timestamp = GST_CLOCK_TIME_NONE;
  aeryon_dbg->prev_duration = GST_CLOCK_TIME_NONE;
  aeryon_dbg->prev_offset_end = GST_BUFFER_OFFSET_NONE;
  aeryon_dbg->prev_offset = GST_BUFFER_OFFSET_NONE;

  if (aeryon_dbg->ts_dump_filename != NULL) {
    aeryon_dbg->ts_dump_fd = gst_fopen (aeryon_dbg->ts_dump_filename, "w");
    if (aeryon_dbg->ts_dump_fd == NULL) {
      GST_ELEMENT_ERROR (aeryon_dbg, RESOURCE, OPEN_WRITE,
      ("Could not open file \"%s\" for writing.", aeryon_dbg->ts_dump_filename),
      GST_ERROR_SYSTEM);
      return FALSE;
    }
    fprintf (aeryon_dbg->ts_dump_fd, "enter_ns,exit_ns,diff_ns,diff_ms\n");
  }

  return TRUE;
}

static gboolean
gst_aeryon_dbg_stop (GstBaseTransform * trans)
{
  GstAeryonDbg *aeryon_dbg;

  aeryon_dbg = GST_AERYON_DBG (trans);

  GST_OBJECT_LOCK (aeryon_dbg);
  if (aeryon_dbg->ts_dump_fd) {
    if (fflush (aeryon_dbg->ts_dump_fd)) {
      GST_ELEMENT_ERROR (aeryon_dbg, RESOURCE, WRITE,
          ("Error while writing to file \"%s\".", aeryon_dbg->ts_dump_filename),
          ("%s", g_strerror (errno)));
    }
    if (fsync (fileno (aeryon_dbg->ts_dump_fd))) {
      // do not post gst error if file does not support sync
      if(errno != EROFS && errno != EINVAL) {
        GST_ELEMENT_ERROR (aeryon_dbg, RESOURCE, WRITE,
            ("Error while syncing to file \"%s\".", aeryon_dbg->ts_dump_filename),
            ("%s", g_strerror (errno)));
      }
    }
    if (fclose (aeryon_dbg->ts_dump_fd) != 0) {
      GST_ELEMENT_ERROR (aeryon_dbg, RESOURCE, CLOSE,
        ("Error closing file \"%s\".", aeryon_dbg->ts_dump_filename),
        GST_ERROR_SYSTEM);
    }
    aeryon_dbg->ts_dump_fd = NULL;
  }
  GST_OBJECT_UNLOCK (aeryon_dbg);

  return TRUE;
}

static gboolean
gst_aeryon_dbg_accept_caps (GstBaseTransform * base,
    GstPadDirection direction, GstCaps * caps)
{
  gboolean ret;
  GstPad *pad;

  /* Proxy accept-caps */

  if (direction == GST_PAD_SRC)
    pad = GST_BASE_TRANSFORM_SINK_PAD (base);
  else
    pad = GST_BASE_TRANSFORM_SRC_PAD (base);

  ret = gst_pad_peer_query_accept_caps (pad, caps);

  return ret;
}

static gboolean
gst_aeryon_dbg_query (GstBaseTransform * base, GstPadDirection direction,
    GstQuery * query)
{
  GstAeryonDbg *aeryon_dbg;
  gboolean ret;

  aeryon_dbg = GST_AERYON_DBG (base);

  ret = GST_BASE_TRANSFORM_CLASS (parent_class)->query (base, direction, query);

  if (GST_QUERY_TYPE (query) == GST_QUERY_LATENCY) {
    gboolean live = FALSE;
    GstClockTime min = 0, max = 0;

    if (ret) {
      gst_query_parse_latency (query, &live, &min, &max);

      if (aeryon_dbg->sync && max < min) {
        GST_ELEMENT_WARNING (base, CORE, CLOCK, (NULL),
            ("Impossible to configure latency before aeryon_dbg sync=true:"
                " max %" GST_TIME_FORMAT " < min %"
                GST_TIME_FORMAT ". Add queues or other buffering elements.",
                GST_TIME_ARGS (max), GST_TIME_ARGS (min)));
      }
    }

    /* Ignore the upstream latency if it is not live */
    GST_OBJECT_LOCK (aeryon_dbg);
    if (live)
      aeryon_dbg->upstream_latency = min;
    else
      aeryon_dbg->upstream_latency = 0;
    GST_OBJECT_UNLOCK (aeryon_dbg);

    gst_query_set_latency (query, live || aeryon_dbg->sync, min, max);
    ret = TRUE;
  }
  return ret;
}

static GstStateChangeReturn
gst_aeryon_dbg_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret;
  GstAeryonDbg *aeryon_dbg = GST_AERYON_DBG (element);
  gboolean no_preroll = FALSE;

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      GST_OBJECT_LOCK (aeryon_dbg);
      aeryon_dbg->blocked = TRUE;
      GST_OBJECT_UNLOCK (aeryon_dbg);
      if (aeryon_dbg->sync)
        no_preroll = TRUE;
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      GST_OBJECT_LOCK (aeryon_dbg);
      aeryon_dbg->blocked = FALSE;
      g_cond_broadcast (&aeryon_dbg->blocked_cond);
      GST_OBJECT_UNLOCK (aeryon_dbg);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      GST_OBJECT_LOCK (aeryon_dbg);
      if (aeryon_dbg->clock_id) {
        GST_DEBUG_OBJECT (aeryon_dbg, "unlock clock wait");
        gst_clock_id_unschedule (aeryon_dbg->clock_id);
      }
      aeryon_dbg->blocked = FALSE;
      g_cond_broadcast (&aeryon_dbg->blocked_cond);
      GST_OBJECT_UNLOCK (aeryon_dbg);
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      GST_OBJECT_LOCK (aeryon_dbg);
      aeryon_dbg->upstream_latency = 0;
      aeryon_dbg->blocked = TRUE;
      GST_OBJECT_UNLOCK (aeryon_dbg);
      if (aeryon_dbg->sync)
        no_preroll = TRUE;
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      break;
    default:
      break;
  }

  if (no_preroll && ret == GST_STATE_CHANGE_SUCCESS)
    ret = GST_STATE_CHANGE_NO_PREROLL;

  return ret;
}
