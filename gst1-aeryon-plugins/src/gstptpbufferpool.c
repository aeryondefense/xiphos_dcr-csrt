#ifdef HAVE_CONFIG_H
    #include <config.h>
#endif

#include <assert.h>
#include <string.h>

#include <gst/video/gstvideometa.h>
#include "gstptpbufferpool.h"

GST_DEBUG_CATEGORY_EXTERN(gst_ptp_src_debug);
#define GST_CAT_DEFAULT gst_ptp_src_debug

GType gst_ptp_meta_api_get_type(void)
{
    static volatile GType type;
    static const gchar *tags[] = { GST_META_TAG_VIDEO_STR, GST_META_TAG_MEMORY_STR, NULL };

    if (g_once_init_enter(&type)) 
    {
        GType _type = gst_meta_api_type_register("GstPtpMetaAPI", tags);
        g_once_init_leave(&type, _type);
    }
    return type;
}

const GstMetaInfo *gst_ptp_meta_get_info(void)
{
    static const GstMetaInfo *meta_info = NULL;

    if (g_once_init_enter(&meta_info))
    {
        const GstMetaInfo *meta = 
            gst_meta_register(gst_ptp_meta_api_get_type(), 
                    "GstPtpMeta", 
                    sizeof(GstPtpMeta), 
                    (GstMetaInitFunction) NULL, 
                    (GstMetaFreeFunction) NULL, 
                    (GstMetaTransformFunction) NULL);
        g_once_init_leave(&meta_info, meta);
    }
    return meta_info;
}

#define gst_ptp_buffer_pool_parent_class parent_class
G_DEFINE_TYPE(GstPtpBufferPool, gst_ptp_buffer_pool, GST_TYPE_BUFFER_POOL);

static void gst_ptp_buffer_pool_free_buffer(GstBufferPool *pool, GstBuffer *buffer);
static GstFlowReturn gst_ptp_buffer_pool_alloc_buffer(GstBufferPool *pool, GstBuffer **buffer, GstBufferPoolAcquireParams *params);
static GstFlowReturn gst_ptp_buffer_pool_acquire_buffer(GstBufferPool *pool, GstBuffer **buffer, GstBufferPoolAcquireParams *params);
static void gst_ptp_buffer_pool_release_buffer(GstBufferPool *pool, GstBuffer *buffer);
static gboolean gst_ptp_buffer_pool_set_config(GstBufferPool *pool, GstStructure *config);
static gboolean gst_ptp_buffer_pool_start(GstBufferPool *pool);
static gboolean gst_ptp_buffer_pool_stop(GstBufferPool *pool);

static void gst_ptp_buffer_pool_finalize(GObject *object);
static void gst_ptp_buffer_pool_init(GstPtpBufferPool *pool);

static void gst_ptp_buffer_pool_class_init(GstPtpBufferPoolClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);
    GstBufferPoolClass *bufferpool_class = GST_BUFFER_POOL_CLASS(klass);

    object_class->finalize = gst_ptp_buffer_pool_finalize;

    bufferpool_class->start = gst_ptp_buffer_pool_start;
    bufferpool_class->stop = gst_ptp_buffer_pool_stop;
    bufferpool_class->set_config = gst_ptp_buffer_pool_set_config;
    bufferpool_class->alloc_buffer = gst_ptp_buffer_pool_alloc_buffer;
    bufferpool_class->acquire_buffer = gst_ptp_buffer_pool_acquire_buffer;
    bufferpool_class->release_buffer = gst_ptp_buffer_pool_release_buffer;
    bufferpool_class->free_buffer = gst_ptp_buffer_pool_free_buffer;
}

static void gst_ptp_buffer_pool_init(GstPtpBufferPool *pool)
{
    (void)pool;
}

static void gst_ptp_buffer_pool_finalize(GObject *object)
{
    G_OBJECT_CLASS(parent_class)->finalize(object);
}

GstBufferPool *gst_ptp_buffer_pool_new(GstPTPCam *ptpcam, GstCaps *caps)
{
    GstPtpBufferPool *pool = NULL;
    GstStructure *s;
    PTPObjectInfo liveview_info;
    PTPCam *camera = NULL;
    uint16_t retval = PTP_RC_OK;
    uint32_t framesize_est;

    memset(&liveview_info, 0, sizeof(liveview_info));
    pool = (GstPtpBufferPool *) g_object_new(GST_TYPE_PTP_BUFFER_POOL, NULL);
    pool->ptpcam = ptpcam;
    gst_ptpcam_ref(ptpcam);
    camera = ptpcam->camera;

    retval = get_sony_liveview_status(camera, &liveview_info, &framesize_est);
    if (retval != PTP_RC_OK)
    {
        GST_WARNING_OBJECT(pool, "failed to get framesize estimate from camera, code %04x. Using default framesize of 192k", retval);
        framesize_est = 192*1024;
    }

    s = gst_buffer_pool_get_config(GST_BUFFER_POOL_CAST(pool));
    gst_buffer_pool_config_set_params(s, caps, (guint)framesize_est, MAX_PTP_VIDEO_BUFFERS, MAX_PTP_VIDEO_BUFFERS);
    gst_buffer_pool_set_config(GST_BUFFER_POOL_CAST(pool), s);

    gst_video_info_from_caps(&pool->info, caps);
    ptp_free_ObjectInfo(&liveview_info);
    return GST_BUFFER_POOL(pool);
}

static gboolean gst_ptp_buffer_pool_start(GstBufferPool *pool)
{
    GstPtpBufferPool *ptp_pool = GST_PTP_BUFFER_POOL(pool);
    GST_LOG_OBJECT(ptp_pool, "PTP Pool start!");

    ptp_pool->ptp_bufs = g_new0(PTPVideoBuf, ptp_pool->num_buffers);
    ptp_pool->buffers = g_new0(GstBuffer *, ptp_pool->num_buffers);
    ptp_pool->num_allocated = 0;
    ptp_pool->num_queued = 0;

    // this will call alloc_buffer and then release_buffer
    // which then queues the buffer to PTPVideo
    if (!GST_BUFFER_POOL_CLASS(parent_class)->start(pool))
    {
        GST_ERROR_OBJECT(ptp_pool, "parent->start() failed!");
        return FALSE;
    }

    // TODO start streaming here?
    GST_INFO_OBJECT(ptp_pool, "starting sony liveview stream");
    gint fps_n, fps_d;
    fps_n = GST_VIDEO_INFO_FPS_N(&ptp_pool->info);
    fps_d = GST_VIDEO_INFO_FPS_D(&ptp_pool->info);
    start_sony_liveview_stream(ptp_pool->ptpcam->camera, fps_n/fps_d);
    
    return TRUE;
}

static gboolean gst_ptp_buffer_pool_stop(GstBufferPool *pool)
{
    GstPtpBufferPool *ptp_pool = GST_PTP_BUFFER_POOL(pool);

    GST_WARNING_OBJECT(pool, "stopping pool");

    stop_sony_liveview_stream(ptp_pool->ptpcam->camera);
    gst_ptpcam_unref(ptp_pool->ptpcam);

    g_free(ptp_pool->buffers);
    g_free(ptp_pool->ptp_bufs);
    ptp_pool->buffers = NULL;
    ptp_pool->ptp_bufs = NULL;
    ptp_pool->num_allocated = 0;

    return TRUE;
}

static gboolean gst_ptp_buffer_pool_set_config(GstBufferPool *pool, GstStructure *config)
{
    GstPtpBufferPool *ptp_pool = GST_PTP_BUFFER_POOL(pool);
    GstCaps *caps;
    guint size, min_buffers, max_buffers;

    GST_LOG_OBJECT(ptp_pool, "PTP pool set config");

    if (!gst_buffer_pool_config_get_params(config, &caps, &size, &min_buffers, &max_buffers))
    {
        GST_ERROR_OBJECT(ptp_pool, "invalid config %" GST_PTR_FORMAT, config);
        return FALSE;
    }
    ptp_pool->size = (gsize)size;
    ptp_pool->num_buffers = min_buffers;

    return GST_BUFFER_POOL_CLASS(parent_class)->set_config(pool, config);
}

static GstFlowReturn gst_ptp_buffer_pool_alloc_buffer(GstBufferPool *pool, GstBuffer **buffer, GstBufferPoolAcquireParams *params)
{
    (void)params;
    GstPtpBufferPool *ptp_pool = GST_PTP_BUFFER_POOL(pool);
    GstBuffer *newbuf;
    GstPtpMeta *meta;
    guint index;

    gpointer data = g_malloc0(ptp_pool->size);
    newbuf = gst_buffer_new_wrapped(data, ptp_pool->size);
    meta = GST_PTP_META_ADD(newbuf);
    index = ptp_pool->num_allocated;
    ptp_pool->num_allocated++;

    meta->ptp_buf = &ptp_pool->ptp_bufs[index];
    ptp_pool->ptp_bufs[index].index = (uint32_t)index;
    ptp_pool->ptp_bufs[index].data = (uint8_t *)data;
    ptp_pool->ptp_bufs[index].data_size = (uint32_t)ptp_pool->size;
    ptp_pool->buffers[index] = newbuf;

    GST_LOG_OBJECT(ptp_pool, "alloc index %d", index);
    *buffer = newbuf;
    return GST_FLOW_OK;
}

static void gst_ptp_buffer_pool_free_buffer(GstBufferPool *pool, GstBuffer *buffer)
{
    GstPtpBufferPool *ptp_pool = GST_PTP_BUFFER_POOL(pool);
    GstPtpMeta *meta = GST_PTP_META_GET(buffer);
    ptp_pool->buffers[meta->ptp_buf->index] = NULL;
    gst_buffer_unref(buffer);
}

static void gst_ptp_buffer_pool_release_buffer(GstBufferPool *pool, GstBuffer *buffer)
{
    GstPtpBufferPool *ptp_pool = GST_PTP_BUFFER_POOL(pool);
    GstPtpMeta *meta = GST_PTP_META_GET(buffer);

    GST_LOG_OBJECT(ptp_pool, "ptp buffer pool queueing buffer!");

    assert(gst_buffer_is_writable(buffer));
    g_atomic_int_inc (&ptp_pool->num_queued);    
    ptp_queue_buffer(ptp_pool->ptpcam->camera->vendor_ext.sony.video_queue, meta->ptp_buf);
}

static GstFlowReturn gst_ptp_buffer_pool_acquire_buffer(GstBufferPool *pool, GstBuffer **buffer, GstBufferPoolAcquireParams *params)
{
    (void)params;
    GstPtpBufferPool *ptp_pool = GST_PTP_BUFFER_POOL(pool);
    PTPVideoBuf ptp_buf;

    if (!gst_buffer_pool_is_active(pool))
        return GST_FLOW_EOS;

    GST_LOG_OBJECT(ptp_pool, "dequeueing PTP buffer..");

    if (ptp_dequeue_buffer_gstreamer(ptp_pool->ptpcam->camera->vendor_ext.sony.video_queue, &ptp_buf) != PTP_RC_OK)
        return GST_FLOW_ERROR;

    //FIXME: empty queue should not trigger an assertion
    assert(g_atomic_int_get(&ptp_pool->num_queued) > 0);  

    g_atomic_int_add(&ptp_pool->num_queued, -1);

    *buffer = ptp_pool->buffers[ptp_buf.index];

    return GST_FLOW_OK;
}


