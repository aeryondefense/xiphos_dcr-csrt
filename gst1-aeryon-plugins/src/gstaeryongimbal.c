#ifdef HAVE_CONFIG_H // What does this do?
#include <config.h>
#endif

#include <sys/time.h>
#include <gst/video/video.h>
#include "gstaeryongimbal.h"
#include "msg_types.h"
#include "gstaeryonmessage.h"
#include "gstaeryonbuffermeta.h"
#include "string.h"
#include "gmbl_math.h"
#include "gen_utils.h"
#include "filters.h"
#include "aeryon_math.h"
#include "acs_target.h"
#include <stdio.h>

GST_DEBUG_CATEGORY (gst_aeryon_gimbal_debug);
#define GST_CAT_DEFAULT gst_aeryon_gimbal_debug

#define Xi   0
#define Yi   1
#define MIN_CONF 0.3

enum
{
    TRACK_IDLE,
    TRACK_UPDATE,
    TRACK_STOP
};

/* Filter signals and args */
enum
{
    /* FILL ME */
    LAST_SIGNAL
};

enum
{
    PROP_0,
    PROP_SILENT,
    PROP_HDZOOM
};


/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE("sink",
                                                                   GST_PAD_SINK,
                                                                   GST_PAD_ALWAYS,
                                                                   GST_STATIC_CAPS("ANY"));

static GstStaticPadTemplate srcany_factory = GST_STATIC_PAD_TEMPLATE("any",
                                                                     GST_PAD_SRC,
                                                                     GST_PAD_ALWAYS,
                                                                     GST_STATIC_CAPS("ANY"));

#define gst_aeryon_gimbal_parent_class parent_class
G_DEFINE_TYPE (GstAeryonGimbal, gst_aeryon_gimbal, GST_TYPE_ELEMENT);

static void gst_aeryon_gimbal_set_property (GObject * object, guint prop_id,
                                           const GValue * value, GParamSpec * pspec);
static void gst_aeryon_gimbal_get_property (GObject * object, guint prop_id,
                                           GValue * value, GParamSpec * pspec);

static gboolean gst_aeryon_gimbal_sink_event (GstPad * pad, GstObject * parent, GstEvent * event);
static GstFlowReturn gst_aeryon_gimbal_chain (GstPad * pad, GstObject * parent, GstBuffer * buf);

/* GObject vmethod implementations */

/* initialize the aeryon_gimbal's class */
static void
gst_aeryon_gimbal_class_init (GstAeryonGimbalClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;

    gobject_class = (GObjectClass *) klass;
    gstelement_class = (GstElementClass *) klass;

    gobject_class->set_property = gst_aeryon_gimbal_set_property;
    gobject_class->get_property = gst_aeryon_gimbal_get_property;

    g_object_class_install_property (gobject_class, PROP_SILENT,
        g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
                              FALSE, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, PROP_HDZOOM,
        g_param_spec_boolean ("hdzoom", "HDZoom", "Is this an HDZoom ?",
                              FALSE, G_PARAM_READWRITE));    

    gst_element_class_set_details_simple(gstelement_class,
        "Aeryon gimbal filter",
        "Aeryon gimbal filter",
        "receive target tracking/scene steering information embedded in "
        "metadata and send actual target telemetry to the gimbal controller",
        "april@aeryon.com");

    gst_element_class_add_pad_template (gstelement_class,
        gst_static_pad_template_get (&srcany_factory));
    gst_element_class_add_pad_template (gstelement_class,
        gst_static_pad_template_get (&sink_factory));
}

/* initialize the new aeryon_gimbal element
 * instantiate pads and add them to element
 * set pad callback functions
 * initialize instance structure
 */
static void
gst_aeryon_gimbal_init(GstAeryonGimbal * filter)
{
    filter->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
    gst_pad_set_event_function (filter->sinkpad,
                               GST_DEBUG_FUNCPTR(gst_aeryon_gimbal_sink_event));
    gst_pad_set_chain_function (filter->sinkpad,
                               GST_DEBUG_FUNCPTR(gst_aeryon_gimbal_chain));
    GST_PAD_SET_PROXY_CAPS (filter->sinkpad);
    gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);

    filter->srcpad_any = gst_pad_new_from_static_template (&srcany_factory, "any");
    GST_PAD_SET_PROXY_CAPS (filter->srcpad_any);
    gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad_any);
    
    filter->silent = FALSE;
    filter->hdzoom = FALSE;
    filter->params.Ki[0] = KI_X;
    filter->params.Ki[1] = KI_Y;
    filter->params.Kp[0] = KP_X;
    filter->params.Kp[1] = KP_Y;
    filter->params.dead_band = DEAD_BAND;
    filter->params.max_target_error[0] = MAX_TARGET_ERROR_X;
    filter->params.max_target_error[1] = MAX_TARGET_ERROR_Y;
    filter->params.max_target_error_i[0] = MAX_TARGET_ERROR_I_X;
    filter->params.max_target_error_i[1] = MAX_TARGET_ERROR_I_Y;
    filter->params.target_distance = TARGET_DISTANCE;
    filter->params.target_frac_filter = TARGET_FRAC_FILTER;
    filter->track_state = TRACK_IDLE;
    filter->counter = 0;
    filter->fps = 24;
    filter->mycount = 0;
    filter->totalCount = 0;
    memset(&filter->target_filt, 0, sizeof(filter->target_filt));
}

static void
gst_aeryon_gimbal_set_property (GObject * object, guint prop_id,
                               const GValue * value, GParamSpec * pspec)
{
    GstAeryonGimbal *filter = GST_AERYON_GIMBAL (object);

    switch (prop_id)
    {
        case PROP_SILENT:
            filter->silent = g_value_get_boolean (value);
            break;
        case PROP_HDZOOM:
            filter->hdzoom = g_value_get_boolean (value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
gst_aeryon_gimbal_get_property (GObject * object, guint prop_id,
                               GValue * value, GParamSpec * pspec)
{
    GstAeryonGimbal *filter = GST_AERYON_GIMBAL(object);

    switch (prop_id)
    {
        case PROP_SILENT:
            g_value_set_boolean (value, filter->silent);
            break;
        case PROP_HDZOOM:
            g_value_set_boolean (value, filter->hdzoom);
            break;            
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

/* GstElement vmethod implementations */

/* this function handles sink events */
static gboolean
gst_aeryon_gimbal_sink_event(GstPad * pad, GstObject * parent, GstEvent * event)
{
    gboolean ret;
    GstGimbalParams *gstparams;
    GstAeryonGimbal *filter;
    USE(filter);
    filter = GST_AERYON_GIMBAL (parent);

    /* Handle any Aeryon sink events first */
    if ((GstAeryonEventType) GST_EVENT_TYPE(event) == GST_EVENT_GIMBAL_PARAMS)
    {
        ret = gst_event_get_gimbal_params(event, &gstparams);
        if (ret)
        {
            // Debug
            filter->params = *(gstparams->params);
        }
    }
    
    switch (GST_EVENT_TYPE (event))
    {
        case GST_EVENT_CAPS:
        {
            GstCaps * caps;

            gst_event_parse_caps (event, &caps);
            
            // Update fps for message sending
            GstVideoInfo info;
            if (gst_video_info_from_caps(&info, caps))
            {
                guint fps = info.fps_n / info.fps_d;
                // If not valid, make it reasonable
                fps = (fps < 1)? 24 : fps;
                fps = (fps > 30)? 30 : fps;
                filter->fps = fps;
            }

            /* and forward */
            ret = gst_pad_push_event (filter->srcpad_any, event);
            break;
        }
        default:
            ret = gst_pad_event_default (pad, parent, event);
            break;
    }
    return ret;
}

// Calculate Kp gain scalar based on zoom level
static float kpx_zoom_factor(float fov)
{   
    if (ZOOM_SCALE_MODE == ZOOM_SCALE_ON)
    {
        return (((MAX_FOV - fov)/(MAX_FOV-MIN_FOV)) * MAX_ZOOM_FACTOR);
    }
    else
    {
        return 0.0;
    }
}
// Converts a target from a pixel box in image space to azimuth 
// & altitude angles from the camera's current position
// Inputs:  Metadata2 from vision pipeline
// Outputs: Send APC message containing AA angles
static void send_gimbal_target_aa(Metadata2 * meta, GstObject * parent)
{

    MSG_SACS_TARGET_AA_TYPE sacs_target_aa;

    memset(&sacs_target_aa, 0, sizeof(sacs_target_aa));
    
    GstAeryonGimbal *filter;
    filter = GST_AERYON_GIMBAL (parent);

    //#define DEBUG_GIMBAL
    #ifdef DEBUG_GIMBAL
      filter->mycount = 0;
      int printon = 1;
  
      if (filter->mycount < 30) {
          printon = 0;
          filter->mycount++;
      }
      else {
          printon = 1;
          filter->mycount = 0;
      }
    #endif
    
    // Camera image resolution & field of view
    const float hres = meta->tracks.image_w;
    const float vres = meta->tracks.image_h;
    const float hfov = meta->frame.fov_x;
    const float vfov = meta->frame.fov_y;

    // Tuning parameters from FTT
    float Kp_x = filter->params.Kp[0];
    float Kp_y = filter->params.Kp[1];
    const float deadband           = filter->params.dead_band;
    const float target_frac_filter = filter->params.target_frac_filter;
    const int   target_rotate_mode = (int)filter->params.target_distance;

    float target_error[2];          // Error in x and y as a fraction (-1 to 1)

    //
    // Track State Machine
    //
    guint new_state = TRACK_IDLE;

    // UPDATE State - UDOT is enabled, and confidence is good. Calculate new NED target coordinates
    if (meta->tracks.state == TRACKER_STATE_UDOT && meta->tracks.confidence[0] >= MIN_CONF )
    {
        new_state = TRACK_UPDATE;
    
        // Find target error from -0.5 to 0.5 (ie. full left/down to full right/up)
        // meta-> tracks holds (x,y) of center of target
        // Positive azimuth change corresponds to looking to the right
        // Positive altitude change corresponds to looking up
        target_error[Xi] = (meta->tracks.gimbal_target_x[0] / hres) - 0.5;
        // Negative because pixels increase top to bottom, but altitude increases from bottom up
        target_error[Yi] = -((meta->tracks.gimbal_target_y[0] / vres) - 0.5);

        // Scale Kp based on zoom level (HDZoom Only)
        if (filter->hdzoom){
            const float factor = kpx_zoom_factor(hfov);
            Kp_x += (Kp_x * factor / 2.0);
            Kp_y += (Kp_y * factor);
        }

        // Perform linear scaling of proportional gain when the error is less than the deadband.
        // Scale from 100% to 0% as the error moves from deadband to 0
        if (target_error[0] < deadband && target_error[0] > -deadband)
        {
            Kp_x = Kp_x * ABS(target_error[0]/deadband );
            //g_print("KPx: %f\n", Kp_x);
        }
        if (target_error[1] < deadband && target_error[1] > -deadband)
        {
            Kp_y = Kp_y * ABS(target_error[1]/deadband );
            //g_print("KPy: %f\n", Kp_y);
        }

        // Determine target error step based on error and gain setting
        const float target_x_step = Kp_x * target_error[Xi];
        const float target_y_step = Kp_y * target_error[Yi];

        // Pass the target through a low pass filter
        LP_FILT( filter->target_filt[Xi], target_x_step, target_frac_filter );
        LP_FILT( filter->target_filt[Yi], target_y_step, target_frac_filter );

        // Determine delta azimuth and altitude based on field of view
        sacs_target_aa.azal[0] = hfov * filter->target_filt[0];
        sacs_target_aa.azal[1] = vfov * filter->target_filt[1];

        // Indicate to DSP that relative azimuth/altitude is coming from UDOT
        sacs_target_aa.target_mode = ACS_TARGET_MODE_UDOT;

        /* Send ACS_TARGET_ROT_MODE_NONE to indicate to acs_target.c that it
         * should choose the rotation mode based on previous state and target
         * availability */
        sacs_target_aa.adjust_mode = ACS_TARGET_ROT_MODE_NONE;

        /* Allow overwritting of UDOT rotation mode via unused TARGET_DISTANCE
         * property in FTT. */
        if (target_rotate_mode != 0){
            sacs_target_aa.adjust_mode = target_rotate_mode;
        }
        
        sacs_target_aa.timestamp = meta->dsp_time;

        #ifdef DEBUG_GIMBAL
            if (printon == 1){
                //g_print("KP: %f, %f\n", Kp_x, Kp_y);
                //g_print("TARGET%d = [%d, %d]\n", totalCount, meta->tracks.gimbal_target_x[0], meta->tracks.gimbal_target_y[0]);
                //g_print("BOUND(x,y) = [%.5f, %.5f]\n", target_error[Xi], target_error[Yi]);
                //g_print("FRAC(x,y) = [%.5f, %.5f]\n", target_x_step, target_y_step);
                //g_print("AZAL%d = [%.5f, %.5f]\n", totalCount, sacs_target_aa.azal[0], sacs_target_aa.azal[1]);
                //g_print("FOV(x,y) = [%.5f, %.5f] RES(x,y) = [%.5f, %.5f] \n", hfov, vfov, hres, vres);
                filter->totalCount++;
            }
        #endif
    }

    // STOP State - If UDOT is disabled, or conficence is low and we are currenlty tracking then stop tracking
    else if (filter->track_state == TRACK_UPDATE)
    {
        new_state = TRACK_STOP;

        // Release tracking by setting tracking mode back to ACS_TARGET_MODE_UDOT
        sacs_target_aa.azal[0] = 0.0;
        sacs_target_aa.azal[1] = 0.0;
        sacs_target_aa.target_mode = ACS_TARGET_MODE_TRACK;
        sacs_target_aa.adjust_mode = ACS_TARGET_ROT_MODE_NONE;
    }

    // IDLE State
    else
    {
        new_state = TRACK_IDLE;
    }

    // Persist new track state
    filter->track_state = new_state;

    // If we're in idle, just return. Otherwise send the message to the flyer DSP
    if (filter->track_state == TRACK_IDLE )
    {
      return;
    }
    else
    {
      // Send the SACS target relative NED coordinates up to APC
      GstMessage *msg = gst_message_new_apc(
          parent,
          MSG_SACS_TARGET_AA_APC_CLASS,
          MSG_SACS_TARGET_AA_APC_ID,
          &sacs_target_aa,
          sizeof (MSG_SACS_TARGET_AA_TYPE));

      gst_element_post_message(GST_ELEMENT(parent), msg);
    }
}

static void send_gimbal_params(GstObject * parent){
    gst_gimbal_params_t params;
    GstAeryonGimbal *filter;
    filter = GST_AERYON_GIMBAL(parent);
    memcpy(&params, &(filter->params), sizeof(gst_gimbal_params_t));
    params.time_ms = get_time_ms();
    
    GstMessage *msg = gst_message_new_apc(
            parent,
            MSG_GST_GIMBAL_PARAMS_APC_CLASS,
            MSG_GST_GIMBAL_PARAMS_APC_ID,
            &params,
            sizeof (gst_gimbal_params_t));    
    gst_element_post_message(GST_ELEMENT(parent), msg);
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_aeryon_gimbal_chain(GstPad * pad, GstObject * parent, GstBuffer * buf)
{
    USE(pad);
    GstAeryonGimbal *filter;
    filter = GST_AERYON_GIMBAL(parent);

    if (filter->counter % filter->fps == 0)
    {
        send_gimbal_params(parent);
    }
    
    // Open buffer's metadata (if present)
    GstAeryonBufferMeta *buffer_meta = GST_AERYON_BUFFER_META_GET(buf);
    if (buffer_meta && buffer_meta->gstmetadata && buffer_meta->gstmetadata->metadata)
    {
        Metadata2 *meta = buffer_meta->gstmetadata->metadata;
        // Calculate and send relative azimuth/altitude to Flyer DSP
        send_gimbal_target_aa(meta, parent);
    }
    
    filter->counter++;
    return gst_pad_push(filter->srcpad_any, buf);
}
