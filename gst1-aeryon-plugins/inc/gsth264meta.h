/* GStreamer
 * Copyright (C) 2017 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _GST_H264META_H_
#define _GST_H264META_H_

#include <gst/gst.h>
#include <gst/base/gstbaseparse.h>
#include <gst/codecparsers/gsth264parser.h>
#include <gst/video/video.h>

G_BEGIN_DECLS

#define GST_TYPE_H264_META   (gst_h264_meta_get_type())
#define GST_H264_META(obj)   (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_H264_META,GstH264meta))
#define GST_H264_META_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_H264_META,GstH264metaClass))
#define GST_IS_H264_META(obj)   (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_H264_META))
#define GST_IS_H264_META_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_H264_META))
// Max 
#define MAX_META_SIZE  (1024)
#define STATS_INTERVAL_SECONDS (1) // Update h264 stats every one second 

//TODO: change the name to be H265ESParse to support 
// more more video es manipulation in the future 
typedef struct _GstH264meta GstH264meta;
typedef struct _GstH264metaClass GstH264metaClass;

typedef struct _H264SystemMeta H264SystemMeta;


// Metadata2 will be inserted on every I frame once set
struct _H264SystemMeta
{
  gint32 pos;
  guint8 data[MAX_META_SIZE]; 
};

struct _GstH264meta
{
  GstBaseParse baseparse;
  gboolean sei_parsing;    // Parsing and dump unregistered SEI data
  guint32  insertion_interval; 

  // h264 stream info
  gint width;
  gint height;
  guint align;
  guint format;
  
  // input codec_data
  GstH264NalParser *nalparser;
  GstBuffer *codec_data_in;
  guint    nal_length_size;
  gboolean packetized;
  gint     current_off;
  GstClockTime last_report;

  // SEI insertion related variables  
  gboolean is_key_frame; 
  gboolean have_sps;
  gboolean have_pps;
  gboolean have_sei;
  gint     sei_insertion_position; 
  // SEI type 5 from upstream 
  gint   orig_sei_offset;
  guint  orig_sei_size;
  guint8 orig_sei_data[MAX_META_SIZE];

  GMutex          meta_mutex;
  H264SystemMeta  system_meta;
  GQueue          *frame_meta_queue;
  //Collect h264 stats like average framerate, bitrate and last valid PTS
  GMutex          stats_mutex;
  guint           stats_callback_id;
  guint64         stats_bytes_sum;
  guint32         stats_frame_sum;
  gfloat          average_framerate;
  gfloat          average_bitrate;
  guint64         last_valid_pts;
  GstClockTime    last_stats_collect_time;
  gint64          timesync_offset;
};

struct _GstH264metaClass
{
  GstBaseParseClass parent_class;
};

// External API to set the meta buffers
void gst_h264_meta_set_system_meta(GstH264meta *h264meta, const char *buffer, gint32 size);
void gst_h264_meta_set_frame_meta(GstH264meta *h264meta, const char *buffer, gint32 size);

GType gst_h264_meta_get_type (void);

G_END_DECLS

#endif
