/* 
 * File:   gstaeryonoverlay.h
 * Author: april
 *
 * Created on July 12, 2016, 11:43 AM
 */

#ifndef GSTAERYONOVERLAY_H
#define	GSTAERYONOVERLAY_H

#include <gst/gst.h>
#include "gstaeryonevent.h"

G_BEGIN_DECLS

#define GST_TYPE_AERYON_OVERLAY           (gst_aeryon_overlay_get_type())
#define GST_AERYON_OVERLAY(obj)          (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_AERYON_OVERLAY, GstAeryonOverlay))
#define GST_AERYON_OVERLAY_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_AERYON_OVERLAY, GstAeryonOverlayClass))
#define GST_IS_AERYON_OVERLAY(obj)       (G_TYPE_CHECK_INSTANCE_TYPE(obj, GST_TYPE_AERYON_OVERLAY))
#define GST_IS_AERYON_OVERLAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass), GST_TYPE_AERYON_OVERLAY)

typedef struct _GstAeryonOverlay         GstAeryonOverlay;
typedef struct _GstAeryonOverlayClass    GstAeryonOverlayClass;

struct _GstAeryonOverlay
{
    GstElement  element;
    GstPad      *sinkpad;
    GstPad      *srcpad_any;
    
    gboolean    silent;
    gboolean    draw;    
};

struct _GstAeryonOverlayClass
{
    GstElementClass parent_class;
};

GType gst_aeryon_overlay_get_type(void);

G_END_DECLS

#endif	/* GSTAERYONOVERLAY_H */

