/* GStreamer
 * Copyright (C) 2016 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _GST_CUDA_JPEG_DEC_H_
#define _GST_CUDA_JPEG_DEC_H_

#include <gst/video/video.h>
#include <gst/video/gstvideodecoder.h>
#include "cuda_jpeg_dec.h"
#include "utils.h"

G_BEGIN_DECLS

#define GST_TYPE_CUDA_JPEG_DEC   (gst_cuda_jpeg_dec_get_type())
#define GST_CUDA_JPEG_DEC(obj)   (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_CUDA_JPEG_DEC,GstCudaJpegDec))
#define GST_CUDA_JPEG_DEC_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_CUDA_JPEG_DEC,GstCudaJpegDecClass))
#define GST_IS_CUDA_JPEG_DEC(obj)   (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_CUDA_JPEG_DEC))
#define GST_IS_CUDA_JPEG_DEC_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_CUDA_JPEG_DEC))


typedef enum NVX_ENCODE_VIDEOTEMPORALTRADEOFF{
    CudaJpegDecTemporalTradeoffLevel_DropNone = 0,
    CudaJpegDecTemporalTradeoffLevel_Drop1in5 = 1,
    CudaJpegDecTemporalTradeoffLevel_Drop1in3 = 2,
    CudaJpegDecTemporalTradeoffLevel_TOTAL    = 4
} NVX_ENCODE_VIDEOTEMPORALTRADEOFF;

typedef struct _GstCudaJpegDec      GstCudaJpegDec;
typedef struct _GstCudaJpegDecClass GstCudaJpegDecClass;

struct _GstCudaJpegDec
{
  GstVideoDecoder base_cudajpegdec;
  CudaJpegDecoder    *instance_p;
  FrameHeader        *pFrameHeader;  
  GstVideoCodecState *input_state;
  GstVideoCodecFrame *current_frame;
  gdouble            last_frame_ms;
  gdouble            min_duration;
  gdouble            filt_duration;
  guint32            min_rate;
  guint32            temporal_tradeoff;
  guint32            index;
  guint16            drop_factor;
  gboolean           force_ar_16_9; // Whether to force always output image with 16x9 aspect ratio
};

struct _GstCudaJpegDecClass
{
  GstVideoDecoderClass base_cudajpegdec_class;
};

GType gst_cuda_jpeg_dec_get_type (void);

G_END_DECLS

#endif
