#ifndef _GST_AERYON_GIMBAL_H_
#define _GST_AERYON_GIMBAL_H_

#include <gst/gst.h>
#include "gstapcmessages.h"

G_BEGIN_DECLS

// Lower UDOT gain with higher zoom
#define ZOOM_SCALE_OFF					0
#define ZOOM_SCALE_ON					1
#define ZOOM_SCALE_MODE     			ZOOM_SCALE_ON

// Kp = Kp * [1 + (MAX_ZOOM_FACTOR * % of zoom range)]
#define MIN_FOV							0.04915
#define MAX_FOV							1.25315
#define MAX_ZOOM_FACTOR					(-0.45)

#ifndef TARGET_FRAC_FILTER
#define TARGET_FRAC_FILTER              1.0
#endif

#ifndef TARGET_DISTANCE
#define TARGET_DISTANCE                 0.0
#endif

#ifndef MAX_TARGET_ERROR_X
#define MAX_TARGET_ERROR_X              1.0
#endif

#ifndef MAX_TARGET_ERROR_Y
#define MAX_TARGET_ERROR_Y              1.0
#endif

#ifndef MAX_TARGET_ERROR_I_X
#define MAX_TARGET_ERROR_I_X            0.0
#endif

#ifndef MAX_TARGET_ERROR_I_Y
#define MAX_TARGET_ERROR_I_Y            0.0
#endif

#ifndef KP_X
#define KP_X                            0.06
#endif

#ifndef KP_Y
#define KP_Y                            0.06
#endif

#ifndef KI_X
#define KI_X                            0.0
#endif

#ifndef KI_Y
#define KI_Y                            0.0
#endif

#ifndef DEAD_BAND
#define DEAD_BAND                       0.3
#endif

#define GST_TYPE_AERYON_GIMBAL           (gst_aeryon_gimbal_get_type())
#define GST_AERYON_GIMBAL(obj)          (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_AERYON_GIMBAL, GstAeryonGimbal))
#define GST_AERYON_GIMBAL_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_AERYON_GIMBAL, GstAeryonGimbalClass))
#define GST_IS_AERYON_GIMBAL(obj)       (G_TYPE_CHECK_INSTANCE_TYPE(obj, GST_TYPE_AERYON_GIMBAL))
#define GST_IS_AERYON_GIMBAL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass), GST_TYPE_AERYON_GIMBAL)

typedef struct _GstAeryonGimbal         GstAeryonGimbal;
typedef struct _GstAeryonGimbalClass    GstAeryonGimbalClass;

struct _GstAeryonGimbal
{
    GstElement  element;
    GstPad      *sinkpad;
    GstPad      *srcpad_any;
    gst_gimbal_params_t params;
    guint       track_state;
    guint       counter;
    gfloat      target_filt[2];
    gboolean    silent;
    gboolean	hdzoom;
    guint       mycount;
    guint       totalCount;
    guint       fps;
};

struct _GstAeryonGimbalClass
{
    GstElementClass parent_class;
};

GType gst_aeryon_gimbal_get_type(void);

G_END_DECLS

#endif //_GST_AERYON_GIMBAL_H_
