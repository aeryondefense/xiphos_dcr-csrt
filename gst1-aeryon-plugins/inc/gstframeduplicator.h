/* 
 * File:   gstframeduplicator.h
 * Author: phabsch
 *
 * Created on Feb 6, 2017
 */

#ifndef GSTFRAMEDUPLICATOR_H
#define	GSTFRAMEDUPLICATOR_H

#include <gst/gst.h>
#include "gstaeryonevent.h"

G_BEGIN_DECLS

#define GST_TYPE_FRAME_DUPLICATOR           (gst_frame_duplicator_get_type())
#define GST_FRAME_DUPLICATOR(obj)          (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_FRAME_DUPLICATOR, GstFrameDuplicator))
#define GST_FRAME_DUPLICATOR_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_FRAME_DUPLICATOR, GstFrameDupliatorClass))
#define GST_IS_FRAME_DUPLICATOR(obj)       (G_TYPE_CHECK_INSTANCE_TYPE(obj, GST_TYPE_FRAME_DUPLICATOR))
#define GST_IS_FRAME_DUPLICATOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass), GST_TYPE_FRAME_DUPLICATOR)

typedef struct _GstFrameDuplicator         GstFrameDuplicator;
typedef struct _GstFrameDuplicatorClass    GstFrameDuplicatorClass;

struct _GstFrameDuplicator
{
    GstElement  element;
    GstPad      *sinkpad;
    GstPad      *srcpad_any;
    GMutex      data_mutex;
    guint       callback_id;
    
    GstClockTime last_sent_pts;
    GstClockTime last_sync_pts;
    GstClockTime last_sync_pts_time;
    int          num_filled_pictures;
    int          send_timeout_ms;
    int          max_gap_fill_ms;
    int          frequency;
    gboolean     enable;
    GstClockTime duration;
    GstBuffer    *stored_buffer;
};

struct _GstFrameDuplicatorClass
{
    GstElementClass parent_class;
};

GType gst_frame_duplicator_get_type(void);

G_END_DECLS

#endif	/* GSTFRAMEDUPLICATOR_H */

