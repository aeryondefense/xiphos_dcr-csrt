/*
 * GStreamer
 * Copyright (C) 2005 Thomas Vander Stichele <thomas@apestaart.org>
 * Copyright (C) 2005 Ronald S. Bultje <rbultje@ronald.bitfreak.net>
 * Copyright (C) 2015  <<user@hostname.org>>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_PTP_SRC_H__
#define __GST_PTP_SRC_H__

#include <gst/gst.h>
#include <gst/base/gstpushsrc.h>
#include <gst/video/video.h>

#include "ptp_cam.h"
#include "ptp_settings.h"

G_BEGIN_DECLS

/* #defines don't like whitespacey bits */
#define GST_TYPE_PTP_SRC \
    (gst_ptp_src_get_type())
#define GST_PTP_SRC(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_PTP_SRC,GstPtpSrc))
#define GST_PTP_SRC_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_PTP_SRC,GstPtpSrcClass))
#define GST_PTP_SRC_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_PTP_SRC, GstPtpSrcClass))
#define GST_IS_PTP_SRC(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_PTP_SRC))
#define GST_IS_PTP_SRC_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_PTP_SRC))

typedef struct _GstPtpSrc      GstPtpSrc;
typedef struct _GstPtpSrcClass GstPtpSrcClass;
typedef struct _GstPTPCam      GstPTPCam;

struct _GstPtpSrc
{
    GstPushSrc      pushsrc;
    GstVideoInfo    info;
    GstPTPCam       *ptpcam;
    
    guint64         offset;
    GstBufferPool   *pool;
    gboolean        is_1080p;      // whether start ptpsrc in 1080p mode
    GstClockTime    last_pts;      // pts of last received video frame 
};

struct _GstPtpSrcClass 
{
    GstPushSrcClass parent_class;
    PTPCam          camera;
    gboolean        cam_opened;
};

struct _GstPTPCam 
{
    PTPCam          *camera;
    volatile gint   refcount;
    GDestroyNotify  destroy_notify;
};

GType gst_ptp_src_get_type (void);

#define GST_PTPCAM_CONTEXT_TYPE    "gst.ptp.PTPCam"
GstContext *gst_context_new_ptpcam(GstPTPCam *ptpcam, gboolean persistent);
gboolean gst_context_get_ptpcam(GstContext *context, GstPTPCam **camera);

// PTPCam wrapper with refcount
#define GST_TYPE_PTPCAM (gst_ptpcam_get_type())
GType gst_ptpcam_get_type(void);
GstPTPCam *gst_ptpcam_new(PTPCam *camera, GDestroyNotify destroy_notify);
GstPTPCam *gst_ptpcam_ref(GstPTPCam *ptpcam);
void gst_ptpcam_unref (GstPTPCam *ptpcam);

G_END_DECLS

#endif /* __GST_PTP_SRC_H__ */
