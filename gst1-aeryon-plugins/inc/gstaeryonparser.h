#ifndef _GST_AERYON_PARSER_H_
#define _GST_AERYON_PARSER_H_

#include <gst/gst.h>
#include "gstaeryonbuffermeta.h"
#include "klv_calculate.h"

G_BEGIN_DECLS

#define GST_TYPE_AERYON_PARSER          (gst_aeryon_parser_get_type())
#define GST_AERYON_PARSER(obj)          (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_AERYON_PARSER, GstAeryonParser))
#define GST_AERYON_PARSER_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_AERYON_PARSER, GstAeryonParserClass))
#define GST_IS_AERYON_PARSER(obj)       (G_TYPE_CHECK_INSTANCE_TYPE(obj, GST_TYPE_AERYON_PARSER))
#define GST_IS_AERYON_PARSER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass), GST_TYPE_AERYON_PARSER)

typedef struct _GstAeryonParser         GstAeryonParser;
typedef struct _GstAeryonParserClass    GstAeryonParserClass;

struct _GstAeryonParser
{
    GstElement  element;
    GstPad      *sinkpad;
    GstPad      *srcpad_any, *srcpad_klv;

    guint       klv_decimator;
    guint       klv_decimator_count;
    gulong      klv_linked_sig_id;
    gulong      klv_unlinked_sig_id;
    GQueue      *queue;
    GMutex      queue_mutex;
    GMutex      klv_cond_mutex;
    GCond       klv_cond;
    gboolean    klv_cond_ready;
    gint        fps_n, fps_d;
    gboolean    klv_running;
    gboolean    klv_enabled;
    GstClockTime klv_timestamp;
    GThread     *klv_thread;
    
    // klv metadata properties
    gint        klv_security_class;
    gchar       *klv_mission_id;
    gchar       *klv_platform_tail_number; // also call sign
    gchar       *klv_platform_designation;
    gchar       *klv_source_sensor;
    
    // klv GPS base context
    klv_gps_ctx_t klv_gps_ctx;
};

struct _GstAeryonParserClass
{
    GstElementClass parent_class;
};

GType gst_aeryon_parser_get_type(void);

G_END_DECLS

#endif //_GST_AERYON_PARSER_H_
