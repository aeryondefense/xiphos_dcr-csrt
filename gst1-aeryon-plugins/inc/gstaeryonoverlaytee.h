#ifndef __GST_AERYONOVERLAYTEE_H__
#define __GST_AERYONOVERLAYTEE_H__

#include <gst/gst.h>

G_BEGIN_DECLS

/* #defines don't like whitespacey bits */
#define GST_TYPE_AERYON_OVERLAY_TEE \
  (gst_aeryon_overlay_tee_get_type())
#define GST_AERYON_OVERLAY_TEE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_AERYON_OVERLAY_TEE,GstAeryonOverlayTee))
#define GST_AERYON_OVERLAY_TEE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_AERYON_OVERLAY_TEE,GstAeryonOverlayTeeClass))
#define GST_IS_AERYON_OVERLAY_TEE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_AERYON_OVERLAY_TEE))
#define GST_IS_AERYON_OVERLAY_TEE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_AERYON_OVERLAY_TEE))
#define GST_AERYON_OVERLAY_TEE_CAST(obj) ((GstAeryonOverlayTee*) obj)

typedef struct _GstAeryonOverlayTee      GstAeryonOverlayTee;
typedef struct _GstAeryonOverlayTeeClass GstAeryonOverlayTeeClass;

typedef enum {
    GST_AERYON_OVERLAY_TEE_PULL_MODE_NEVER,
    GST_AERYON_OVERLAY_TEE_PULL_MODE_SINGLE,
} GstAeryonOverlayTeePullMode;


struct _GstAeryonOverlayTee
{
    GstElement element;

    /*< private >*/
    GstPad         *sinkpad ;
    GstPad         *allocpad;

    GHashTable     *pad_indexes;

    guint           next_pad_index;

    gboolean        has_chain;
    gboolean        silent;
    gchar          *last_message;

    GstPadMode      sink_mode;
    GstAeryonOverlayTeePullMode  pull_mode;
    GstPad         *pull_pad;

    gboolean        allow_not_linked;

    GHashTable     *srcpads_bypass;  //srcpads do not need overlay, we will forward buffers immediately  
    GHashTable     *srcpads_overlay; //srcoads requires overlay, we will hold buffer until all bypass srcpads have processed the buffer

    guint64         gating_timestamp; //latest buffer processed by bypass srcpad
    GQueue          *pending_buffers; 
    GMutex          data_mutex;
    gboolean         draw;
};

struct _GstAeryonOverlayTeeClass 
{
    GstElementClass parent_class;
};

void gst_aeryon_overlay_tee_update_ts(GstAeryonOverlayTee *tee, GstPad *srcpad_bypass, guint64 timestamp);
gboolean gst_aeryon_overlay_tee_set_overlay(GstAeryonOverlayTee *tee, GstPad *pad, gboolean flag);

GType gst_aeryon_overlay_tee_get_type (void);

G_END_DECLS

#endif /* __GST_AERYONOVERLAYTEE_H__ */
