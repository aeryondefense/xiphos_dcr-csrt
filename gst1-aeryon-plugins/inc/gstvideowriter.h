/* GStreamer
 * Copyright (C) 2016 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _GST_VIDEO_WRITER_H_
#define _GST_VIDEO_WRITER_H_

#include <gst/base/gstbasesink.h>
#include <stdio.h>
#include "video_writer_mux.h"
#include "klv.h"

G_BEGIN_DECLS

#define GST_TYPE_VIDEO_WRITER            (gst_video_writer_get_type())
#define GST_VIDEO_WRITER(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_VIDEO_WRITER, GstVideoWriter))
#define GST_VIDEO_WRITER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass),  GST_TYPE_VIDEO_WRITER, GstVideoWriterClass))
#define GST_IS_VIDEO_WRITER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_VIDEO_WRITER))
#define GST_IS_VIDEO_WRITER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),  GST_TYPE_VIDEO_WRITER))

typedef struct _GstVideoWriter          GstVideoWriter;
typedef struct _GstVideoWriterClass     GstVideoWriterClass;
typedef struct _GstVideoWriterProps     GstVideoWriterProps;
typedef struct _GstVideoWriterState     GstVideoWriterState;

struct _GstVideoWriterProps
{
  gchar                  *location;
  gint                    buffer_size;
  gboolean                write_klv;
  gboolean                record;
};

/**
 * You, avid reader, may be wondering why I decided to make this a separate
 * struct instead of integrating it with the GstVideoWriter struct. The reason
 * for this is because I can then easily call memset(0) on GstVideoWriterState
 * in order to initialize its state.
 */
struct _GstVideoWriterState
{
  /* File details */
  vw_mux_t               *mux_file;
  int                     is_recording;
  char                    filename[128];
  size_t                  file_size;
  int                     num_frames;
  int                     num_klv;
  int                     file_index;
};

struct _GstVideoWriter
{
  GstElement              element;
  GstPad                 *videopad;
  GstPad                 *klvpad;
  
  GstClockTime            last_pts;
  
  GstVideoWriterProps     props;
  GstVideoWriterState     state;
  bool                    first_key_frame; // This is a workaround to ignore the very first GOP
                                           // due to the very first or second output of omxh264enc are always corrupted  
};

struct _GstVideoWriterClass
{
  GstElementClass parent_class;
};

GType gst_video_writer_get_type (void);

G_END_DECLS

#endif
