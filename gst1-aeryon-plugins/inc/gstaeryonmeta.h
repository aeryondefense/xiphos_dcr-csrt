#ifndef _GST_AERYON_META_H_
#define _GST_AERYON_META_H_

#include <gst/gst.h>
#include "gstaeryonbuffermeta.h"
#include "searchable_rbuf.h"

G_BEGIN_DECLS

#define GST_TYPE_AERYON_META (gst_aeryon_meta_get_type())
#define GST_AERYON_META(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_AERYON_META, GstAeryonMeta))
#define GST_AERYON_META_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_AERYON_META, GstAeryonMetaClass))
#define GST_IS_AERYON_META(obj) (G_TYPE_CHECK_INSTANCE_TYPE(obj), GST_TYPE_AERYON_META)
#define GST_IS_AERYON_META_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass), GST_TYPE_AERYON_META)

typedef struct _GstAeryonMeta       GstAeryonMeta;
typedef struct _GstAeryonMetaClass  GstAeryonMetaClass;

struct _GstAeryonMeta
{
    GstElement element;
    GstPad *sinkpad, *srcpad;

    searchable_rbuf ringbuffer;
    gpointer        ringbuffer_backing;
    GRWLock         ringbuffer_rwlock;

    GstAeryonMetadata *metadata;
    Metadata2          *meta;
    
    guint           num_events;
    GstClockTimeDiff pipeline_to_meta_offset;
    GstClockTime    base_time;
};

struct _GstAeryonMetaClass
{
    GstElementClass parent_class;
};

GType gst_aeryon_meta_get_type(void);

G_END_DECLS

#endif //_GST_AERYON_META_H_
