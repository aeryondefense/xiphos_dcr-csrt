#ifndef _GST_XIPHOS_VISION_H_
#define _GST_XIPHOS_VISION_H_

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/base/gstbasetransform.h>
#include "gstaeryonevent.h"
#include "searchable_rbuf.h"
#include "gstapcmessages.h"
#include "targetinjector.h"
#include "klv_calculate.h"

G_BEGIN_DECLS

#define GST_TYPE_XIPHOS_VISION           (gst_xiphos_vision_get_type())
#define GST_XIPHOS_VISION(obj)          (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_XIPHOS_VISION, GstXiphosVision))
#define GST_XIPHOS_VISION_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_XIPHOS_VISION, GstXiphosVisionClass))
#define GST_IS_XIPHOS_VISION(obj)       (G_TYPE_CHECK_INSTANCE_TYPE(obj, GST_TYPE_XIPHOS_VISION))
#define GST_IS_XIPHOS_VISION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass), GST_TYPE_XIPHOS_VISION)

typedef struct _GstXiphosVision         GstXiphosVision;
typedef struct _GstXiphosVisionClass    GstXiphosVisionClass;

typedef struct _GstXiphosPtsIdElement {
    long pipe_ts;
    int image_id;
} GstXiphosPtsIdElement;

typedef struct vision_stats
{
    float fov_x;
    float fov_y;
    float f_length;
} vision_stats_t;

struct _GstXiphosVision
{
    GstBaseTransform  element;

    // properties
    gboolean        silent;
    gboolean        draw;
    guint           stream_id;
    guint           stab;
    guint           egoic;
    guint           sensor;
    guint           apc_app_id;
    gchar*          injector_path;
    gchar*          target_ejector_path;
    gchar*          stable_ejector_path;
    volatile guint  update_delay;
    vision_stats_t  stats;

    gboolean            caps_changed;
    gint                fps;
    guint               f_duration;
    gpointer            pts_id_head;
    gint                pts_id_cur;
    gint                pts_id_tail;    
    gint                num_pst_id_elems;
    gint                error_flags;
    gboolean            cool_down;
    gboolean            negotiated; 
    GstVideoInfo        in_info;
    GstVideoInfo        out_info;
    perf_mon_t          perfdata;
    void*               xipvis_;
    guint               newconfig_;
    guint               last_lock;
    gdouble             last_lock_time_ms;
    guint               ctr;
    gboolean            hi_power;
    TrackerConfig       tracker_config;
    TrackerConfig       request_tracker_config;    
    gst_vision_params_t vision_params;
    guint64             current_frame_pts;
    GQueue              img_queue;
    GMutex              queue_lock;
    gdouble             update_accum;
    guint               palette;
    guint               palette_ctr;
    gboolean            palette_changed;
    gboolean            isotherm_changed;
    gboolean            retrack;
    cam_isotherm_config_t isotherm;
    guint               last_x;
    guint               last_y;
    injector_cmd_t*     injector_cmd;
    injector_t          injector;
    FILE*               target_ejector_fd;
    gint                target_ejector_cnt;
    FILE*               stable_ejector_fd;
    klv_gps_ctx_t       gps_ctx;
    GMutex              stats_mutex;
};

struct _GstXiphosVisionClass
{
    GstBaseTransformClass parent_class;
};

GType gst_xiphos_vision_get_type(void);

G_END_DECLS

#endif //_GST_XIPHOS_VISION_H_
