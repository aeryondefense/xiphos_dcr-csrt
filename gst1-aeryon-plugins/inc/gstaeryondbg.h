/* GStreamer
 * Copyright (C) 1999,2000 Erik Walthinsen <omega@cse.ogi.edu>
 *                    2000 Wim Taymans <wtay@chello.be>
 *
 * gstidentity.h:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */


#ifndef __GST_AERYON_DBG_H__
#define __GST_AERYON_DBG_H__

#include <stdio.h>

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>

G_BEGIN_DECLS


#define GST_TYPE_AERYON_DBG \
  (gst_aeryon_dbg_get_type())
#define GST_AERYON_DBG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_AERYON_DBG,GstAeryonDbg))
#define GST_AERYON_DBG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_AERYON_DBG,GstAeryonDbgClass))
#define GST_IS_AERYON_DBG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_AERYON_DBG))
#define GST_IS_AERYON_DBG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_AERYON_DBG))

typedef struct _GstAeryonDbg GstAeryonDbg;
typedef struct _GstAeryonDbgClass GstAeryonDbgClass;

/**
 * GstAeryonDbg:
 *
 * Opaque #GstAeryonDbg data structure
 */
struct _GstAeryonDbg {
  GstBaseTransform 	 element;

  /*< private >*/
  GstClockID     clock_id;
  gint           error_after;
  gfloat         drop_probability;
  gint           datarate;
  guint          sleep_time;
  gboolean       dump;
  gboolean       sync;
  gboolean       check_imperfect_timestamp;
  gboolean       check_imperfect_offset;
  gboolean       single_segment;
  GstClockTime   prev_timestamp;
  GstClockTime   prev_duration;
  guint64        prev_offset;
  guint64        prev_offset_end;
  guint64        offset;
  gboolean       signal_handoffs;
  GstClockTime   upstream_latency;
  GCond          blocked_cond;
  gboolean       blocked;
  gchar          *ts_dump_filename;
  FILE           *ts_dump_fd;
};

struct _GstAeryonDbgClass {
  GstBaseTransformClass parent_class;

  /* signals */
  void (*handoff) (GstElement *element, GstBuffer *buf);
};

G_GNUC_INTERNAL GType gst_aeryon_dbg_get_type (void);

G_END_DECLS

#endif /* __GST_AERYON_DBG_H__ */
