#ifndef _GST_PTP_BUFFER_POOL_H_
#define _GST_PTP_BUFFER_POOL_H_

#include <gst/gst.h>
#include "gstptpsrc.h"

typedef struct _GstPtpBufferPool        GstPtpBufferPool;
typedef struct _GstPtpBufferPoolClass   GstPtpBufferPoolClass;
typedef struct _GstPtpMeta              GstPtpMeta;

GST_DEBUG_CATEGORY_EXTERN(ptpbuffer_debug);

G_BEGIN_DECLS

#define GST_TYPE_PTP_BUFFER_POOL        (gst_ptp_buffer_pool_get_type())
#define GST_IS_PTP_BUFFER_POOL(obj)     (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_PTP_BUFFER_POOL))
#define GST_PTP_BUFFER_POOL(obj)        (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_PTP_BUFFER_POOL, GstPtpBufferPool))
#define GST_PTP_BUFFER_POOL_CAST(obj)   ((GstPtpBufferPool *)(obj))

struct _GstPtpBufferPool
{
    GstBufferPool   parent;
    GstPTPCam       *ptpcam;
    GstVideoInfo    info;
    
    gsize           size;
    guint           num_buffers;
    guint           num_allocated;
    guint           num_queued;
    gboolean        streaming;

    PTPVideoBuf     *ptp_bufs;
    GstBuffer       **buffers;
};

struct _GstPtpBufferPoolClass
{
    GstBufferPoolClass  parent_class;
};

struct _GstPtpMeta
{
    GstMeta     meta;
    PTPVideoBuf *ptp_buf;
};

GType gst_ptp_meta_api_get_type(void);
const GstMetaInfo *gst_ptp_meta_get_info(void);
#define GST_PTP_META_GET(buf) ((GstPtpMeta *)gst_buffer_get_meta(buf, gst_ptp_meta_api_get_type()))
#define GST_PTP_META_ADD(buf) ((GstPtpMeta *)gst_buffer_add_meta(buf, gst_ptp_meta_get_info(), NULL))

GType gst_ptp_buffer_pool_get_type(void);

GstBufferPool   *gst_ptp_buffer_pool_new(GstPTPCam *ptpcam, GstCaps *caps);
GstFlowReturn   gst_ptp_buffer_pool_process(GstPtpBufferPool *pool, GstBuffer *buf);

#endif
