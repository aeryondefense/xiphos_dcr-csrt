#ifndef _GST_AERYON_VISION_H_
#define _GST_AERYON_VISION_H_

#include <gst/gst.h>
#include "gstaeryonevent.h"

G_BEGIN_DECLS

#define GST_TYPE_AERYON_VISION           (gst_aeryon_vision_get_type())
#define GST_AERYON_VISION(obj)          (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_AERYON_VISION, GstAeryonVision))
#define GST_AERYON_VISION_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_AERYON_VISION, GstAeryonVisionClass))
#define GST_IS_AERYON_VISION(obj)       (G_TYPE_CHECK_INSTANCE_TYPE(obj, GST_TYPE_AERYON_VISION))
#define GST_IS_AERYON_VISION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass), GST_TYPE_AERYON_VISION)

typedef struct _GstAeryonVision         GstAeryonVision;
typedef struct _GstAeryonVisionClass    GstAeryonVisionClass;

struct _GstAeryonVision
{
    GstElement  element;
    GstPad      *sinkpad;
    GstPad      *srcpad_any;
    
    gboolean    silent;
    
    TrackerConfig tracker_config;
    
};

struct _GstAeryonVisionClass
{
    GstElementClass parent_class;
};

GType gst_aeryon_vision_get_type(void);

G_END_DECLS

#endif //_GST_AERYON_VISION_H_
