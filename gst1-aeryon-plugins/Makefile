###############################################################################
# vim: noet
#
# Copyright 2006-2014, Aeryon Labs - Mike Peasgood
#
# Makefile for the a test application for the SLUG board
#

# Path to the top of the software tree
TOP_DIR := ..
MAKEDIR := $(TOP_DIR)/make

# Supported targets (one or more of AVR, XSCALE, X86)
VALID_TARGETS  := TK1 TX1 X86 X86_64

# Flag to build a shared library
SHLIB_NAME = $(MODULE)

# Required libraries
APPDEPS := ptp_cam2 gst1-aeryon-libs searchable_rbuf sw_tools gmbl_ctrl math \
           video_writer video_mux klv gps cam_vsepr cam_cslm xiphos cuda_jpeg_dec \
           cuda_utils hwtimesync
HDRDEPS := acs xiphos perfmon apc cuda_jpeg_dec cuda_utils hwtimesync

PROJECT_LIBS = -lpcre -lffi -lsw_tools -lusb-1.0 -lzmq \
               -ludev -lstdc++ -lmath \
               -lklv -lcuda -lcudart_static -lnppi -lnppc \
               -lgmbl_ctrl -lm -lrt -ldl $(FFMPEG_LIBS) $(GSTREAMER_LIBS) \
	  -lopencv_imgcodecs \
	  -lopencv_highgui \
	  -lopencv_videoio \
	  -lopencv_video \
	  -lopencv_imgproc \
	  -lopencv_core

OPENCV3_LOCAL_DIR = /usr/local
OPENCV3_LOCAL_CFLAGS = \
	   -I$(OPENCV3_LOCAL_DIR)/include/opencv2
OPENCV3_LOCAL_LDFLAGS = -L$(OPENCV3_LOCAL_DIR)/lib \
	  -lopencv_imgcodecs \
	  -lopencv_highgui \
	  -lopencv_videoio \
	  -lopencv_video \
	  -lopencv_imgproc \
	  -lopencv_core

LOCAL_CFLAGS = -DTRACE -DTRACE_FLUSH -DGST_USE_UNSTABLE_API -O2 -g 

# Local compile and link flags
ifeq ($(TARGET), TX1)
PROJECT_LIBS +=  -lsystemd 
LOCAL_SHLIB_FLAGS = -L$(SHLIB_DIR)
endif

ifeq ($(TARGET), TK1)
PROJECT_LIBS +=  -lsystemd 
LOCAL_SHLIB_FLAGS = -L$(SHLIB_DIR)
endif

LOCAL_CFLAGS  += $(FFMPEG_CFLAGS) $(GSTREAMER_CFLAGS)
LOCAL_LDFLAGS += $(PTHREAD_LDFLAGS) $(OPENCV3_LOCAL_LDFLAGS) -g 

# Include the project build rules
include $(MAKEDIR)/rules.mk
